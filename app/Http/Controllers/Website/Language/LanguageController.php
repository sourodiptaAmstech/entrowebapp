<?php

namespace App\Http\Controllers\Website\Language;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\LanguageService;


class LanguageController extends Controller
{
    public function UpdatePassengerLanguage(Request $request)
	{
		try{
			$this->validate($request, [
                'language' => 'required',
            ]);
            // lang:en|fr
            //195vdBTr8hAYIx8Xhn5UmcL8IpNMToTUb2r_E1sS
			$request->lang = $request->language;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$languageService = new LanguageService;
			$result = $languageService->accessPassengerUpdateLang($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function UpdateDriverLanguage(Request $request)
	{
		try{
			$this->validate($request, [
                'language' => 'required',
            ]);

			$request->lang = $request->language;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$languageService = new LanguageService;
			$result = $languageService->accessDriverUpdateLang($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}
	

	public function UpdateFleetownerLanguage(Request $request)
	{
		try{
			$this->validate($request, [
                'language' => 'required',
            ]);

			$request->lang = $request->language;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$languageService = new LanguageService;
			$result = $languageService->accessFleetownerUpdateLang($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function GetCancelReason(Request $request)
	{
		try{
			$this->validate($request, [
                'reason_given_by' => 'required',
            ]);
            
			$request['timeZone'] = $request->timeZone;
			$request->reason_given_by = $request->reason_given_by;

        	$languageService = new LanguageService;
			$result = $languageService->accessGetCancelReason($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
