<!-- header -->
@include('partials.driver_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.driver_sidebar')
            </div>
            <div class="col-lg-8">
                <form class="" id="driverProfileUpdateFrom">
                    <div class="px-sm-3 pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success dupdatesuc_alert" style="display: none;">
                            <p class="text-success" id="dupdatesuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.PERSONAL_INFO')}}</h2>
                        <div class="form-group">
                            <label for="first_name" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_FIRST_NAME')}}</label>
                            <input type="text" name="first_name" id="first_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="fname"></span>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_LAST_NAME')}}</label>
                            <input type="text" name="last_name" id="last_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.LAST_NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="lname"></span>
                        </div>
                        <div class="form-group">
                            <label for="gender" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_GENDER')}}</label>
                            <select class="form-control inset-input" name="gender" id="gender">
                                <option value="">{{trans('weblng.FIELD.Select')}}</option>
                                <option value="Male">{{trans('weblng.FIELD.MALE')}}</option>
                                <option value="Female">{{trans('weblng.FIELD.FEMALE')}}</option>
                            </select>
                        </div>
                        <div class="form-group mb-5">
                            <label for="email_id" class="font-weight-bold">{{trans('weblng.FIELD.EMAIL_ADDRESS')}}</label>
                            <input type="email" name="email_id" id="email_id" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.EMAIL')}}">
                            <span class="text-danger" id="emailadd"></span>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.SERVICE_DETAILS')}}</h2>
                        <div class="form-group">
                            <label for="service_type" class="font-weight-bold">{{trans('weblng.FIELD.SERVICE_TYPE')}}</label>
                            <select name="service_type" id="service_type" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="service"></span>
                        </div>

                        <div class="form-group">
                            <label for="car_make" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MAKE')}}</label>
                            <select name="car_make" id="car_make" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="make"></span>
                        </div>
                        <div class="form-group">
                            <label for="car_model" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MODEL')}}</label>
                            <select name="car_model" id="car_model" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="model"></span>
                        </div>
                        <div class="form-group">
                            <label for="model_year" class="font-weight-bold">{{trans('weblng.FIELD.MODEL_YEAR')}}</label>
                            <select name="model_year" id="model_year" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="year"></span>
                        </div>

                        <div class="form-group">
                            <label for="car_number" class="font-weight-bold">{{trans('weblng.FIELD.CAR_NO')}}</label>
                            <input type="text" class="form-control inset-input" name="car_number" id="car_number" placeholder="{{trans('weblng.FIELD.CAR_NO_PLACE')}}" onkeyup="return validate()">
                            <span class="text-danger" id="carno"></span>
                        </div>
                        <a href="{{ route('driver.change.password.index') }}" class="btn btn-md grad text-white">{{trans('weblng.CHANGE_PASSWORD')}}</a>

                        <a href="{{ route('driver.document') }}" class="btn btn-md grad text-white">{{trans('weblng.DOC_UPLOAD')}}</a>

                        <div class="d-flex justify-content-start mb-3 mt-5">
                            <button type="submit" class="btn btn-lrg grad text-white">{{trans('weblng.FIELD.SAVE')}}</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>

<script>
    jq = jQuery.noConflict();
    
    function validate(){
        var status=null;

        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            jq('.px-sm-3').addClass('pt-5');
            jq('.dupdate_alert').hide();
            jq('#dupdate-msg').html('');
            jq('.dupdatesuc_alert').hide();
            jq('#dupdatesuc-msg').html('');
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var serviceType = document.getElementById('service_type').value
        if (serviceType == '') {
            document.getElementById("service").innerHTML='{{trans('weblng.VALIDATION_MSG.SERVICE_TYPE')}}'
            document.getElementById('service_type').classList.add('has-error')
            status=false
        } else {
            document.getElementById("service").innerHTML=''
            document.getElementById('service_type').classList.remove('has-error')
        }

        var carMake = document.getElementById('car_make').value
        if (carMake == '') {
            document.getElementById("make").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_MAKE')}}'
            document.getElementById('car_make').classList.add('has-error')
            status=false
        } else {
            document.getElementById("make").innerHTML=''
            document.getElementById('car_make').classList.remove('has-error')
        }

        var carModel = document.getElementById('car_model').value
        if (carModel == '') {
            document.getElementById("model").innerHTML='{{trans('weblng.VALIDATION_MSG.MODEL')}}'
            document.getElementById('car_model').classList.add('has-error')
            status=false
        } else {
            document.getElementById("model").innerHTML=''
            document.getElementById('car_model').classList.remove('has-error')
        }

        var modelYear = document.getElementById('model_year').value
        if (modelYear == '') {
            document.getElementById("year").innerHTML='{{trans('weblng.VALIDATION_MSG.YEAR')}}'
            document.getElementById('model_year').classList.add('has-error')
            status=false
        } else {
            document.getElementById("year").innerHTML=''
            document.getElementById('model_year').classList.remove('has-error')
        }

        var carNumber = document.getElementById('car_number').value
        if (carNumber == '') {
            document.getElementById("carno").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_NUMBER')}}'
            document.getElementById('car_number').classList.add('has-error')
            status=false
        } else {
            document.getElementById("carno").innerHTML=''
            document.getElementById('car_number').classList.remove('has-error')
        }
        
        return status
    }

jq(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    //SUBMIT DRIVER FORM.................................
    jq('#driverProfileUpdateFrom').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var first_name = jq('#first_name').val();
        var last_name = jq('#last_name').val();
        var gender = jq('#gender').val();
        var email_id = jq('#email_id').val();
        var service_type = jq('#service_type').val();
        var car_make = jq('#car_make').val();
        var car_model = jq('#car_model').val();
        var model_year = jq('#model_year').val();
        var car_number = jq('#car_number').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('driver.profile.update') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, gender: gender, email_id: email_id, service_type: service_type, car_make: car_make, car_model: car_model,model_year: model_year, car_number: car_number,
                timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                var first_name = response.data.user_profile.first_name;
                var last_name = response.data.user_profile.last_name;
                jq('.profile_name').text(first_name.charAt(0).toUpperCase()+ first_name.slice(1)+' '+last_name.charAt(0).toUpperCase()+ last_name.slice(1));
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdatesuc_alert').show();
                jq('#dupdatesuc-msg').html(response.message);
            },

            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('service_type')) {
                        jq('#service').html(responseMsg.errors.service_type).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_make')) {
                        jq('#make').html(responseMsg.errors.car_make).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_model')) {
                        jq('#model').html(responseMsg.errors.car_model).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('model_year')) {
                        jq('#year').html(responseMsg.errors.model_year).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_number')) {
                        jq('#carno').html(responseMsg.errors.car_number).promise().done(function(){
                            jq('#car_number').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#emailadd').html(responseMsg.message).promise().done(function(){
                            jq('#email_id').addClass('has-error');
                        });
                    }
                }
                if (response.status == 403){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT DRIVER FORM.................................
});
</script>
<!-- footer -->
@include('partials.driver_footer')