@include('partials.header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="container mb-5">
    <div class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg">
        <h1 class="text-center"><strong>{{trans('weblng.SIGN_UP.SIGN_UP_TO')}} <span class="text-theme">{{trans('weblng.LOGIN.RIDE')}}</span></strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <div class="alert alert-danger signup_alert" style="display: none;">
                <p class="text-danger" id="signup-msg"></p>
            </div>

            <div class="mb-3">
            <div class="row justify-content-center">
                <div class="d-inline">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio1" name="user" value="passenger" onchange="return validate()" checked>
                        <label class="custom-control-label font-weight-bold" for="radio1">{{trans('weblng.LOGIN.CUSTOMER')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio2" name="user" value="driver" onchange="return validate()">
                        <label class="custom-control-label font-weight-bold" for="radio2">{{trans('weblng.LOGIN.DRIVER')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio3" name="user" value="fleetowner" onchange="return validate()">
                        <label class="custom-control-label font-weight-bold" for="radio3">{{trans('weblng.LOGIN.FLEETOWNER')}}</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <span id="utype" class="text-danger" style="margin-right: 250px;"></span>
            </div>
            </div>
    {{--PASSENGER SIGNUP  --}}
        <div class="passenger_fields">
            <form id="submitPassengerForm">
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="fname" class="font-weight-bold">{{trans('weblng.FIELD.FIRST_NAME')}}</label>
                    <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME_PLACE')}}" onkeyup="return validate()" >
                    <span class="text-danger" id="fname"></span>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="last_name" class="font-weight-bold">{{trans('weblng.FIELD.LAST_NAME')}}</label>
                    <input type="text" class="form-control inset-input" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="{{trans('weblng.FIELD.LAST_NAME_PLACE')}}" onkeyup="return validate()" >
                    <span class="text-danger" id="lname"></span>
                </div>
                </div>
            </div>
          
            <div class="form-group">
                <label for="mobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}&nbsp;<span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span></label>
                <input type="hidden" name="code" id="code">
                <input type="text" class="form-control inset-input" name="mobile_no" id="mobile_no" value="{{ old('mobile_no') }}" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validate()" >
                <span class="text-danger" id="mobile"></span>
            </div>
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="password" class="font-weight-bold">{{trans('weblng.FIELD.CREATE_PASS')}}</label>
                    <input type="password" class="form-control inset-input" name="password" id="password" placeholder="{{trans('weblng.FIELD.PASSWORD_PLACE')}}" onkeyup="return validate()" >
                    <span class="text-danger" id="pass"></span>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="password_confirmation" class="font-weight-bold">{{trans('weblng.FIELD.CONFIRM_PASSWORD')}}</label>
                    <input type="password" class="form-control inset-input" name="password_confirmation" id="password_confirmation" placeholder="{{trans('weblng.FIELD.CONFIRM_PASSWORD_PLACE')}}" onkeyup="return validate()" >
                    <span class="text-danger" id="cpass"></span>
                </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.HEADER.SIGNUP')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="reload" style="display: none;"></i></div></button>
            </div>
            </form>
        </div>
    {{--END PASSENGER SIGNUP  --}}
    {{--DRIVER SIGNUP  --}}
        <div class="driver_fields" style="display: none">
            <form id="submitDriverForm">
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="dfirst_name" class="font-weight-bold">{{trans('weblng.FIELD.FIRST_NAME')}}</label>
                    <input type="text" name="first_name" id="dfirst_name" value="{{ old('first_name') }}" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME_PLACE')}}" onkeyup="return validateDriver()">
                    <span class="text-danger" id="fdname"></span>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="dlast_name" class="font-weight-bold">{{trans('weblng.FIELD.LAST_NAME')}}</label>
                    <input type="text" class="form-control inset-input" name="last_name" id="dlast_name" value="{{ old('last_name') }}" placeholder="{{trans('weblng.FIELD.LAST_NAME_PLACE')}}" onkeyup="return validateDriver()">
                    <span class="text-danger" id="ldname"></span>
                </div>
                </div>
            </div>

            <div class="form-group">
            <label for="gender" class="font-weight-bold">{{trans('weblng.FIELD.GENDER')}}</label>
                <select name="gender" id="gender" class="form-control inset-input" onchange="return validateDriver()">
                    <option value="">{{trans('weblng.FIELD.Select')}}</option>
                    <option value="Male">{{trans('weblng.FIELD.MALE')}}</option>
                    <option value="Female">{{trans('weblng.FIELD.FEMALE')}}</option>
                </select>
                <span class="text-danger" id="gend"></span>
            </div>
           
            <div class="form-group">
            <label for="dmobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}&nbsp;<span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span></label>
                <input type="hidden" name="dcode" id="dcode">
                <input type="text" class="form-control inset-input" name="mobile_no" id="dmobile_no" value="{{ old('mobile_no') }}" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validateDriver()">
                <span class="text-danger" id="dmobile"></span>
            </div>
            <div class="form-group">
                <label for="service_type" class="font-weight-bold">{{trans('weblng.FIELD.SERVICE_TYPE')}}</label>
                <select name="service_type" id="service_type" class="form-control inset-input" onchange="return validateDriver()">
                    
                </select>
                <span class="text-danger" id="service"></span>
            </div>
            <div class="form-group">
                <label for="car_make" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MAKE')}}</label>
                <select name="car_make" id="car_make" class="form-control inset-input" onchange="return validateDriver()">
                    
                </select>
                <span class="text-danger" id="make"></span>
            </div>
            <div class="form-group">
                <label for="car_model" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MODEL')}}</label>
                <select name="car_model" id="car_model" class="form-control inset-input" onchange="return validateDriver()">
                    
                </select>
                <span class="text-danger" id="model"></span>
            </div>
            <div class="form-group">
                <label for="model_year" class="font-weight-bold">{{trans('weblng.FIELD.MODEL_YEAR')}}</label>
                <select name="model_year" id="model_year" class="form-control inset-input" onchange="return validateDriver()">
                    
                </select>
                <span class="text-danger" id="year"></span>
            </div>
            <div class="form-group">
                <label for="car_number" class="font-weight-bold">{{trans('weblng.FIELD.CAR_NO')}}</label>
                <input type="text" class="form-control inset-input" name="car_number" id="car_number" value="{{ old('car_number') }}" placeholder="{{trans('weblng.FIELD.CAR_NO_PLACE')}}" onkeyup="return validateDriver()">
                <span class="text-danger" id="carno"></span>
            </div>
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="dpassword" class="font-weight-bold">{{trans('weblng.FIELD.CREATE_PASS')}}</label>
                    <input type="password" class="form-control inset-input" name="password" id="dpassword" placeholder="{{trans('weblng.FIELD.PASSWORD')}}" onkeyup="return validateDriver()">
                    <span class="text-danger" id="dpass"></span>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="dpassword_confirmation" class="font-weight-bold">{{trans('weblng.FIELD.CONFIRM_PASSWORD')}}</label>
                    <input type="password" class="form-control inset-input" name="password_confirmation" id="dpassword_confirmation" placeholder="{{trans('weblng.FIELD.CONFIRM_PASSWORD_PLACE')}}" onkeyup="return validateDriver()">
                    <span class="text-danger" id="dcpass"></span>
                </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.LOGIN.SIGN_UP')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="dreload" style="display: none;"></i></div></button>
            </div>
            </form>
        </div>
    {{--END DRIVER SIGNUP  --}}
    {{--FLEETOWNER SIGNUP  --}}
        <div class="fleetowner_fields" style="display: none">
            <form id="submitFleetownerForm">
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="ffirst_name" class="font-weight-bold">{{trans('weblng.FIELD.FIRST_NAME')}}</label>
                    <input type="text" name="ffirst_name" id="ffirst_name" value="{{ old('ffirst_name') }}" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME_PLACE')}}" onkeyup="return validateFleet()" >
                    <span class="text-danger" id="ffname"></span>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="flast_name" class="font-weight-bold">{{trans('weblng.FIELD.LAST_NAME')}}</label>
                    <input type="text" class="form-control inset-input" name="flast_name" id="flast_name" value="{{ old('flast_name') }}" placeholder="{{trans('weblng.FIELD.LAST_NAME_PLACE')}}" onkeyup="return validateFleet()" >
                    <span class="text-danger" id="flname"></span>
                </div>
                </div>
            </div>


            <div class="form-group">
            <label for="fgender" class="font-weight-bold">{{trans('weblng.FIELD.GENDER')}}</label>
                <select name="fgender" id="fgender" class="form-control inset-input" onchange="return validateFleet()">
                    <option value="">{{trans('weblng.FIELD.Select')}}</option>
                    <option value="Male">{{trans('weblng.FIELD.MALE')}}</option>
                    <option value="Female">{{trans('weblng.FIELD.FEMALE')}}</option>
                </select>
                <span class="text-danger" id="fgend"></span>
            </div>
          
            <div class="form-group">
                <label for="fmobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}&nbsp;<span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span></label>
                <input type="hidden" name="fcode" id="fcode">
                <input type="text" class="form-control inset-input max-w-100" name="fmobile_no" id="fmobile_no" value="{{ old('fmobile_no') }}" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validateFleet()" >
                <span class="text-danger" id="fmobile"></span>
            </div>
            
            <div class="form-group">
                <label for="email_id" class="font-weight-bold">{{trans('weblng.FIELD.EMAIL')}}</label>
                <input type="email" class="form-control inset-input" name="email_id" id="email_id" value="{{ old('email_id') }}" placeholder="{{trans('weblng.FIELD.EMAIL_ADDRESS')}}" onkeyup="return validateFleet()">
                <span class="text-danger" id="email"></span>
            </div>

            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="fpassword" class="font-weight-bold">{{trans('weblng.FIELD.CREATE_PASS')}}</label>
                    <input type="password" class="form-control inset-input" name="password" id="fpassword" placeholder="{{trans('weblng.FIELD.PASSWORD_PLACE')}}" onkeyup="return validateFleet()" >
                    <span class="text-danger" id="fpass"></span>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="fpassword_confirmation" class="font-weight-bold">{{trans('weblng.FIELD.CONFIRM_PASSWORD')}}</label>
                    <input type="password" class="form-control inset-input" name="password_confirmation" id="fpassword_confirmation" placeholder="{{trans('weblng.FIELD.CONFIRM_PASSWORD_PLACE')}}" onkeyup="return validateFleet()" >
                    <span class="text-danger" id="fcpass"></span>
                </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.HEADER.SIGNUP')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="reload" style="display: none;"></i></div></button>
            </div>
            </form>
        </div>
    {{--END FLEETOWNER SIGNUP  --}}
            <div class="d-flex justify-content-center mb-5">
                <h5 class="text-muted">{{trans('weblng.SIGN_UP.HAVE_ACCOUNT')}} <strong>
                    <a href="{{ url('login') }}" class="text-dark">{{trans('weblng.LOGIN.SIGN_IN')}}</a>
                </strong></h5>
            </div>
           
        </div>
    </div>
    <!--Hidden-->
    <input type="hidden" name="hidden_otp" id="hidden_otp">
    <input type="hidden" name="hidden_user" id="hidden_user">
    <!-- Modal -->
    <div class="modal fade" id="verificationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="p-2 w-100">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="{{URL::asset('/')}}storage/assets/img/signs.png" alt="" class="m-close">
            </button>
        </div>
        <form id="modalOTP">
            <div class="modal-body w-100">
                <h1 class="text-center"><strong>{{trans('weblng.SIGN_UP.VERIFY_ACCOUNT')}} <span class="text-theme">{{trans('weblng.SIGN_UP.ACCOUNT')}}</span></strong></h1>
                <p class="text-center text-muted my-2">{{trans('weblng.SIGN_UP.OTP_MESSAGES')}}</p>
                <div class="row justify-content-center my-3">
                    <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
                </div>
                <p class="text-center my-2">{{trans('weblng.FIELD.OTP')}}</p>
                <p class="text-center text-danger my-1" id="otperror"></p>
                <div class="d-flex">
                    <div class="col-md-8 offset-md-2">
                    <input type="text" class="form-control inset-input" name="otp" id="otp" placeholder="{{trans('weblng.FIELD.OTP_PLACE')}}" onkeyup="return validateOTP()">
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-5">
                <button type="submit" class="btn btn-lrg grad text-white mx-auto">{{trans('weblng.BOOKING.SUBMIT')}}</button>
            </div>
        </form>
        </div>
    </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
<script>
    jq = jQuery.noConflict();
    use :
        var s = jq("#mobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });
        var s = jq("#dmobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });
        var s = jq("#fmobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

    insteadof :
        var d = jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#code").val(d);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#code").val((jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });

        var d = jq("#dmobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#dcode").val(d);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#dcode").val((jq("#dmobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });

        var fd = jq("#fmobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#fcode").val(fd);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#fcode").val((jq("#fmobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });


    function validateFleet(){
        var status=null;

        var option=document.getElementsByName('user');
        if (!(option[0].checked || option[1].checked || option[2].checked)) {
            document.getElementById("utype").innerHTML='{{trans('weblng.VALIDATION_MSG.USER')}}'
            status = false;
        } else {
            jq('.offset-lg-2').addClass('pt-5');
            jq('.signup_alert').hide();
            jq('#signup-msg').html('');
            document.getElementById("utype").innerHTML=''
            status=true
        }
        
        var ffname = document.getElementById('ffirst_name').value
        if (ffname == '') {
            document.getElementById("ffname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('ffirst_name').classList.add('has-error')
            status=false
        } else if (ffname.length<3) {
            document.getElementById("ffname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('ffirst_name').classList.add('has-error')
            status=false
        } else if (ffname.length>255) {
            document.getElementById("ffname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('ffirst_name').classList.add('has-error')
            status=false
        } else {
            document.getElementById("ffname").innerHTML=''
            document.getElementById('ffirst_name').classList.remove('has-error')
        }

        var flname = document.getElementById('flast_name').value
        if (flname == '') {
            document.getElementById("flname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('flast_name').classList.add('has-error')
            status=false
        } else if (flname.length<3) {
            document.getElementById("flname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('flast_name').classList.add('has-error')
            status=false

        } else if (flname.length>255) {
            document.getElementById("flname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('flast_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("flname").innerHTML=''
            document.getElementById('flast_name').classList.remove('has-error')
        }

        var fGender = document.getElementById('fgender').value
        if (fGender == '') {
            document.getElementById("fgend").innerHTML='{{trans('weblng.VALIDATION_MSG.GENDER')}}'
            document.getElementById('fgender').classList.add('has-error')
            status=false
        } else {
            document.getElementById("fgend").innerHTML=''
            document.getElementById('fgender').classList.remove('has-error')
        }

        var fmobileno = document.getElementById('fmobile_no').value
        if (fmobileno == '') {
            document.getElementById("fmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE')}}'
            document.getElementById('fmobile_no').classList.add('has-error')
            status=false

        } else if (!fmobileno.match(/^([0-9\s\-\+\(\)]*)$/) || fmobileno.length<5 || fmobileno.length>10) {
            document.getElementById("fmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE_REQ')}}'
            document.getElementById('fmobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("fmobile").innerHTML=''
            document.getElementById('fmobile_no').classList.remove('has-error')
        }

        var Email = document.getElementById('email_id').value
        if (Email == '') {
            document.getElementById("email").innerHTML='{{trans('weblng.VALIDATION_MSG.EMAIL_MSG')}}'
            document.getElementById('email_id').classList.add('has-error')
            status=false
        } else {
            document.getElementById("email").innerHTML=''
            document.getElementById('email_id').classList.remove('has-error')
        }

        var fpassword = document.getElementById('fpassword').value
        if (fpassword == '') {
            document.getElementById("fpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD')}}'
            document.getElementById('fpassword').classList.add('has-error')
            status=false
        } else if (fpassword.length<6||fpassword.length>255) {
            document.getElementById("fpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_REQ')}}'
            document.getElementById('fpassword').classList.add('has-error')
            status=false

        } else {
            document.getElementById("fpass").innerHTML=''
            document.getElementById('fpassword').classList.remove('has-error')
        }

        var fpassword_confirm = document.getElementById('fpassword_confirmation').value
        if (fpassword_confirm == '') {
            document.getElementById("fcpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM')}}'
            document.getElementById('fpassword_confirmation').classList.add('has-error')
            status=false
        } else if (fpassword_confirm.length<6||fpassword_confirm.length>255) {
            document.getElementById("fcpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM_REQ')}}'
            document.getElementById('fpassword_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("fcpass").innerHTML=''
            document.getElementById('fpassword_confirmation').classList.remove('has-error')
        }
        
        return status
    }


    function validate(){
        var status=null;

        var option=document.getElementsByName('user');
        if (!(option[0].checked || option[1].checked || option[2].checked)) {
            document.getElementById("utype").innerHTML='{{trans('weblng.VALIDATION_MSG.USER')}}'
            status = false;
        } else {
            jq('.offset-lg-2').addClass('pt-5');
            jq('.signup_alert').hide();
            jq('#signup-msg').html('');
            document.getElementById("utype").innerHTML=''
            status=true
        }
        
        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var mobileno = document.getElementById('mobile_no').value
        if (mobileno == '') {
            document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE')}}'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else if (!mobileno.match(/^([0-9\s\-\+\(\)]*)$/) || mobileno.length<5 || mobileno.length>10) {
            document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE_REQ')}}'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("mobile").innerHTML=''
            document.getElementById('mobile_no').classList.remove('has-error')
        }

        var password = document.getElementById('password').value
        if (password == '') {
            document.getElementById("pass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD')}}'
            document.getElementById('password').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("pass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_REQ')}}'
            document.getElementById('password').classList.add('has-error')
            status=false

        } else {
            document.getElementById("pass").innerHTML=''
            document.getElementById('password').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('password_confirmation').value
        if (password_confirm == '') {
            document.getElementById("cpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM')}}'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("cpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM_REQ')}}'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("cpass").innerHTML=''
            document.getElementById('password_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    function validateDriver(){

        var status=null;

        var option=document.getElementsByName('user');
        if (!(option[0].checked || option[1].checked || option[2].checked)) {
            document.getElementById("utype").innerHTML='{{trans('weblng.VALIDATION_MSG.USER')}}'
            status = false;
        } else {
            jq('.offset-lg-2').addClass('pt-5');
            jq('.signup_alert').hide();
            jq('#signup-msg').html('');
            document.getElementById("utype").innerHTML=''
            status=true
        }
        
        var dfname = document.getElementById('dfirst_name').value
        if (dfname == '') {
            document.getElementById("fdname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else if (dfname.length<3) {
            document.getElementById("fdname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else if (dfname.length>255) {
            document.getElementById("fdname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else {
            document.getElementById("fdname").innerHTML=''
            document.getElementById('dfirst_name').classList.remove('has-error')
        }

        var dlname = document.getElementById('dlast_name').value
        if (dlname == '') {
            document.getElementById("ldname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false
        } else if (dlname.length<3) {
            document.getElementById("ldname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false

        } else if (dlname.length>255) {
            document.getElementById("ldname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("ldname").innerHTML=''
            document.getElementById('dlast_name').classList.remove('has-error')
        }

        var Gender = document.getElementById('gender').value
        if (Gender == '') {
            document.getElementById("gend").innerHTML='{{trans('weblng.VALIDATION_MSG.GENDER')}}'
            document.getElementById('gender').classList.add('has-error')
            status=false
        } else {
            document.getElementById("gend").innerHTML=''
            document.getElementById('gender').classList.remove('has-error')
        }

        var mobileno = document.getElementById('dmobile_no').value
        if (mobileno == '') {
            document.getElementById("dmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE')}}'
            document.getElementById('dmobile_no').classList.add('has-error')
            status=false

        } else if (!mobileno.match(/^([0-9\s\-\+\(\)]*)$/) || mobileno.length<5 || mobileno.length>10) {
            document.getElementById("dmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE_REQ')}}'
            document.getElementById('dmobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dmobile").innerHTML=''
            document.getElementById('dmobile_no').classList.remove('has-error')
        }

        var serviceType = document.getElementById('service_type').value
        if (serviceType == '') {
            document.getElementById("service").innerHTML='{{trans('weblng.VALIDATION_MSG.SERVICE_TYPE')}}'
            document.getElementById('service_type').classList.add('has-error')
            status=false
        } else {
            document.getElementById("service").innerHTML=''
            document.getElementById('service_type').classList.remove('has-error')
        }

        var carMake = document.getElementById('car_make').value
        if (carMake == '') {
            document.getElementById("make").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_MAKE')}}'
            document.getElementById('car_make').classList.add('has-error')
            status=false
        } else {
            document.getElementById("make").innerHTML=''
            document.getElementById('car_make').classList.remove('has-error')
        }

        var carModel = document.getElementById('car_model').value
        if (carModel == '') {
            document.getElementById("model").innerHTML='{{trans('weblng.VALIDATION_MSG.MODEL')}}'
            document.getElementById('car_model').classList.add('has-error')
            status=false
        } else {
            document.getElementById("model").innerHTML=''
            document.getElementById('car_model').classList.remove('has-error')
        }

        var modelYear = document.getElementById('model_year').value
        if (modelYear == '') {
            document.getElementById("year").innerHTML='{{trans('weblng.VALIDATION_MSG.YEAR')}}'
            document.getElementById('model_year').classList.add('has-error')
            status=false
        } else {
            document.getElementById("year").innerHTML=''
            document.getElementById('model_year').classList.remove('has-error')
        }

        var carNumber = document.getElementById('car_number').value
        if (carNumber == '') {
            document.getElementById("carno").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_NUMBER')}}'
            document.getElementById('car_number').classList.add('has-error')
            status=false
        } else {
            document.getElementById("carno").innerHTML=''
            document.getElementById('car_number').classList.remove('has-error')
        }

        var password = document.getElementById('dpassword').value
        if (password == '') {
            document.getElementById("dpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD')}}'
            document.getElementById('dpassword').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("dpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_REQ')}}'
            document.getElementById('dpassword').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dpass").innerHTML=''
            document.getElementById('dpassword').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('dpassword_confirmation').value
        if (password_confirm == '') {
            document.getElementById("dcpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM')}}'
            document.getElementById('dpassword_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("dcpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM_REQ')}}'
            document.getElementById('dpassword_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dcpass").innerHTML=''
            document.getElementById('dpassword_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    function validateOTP(){
        var otp = document.getElementById('otp').value;
        if (!otp.match(/^([0-9\s\-\+\(\)]*)$/) || otp.length>4) {
            document.getElementById("otperror").innerHTML='{{trans('weblng.VALIDATION_MSG.OTP_VALID')}}';
            return false;
        } else {
            document.getElementById("otperror").innerHTML='';
            return true;
        }
    }

jq(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    var user = $('input[name="user"]:checked').val()
    if (user=='passenger') {
        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
    }

    jq('#car_make').change(function(){
        var make = jq(this).val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.model') }}",
            method: "POST",
            data: {make: make, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#car_model').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_MODEL')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.model+'">'+value.model+'</option>';
                });
                $('#car_model').append(options);
                jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                    jq('#car_model').addClass('has-error');
                });
                $('#model_year').html('');
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#car_model').html('');
                    jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                        jq('#car_model').addClass('has-error');
                    });

                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
            }
        });
    });

    jq('#car_model').change(function(){
        var model = jq(this).val();
        var make = jq('#car_make').val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.year') }}",
            method: "POST",
            data: {make: make, model: model, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#model_year').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_YEAR')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.year+'">'+value.year+'</option>';
                });
                $('#model_year').append(options);
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
            }
        });

    });

    $('#radio1').on('click',function(){
       if(this.checked){
        $('.passenger_fields').show();
        $('.driver_fields').hide();
        $('.fleetowner_fields').hide();

        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
       }
    });

    $('#radio3').on('click',function(){
       if(this.checked){
        $('.passenger_fields').hide();
        $('.driver_fields').hide();
        $('.fleetowner_fields').show();
       }
    });

//DRIVER SERVICE MAKE..........
    $('#radio2').on('click',function(){
       if(this.checked){
        $('.passenger_fields').hide();
        $('.driver_fields').show();
        $('.fleetowner_fields').hide();

        $('.passb_header').hide();
        $('.passl_header').hide();
        $('.driv_header').show();
        $('.passb_footer').hide();
        $('.passl_footer').hide();
        $('.driv_footer').show();

        jq("input[name='mobile_no']").css('padding-left','95px');

        jq.ajax({
            url: "{{ route('service.list') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('#service_type').empty();
                var options = '<option value="">{{trans('weblng.CHOOSE_SERVICE')}}</option>';
                $.each(response.data, function(key, value) {
                  options += '<option value="'+value.id+'">'+value.name+'</option>';
                });

                $('#service_type').append(options);
            },
            error: function(response){
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                }
            }
        });

        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.make') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#car_make').empty();
                var options = '<option value="">{{trans('weblng.CHOOSE_MAKE')}}</option>';
                $.each(response.data, function(key, value) {
                    options += '<option style="color:#080600;" value="'+value.make+'">'+value.make+'</option>';
                });
                $('#car_make').append(options);
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                }
            }
        });



       }
    });
//END DRIVER SERVICE MAKE..........

//MODEL OTP FOR PASSENGER.......................
    jq('#modalOTP').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var otp = jq('#otp').val();
        var hidden_otp = jq('#hidden_otp').val();
        var hidden_user = jq('#hidden_user').val();
        jq.ajax({
            url: "{{ route('verifyotp') }}",
            method: "POST",
            data: { otp: otp, hidden_otp: hidden_otp, hidden_user: hidden_user},
            success: function(response){
                $("#verificationModal").modal('hide');
                $('#bootboxModal').modal('show');
                $('.bootboxBody').text('{{trans('weblng.VALIDATION_MSG.OTP_SUCCESS')}}');
                //PASSENGER REGISTER.....................
                if (response.hidden_user=='passenger') {
                    var first_name = jq('#first_name').val();
                    var last_name = jq('#last_name').val();
                    var mobile_no = jq('#mobile_no').val();
                    var isdCode = jq('#code').val();
                    var password = jq('#password').val();
                    var password_confirmation = $('#password_confirmation').val();
                    var isActive = response.isActive;
                    jq.ajax({
                        url: "{{ route('passenger.signup') }}",
                        method: "POST",
                        data: { first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, password: password, password_confirmation: password_confirmation, isActive: isActive, timeZone: timeZone },
                        success: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                window.location.href = '{{ route('passenger.emergencyContact') }}';
                             });
                        },
                        error: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                if (response.status == 404){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 403){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 500){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                            });
                        }
                    })
                }
                //END PASSENGER REGISTER.................
                //DRIVER REGISTER........................
                if (response.hidden_user=='driver') {
                    var first_name = jq('#dfirst_name').val();
                    var last_name = jq('#dlast_name').val();
                    var mobile_no = jq('#dmobile_no').val();
                    var isdCode = jq('#dcode').val();
                    var service_type = jq('#service_type').val();
                    var car_make = jq('#car_make').val();
                    var car_model = jq('#car_model').val();
                    var model_year = jq('#model_year').val();
                    var car_number = jq('#car_number').val();
                    var gender = jq('#gender').val();
                    var password = jq('#dpassword').val();
                    var password_confirmation = $('#dpassword_confirmation').val();
                    var isActive = response.isActive;
                    jq.ajax({
                        url: "{{ route('driver.signup') }}",
                        method: "POST",
                        data: { first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, service_type: service_type, car_make: car_make, car_model: car_model, model_year: model_year, car_number: car_number, password: password, password_confirmation: password_confirmation, isActive: isActive, gender: gender, timeZone: timeZone },
                        success: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                window.location.href = '{{ route('driver.email') }}';
                            });
                        },
                        error: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                if (response.status == 404){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 403){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 500){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                            });
                        }
                    })
                }
                //END DRIVER REGISTER....................
                //FLEETOWNER REGISTER.....................
                if (response.hidden_user=='fleetowner') {
                    var first_name = jq('#ffirst_name').val();
                    var last_name = jq('#flast_name').val();
                    var mobile_no = jq('#fmobile_no').val();
                    var isdCode = jq('#fcode').val();
                    var gender = jq('#fgender').val();
                    var email_id = jq('#email_id').val();
                    var password = jq('#fpassword').val();
                    var password_confirmation = $('#fpassword_confirmation').val();
                    var isActive = response.isActive;
                    jq.ajax({
                        url: "{{ route('fleetowner.signup') }}",
                        method: "POST",
                        data: { first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, gender: gender, email_id: email_id, password: password, password_confirmation: password_confirmation, isActive: isActive, timeZone: timeZone },
                        success: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                window.location.href = '{{ route('fleetowner.home') }}';
                             });
                        },
                        error: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                if (response.status == 404){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 403){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 500){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.offset-lg-2').removeClass('pt-5');
                                    jq('.signup_alert').show();
                                    jq('#signup-msg').html(responseMsg.message)
                                }
                            });
                        }
                    })
                }
                //END FLEETOWNER REGISTER.................
            },
            error: function(response){
                jq('#otperror').html(response.responseJSON.message);
            }

        });
    });
//END MODEL OTP FOR PASSENGER.......................
                
//SUBMIT DRIVER FORM.................................
    jq('#submitDriverForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = jq("input[type=radio][name=user]:checked").val();
        var first_name = jq('#dfirst_name').val();
        var last_name = jq('#dlast_name').val();
        var mobile_no = jq('#dmobile_no').val();
        var isdCode = jq('#dcode').val();
        var service_type = jq('#service_type').val();
        var car_make = jq('#car_make').val();
        var car_model = jq('#car_model').val();
        var model_year = jq('#model_year').val();
        var car_number = jq('#car_number').val();
        var gender = jq('#gender').val();
        var password = jq('#dpassword').val();
        var password_confirmation = $('#dpassword_confirmation').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('driver.signup.otp') }}",
            method:"POST",
            data:{ user: user, first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, service_type: service_type, car_make: car_make, car_model: car_model, model_year: model_year, car_number: car_number, password: password, password_confirmation: password_confirmation, gender: gender, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                var otp = response.data.OTP;
                jq('#hidden_otp').val(otp);
                jq('#hidden_user').val('driver');
                $("#verificationModal").modal('show');
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#fdname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#dfirst_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#ldname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#dlast_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('gender')) {
                        jq('#gend').html(responseMsg.errors.gender).promise().done(function(){
                            jq('#gender').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        jq('#dmobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            jq('#dmobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('service_type')) {
                        jq('#service').html(responseMsg.errors.service_type).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_make')) {
                        jq('#make').html(responseMsg.errors.car_make).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_model')) {
                        jq('#model').html(responseMsg.errors.car_model).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('model_year')) {
                        jq('#year').html(responseMsg.errors.model_year).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_number')) {
                        jq('#carno').html(responseMsg.errors.car_number).promise().done(function(){
                            jq('#car_number').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        jq('#dpass').html(responseMsg.errors.password).promise().done(function(){
                            jq('#dpassword').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                        jq('#dcpass').html(responseMsg.errors.password_confirmation).promise().done(function(){
                            jq('#dpassword_confirmation').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#dmobile').html(responseMsg.message).promise().done(function(){
                            jq('#dmobile_no').addClass('has-error');
                        });
                    }
                    jq('#utype').html(responseMsg.errors.user);
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('#dmobile').html(responseMsg.message).promise().done(function(){
                        jq('#dmobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message);
                }
            }
      });
    });
//END SUBMIT DRIVER FORM.................................

//SUBMIT PASSENGER FORM.................................
    jq('#submitPassengerForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = jq("input[type=radio][name=user]:checked").val();
        var first_name = jq('#first_name').val();
        var last_name = jq('#last_name').val();
        var mobile_no = jq('#mobile_no').val();
        var isdCode = jq('#code').val();
        var password = jq('#password').val();
        var password_confirmation = $('#password_confirmation').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.signup.otp') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, password: password, user: user, password_confirmation: password_confirmation, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                var otp = response.data.OTP;
                jq('#hidden_otp').val(otp);
                jq('#hidden_user').val('passenger');
                $("#verificationModal").modal('show');
            },

            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        jq('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            jq('#mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        jq('#pass').html(responseMsg.errors.password).promise().done(function(){
                            jq('#password').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                        jq('#cpass').html(responseMsg.errors.password_confirmation).promise().done(function(){
                            jq('#password_confirmation').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#mobile').html(responseMsg.message).promise().done(function(){
                            jq('#mobile_no').addClass('has-error');
                        });
                    }
                    jq('#utype').html(responseMsg.errors.user);
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('#mobile').html(responseMsg.message).promise().done(function(){
                        jq('#mobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message);
                }
            }
      });
    });
//END SUBMIT PASSENGER FORM.................................

//SUBMIT FLEETOWNER FORM.................................
    jq('#submitFleetownerForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = jq("input[type=radio][name=user]:checked").val();
        var first_name = jq('#ffirst_name').val();
        var last_name = jq('#flast_name').val();
        var mobile_no = jq('#fmobile_no').val();
        var isdCode = jq('#fcode').val();
        var gender = jq('#fgender').val();
        var email_id = jq('#email_id').val();
        var password = jq('#fpassword').val();
        var password_confirmation = $('#fpassword_confirmation').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('fleetowner.signup.otp') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, password: password, user: user, gender: gender, email_id: email_id, password_confirmation: password_confirmation, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                var otp = response.data.OTP;
                jq('#hidden_otp').val(otp);
                jq('#hidden_user').val('fleetowner');
                $("#verificationModal").modal('show');
            },

            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#ffname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#ffirst_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#flname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#flast_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('gender')) {
                        jq('#fgend').html(responseMsg.errors.gender).promise().done(function(){
                            jq('#fgender').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        jq('#fmobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            jq('#fmobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('email_id')) {
                        jq('#email').html(responseMsg.errors.email_id).promise().done(function(){
                            jq('#email_id').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        jq('#fpass').html(responseMsg.errors.password).promise().done(function(){
                            jq('#fpassword').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                        jq('#fcpass').html(responseMsg.errors.password_confirmation).promise().done(function(){
                            jq('#fpassword_confirmation').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        // jq('#fmobile').html(responseMsg.message).promise().done(function(){
                        //     jq('#fmobile_no').addClass('has-error');
                        // });
                        jq('.offset-lg-2').removeClass('pt-5');
                        jq('.signup_alert').show();
                        jq('#signup-msg').html(responseMsg.message);
                    }
                    jq('#utype').html(responseMsg.errors.user);
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('#fmobile').html(responseMsg.message).promise().done(function(){
                        jq('#fmobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message);
                }
            }
      });
    });
//END SUBMIT FLEETOWNER FORM.................................

  });  
</script>
<!-- footer -->
@include('partials.footer')