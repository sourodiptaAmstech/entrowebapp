<!-- header -->
@include('partials.passenger_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="container mb-5">
    <div class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg">
        <h1 class="text-center"><strong>{{trans('weblng.HEADER.LEASE')}}</strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <div class="alert alert-danger leasing_alert" style="display: none;">
                <p class="text-danger" id="leasing-msg"></p>
            </div>
            <div class="alert alert-success leasingsuc_alert" style="display: none;">
                <p class="text-success" id="leasingsuc-msg"></p>
            </div>

            <div class="row justify-content-center">
                <div class="d-inline mb-3">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" checked id="lease1" name="lease_type" value="hourly">
                        <label class="custom-control-label font-weight-bold" for="lease1">{{trans('weblng.LEASE.HOURLY')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="lease2" name="lease_type" value="daily">
                        <label class="custom-control-label font-weight-bold" for="lease2">{{trans('weblng.LEASE.DAILY')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="lease3" name="lease_type" value="long">
                        <label class="custom-control-label font-weight-bold" for="lease3">{{trans('weblng.LEASE.LONGTERM')}}</label>
                    </div>
                </div>
            </div>

            <div id="HourlyLease">
                <form id="HourlyLeaseForm">
                    <div class="form-group">
                        <label for="dateOfJourny" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_DATE')}}</label>
                        <input type="text" class="form-control inset-input" name="dateOfJourny" id="dateOfJourny" placeholder="{{trans('weblng.DATE')}}" onblur="return validateHourly()">
                        <span class="text-danger" id="date"></span>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label for="fromTime" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_START')}}</label>
                            <input type="text" class="form-control inset-input"
                            name="fromTime" id="fromTime" placeholder="{{trans('weblng.FIELD.FROMTIME')}}" onblur="return validateHourly()">
                            <span class="text-danger" id="ftime"></span>
                        </div>
                        </div>
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label for="toTime" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_END')}}</label>
                            <input type="text" class="form-control inset-input" name="toTime" id="toTime" placeholder="{{trans('weblng.FIELD.ENDTIME')}}" onblur="return validateHourly()">
                             <span class="text-danger" id="ttime"></span>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="font-weight-bold">{{trans('weblng.FIELD.LOCATION')}}</label>

                        <input type="text" class="form-control inset-input" name="address" id="address" placeholder="{{trans('weblng.FIELD.LOCATION_PLACE')}}" autocomplete="on" runat="server" onkeyup="return validateHourly()" onblur="return validateHourly()">
                        <span class="text-danger" id="addrs"></span>
                        <input type="hidden" id="cityLat" name="cityLat" />
                        <input type="hidden" id="cityLng" name="cityLng" />

                    </div>
                    <div class="form-group">
                        <label for="services_type" class="font-weight-bold">{{trans('weblng.FIELD.SELECT_SERVICE')}}</label>
                        <select class="form-control inset-input" name="services_type" id="services_type" onchange="return validateHourly()">
                          
                        </select>
                        <span class="text-danger" id="service"></span>
                    </div>
                    <div class="form-group" id="vehicle_equipt" style="display: none;">
                        <label for="vehicle_equipment" class="font-weight-bold">{{trans('weblng.FIELD.VEHICLE_EQUIP')}}</label>
                        <textarea class="form-control inset-input" name="vehicle_equipment" id="vehicle_equipment" placeholder="{{trans('weblng.FIELD.VEHICLE_EQUIP_PLACE')}}"></textarea>
                        <span class="text-danger" id="vedetail"></span>
                    </div>
                    <div class="form-group">
                        <label for="no_passenger" class="font-weight-bold">{{trans('weblng.FIELD.PASSENGER_NO')}}</label>
                        <input type="number" class="form-control inset-input" name="no_passenger" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" id="no_passenger" placeholder="{{trans('weblng.FIELD.PASSENGER_NO_PLACE')}}" onkeyup="return validateHourly()" onchange="return validateHourly()">
                        <span class="text-danger" id="nopass"></span>
                    </div>
                    <div class="form-group">
                        <label for="details" class="font-weight-bold">{{trans('weblng.FIELD.LEASE_DETAILS')}}</label>
                        <textarea class="form-control inset-input" name="details" id="details" placeholder="{{trans('weblng.DETAILS')}}" onblur="return validateHourly()"></textarea>
                        <span class="text-danger" id="detail"></span>
                    </div>
                    <div class="d-flex justify-content-center mb-3 mt-5">
                        <button type="submit" class="btn btn-lg grad text-white" style="width: 220px;">{{trans('weblng.LEASE.REQUEST_QUOTE')}}</button>
                    </div>
                </form>
            </div>

            <div id="DailyLease" style="display: none">
                <form id="DailyLeaseForm">
                    <div class="form-group">
                        <label for="dateOfJournyd" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_DATE')}}</label>
                        <input type="text" class="form-control inset-input" name="dateOfJournyd" id="dateOfJournyd" placeholder="{{trans('weblng.DATE')}}" onblur="return validateDaily()">
                        <span class="text-danger" id="dated"></span>
                    </div>
                    <div class="form-group">
                        <label for="addressd" class="font-weight-bold">{{trans('weblng.FIELD.LOCATION')}}</label>

                        <input type="text" class="form-control inset-input" name="addressd" id="addressd" placeholder="{{trans('weblng.FIELD.LOCATION_PLACE')}}" autocomplete="on" runat="server" onkeyup="return validateDaily()" onblur="return validateDaily()">
                        <span class="text-danger" id="addrsd"></span>
                        <input type="hidden" id="cityLatd" name="cityLatd" />
                        <input type="hidden" id="cityLngd" name="cityLngd" />

                    </div>
                    <div class="form-group">
                        <label for="services_typed" class="font-weight-bold">{{trans('weblng.FIELD.SELECT_SERVICE')}}</label>
                        <select class="form-control inset-input" name="services_typed" id="services_typed" onchange="return validateDaily()">
                           
                        </select>
                        <span class="text-danger" id="serviced"></span>
                    </div>
                    <div class="form-group" id="vehicle_equiptd" style="display: none;">
                        <label for="vehicle_equipmentd" class="font-weight-bold">{{trans('weblng.FIELD.VEHICLE_EQUIP')}}</label>
                        <textarea class="form-control inset-input" name="vehicle_equipmentd" id="vehicle_equipmentd" placeholder="{{trans('weblng.FIELD.VEHICLE_EQUIP_PLACE')}}"></textarea>
                        <span class="text-danger" id="vedetail"></span>
                    </div>
                    <div class="form-group">
                        <label for="no_passengerd" class="font-weight-bold">{{trans('weblng.FIELD.PASSENGER_NO')}}</label>
                        <input type="number" class="form-control inset-input" name="no_passengerd" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" id="no_passengerd" placeholder="{{trans('weblng.FIELD.PASSENGER_NO_PLACE')}}"  onkeyup="return validateDaily()" onchange="return validateDaily()">
                        <span class="text-danger" id="nopassd"></span>
                    </div>
                    <div class="form-group">
                        <label for="detailsd" class="font-weight-bold">{{trans('weblng.FIELD.LEASE_DETAILS')}}</label>
                        <textarea class="form-control inset-input" name="detailsd" id="detailsd" placeholder="{{trans('weblng.DETAILS')}}" onblur="return validateDaily()"></textarea>
                        <span class="text-danger" id="detaild"></span>
                    </div>
                    <div class="d-flex justify-content-center mb-3 mt-5">
                        <button type="submit" class="btn btn-lg grad text-white" style="width: 220px;">{{trans('weblng.LEASE.REQUEST_QUOTE')}}</button>
                    </div>
                </form>
            </div>

            <div id="LongLease" style="display: none">
                <form id="LongLeaseForm">
                    <div class="row">
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label for="fromdate" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_SDATE')}}</label>
                            <input type="text" class="form-control inset-input"
                            name="fromdate" id="fromdate" placeholder="{{trans('weblng.FIELD.FROMDATE')}}" onblur="return validateLong()">
                            <span class="text-danger" id="ftimel"></span>
                        </div>
                        </div>
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label for="todate" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_EDATE')}}</label>
                            <input type="text" class="form-control inset-input" name="todate" id="todate" placeholder="{{trans('weblng.FIELD.ENDATE')}}" onblur="return validateLong()">
                             <span class="text-danger" id="ttimel"></span>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="addressl" class="font-weight-bold">{{trans('weblng.FIELD.LOCATION')}}</label>
                        <input type="text" class="form-control inset-input" name="addressl" id="addressl" placeholder="{{trans('weblng.FIELD.LOCATION_PLACE')}}" autocomplete="on" runat="server" onkeyup="return validateLong()" onblur="return validateLong()">
                        <span class="text-danger" id="addrsl"></span>
                        <input type="hidden" id="cityLatl" name="cityLatl" />
                        <input type="hidden" id="cityLngl" name="cityLngl" />
                    </div>
                    <div class="form-group">
                        <label for="services_typel" class="font-weight-bold">{{trans('weblng.FIELD.SELECT_SERVICE')}}</label>
                        <select class="form-control inset-input" name="services_typel" id="services_typel" onchange="return validateLong()">
                          
                        </select>
                        <span class="text-danger" id="servicel"></span>
                    </div>
                    <div class="form-group" id="vehicle_equiptl" style="display: none;">
                        <label for="vehicle_equipmentl" class="font-weight-bold">{{trans('weblng.FIELD.VEHICLE_EQUIP')}}</label>
                        <textarea class="form-control inset-input" name="vehicle_equipmentl" id="vehicle_equipmentl" placeholder="{{trans('weblng.FIELD.VEHICLE_EQUIP_PLACE')}}"></textarea>
                        <span class="text-danger" id="vedetail"></span>
                    </div>
                    <div class="form-group">
                        <label for="detailsl" class="font-weight-bold">{{trans('weblng.FIELD.LEASE_DETAILS')}}</label>
                        <textarea class="form-control inset-input" name="detailsl" id="detailsl" placeholder="{{trans('weblng.DETAILS')}}" onblur="return validateLong()"></textarea>
                        <span class="text-danger" id="detaill"></span>
                    </div>
                    <div class="d-flex justify-content-center mb-3 mt-5">
                        <button type="submit" class="btn btn-lg grad text-white" style="width: 220px;">{{trans('weblng.LEASE.REQUEST_QUOTE')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0j65u08oCX4Zth6i0S9ePk0ONERbdZ_w&libraries=places"></script>

<script>
    function initialize() {
      var input = document.getElementById('address');
      var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('address').value = place.name;
            document.getElementById('cityLat').value = place.geometry.location.lat();
            document.getElementById('cityLng').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialized() {
      var input = document.getElementById('addressd');
      var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('addressd').value = place.name;
            document.getElementById('cityLatd').value = place.geometry.location.lat();
            document.getElementById('cityLngd').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initialized);

    function initializel() {
      var input = document.getElementById('addressl');
      var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('addressl').value = place.name;
            document.getElementById('cityLatl').value = place.geometry.location.lat();
            document.getElementById('cityLngl').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initializel);


    function validateHourly(){
        var status=null;
        
        var dateOfJourny = document.getElementById('dateOfJourny').value
        if (dateOfJourny == '') {
            document.getElementById("date").innerHTML='{{trans('weblng.VALIDATION_MSG.JOURNYDATE')}}'
            document.getElementById('dateOfJourny').classList.add('has-error')
            status=false
        } else {
            jq('.offset-lg-2').addClass('pt-5');
            jq('.leasing_alert').hide();
            jq('#leasing-msg').html('');
            jq('.leasingsuc_alert').hide();
            jq('#leasingsuc-msg').html('');
            document.getElementById("date").innerHTML=''
            document.getElementById('dateOfJourny').classList.remove('has-error')
            status=true;
        }

        var fromTime = document.getElementById('fromTime').value
        if (fromTime == '') {
            document.getElementById("ftime").innerHTML='{{trans('weblng.VALIDATION_MSG.FROMTIME')}}'
            document.getElementById('fromTime').classList.add('has-error')
            status=false
        } else {
            document.getElementById("ftime").innerHTML=''
            document.getElementById('fromTime').classList.remove('has-error')
        }

        var toTime = document.getElementById('toTime').value
        if (toTime == '') {
            document.getElementById("ttime").innerHTML='{{trans('weblng.VALIDATION_MSG.TOTIME')}}'
            document.getElementById('toTime').classList.add('has-error')
            status=false
        } else {
            document.getElementById("ttime").innerHTML=''
            document.getElementById('toTime').classList.remove('has-error')
        }

        var address = document.getElementById('address').value
        if (address == '') {
            jq("#cityLat").val('');
            jq("#cityLng").val('');
            document.getElementById("addrs").innerHTML='{{trans('weblng.VALIDATION_MSG.ADDRESS')}}'
            document.getElementById('address').classList.add('has-error')
            status=false

        } else {
            document.getElementById("addrs").innerHTML=''
            document.getElementById('address').classList.remove('has-error')
        }

        var services_type = document.getElementById('services_type').value
        if (services_type == '') {
            document.getElementById("service").innerHTML='{{trans('weblng.VALIDATION_MSG.SERVICE_TYPE')}}'
            document.getElementById('services_type').classList.add('has-error')
            status=false

        } else {
            document.getElementById("service").innerHTML=''
            document.getElementById('services_type').classList.remove('has-error')
        }

        var no_passenger = document.getElementById('no_passenger').value
        if (no_passenger == '') {
            document.getElementById("nopass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSENGERNO')}}'
            document.getElementById('no_passenger').classList.add('has-error')
            return false
        } else {
            document.getElementById("nopass").innerHTML=''
            document.getElementById('no_passenger').classList.remove('has-error')
        }

        var details = document.getElementById('details').value
        if (details == '') {
            document.getElementById("detail").innerHTML='{{trans('weblng.VALIDATION_MSG.DETAILS')}}'
            document.getElementById('details').classList.add('has-error')
            return false
        } else {
            document.getElementById("detail").innerHTML=''
            document.getElementById('details').classList.remove('has-error')
        }
        
        return status
    }

    function validateDaily(){
        var status=null;
        
        var dateOfJourny = document.getElementById('dateOfJournyd').value
        if (dateOfJourny == '') {
            document.getElementById("dated").innerHTML='{{trans('weblng.VALIDATION_MSG.JOURNYDATE')}}'
            document.getElementById('dateOfJournyd').classList.add('has-error')
            status=false
        } else {
            jq('.offset-lg-2').addClass('pt-5');
            jq('.leasing_alert').hide();
            jq('#leasing-msg').html('');
            jq('.leasingsuc_alert').hide();
            jq('#leasingsuc-msg').html('');
            document.getElementById("dated").innerHTML=''
            document.getElementById('dateOfJournyd').classList.remove('has-error')
            status=true;
        }

        var address = document.getElementById('addressd').value
        if (address == '') {
            jq("#cityLatd").val('');
            jq("#cityLngd").val('');
            document.getElementById("addrsd").innerHTML='{{trans('weblng.VALIDATION_MSG.ADDRESS')}}'
            document.getElementById('addressd').classList.add('has-error')
            status=false

        } else {
            document.getElementById("addrsd").innerHTML=''
            document.getElementById('addressd').classList.remove('has-error')
        }

        var services_type = document.getElementById('services_typed').value
        if (services_type == '') {
            document.getElementById("serviced").innerHTML='{{trans('weblng.VALIDATION_MSG.SERVICE_TYPE')}}'
            document.getElementById('services_typed').classList.add('has-error')
            status=false

        } else {
            document.getElementById("serviced").innerHTML=''
            document.getElementById('services_typed').classList.remove('has-error')
        }

        var no_passenger = document.getElementById('no_passengerd').value
        if (no_passenger == '') {
            document.getElementById("nopassd").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSENGERNO')}}'
            document.getElementById('no_passengerd').classList.add('has-error')
            return false
        } else {
            document.getElementById("nopassd").innerHTML=''
            document.getElementById('no_passengerd').classList.remove('has-error')
        }

        var details = document.getElementById('detailsd').value
        if (details == '') {
            document.getElementById("detaild").innerHTML='{{trans('weblng.VALIDATION_MSG.DETAILS')}}'
            document.getElementById('detailsd').classList.add('has-error')
            return false
        } else {
            document.getElementById("detaild").innerHTML=''
            document.getElementById('detailsd').classList.remove('has-error')
        }
        
        return status
    }

    function validateLong(){
        var status=null;
        
        var fromdate = document.getElementById('fromdate').value
        if (fromdate == '') {
            document.getElementById("ftimel").innerHTML='{{trans('weblng.VALIDATION_MSG.FROMDATE')}}'
            document.getElementById('fromdate').classList.add('has-error')
            status=false
        } else {
           
            // $('#todate').datetimepicker('destroy');
            // $('#todate').datetimepicker('destroy').datetimepicker({
            //     format: 'YYYY-MM-DD',
            //     minDate: new Date(fromdate),
            //      allowInputToggle: true,
            // });
            
            
            jq('.offset-lg-2').addClass('pt-5');
            jq('.leasing_alert').hide();
            jq('#leasing-msg').html('');
            jq('.leasingsuc_alert').hide();
            jq('#leasingsuc-msg').html('');
            document.getElementById("ftimel").innerHTML=''
            document.getElementById('fromdate').classList.remove('has-error')
            status=true;
        }

        var todate = document.getElementById('todate').value
        if (todate == '') {
            document.getElementById("ttimel").innerHTML='{{trans('weblng.VALIDATION_MSG.TODATE')}}'
            document.getElementById('todate').classList.add('has-error')
            status=false
        } else {

            // $('#fromdate').datetimepicker('destroy');
            // $('#fromdate').datetimepicker({
            //     format: 'YYYY-MM-DD',
            //     maxDate: new Date(todate)
            // });
            
            document.getElementById("ttimel").innerHTML=''
            document.getElementById('todate').classList.remove('has-error')
            
        }

        var address = document.getElementById('addressl').value
        if (address == '') {
            jq("#cityLatl").val('');
            jq("#cityLngl").val('');
            document.getElementById("addrsl").innerHTML='{{trans('weblng.VALIDATION_MSG.ADDRESS')}}'
            document.getElementById('addressl').classList.add('has-error')
            status=false

        } else {
            document.getElementById("addrsl").innerHTML=''
            document.getElementById('addressl').classList.remove('has-error')
        }

        var services_type = document.getElementById('services_typel').value
        if (services_type == '') {
            document.getElementById("servicel").innerHTML='{{trans('weblng.VALIDATION_MSG.SERVICE_TYPE')}}'
            document.getElementById('services_typel').classList.add('has-error')
            status=false

        } else {
            document.getElementById("servicel").innerHTML=''
            document.getElementById('services_typel').classList.remove('has-error')
        }

        var details = document.getElementById('detailsl').value
        if (details == '') {
            document.getElementById("detaill").innerHTML='{{trans('weblng.VALIDATION_MSG.DETAILS')}}'
            document.getElementById('detailsl').classList.add('has-error')
            return false
        } else {
            document.getElementById("detaill").innerHTML=''
            document.getElementById('detailsl').classList.remove('has-error')
        }
        
        return status
    }

    $('#lease1').on('click',function(){
        if(this.checked){
            $('#HourlyLease').show();
            $('#DailyLease').hide();
            $('#LongLease').hide();
            Servicelist()
        }
    });
    $('#lease2').on('click',function(){
        if(this.checked){
            $('#HourlyLease').hide();
            $('#DailyLease').show();
            $('#LongLease').hide();

            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('service.leasing.list') }}",
                method: "GET",
                data: {timeZone: timeZone},
                success: function(response){
                    jq(".overlay").hide();
                    jq('#services_typed').empty();
                    var options = '<option value="">{{trans('weblng.CHOOSE_SERVICE')}}</option>';
                    jq.each(response.data, function(key, value) {
                      options += '<option value="'+value.id+'">'+value.name+'</option>';
                    });
                    jq('#services_typed').append(options);
                },
                error: function(response){
                    jq(".overlay").hide();
                    if (response.status == 400){
                        var responseMsg = jq.parseJSON(response.responseText);
                        jq('.px-sm-3').removeClass('pt-5');
                        jq('.leasing_alert').show();
                        jq('#leasing-msg').html(responseMsg.message).promise().done(function(){
                                jq('#services_typed').addClass('has-error');
                            });
                    }
                    if (response.status == 500){
                        var responseMsg = jq.parseJSON(response.responseText);
                        jq('.px-sm-3').removeClass('pt-5');
                        jq('.leasing_alert').show();
                        jq('#leasing-msg').html(responseMsg.message).promise().done(function(){
                                jq('#services_typed').addClass('has-error');
                            });;
                    }
                }
            });
        }
    });
    $('#lease3').on('click',function(){
        if(this.checked){
            $('#HourlyLease').hide();
            $('#DailyLease').hide();
            $('#LongLease').show();
            
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('service.leasing.list') }}",
                method: "GET",
                data: {timeZone: timeZone},
                success: function(response){
                    jq(".overlay").hide();
                    jq('#services_typel').empty();
                    var options = '<option value="">{{trans('weblng.CHOOSE_SERVICE')}}</option>';
                    jq.each(response.data, function(key, value) {
                      options += '<option value="'+value.id+'">'+value.name+'</option>';
                    });
                    jq('#services_typel').append(options);
                },
                error: function(response){
                    jq(".overlay").hide();
                    if (response.status == 400){
                        var responseMsg = jq.parseJSON(response.responseText);
                        jq('.px-sm-3').removeClass('pt-5');
                        jq('.leasing_alert').show();
                        jq('#leasing-msg').html(responseMsg.message).promise().done(function(){
                                jq('#services_typel').addClass('has-error');
                            });
                    }
                    if (response.status == 500){
                        var responseMsg = jq.parseJSON(response.responseText);
                        jq('.px-sm-3').removeClass('pt-5');
                        jq('.leasing_alert').show();
                        jq('#leasing-msg').html(responseMsg.message).promise().done(function(){
                                jq('#services_typel').addClass('has-error');
                            });
                    }
                }
            });
        }
    });

Servicelist();

function Servicelist(){

    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    jq(".overlay").show();
    jq.ajax({
        url: "{{ route('service.leasing.list') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            jq(".overlay").hide();
            jq('#services_type').empty();
            var options = '<option value="">{{trans('weblng.CHOOSE_SERVICE')}}</option>';
            jq.each(response.data, function(key, value) {
              options += '<option value="'+value.id+'">'+value.name+'</option>';
            });
            jq('#services_type').append(options);
        },
        error: function(response){
            jq(".overlay").hide();
            if (response.status == 400){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.leasing_alert').show();
                jq('#leasing-msg').html(responseMsg.message).promise().done(function(){
                        jq('#services_type').addClass('has-error');
                    });
            }
            if (response.status == 500){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.leasing_alert').show();
                jq('#leasing-msg').html(responseMsg.message).promise().done(function(){
                        jq('#services_type').addClass('has-error');
                    });;
            }
        }
    });
}

$('#services_type').on('change', function(){
    var services_type = jq('#services_type').val();
    if (services_type==4) {
        $('#vehicle_equipt').show();
    } else {
        $('#vehicle_equipt').hide();
    }
});

$('#services_typed').on('change', function(){
    var services_type = jq('#services_typed').val();
    if (services_type==4) {
        $('#vehicle_equiptd').show();
    } else {
        $('#vehicle_equiptd').hide();
    }
});

$('#services_typel').on('change', function(){
    var services_type = jq('#services_typel').val();
    if (services_type==4) {
        $('#vehicle_equiptl').show();
    } else {
        $('#vehicle_equiptl').hide();
    }
});

//SUBMIT HOURLY LEASE.................................
    jq('#HourlyLeaseForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var lease_type = jq("input[type=radio][name=lease_type]:checked").val();
        var dateOfJourny = jq('#dateOfJourny').val();
        var fromTime = jq('#fromTime').val();
        var toTime = jq('#toTime').val();
        var address = jq('#address').val();
        var services_type = jq('#services_type').val();
        var no_passenger = jq('#no_passenger').val();
        var details = jq('#details').val();
        var vehicle_equipment = jq('#vehicle_equipment').val();
        var longitude = jq('#cityLng').val();
        var latitude = jq('#cityLat').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.leasing.hourly') }}",
            method:"POST",
            data:{ lease_type: lease_type, dateOfJourny: dateOfJourny, fromTime: fromTime, toTime: toTime, address: address, services_type: services_type, no_passenger: no_passenger, details: details, vehicle_equipment: vehicle_equipment, longitude: longitude, latitude: latitude, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                jq('.offset-lg-2').removeClass('pt-5');
                jq('.leasingsuc_alert').show();
                jq('#leasingsuc-msg').html(response.message);
                jq('#dateOfJourny').val('');
                jq('#fromTime').val('');
                jq('#toTime').val('');
                jq('#address').val('');
                jq("#cityLat").val('');
                jq("#cityLng").val('');
                jq('#services_type').val('');
                jq('#no_passenger').val('');
                jq('#details').val('');
                jq('#vehicle_equipment').val('');
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('dateOfJourny')) {
                        jq('#date').html(responseMsg.errors.dateOfJourny).promise().done(function(){
                            jq('#dateOfJourny').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('fromTime')) {
                        jq('#ftime').html(responseMsg.errors.fromTime).promise().done(function(){
                            jq('#fromTime').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('toTime')) {
                        jq('#ttime').html(responseMsg.errors.toTime).promise().done(function(){
                            jq('#toTime').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('address')) {
                        jq('#addrs').html(responseMsg.errors.address).promise().done(function(){
                            jq('#address').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('longitude')) {
                        jq('#addrs').html('{{trans('weblng.VALIDATION_MSG.ADDRESS_DROPDOWN')}}').promise().done(function(){
                            jq('#address').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('services_type')) {
                        jq('#service').html(responseMsg.errors.services_type).promise().done(function(){
                            jq('#services_type').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('no_passenger')) {
                        jq('#nopass').html(responseMsg.errors.no_passenger).promise().done(function(){
                            jq('#no_passenger').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('details')) {
                        jq('#detail').html(responseMsg.errors.details).promise().done(function(){
                            jq('#details').addClass('has-error');
                        });
                    }
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.leasing_alert').show();
                    jq('#leasing-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.leasing_alert').show();
                    jq('#leasing-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT HOURLY LEASE.................................

//SUBMIT DAILY LEASE.................................
    jq('#DailyLeaseForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var lease_type = jq("input[type=radio][name=lease_type]:checked").val();
        var dateOfJourny = jq('#dateOfJournyd').val();
        var address = jq('#addressd').val();
        var services_type = jq('#services_typed').val();
        var no_passenger = jq('#no_passengerd').val();
        var details = jq('#detailsd').val();
        var vehicle_equipment = jq('#vehicle_equipmentd').val();
        var longitude = jq("#cityLngd").val();
        var latitude = jq("#cityLatd").val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.leasing.daily') }}",
            method:"POST",
            data:{ lease_type: lease_type, dateOfJourny: dateOfJourny, address: address, services_type: services_type, no_passenger: no_passenger, details: details, vehicle_equipment: vehicle_equipment, longitude: longitude, latitude: latitude, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                jq('.offset-lg-2').removeClass('pt-5');
                jq('.leasingsuc_alert').show();
                jq('#leasingsuc-msg').html(response.message);
                jq('#dateOfJournyd').val('');
                jq('#addressd').val('');
                jq("#cityLatd").val('');
                jq("#cityLngd").val('');
                jq('#services_typed').val('');
                jq('#no_passengerd').val('');
                jq('#detailsd').val('');
                jq('#vehicle_equipmentd').val('');
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('dateOfJourny')) {
                        jq('#dated').html(responseMsg.errors.dateOfJourny).promise().done(function(){
                            jq('#dateOfJournyd').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('address')) {
                        jq('#addrsd').html(responseMsg.errors.address).promise().done(function(){
                            jq('#addressd').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('longitude')) {
                        jq('#addrsd').html('{{trans('weblng.VALIDATION_MSG.ADDRESS_DROPDOWN')}}').promise().done(function(){
                            jq('#addressd').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('services_type')) {
                        jq('#serviced').html(responseMsg.errors.services_type).promise().done(function(){
                            jq('#services_typed').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('no_passenger')) {
                        jq('#nopassd').html(responseMsg.errors.no_passenger).promise().done(function(){
                            jq('#no_passengerd').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('details')) {
                        jq('#detaild').html(responseMsg.errors.details).promise().done(function(){
                            jq('#detailsd').addClass('has-error');
                        });
                    }
        
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.leasing_alert').show();
                    jq('#leasing-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.leasing_alert').show();
                    jq('#leasing-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT DAILY LEASE.................................

//SUBMIT LONG LEASE.................................
    jq('#LongLeaseForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var lease_type = jq("input[type=radio][name=lease_type]:checked").val();
        var fromdate = jq('#fromdate').val();
        var todate = jq('#todate').val();
        var address = jq('#addressl').val();
        var services_type = jq('#services_typel').val();
        var details = jq('#detailsl').val();
        var vehicle_equipment = jq('#vehicle_equipmentl').val();
        var longitude = jq("#cityLngl").val();
        var latitude = jq("#cityLatl").val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.leasing.longtime') }}",
            method:"POST",
            data:{ lease_type: lease_type, fromdate: fromdate, todate: todate, address: address, services_type: services_type, details: details, vehicle_equipment: vehicle_equipment, longitude: longitude, latitude: latitude, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                jq('.offset-lg-2').removeClass('pt-5');
                jq('.leasingsuc_alert').show();
                jq('#leasingsuc-msg').html(response.message);
                jq('#fromdate').val('');
                jq('#todate').val('');
                jq('#addressl').val('');
                jq("#cityLatl").val('');
                jq("#cityLngl").val('');
                jq('#services_typel').val('');
                jq('#detailsl').val('');
                jq('#vehicle_equipmentl').val('');
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('fromdate')) {
                        jq('#ftimel').html(responseMsg.errors.fromdate).promise().done(function(){
                            jq('#fromdate').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('todate')) {
                        jq('#ttimel').html(responseMsg.errors.todate).promise().done(function(){
                            jq('#todate').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('address')) {
                        jq('#addrsl').html(responseMsg.errors.address).promise().done(function(){
                            jq('#addressl').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('longitude')) {
                        jq('#addrsl').html('{{trans('weblng.VALIDATION_MSG.ADDRESS_DROPDOWN')}}').promise().done(function(){
                            jq('#addressl').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('services_type')) {
                        jq('#servicel').html(responseMsg.errors.services_type).promise().done(function(){
                            jq('#services_typel').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('details')) {
                        jq('#detaill').html(responseMsg.errors.details).promise().done(function(){
                            jq('#detailsl').addClass('has-error');
                        });
                    }
                    
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.leasing_alert').show();
                    jq('#leasing-msg').html(responseMsg.message);
                }
                if (response.status == 401){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.leasing_alert').show();
                    jq('#leasing-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.leasing_alert').show();
                    jq('#leasing-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT LONG LEASE.................................

</script>
<!-- footer -->
@include('partials.passenger_footer')