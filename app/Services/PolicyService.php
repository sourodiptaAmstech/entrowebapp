<?php

namespace App\Services;

use App\Services\CurlService;


class PolicyService
{
	//get passenger's privacy policy
	private function getServicePolicy($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/privacy/policy';
	      $method = "POST";
	      $array = ['languageParam'=>$data->languageParam];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	//get driver's privacy policy
	private function getServicePolicyDriver($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/privacy/policy/driver';
	      $method = "POST";
	      $array = ['languageParam'=>$data->languageParam];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getServiceTerm($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/terms/conditions';
	      $method = "POST";
	      $array = ['languageParam'=>$data->languageParam];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getServiceTermDriver($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/terms/conditions/driver';
	      $method = "POST";
	      $array = ['languageParam'=>$data->languageParam];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getHomeContents($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/home/contents';
	      $method = "POST";
	      $array = ['languageParam'=>$data->languageParam];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getHomeValues($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/home/value';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


  	public function accessGetServicePolicy($data)
	{
    	return $this->getServicePolicy($data);
  	}
  	public function accessGetServicePolicyDriver($data)
	{
    	return $this->getServicePolicyDriver($data);
  	}

  	public function accessGetServiceTerm($data)
	{
    	return $this->getServiceTerm($data);
  	}

  	public function accessGetServiceTermDriver($data)
	{
    	return $this->getServiceTermDriver($data);
  	}

  	public function accessGetHomeContents($data)
	{
    	return $this->getHomeContents($data);
  	}

  	public function accessGetHomeValues($data)
	{
    	return $this->getHomeValues($data);
  	}

}