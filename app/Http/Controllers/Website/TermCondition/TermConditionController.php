<?php

namespace App\Http\Controllers\Website\TermCondition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PolicyService;


class TermConditionController extends Controller
{
    public function privacyPolicy()
    {
        return view('pages.policy.privacy-policy');
    }

    public function termsConditions()
    {
        return view('pages.policy.terms-conditions');
    }

    public function GetPrivacyPolicy(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request->languageParam = $request->languageParam;

			$obj = new PolicyService;
			$result = $obj->accessGetServicePolicy($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

    public function GetPrivacyPolicyDriver(Request $request)
    {
        try{
            $request['timeZone'] = $request->timeZone;
            $request->languageParam = $request->languageParam;

            $obj = new PolicyService;
            $result = $obj->accessGetServicePolicyDriver($request);

            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

	public function GetTermsConditions(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request->languageParam = $request->languageParam;

			$obj = new PolicyService;
			$result = $obj->accessGetServiceTerm($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

    public function GetTermsConditionsDriver(Request $request)
    {
        try{
            $request['timeZone'] = $request->timeZone;
            $request->languageParam = $request->languageParam;

            $obj = new PolicyService;
            $result = $obj->accessGetServiceTermDriver($request);

            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }


    public function GetHomeContents(Request $request)
    {
        try{
            $request['timeZone'] = $request->timeZone;
            $request->languageParam = $request->languageParam;

            $obj = new PolicyService;
            $result = $obj->accessGetHomeContents($request);

            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function GetHomeValue(Request $request)
    {
        try{
            $request['timeZone'] = $request->timeZone;

            $obj = new PolicyService;
            $result = $obj->accessGetHomeValues($request);

            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }


	public function passengerPrivacyPolicy()
    {
        return view('pages.policy.pass-privacy-policy');
    }

    public function passengerTermsConditions()
    {
        return view('pages.policy.pass-terms-conditions');
    }

    public function driverPrivacyPolicy()
    {
        return view('pages.policy.driver-privacy-policy');
    }

    public function driverTermsConditions()
    {
        return view('pages.policy.driver-terms-conditions');
    }

    public function fleetownerPrivacyPolicy()
    {
        return view('pages.policy.fleetowner-privacy-policy');
    }

    public function fleetownerTermsConditions()
    {
        return view('pages.policy.fleetowner-terms-conditions');
    }

}
