<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{trans('weblng.TITLE')}}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Font Lato -->

        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/bootstrap-datetimepicker.min.css" crossorigin="anonymous">        
        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/bootstrap-select.min.css" crossorigin="anonymous">        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/main.css" crossorigin="anonymous">
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">

                <!-- section main -->
                    <section id="main">

                    <div class="container">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="navbar-brand ml-0" href="{{ url('home') }}">
                                <img class="logo" src="{{URL::asset('/')}}storage/assets/img/logo.png" alt="">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-5">
                                    <li class="nav-item {{ Request::is('home') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('home') }}">
                                            <strong>{{trans('weblng.HEADER.HOME')}}</strong>
                                        </a>
                                    </li>
                                   {{--  <div class="passb_header">
                                    <li class="nav-item {{ Request::is('ride') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('ride') }}">
                                            <strong>Book a Ride</strong>
                                        </a>
                                    </li>
                                    </div> --}}
                                   {{--  <div class="passl_header">
                                    <li class="nav-item {{ Request::is('lease') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('lease') }}">
                                            <strong>Leasing</strong>
                                        </a>
                                    </li>
                                    </div> --}}
                                   {{--  <div class="driv_header">
                                    <li class="nav-item">
                                        <a class="nav-link px-4" href="javascript:void(0)">
                                            <strong>Find a Ride</strong>
                                        </a>
                                    </li>
                                    </div> --}}
                                    <li class="nav-item {{ Request::is('login') || Request::is('forgotPassword') || Request::is('resetPassword') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('login') }}">
                                            <strong>{{trans('weblng.HEADER.LOGIN')}}</strong>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::is('signup') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('signup') }}">
                                            <strong>{{trans('weblng.HEADER.SIGNUP')}}</strong>
                                        </a>
                                    </li>
                                   
                                    <li class="nav-item">
                                        <a class="nav-link px-4 py-0" href="#">
                                            <strong>
                                                <select name="languagePicker" id="languagePicker">
                                                    {{-- <option value="">{{trans('weblng.HOME.CHANGE_LANG')}}</option> --}}
                                                    <option value="en">{{trans('weblng.HEADER.ENGLISH')}}</option>
                                                    <option value="fr">{{trans('weblng.HEADER.FRENCH')}}</option>
                                                </select>
                                            </strong>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </nav>
                    </div>


                    <div id="bootboxModal" class="modal fade" data-backdrop="static">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body bootboxBody">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary bootboxBtn">{{trans('weblng.HEADER.OK')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>

            <div class="overlay">
                <div class="overlay__inner">
                    <div class="overlay__content"><span class="spinner"></span></div>
                </div>
            </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<style>
    .overlay {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        position: fixed;
        background: #222222c4;
        z-index:999999;
        display: none;
    }

    .overlay__inner {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        position: absolute;
    }

    .overlay__content {
        left: 50%;
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%);
    }

    .spinner {
        width: 75px;
        height: 75px;
        display: inline-block;
        border-width: 2px;
        border-color: rgba(255, 255, 255, 0.05);
        border-top-color: #fff;
        animation: spin 1s infinite linear;
        border-radius: 100%;
        border-style: solid;
    }

    @keyframes spin {
      100% {
        transform: rotate(360deg);
      }
    }
</style>

<script>
    var cookieValue = Cookies.get('languageCookie');
    if (cookieValue==null) {
        document.cookie = "languageCookie=en; expires=0; path=/;";
    }
    if (cookieValue==''||cookieValue==null) {
        $('#languagePicker').val('en');
    } else {
        $('#languagePicker').val(cookieValue);
    }
    $('#languagePicker').change(function(){
        var language = $(this).val()
        document.cookie = "languageCookie="+language+"; expires=0; path=/;";
        location.reload()
    })
</script>