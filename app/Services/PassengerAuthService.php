<?php

namespace App\Services;

use App\Services\CurlService;

class PassengerAuthService
{
	private function login($data)
	{
    try{
      $curl_url = env('serverURL').'passenger/login';
      
      $method = "POST";
      $array = ['device_id'=>$data->device_id,'device_token'=>$data->device_token,'device_type'=>$data->device_type,'login_by'=>$data->login_by,'username'=>$data->username,'isd_code'=>$data->isdCode,'password'=>$data->password];
      
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      //print_r($httpcode);
      curl_close($curl);
      $response=json_decode($response,true);
      //dd($response);
      
      if($httpcode==200){
        $IP = $_SERVER['REMOTE_ADDR'];
        $mac = shell_exec("ip link | awk '{print $2}'");
        preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
        $output = array_combine($matches[1], $matches[2]);
        $mac_address_values=$IP.":".env('APP_KEY');
        foreach($output as $key=>$val){
          if($val!=""){
            if($mac_address_values!=="")
            {  $mac_address_values=":".$mac_address_values.$val;}
            else
            {  $mac_address_values=$mac_address_values.$val;}
          }
        }
        $entroSignature= base64_encode(($mac_address_values));
         
        $cookie_name1 = "entroSignature";
        $cookie_value1 = base64_encode(($entroSignature));
        setcookie($cookie_name1, $cookie_value1, 0, "/");

        $cookie_name = "entroSecurity";
        $cookie_value = base64_encode(encrypt($response["data"]["token_type"]." ".$response["data"]["access_token"]));
        setcookie($cookie_name, $cookie_value, 0, "/");
      }
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

	}

  private function socialLogin($data)
  {
    try{
      $curl_url = env('serverURL').'passenger/login';
      $method = "POST";
      $array = ['social_unique_id'=>$data->social_unique_id,'device_id'=>$data->device_id,'device_token'=>$data->device_token,'device_type'=>$data->device_type,'login_by'=>$data->login_by ];
      // dd($array);
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);
    
      if($httpcode==200){
        $IP = $_SERVER['REMOTE_ADDR'];
        $mac = shell_exec("ip link | awk '{print $2}'");
        preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
        $output = array_combine($matches[1], $matches[2]);
        $mac_address_values=$IP.":".env('APP_KEY');
        foreach($output as $key=>$val){
          if($val!=""){
            if($mac_address_values!=="")
            {  $mac_address_values=":".$mac_address_values.$val;}
            else
            {  $mac_address_values=$mac_address_values.$val;}
          }
        }
        $entroSignature= base64_encode(($mac_address_values));
         
        $cookie_name1 = "entroSignature";
        $cookie_value1 = base64_encode(($entroSignature));
        setcookie($cookie_name1, $cookie_value1, 0, "/");

        $cookie_name = "entroSecurity";
        $cookie_value = base64_encode(encrypt($response["data"]["token_type"]." ".$response["data"]["access_token"]));
        setcookie($cookie_name, $cookie_value, 0, "/");
      }
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }


  private function forgetPassword($data)
  {
    try{   
      $curl_url = env('serverURL').'passenger/forget/password';
      $method = "POST";
      $array = ['mobile_no'=>$data->mobile_no, 'isdCode'=>$data->isdCode];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }

  private function resetPassword($data)
  {
    try{ 
      $curl_url = env('serverURL').'passenger/reset/password';
      $method = "POST";
      $array = ['mobile_no'=>$data->mobile_no, 'isdCode'=>$data->isdCode, 'password'=>$data->password, 'password_confirmation'=>$data->password_confirmation];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }


  private function logout($data)
  {
    try{ 
      $curl_url = env('serverURL').'passenger/logout';
      $method = "GET";
      $json_encode = '';
      $timeZone = $data->timeZone;
      $token = $data->token;
      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);
      
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    
  }


	public function accessLogin($data){
      return $this->login($data);
  }

  public function accessSocialLogin($data){
      return $this->socialLogin($data);
  }

  public function accessForgetPassword($data){
      return $this->forgetPassword($data);
  }

  public function accessResetPassword($data){
      return $this->resetPassword($data);
  }

  public function accessLogout($data){
      return $this->logout($data);
  }

}