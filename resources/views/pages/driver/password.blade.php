<!-- header -->
@include('partials.driver_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.driver_sidebar')
            </div>
            <div class="col-lg-8">
                <form class="" id="changePasswordForm">
                    <div class="px-sm-3 pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success changsuc_alert" style="display: none;">
                            <p class="text-success" id="changsuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.CHANGE_PASSWORD')}}</h2>
                        <div class="form-group">
                            <label for="old_password" class="font-weight-bold">{{trans('weblng.FIELD.OLD_PASSWORD')}}</label>
                            <input type="password" name="old_password" id="old_password" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.OLD_PASSWORD_PLACE')}}" onkeyup="return validate()">
                            <span class="text-danger" id="oldpass"></span>
                        </div>
                        <div class="form-group">
                            <label for="password" class="font-weight-bold">{{trans('weblng.FIELD.NEW_PASSWORD')}}</label>
                            <input type="password" name="password" id="password" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.NEW_PASSWORD_PLACE')}}" onkeyup="return validate()">
                            <span class="text-danger" id="passw"></span>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation" class="font-weight-bold">{{trans('weblng.FIELD.NEW_CONFIRM_PASSWORD')}}</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.CONFIRM_PASSWORD_PLACE')}}" onkeyup="return validate()">
                            <span class="text-danger" id="passwc"></span>
                        </div>

                        <div class="d-flex justify-content-start mb-3 mt-5">
                            <button type="submit" class="btn btn-lrg grad text-white">{{trans('weblng.FIELD.SAVE')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    jq = jQuery.noConflict();

      function validate(){
        var status=null;

        var old_password = document.getElementById('old_password').value;
        if (old_password == '') {
            document.getElementById("oldpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_OLD')}}'
            document.getElementById('old_password').classList.add('has-error')
            status = false
        } else if (old_password.length<6||old_password.length>255) {
            document.getElementById("oldpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_OLD_PLACE')}}'
            document.getElementById('old_password').classList.add('has-error')
            status=false
        } else {
            jq('.px-sm-3').addClass('pt-5');
            jq('.dupdate_alert').hide();
            jq('#dupdate-msg').html('');
            jq('.changsuc_alert').hide();
            jq('#changsuc-msg').html('');
            document.getElementById("oldpass").innerHTML='';
            document.getElementById('old_password').classList.remove('has-error')
            status=true;
        }

        var password = document.getElementById('password').value
        if (password == '') {
            document.getElementById("passw").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD')}}'
            document.getElementById('password').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("passw").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_REQ')}}'
            document.getElementById('password').classList.add('has-error')
            status=false

        } else {
            document.getElementById("passw").innerHTML=''
            document.getElementById('password').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('password_confirmation').value
        if (password_confirm == '') {
            document.getElementById("passwc").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM')}}'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("passwc").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM_REQ')}}'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("passwc").innerHTML=''
            document.getElementById('password_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    jq(document).ready(function(){
        $('#changePasswordForm').on('submit', function(event){
            event.preventDefault();
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            var old_password = $('#old_password').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('driver.change.password') }}",
                method:"POST",
                data:{ old_password: old_password, password: password, password_confirmation: password_confirmation, timeZone: timeZone },
                success: function(response){
                    jq(".overlay").hide();
                    $('.px-sm-3').removeClass('pt-5');
                    $('.changsuc_alert').show();
                    $('#changsuc-msg').html(response.message);
                    $('#old_password').val('');
                    $('#password').val('');
                    $('#password_confirmation').val('');
                },
                error: function(response){
                    jq(".overlay").hide();
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('old_password')) {
                            jq('#oldpass').html(responseMsg.errors.old_password).promise().done(function(){
                                jq('#old_password').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password')) {
                            jq('#passw').html(responseMsg.errors.password).promise().done(function(){
                                jq('#password').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                            jq('#passwc').html(responseMsg.errors.password_confirmation).promise().done(function(){
                                jq('#password_confirmation').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('exception')) {
                            $('.px-sm-3').removeClass('pt-5');
                            $('.dupdate_alert').show();
                            $('#dupdate-msg').html(responseMsg.message);
                        } 
                    }
                    if (response.status == 403){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.px-sm-3').removeClass('pt-5');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.message);
                    }
                    if (response.status == 400){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.px-sm-3').removeClass('pt-5');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.message);
                    }
                    if (response.status == 500){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.px-sm-3').removeClass('pt-5');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.message);
                    }
                }
            });
        });
    });
</script>
<!-- footer -->
@include('partials.driver_footer')