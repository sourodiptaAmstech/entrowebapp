<!-- header -->
@include('partials.passenger_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">

                <form class="" id="promoCode">
                    <div class="px-sm-3 pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <div class="alert alert-success updatesuc_alert" style="display: none;">
                            <p class="text-success" id="updatesuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.ADD_PROMO')}}</h2>
                        <div class="form-group">
                            <input type="text" name="promo_code" id="promo_code" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.PROMOCODE')}}" onkeyup="return validate()">
                            <span class="text-danger" id="promo"></span>
                        </div>
                        <div class="d-flex justify-content-start mb-5 mt-1">
                            <button type="submit" class="btn btn-lg grad text-white">{{trans('weblng.FIELD.SAVE')}}</button>
                        </div>
                    </div>
                </form>
                
                <div class="container my-2" id="promobody">
                    
                </div>

            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    function validate(){
        var status=null;
        var promoCode = document.getElementById('promo_code').value;
        if (promoCode == '') {
            document.getElementById("promo").innerHTML='{{trans('weblng.VALIDATION_MSG.PROMOCODE')}}'
            document.getElementById('promo_code').classList.add('has-error')
            status = false
        } else {
            jq('.px-sm-3').addClass('pt-5');
            jq('.update_alert').hide();
            jq('#update-msg').html('');
            jq('.updatesuc_alert').hide();
            jq('#updatesuc-msg').html('');
            document.getElementById("promo").innerHTML='';
            document.getElementById('promo_code').classList.remove('has-error')
            status=true;
        }
        return status
    }

    jq = jQuery.noConflict();
    jq(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $('#promoCode').on('submit', function(event){
            event.preventDefault();
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var promo_code = $('#promo_code').val();
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('passenger.add.promocode') }}",
                method:"POST",
                data:{ promo_code: promo_code, timeZone: timeZone },
                success: function(response){
                    jq(".overlay").hide();
                    console.log(response)
                    getPromoCode()
                    $('.px-sm-3').removeClass('pt-5');
                    $('.updatesuc_alert').show();
                    $('#updatesuc-msg').html(response.data.message);
                    $('#promo_code').val('');
                    
                },
                error: function(response){
                    jq(".overlay").hide();
                    console.log(response)
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('promo_code')) {
                            jq('#promo').html(responseMsg.errors.promo_code).promise().done(function(){
                                jq('#promo_code').addClass('has-error');
                            });
                        } else {
                            console.log('lol')
                            $('.px-sm-3').removeClass('pt-5');
                            $('.update_alert').show();
                            $('#update-msg').html(responseMsg.data.message);
                        } 
                    }
           
                    if (response.status == 400){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.px-sm-3').removeClass('pt-5');
                        $('.update_alert').show();
                        $('#update-msg').html(responseMsg.data.message);
                    }
                }
            });
        });
        getPromoCode()
    });

function getPromoCode(){
    jq(".overlay").show();
    jq.ajax({
        url: "{{ route('passenger.get.promocode') }}",
        method:"GET",
        data:{ timeZone: timeZone },
        success: function(response){
            jq(".overlay").hide();
            $('#promobody').html('');
            var options = '';
            $.each(response.data.getPromo, function(key, value) {
                options += `
                <div class="row">
                    <div class="col-lg-10 px-5 py-1 shadow-lg bg-white">
                        <div class="row my-4">
                            <div class="col-sm-12 mx-0 px-0">
                                <h2 class="font-weight-bold1 text-muted">{{trans('weblng.FLAT')}}
                                    `+value.currency+``+value.discount+` {{trans('weblng.OFF')}}
                                </h2>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-sm-12 mx-0 px-0">
                                <h4 class="font-weight-bold1 text-muted">
                                {{trans('weblng.COUPON_APPLIED')}}<br> `+value.promo_code+`
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10 shadow-lg px-5 py-1" style="background: #f3f5f7;">
                        <div class="row my-2">
                            <div class="col-sm-12 mx-0 px-0">
                                <h4 class="font-weight-bold1 text-muted float-right">
                                    {{trans('weblng.VALID_UNTILL')}} `+moment(value.expiration).format('MMM Do YYYY')+`
                                </h4>
                            </div>
                        </div>
                    </div>
                </div><br>
                `;
            });
            $('#promobody').append(options);
        },
        error: function(response){
            jq(".overlay").hide();
            console.log(response);
        }
    });
}
</script>
<!-- footer -->
@include('partials.passenger_footer')