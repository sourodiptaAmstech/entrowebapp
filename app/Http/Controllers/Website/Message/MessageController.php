<?php

namespace App\Http\Controllers\Website\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\MessageService;
use App\Services\AdminChatSupportService;


class MessageController extends Controller
{
	public function sendMessage(Request $request)
	{
		try{
			$this->validate($request, [
                'message' => 'required',
            ]);
			
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->request_id = $request->request_id;
			$request->user_scope = $request->user_scope;
			$request->message = $request->message;

        	$messageService = new MessageService;
			$result = $messageService->accessSendMessage($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function getMessage(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->request_id = $request->request_id;

        	$messageService = new MessageService;
			$result = $messageService->accessGetMessage($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function sendMessageDriver(Request $request)
	{
		try{
			$this->validate($request, [
                'message' => 'required',
            ]);
			
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->request_id = $request->request_id;
			$request->user_scope = $request->user_scope;
			$request->message = $request->message;

        	$messageService = new MessageService;
			$result = $messageService->accessSendMessageDriver($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function getMessageDriver(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->request_id = $request->request_id;

        	$messageService = new MessageService;
			$result = $messageService->accessGetMessageDriver($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function sendPassengerMessageAdminIndex(){
		return view('pages.passenger.message.chat-support');
	}

	public function sendPassengerMessageAdmin(Request $request)
	{
		try{
			$this->validate($request, [
                'message' => 'required',
            ]);
			
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->user_scope = $request->user_scope;
			$request->message = $request->message;

        	$messageService = new AdminChatSupportService;
			$result = $messageService->accessSendPassengerMessage($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function getPassengerMessageAdmin(Request $request)
	{
		try{
	
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->user_scope = $request->user_scope;

        	$messageService = new AdminChatSupportService;
			$result = $messageService->accessGetPassengerMessage($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function sendDriverMessageAdminIndex(){
		return view('pages.driver.message.chat-support');
	}

	public function sendDriverMessageAdmin(Request $request)
	{
		try{
			$this->validate($request, [
                'message' => 'required',
            ]);
			
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->user_scope = $request->user_scope;
			$request->message = $request->message;

        	$messageService = new AdminChatSupportService;
			$result = $messageService->accessSendDriverMessage($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function getDriverMessageAdmin(Request $request)
	{
		try{
	
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->user_scope = $request->user_scope;

        	$messageService = new AdminChatSupportService;
			$result = $messageService->accessGetDriverMessage($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
