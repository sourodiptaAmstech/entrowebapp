<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerAuthService;
use App\Services\DriverAuthService;
use App\Services\FleetownerAuthService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ForgetPasswordController extends Controller
{
    public function forgetPasswordIndex()
    {
        return view('pages.auth.forgotPassword');
    }

    public function forgetPassword(Request $request)
    {
        try{
            $this->validate($request, [
                'user' => 'required',
                'mobile_no' => 'required|digits_between:5,10',
                'isdCode' => 'required',
            ]);
            $request->mobile_no = $request->mobile_no;
            $request->isdCode = '+'.$request->isdCode;
            $request['timeZone'] = $request->timeZone;
            if($request->user=='passenger'){
                $passengerAuth = new PassengerAuthService;
                $result = $passengerAuth->accessForgetPassword($request);
            }
            if($request->user == 'driver'){
                $driverAuth = new DriverAuthService;
                $result = $driverAuth->accessForgetPassword($request);
            }
            if($request->user == 'fleetowner'){
                $fleetAuth = new FleetownerAuthService;
                $result = $fleetAuth->accessForgetPassword($request);
            }
            if($result['statusCode']==201){
                return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors'],'user'=>$request->user,'mobile_no'=>$request->mobile_no,'isdCode'=>$request->isdCode],$result['statusCode']);
            }
            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function resetPasswordIndex(Request $request)
    {
        $base64Decode = base64_decode($request->data);
        $decode_data = json_decode($base64Decode);
        $otp = $decode_data->otp;
        $user = $decode_data->user;
        $mobile = $decode_data->mobile;
        $isd = $decode_data->isd;
        return view('pages.auth.resetPassword',compact('otp','user','mobile','isd'));
    }


    public function resetPassword(Request $request)
    {
        try{
            $this->validate($request, [
                'otp' => 'required',
                'password' => 'required|between:6,255|confirmed',
                'password_confirmation'=>'required'
            ]);

            $request->mobile_no = $request->mobile_no;
            $request->isdCode = $request->isdCode;
            $request->password = $request->password;
            $request->password_confirmation = $request->password_confirmation;
            $request['timeZone'] = $request->timeZone;
            if ($request->otp==$request->otpH) {
                if($request->user=='passenger'){
                    $passengerAuth = new PassengerAuthService;
                    $result = $passengerAuth->accessResetPassword($request);
                }
                if($request->user == 'driver'){
                    $driverAuth = new DriverAuthService;
                    $result = $driverAuth->accessResetPassword($request);
                }
                if($request->user == 'fleetowner'){
                    $fleetAuth = new FleetownerAuthService;
                    $result = $fleetAuth->accessResetPassword($request);
                }

            } else {
                return response(['message'=>'OTP does not natched. Please enter valid OTP.',"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],404);
            }
            
            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

}
