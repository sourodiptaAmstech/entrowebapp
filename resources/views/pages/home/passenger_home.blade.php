<!-- header -->
@include('partials.passenger_header')
<!-- Banner -->
<div class="container mb-5">
    <div class="row mt-5">
        <div class="col-lg-7 mt-4">
            <div class="py-3 px-4 text-white header-text">
                <h2>{{trans('weblng.HOME.RIDETOWN')}}</h2>
                <h1 class="font-weight-bold1" style="
    font-size: x-large;
">{{trans('weblng.HOME.ENTRO_OUTSTATION')}}</h1>
            </div>
        </div>
        <div class="col-lg-5">
        <form class="bg-white my-5 py-5 px-md-4 rounded-border shadow-lg home-header" id="request_passenger_form">
            <div class="px-3">
                <div class="position-relative">
                    <div class="form-group">
                        <div class="alert alert-danger home_alert" style="display: none;">
                            <p class="text-danger" id="home-msg"></p>
                        </div>
                        <div class="alert alert-success home_alert_suc" style="display: none;">
                            <p class="text-success" id="home-msg-suc"></p>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="text-center"><strong>{{trans('weblng.HOME.REQUESTA')}} <span class="text-theme-header">{{trans('weblng.HOME.RIDENOW')}}</span></strong></h2>

            <div class="pt-4 px-3">

                <div class="position-relative">
                    <div class="form-group">
                        <input type="text" class="form-control inset-input" name="source" id="source" placeholder="{{trans('weblng.HOME.PICKUP_LOCATION')}}" onkeyup="return validateHome()" onblur="return validateHome()">
                        <span class="text-danger" id="source-msg"></span>
                        <input type="hidden" id="sourceLat" name="sourceLat" />
                        <input type="hidden" id="sourceLng" name="sourceLng" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control inset-input" name="destination" id="destination" placeholder="{{trans('weblng.HOME.DEST_LOCATION')}}" onkeyup="return validateHome()" onblur="return validateHome()">
                        <span class="text-danger" id="destin-msg"></span>
                        <input type="hidden" id="destinationLat" name="destinationLat" />
                        <input type="hidden" id="destinationLng" name="destinationLng" />
                    </div>
                    <div class="position-absolute prepend">
                        <div class="pre-circle"></div>
                        <div class="pre-line"></div>
                        <div class="pre-square"></div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-lrg grad text-white w-100 mb-3" id="requestNow" style="font-size: 14px;">{{trans('weblng.HOME.REQUESTNOW')}}</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-lrg btn-outline-dark w-100" id="scheduleLater" style="font-size: 14px;">{{trans('weblng.HOME.SCHEDULE_LATER')}}</button>
                    </div>
                </div>



            </div>
        </form>
        </div>
    </div>
</div>

</section>
<section class="w-100 our-service" style="
    background-color: white;
">
    <div class="container my-5">
        <h2 class="pt-5 text-center"><strong><span class="text-theme">{{trans('weblng.HOME.Easy_book')}}</span></strong></h2>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SelectYourRide')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="selectRide1">
                    {{trans('weblng.HOME.SelectYourRideText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SelectyourDestination')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="home_destination1">
                    {{trans('weblng.HOME.SelectyourDestinationText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.CardPayment')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay1">
                    {{trans('weblng.HOME.CardPaymentText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/carsmiley.jpg" alt="" style="
    width: 45%;
    height: auto;
">
                </div>
                <h4 class="text-center my-4 text-process carsmiley">{{trans('weblng.HOME.EnjoyYourRide')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay1">
                    {{trans('weblng.HOME.EnjoyYourRideText')}}
                </h6>
            </div>
        </div>
    </div>
</section>
<!-- Our service -->
<section class="w-100 our-service" style="
    background-color: white;
">
    <div class="container mb-5">
        <h2 class="pt-0 text-center"><strong><span class="text-theme">{{trans('weblng.HOME.OUR_SERVICE')}}</span> </strong></h2>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SELECT_RIDE')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="selectRide">
                
                </h6>
            </div>
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.DESTINATION')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="home_destination">
                
                </h6>
            </div>
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.CARDPAYMENT')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay">
                
                </h6>
            </div>
        </div>
    </div>
</section>
<!-- About -->
<section class="w-100 about" id="about-entro">
    <div class="container">
        <div class="row my-5 py-5">
            <div class="col-lg-6 offset-lg-6 text-white">
                <h2>{{trans('weblng.HOME.ABOUTOUR')}}</h2>
                <div class="my-3">
                    <img src="{{URL::asset('/')}}storage/assets/img/h_underline_white.png" alt="">
                </div>
                <h6 id="rideShare" style="color: powderblue;">
                
                </h6>
            </div>
        </div>
    </div>
</section>
{{-- Extra section --}}

<!-- Ride Sharing -->
{{-- <section class="w-100 rd">
    <div class="container-fluid my-5">
        
        <div class="row ride-sharing py-5">
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="happycustomer"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.HAPPYCUSTOMER')}}</h6>
            </div>
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="ridebooked"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.RIDEBOOK')}}</h6>
            </div>
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="drivertotal"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.DRIVER')}}</h6>
            </div>
            <div class="col-lg-3 py-5">
                <h2 class="text-theme text-center font-weight-bold" id="cartotal"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.CAR')}}</h6>
            </div>
        </div>
    </div>
</section> --}}
<!-- Book a Ride -->
<section class="w-100 book-ride" style="
    background-color: white;
">
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="pt-5"><strong><span class="text-theme">{{trans('weblng.HOME.BOOK_RIDE')}} </span></strong></h2>
                <div class="my-3">
                    <h6 class="text-muted" id="theapp">
                    
                    </h6>
                </div>
                <div class="mb-5 mt-5">
                    <button class="btn btn-lrg grad text-white mr-3 mb-3">
                        <div class="d-flex">
                            <img src="{{URL::asset('/')}}storage/assets/img/apple.png" alt="">
                            <div>
                                <div class="d-block">
                                    <h6 style="font-size: 15px">{{trans('weblng.HOME.DOWNLOAD_ON')}}</h6>
                                </div>
                                <div class="d-block">{{trans('weblng.HOME.APPSTORE')}}</div>
                            </div>
                        </div>
                    </button>
                    <button class="btn btn-lrg grad-black text-white mr-3 mb-3">
                        <div class="d-flex">
                            <img src="{{URL::asset('/')}}storage/assets/img/google-play.png" alt="">
                            <div>
                                <div class="d-block">
                                    <h6 style="font-size: 15px">{{trans('weblng.HOME.ANDROID')}}</h6>
                                </div>
                                <div class="d-block">{{trans('weblng.HOME.Google_Play')}}</div>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row justify-content-center my-3">
                    <img class="img-fluid-home" src="{{URL::asset('/')}}storage/assets/img/book_ride.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer top -->
<section class="w-100" id="footer-top-home">
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.BEST_PRICE')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.HOMEPICK')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.EASYBOOKING')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_4.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.CUSTOMERCARE')}}</h4>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
@media only screen and (max-width: 499px) {
  .carsmiley {
    padding-top: 18px;
  }
}
@media only screen and (min-width: 500px) and (max-width: 992px) {
  .carsmiley {
    padding-top: 98px;
  }
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
{{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkne03wdd4Jbx-WN6K1u72eVW9lW2nhmc&libraries=places&callback=initMap"></script> --}}
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCigjyVjdY8tB90olMzBp7bp5bW8izPYdE&libraries=places&callback=initMap"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>

<script>
t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');

    function initializeSource() {
      var input = document.getElementById('source');
      var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
          //  document.getElementById('source').value = place.name;
            document.getElementById('sourceLat').value = place.geometry.location.lat();
            document.getElementById('sourceLng').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initializeSource);

    function initializeDestination() {
      var input = document.getElementById('destination');
      var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
           // document.getElementById('destination').value = place.name;
            document.getElementById('destinationLat').value = place.geometry.location.lat();
            document.getElementById('destinationLng').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initializeDestination);


    function validateHome(){
        var status=null;

        var source = document.getElementById('source').value
        if (source == '') {
            $('#sourceLat').val('');
            $('#sourceLng').val('');
            $(".pre-line").height(82);
            document.getElementById("source-msg").innerHTML='{{trans('weblng.HOME.SOURCE')}}'
            status=false
        } else {
            $(".pre-line").height(60);
            $('.home-header').addClass('py-5');
            $('.home_alert').hide();
            $('#home-msg').html('');
            $('.home_alert_suc').hide();
            $('#home-msg-suc').html('');
            document.getElementById("source-msg").innerHTML=''
            status=true;
        }

        var destination = document.getElementById('destination').value
        if (destination == '') {
            $('#destinationLat').val('');
            $('#destinationLng').val('');
            document.getElementById("destin-msg").innerHTML='{{trans('weblng.HOME.DESTINATION_REQ')}}'
            status=false
        } else {
            $('.home-header').addClass('py-5');
            $('.home_alert').hide();
            $('#home-msg').html();
            $('.home_alert_suc').hide();
            $('#home-msg-suc').html('');
            document.getElementById("destin-msg").innerHTML=''
        }

        return status
    }

    function requestFormNow(){
        $('#FormOnPassengerMap').html(`
            <div class="col-lg-12 px-0 d-flex align-items-center" id="requestFormNow">
                <div class="py-5 px-md-4 home-header w-100">

                    <form class="px-md2-6" id="requestConfirmForm">
                        <div class="pt-0 px-0">

                            <div class="form-group">
                                <div class="alert alert-danger request_alert" style="display: none;">
                                    <p class="text-danger" id="request-msg"></p>
                                </div>
                            </div>

                            <div id="content_form">

                                <div id="demo" class="carousel slide" data-ride="carousel">

                                    <div class="carousel-inner">
                                       
                                    </div>

                                    <a class="carousel-control-prev" href="#demo" data-slide="prev" style="background-color: #e6f2ff;height: 40px;">
                                        <span class="carousel-control-prev-icon"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#demo" data-slide="next" style="background-color: #e6f2ff;height: 40px;">
                                        <span class="carousel-control-next-icon"></span>
                                    </a>
                                </div>

                            </div>
                            <div>
                                <p class="cancelFee"></P>
                            </div>
                            <div class="form-group">
                                <select name="payment_method" id="payment_method" class="form-control inset-input" onchange="paymentType()">
                                    <option value="CASH">{{trans('weblng.INVOICE.CASH')}}</option>
                                    <option value="CARD">{{trans('weblng.HOME.CARD')}}</option>
                                </select>
                            </div>

                            <div class="form-group cardID" style="display:none;">
                                <select id="card_id" style="width:100%;">
                                </select>
                            </div>

                            <div class="form-group isFemale">
                                <div class="d-inline">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="female_friendly" name="female_friendly">
                                        <label for="female_friendly" class="custom-control-label">{{trans('weblng.HOME.ISFEMALE')}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 Confirm">
                                <button type="submit" class="btn btn-lg grad text-white w-100 my-3">{{trans('weblng.HOME.CONFIRM_REQUEST')}}</button>
                            </div>
                            <div class="col-sm-12 Cancel" style="display:none;">
                                <button type="button" class="btn btn-lg grad text-white w-100 my-3" id="cancel1">{{trans('weblng.HOME.CANCEL_REQUEST')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>`);
    }

    function requestFormSchedule(){
        $('#FormOnPassengerMap').html(`
            <div class="col-lg-12 px-0 d-flex align-items-center">
                <div class="py-0 px-md-4 home-header w-100">

                    <form class="px-md2-6" id="requestConfirmFormSchedule">
                        <div class="pt-0 px-0">

                            <div class="form-group">
                                <div class="alert alert-danger request_alert" style="display: none;">
                                    <p class="text-danger" id="request-msg"></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="date" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_DATE')}}</label>
                                <input type="text" class="form-control inset-input" name="date" id="date" placeholder="{{trans('weblng.FIELD.CHOOSE_DATE_PLACE')}}">
                                <span class="text-danger" id="sdate"></span>
                            </div>
                        
                            <div class="form-group">
                                <label for="time" class="font-weight-bold">{{trans('weblng.FIELD.CHOOSE_TIME')}}</label>
                                <input type="text" class="form-control inset-input"
                                name="time" id="time" placeholder="{{trans('weblng.FIELD.CHOOSE_TIME')}}">
                                <span class="text-danger" id="stime"></span>
                            </div>

                            <div class="form-group">
                                <select name="payment_method" id="payment_method" class="form-control inset-input" onchange="paymentType()">
                                    <option value="CASH">{{trans('weblng.INVOICE.CASH')}}</option>
                                    <option value="CARD">{{trans('weblng.HOME.CARD')}}</option>
                                </select>
                            </div>

                            <div class="form-group cardID" style="display:none;">
                                <select id="card_id" style="width:100%;">
                                </select>
                            </div>
                           

                            <div class="form-group isFemaleSc">
                                <div class="d-inline">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="female_friendly" name="female_friendly">
                                        <label for="female_friendly" class="custom-control-label">{{trans('weblng.HOME.ISFEMALE')}}</label>
                                    </div>
                                </div>

                            </div>
                            <div>
                                <p class="cancelFeeSc"></P>
                            </div>

                            <div id="content_form">

                                <div id="demo" class="carousel slide" data-ride="carousel">

                                    <div class="carousel-inner">
                                       
                                    </div>

                                    <a class="carousel-control-prev" href="#demo" data-slide="prev" style="background-color: #e6f2ff;height: 40px;">
                                        <span class="carousel-control-prev-icon"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#demo" data-slide="next" style="background-color: #e6f2ff;height: 40px;">
                                        <span class="carousel-control-next-icon"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-lg grad text-white w-100 my-3">{{trans('weblng.HOME.CONFIRM_SCHEDULE')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>`);
    }


    jq = jQuery.noConflict();
    jq(document).ready(function(){

        jq.ajaxSetup({
            headers: {
                    'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
                }
        });
        var cookieValue = Cookies.get('languageCookie');
        if (cookieValue == '' || cookieValue == 'en') {
            var languageParam = 'en';
        } else {
            var languageParam = 'fr';
        }
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('home.contents.get') }}",
            method: "POST",
            data: {languageParam: languageParam, timeZone: timeZone},
            success: function(response){
                $.each(response.data, function(key,value){
                    if (value.key=='card_payment'||value.key=='card_payment_fr') {
                        $('#cardpay').html(value.value)
                    }
                    if (value.key=='entro_ride_form_app'||value.key=='entro_ride_form_app_fr') {
                        $('#theapp').html(value.value)
                    }
                    if (value.key=='entro_ride_sharing'||value.key=='entro_ride_sharing_fr') {
                        $('#rideShare').html(value.value)
                    }
                    if (value.key=='ride_sharing_analytics'||value.key=='ride_sharing_analytics_fr') {
                        $('#analytics').html(value.value)
                    }
                    if (value.key=='select_your_destination'||value.key=='select_your_destination_fr') {
                        $('#home_destination').html(value.value)
                    }
                    if (value.key=='select_your_ride'||value.key=='select_your_ride_fr') {
                        $('#selectRide').html(value.value)
                    }
                })
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
            }
        });

        jq.ajax({
            url: "{{ route('home.value.get') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('#cartotal').text(response.data.services)
                $('#happycustomer').text(response.data.totalCustomer)
                $('#drivertotal').text(response.data.totalDriver)
                $('#ridebooked').text(response.data.totalRide)
            },
            error: function(response){
            }
        });

      jq('#requestNow').on('click',function(event){
        $('#request_passenger_form').on('submit', function(event){
            event.preventDefault();
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var source = $('#source').val();
            var destination = $('#destination').val();
            var sourceLat = $('#sourceLat').val();
            var sourceLng = $('#sourceLng').val();
            var destinationLat = $('#destinationLat').val();
            var destinationLng = $('#destinationLng').val();
            $(".overlay").show();
            jq.ajax({
                url: "{{ route('passenger.estimatedFare') }}",
                method:"POST",
                data:{ source: source, destination: destination, sourceLat:sourceLat, sourceLng: sourceLng, destinationLat: destinationLat, destinationLng: destinationLng, timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    $("#bookingMap").modal('show');
                    $('#source_map').val(source);
                    $('#destination_map').val(destination);
                    console.log(response);
                    requestFormNow();
                    paymentType();
                    if (response.isFemaleFriendlyFeatureOn=='Y') {
                        $('.isFemale').show();
                    }else{
                        $('.isFemale').hide();
                    }
                    if (response.last_paid_by=='CASH') {
                       jq('#payment_method').val(response.last_paid_by);
                        paymentType();
                    }else{
                       jq('#payment_method').val(response.last_paid_by);
                       paymentType();
                    }
                    if(response.data[0].cancelFee!=0){
                        $('.cancelFee').text('{{trans('weblng.HOME.PENDING_FARE')}}'+response.data[0].cancelFee+' {{trans('weblng.HOME.PENDING_FARE_ADJUSTED')}}');
                        $('.cancelFee').addClass('py-1');
                    }
                    var options='';
                    $.each(response.data, function(key, value) {
                        var statusCode = value.carDetails.statusCode;
                        if (statusCode==404) {
                            var carStatus = '{{trans('weblng.HOME.NOT_AVAILABLE')}}';
                        }else{
                            var carStatus = '{{trans('weblng.HOME.AVAILABLE')}}';
                        }
                        options += `
                                <div class="carousel-item `+key+`carousel">

                                    <div class="col-lg-12">
                                      <div class="col-lg-12"><p class="text-center">`+
                                      value.estimated_fare.estimated_duration_hr+`hrs `+value.estimated_fare.estimated_duration_min+`min `+value.estimated_fare.estimated_duration_sec+`sec</p></div>
                                      <div class="col-lg-12">
                                        <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center">
                                          <img src="`+value.image+`" alt="Profile">
                                        </div>
                                      </div>

                                      <div class="col-lg-12">
                                        <h4 class="text-center font-weight-bold">`+value.name+`</h4>
                                      </div>

                                      <div class="col-lg-12">
                                        <p class="text-center">{{trans('weblng.HOME.ESTIMATED_COST')}}: `+value.estimated_fare.currency+``+value.estimated_fare.estimated_cost+`</p>
                                      </div>

                                      <div class="col-lg-12">
                                        <p class="text-center">`+carStatus+`</p>
                                      </div>
                                      

                                      <div class="col-lg-12">
                                        <div class="text-center py-1">
                                            <div class="d-inline">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" name="service_type" id="service_type`+value.service_type_id+`" value="`+value.service_type_id+`" onchange="return validate()">
                                                    <label for="service_type`+value.service_type_id+`" class="custom-control-label"></label>
                                                </div>
                                            </div>
                                            <input type="hidden" name="request_id" id="request_id" value="`+value.request_id+`">
                                        </div>
                                      </div>
                                    </div>

                                </div>
                        `;
                    });
                    
                    $('.carousel-inner').append(options);
                    $('.0carousel').addClass('active');

                },
                error: function(response){
                    console.log(response.status);
                    $(".overlay").hide();
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('source')) {
                            $(".pre-line").height(82);
                            $('#source-msg').html(responseMsg.errors.source);
                        }
                        if (responseMsg.errors.hasOwnProperty('destination')) {
                            $('#destin-msg').html(responseMsg.errors.destination);
                        }
                        if (responseMsg.errors.hasOwnProperty('sourceLat')) {
                            $(".pre-line").height(82);
                            jq('#source-msg').html('{{trans('weblng.VALIDATION_MSG.ADDRESS_DROPDOWN')}}');
                        }
                        if (responseMsg.errors.hasOwnProperty('destinationLat')) {
                            jq('#destin-msg').html('{{trans('weblng.VALIDATION_MSG.ADDRESS_DROPDOWN')}}');
                        }
                    }

                    if (response.status == 401){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.home-header').removeClass('py-5');
                        $('.home-header').addClass('py-4');
                        $('.home_alert').show();
                        $('#home-msg').html(responseMsg.message);
                        $('#source-msg').html('');
                        $('#destin-msg').html('');
                        $(".pre-line").height(60);
                    }
                    if (response.status == 404){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.home-header').removeClass('py-5');
                        $('.home-header').addClass('py-4');
                        $('.home_alert').show();
                        $('#home-msg').html(responseMsg.message);
                        $('#source-msg').html('');
                        $('#destin-msg').html('');
                        $(".pre-line").height(60);
                    }
                }
            });
        });

      });

      jq('#scheduleLater').on('click',function(event){
        $('#request_passenger_form').on('submit', function(event){
            event.preventDefault();
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var source = $('#source').val();
            var destination = $('#destination').val();
            var sourceLat = $('#sourceLat').val();
            var sourceLng = $('#sourceLng').val();
            var destinationLat = $('#destinationLat').val();
            var destinationLng = $('#destinationLng').val();
            $(".overlay").show();
            jq.ajax({
                url: "{{ route('passenger.estimatedFare') }}",
                method:"POST",
                data:{ source: source, destination: destination, sourceLat:sourceLat, sourceLng: sourceLng, destinationLat: destinationLat, destinationLng: destinationLng, timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    $("#bookingMap").modal('show');
                    $('#source_map').val(source);
                    $('#destination_map').val(destination);
                    requestFormSchedule();
                    paymentType();
                    console.log(response);
                    if (response.isFemaleFriendlyFeatureOn=='Y') {
                        $('.isFemaleSc').show();
                    }else{
                        $('.isFemaleSc').hide();
                    }
                    if(response.data[0].cancelFee!=0){
                        $('.cancelFeeSc').text('{{trans('weblng.HOME.PENDING_FARE')}}'+response.data[0].cancelFee+' {{trans('weblng.HOME.PENDING_FARE_ADJUSTED')}}');
                        $('.cancelFeeSc').addClass('py-1');
                    }
                    if (response.last_paid_by=='CASH') {
                       jq('#payment_method').val(response.last_paid_by);
                        paymentType();
                    }else{
                       jq('#payment_method').val(response.last_paid_by);
                       paymentType();
                    }
                    var options_sc='';
                    $.each(response.data, function(key, value) {
                        var statusCode = value.carDetails.statusCode;
                        if (statusCode==404) {
                            var carStatus = '{{trans('weblng.HOME.NOT_AVAILABLE')}}';
                        }else{
                            var carStatus = '{{trans('weblng.HOME.AVAILABLE')}}';
                        }
                        options_sc += `
                        <div class="carousel-item `+key+`carousel">
                        <div class="col-lg-12">
                          <div class="col-lg-12"><p class="text-center">`+
                          value.estimated_fare.estimated_duration_hr+`hrs `+value.estimated_fare.estimated_duration_min+`min `+value.estimated_fare.estimated_duration_sec+`sec</p></div>
                          <div class="col-lg-12">
                            <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center">
                              <img src="`+value.image+`" alt="Profile">
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <h4 class="text-center font-weight-bold">`+value.name+`</h4>
                          </div>

                          <div class="col-lg-12">
                            <p class="text-center">{{trans('weblng.HOME.ESTIMATED_COST')}}: `+value.estimated_fare.currency+``+value.estimated_fare.estimated_cost+`</p>
                          </div>

                          <div class="col-lg-12">
                            <p class="text-center">`+carStatus+`</p>
                          </div>

                          <div class="col-lg-12">
                            <div class="text-center py-1">
                                <div class="d-inline">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="service_type_sh" id="service_type`+value.service_type_id+`" value="`+value.service_type_id+`" onchange="return validateSh()">
                                        <label for="service_type`+value.service_type_id+`" class="custom-control-label"></label>
                                    </div>
                                </div>
                              <input type="hidden" name="request_id" id="request_id" value="`+value.request_id+`">
                            </div>
                          </div>
                        </div>
                        </div>`;
                    });
                    $('.carousel-inner').append(options_sc);
                    DateTime();
                   $('.0carousel').addClass('active');
                },
                error: function(response){
                    console.log(response.status);
                    $(".overlay").hide();
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('source')) {
                            $(".pre-line").height(82);
                            $('#source-msg').html(responseMsg.errors.source);
                        }
                        if (responseMsg.errors.hasOwnProperty('destination')) {
                            $('#destin-msg').html(responseMsg.errors.destination);
                        }
                        if (responseMsg.errors.hasOwnProperty('sourceLat')) {
                            $(".pre-line").height(82);
                            jq('#source-msg').html('{{trans('weblng.VALIDATION_MSG.ADDRESS_DROPDOWN')}}');
                        }
                        if (responseMsg.errors.hasOwnProperty('destinationLat')) {
                            jq('#destin-msg').html('{{trans('weblng.VALIDATION_MSG.ADDRESS_DROPDOWN')}}');
                        }
                    }

                    if (response.status == 401){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.home-header').removeClass('py-5');
                        $('.home-header').addClass('py-4');
                        $('.home_alert').show();
                        $('#home-msg').html(responseMsg.message);
                        $('#source-msg').html('');
                        $('#destin-msg').html('');
                        $(".pre-line").height(60);
                    }
                    if (response.status == 404){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.home-header').removeClass('py-5');
                        $('.home-header').addClass('py-4');
                        $('.home_alert').show();
                        $('#home-msg').html(responseMsg.message);
                        $('#source-msg').html('');
                        $('#destin-msg').html('');
                        $(".pre-line").height(60);
                    }
                }
            });
        });

      });
});
function DateTime(){
    $('#date').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: new Date()
    });
    
    $('#time').datetimepicker({
        format: 'hh:mm A',
    });
}

function paymentType(){
    var payment_method = jq('#payment_method').val();
    if(payment_method=='CARD'){
        $('.cardID').show();
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.ListCard') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                var options_cd='';
                $.each(response.data.card, function(key, value) {
                    if (value.brand.trim()=='visa') {
                        var card_img = `{{URL::asset('/')}}storage/assets/img/credit-card-visa.png`;
                    } else if (value.brand.trim()=='verve') {
                        var card_img = `{{URL::asset('/')}}storage/assets/img/verve.png`;
                    } else {
                        var card_img = `{{URL::asset('/')}}storage/assets/img/master-card.png`;
                    }
                    if (value.is_default==1) {
                        options_cd += `<option data-img_src="`+card_img+`" value="`+value.user_cards_id+`" selected>**** **** **** `+value.last_four+`</option>`;
                    } else {
                        options_cd += `<option data-img_src="`+card_img+`" value="`+value.user_cards_id+`">**** **** **** `+value.last_four+`</option>`;
                    }
                });
                $('#card_id').append(options_cd);
                function custom_template(obj){
                    var data = $(obj.element).data();
                    var text = $(obj.element).text();
                    if(data && data['img_src']){
                        img_src = data['img_src'];
                        template = $("<div class=\"row\"><img src=\"" + img_src + "\" style=\"width:20%;height:auto;padding: 5px;\"/><p style=\"font-weight: 700;font-size:14pt;text-align:center;padding: 20px;\">" + text + "</p></div>");
                        return template;
                    }
                }
                var options_sl = {
                    'templateSelection': custom_template,
                    'templateResult': custom_template,
                }
                $('#card_id').select2(options_sl);
                $('.select2-container--default .select2-selection--single').css({'height': 'auto'});
            }
        });
    } else {
        $('.cardID').hide();
        $('#card_id').val('');
    }

}

</script>

<!-- footer -->
<section class="w-100">
@include('partials.passenger_footer')
