<?php

namespace App\Services;


class CurlService 
{
	private function Curl($curl_url,$method,$json_encode,$timeZone)
	{
		$curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POSTFIELDS =>$json_encode,
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "ApplicationType: web",
            "Version: 1.0",
            "timeZone: ".$timeZone,
            "Content-Type: application/json"
          ),
        ));

        return $curl;
	}

  private function authCurl($curl_url,$method,$json_encode,$timeZone,$token)
  {
    $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POSTFIELDS => $json_encode,
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "ApplicationType: web",
            "Version: 1.0",
            "timeZone: ".$timeZone,
            "Content-Type: application/json",
            "Authorization: ".$token,
          ),
        ));
        return $curl;
  }

  private function authFileCurl($curl_url,$postfields,$data)
  {
    $headers =array(
        "Content-Type: multipart/form-data",
        "Accept: application/json",
        "ApplicationType: web",
        "Version: 1.0",
        "timeZone: $data->timeZone",
        "Authorization: $data->token"
    );
    $curl = curl_init();
    $options = array(
      CURLOPT_URL => $curl_url,
      CURLOPT_HEADER => true,
      CURLOPT_POST => 1,
      CURLOPT_HTTPHEADER => $headers,
      CURLOPT_POSTFIELDS => $postfields,
      CURLOPT_INFILESIZE => $data->size,
      CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($curl, $options);

    return $curl;

  }

	public function accessCurl($curl_url,$method,$json_encode,$timeZone){
      return $this->Curl($curl_url,$method,$json_encode,$timeZone);
  }

  public function accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token){
      return $this->authCurl($curl_url,$method,$json_encode,$timeZone,$token);
  }

  public function accessAuthFileCurl($curl_url,$postfields,$data){
      return $this->authFileCurl($curl_url,$postfields,$data);
  }

}
