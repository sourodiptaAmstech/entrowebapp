@include('partials.header')
<style>
    .has-error{
        border:1px solid red;
    }
</style>
<div class="container mb-5">
    <form class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="forgetPasswordForm">
        <h1 class="text-center"><strong>{{trans('weblng.RESET.FORGOT_YOUR')}} <span class="text-theme">{{trans('weblng.FIELD.PASSWORD')}}?</span></strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <div class="alert alert-danger forget_alert" style="display: none;">
                <p class="text-danger" id="forget-msg"></p>
            </div>

            <div class="mb-4">
            <div class="row justify-content-center">
                <div class="d-inline">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio1" name="user" value="passenger" onchange="return validate()">
                        <label class="custom-control-label font-weight-bold" for="radio1">{{trans('weblng.LOGIN.CUSTOMER')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline fleet-margin">
                        <input type="radio" class="custom-control-input" id="radio2" name="user" value="driver" onchange="return validate()">
                        <label class="custom-control-label font-weight-bold" for="radio2">{{trans('weblng.LOGIN.DRIVER')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline fleet-margin">
                        <input type="radio" class="custom-control-input" id="radio3" name="user" value="fleetowner" onchange="return validate()">
                        <label class="custom-control-label font-weight-bold" for="radio3">{{trans('weblng.LOGIN.FLEETOWNER')}}</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <span id="utype" class="text-danger"></span>
            </div>
            </div>

            <div class="form-group">
                <input type="hidden" name="code" id="code">
                &nbsp;<span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span>
                <input type="text" name="mobile_no" id="mobile_no" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="mobile"></span>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.BOOKING.SUBMIT')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="freload" style="display: none;"></i></div></button>
            </div>
        </div>
    </form>

</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    
    jq = jQuery.noConflict();
    use :
        var s = jq("#mobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

    insteadof :
        var d = jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#code").val(d);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#code").val((jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });

    function validate(){
        var status=null;

        var option=document.getElementsByName('user');
        if (!(option[0].checked || option[1].checked || option[2].checked)) {
            document.getElementById("utype").innerHTML='{{trans('weblng.VALIDATION_MSG.USER')}}'
            status = false;
        } else {
            $('.offset-lg-2').addClass('pt-5');
            $('.forget_alert').hide();
            $('#forget-msg').html('');
            document.getElementById("utype").innerHTML=''
            status=true
        }

        var mobileno = document.getElementById('mobile_no').value
        if (mobileno == '') {
            document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE')}}'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else if (!mobileno.match(/^([0-9\s\-\+\(\)]*)$/) || mobileno.length<5 || mobileno.length>10) {
            document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE_REQ')}}'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("mobile").innerHTML=''
            document.getElementById('mobile_no').classList.remove('has-error')
        }

        return status
    }

jq(document).ready(function(){
 
    jq('#forgetPasswordForm').on('submit', function(event){
        event.preventDefault();
        
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = jq("input[type=radio][name=user]:checked").val();
        var mobile_no = jq('#mobile_no').val();
        var isdCode = jq('#code').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('forget.password') }}",
            method:"POST",
            data:{ mobile_no: mobile_no, isdCode: isdCode, user: user, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                var value = { otp:response.data.otp, user:response.user, mobile: response.mobile_no, isd:response.isdCode };
                var JSONvalue = JSON.stringify(value);
            
                $('#bootboxModal').modal('show');
                $('.bootboxBody').text('{{trans('weblng.RESET.OTP_SEND')}}');
                $('button.bootboxBtn').on('click', function(){
                    $("#bootboxModal").modal('hide');
                    window.location.href = 'resetPassword?'+'data='+btoa(JSONvalue);
                });
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        jq('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            jq('#mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('isdCode')) {
                        jq('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            jq('#mobile_no').addClass('has-error');
                        });
                    }

                    jq('#utype').html(responseMsg.errors.user);
                    
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        $('.offset-lg-2').removeClass('pt-5');
                        $('.forget_alert').show();
                        jq('#forget-msg').html(responseMsg.message);
                    } 
                }
                if (response.status == 403){
                    var responseMsg = jq.parseJSON(response.responseText);
                    $('.offset-lg-2').removeClass('pt-5');
                    $('.forget_alert').show();
                    jq('#forget-msg').html(responseMsg.message);
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    $('.offset-lg-2').removeClass('pt-5');
                    $('.forget_alert').show();
                    jq('#forget-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    $('.offset-lg-2').removeClass('pt-5');
                    $('.forget_alert').show();
                    jq('#forget-msg').html(responseMsg.message);
                }
            }
        });
    });
});
</script>
<!-- footer -->
@include('partials.footer')