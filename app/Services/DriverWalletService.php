<?php

namespace App\Services;

use App\Services\CurlService;


class DriverWalletService
{
    private function paymentStatistics($data)
	{
	    try{
	      $curl_url = env('serverURL').'driver/get/payment/statistics';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
    }
    
    public function accessPaymentStatistics($data)
	{
    	return $this->paymentStatistics($data);
  	}

}