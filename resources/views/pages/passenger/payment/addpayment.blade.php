<!-- header -->
@include('partials.passenger_header')
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">

              <div class="px-sm-3 pt-5">
                <h2 class="font-weight-bold mb-4">{{trans('weblng.MYADDEDCARD')}}</h2>

                <ul class="list-group listGroup">
                </ul>

                <div class="d-flex justify-content-start mb-3 mt-5">
                    <button type="button" class="btn btn-lg grad text-white" id="addDebitCard">{{trans('weblng.MYNEWCARD')}}</button>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <form id="paymentForm">
                          <div class="modal-body">
                            <h5>{{trans('weblng.CHARGE')}} <span>&#8358;</span>{{trans('weblng.CHARGE_NEXT')}}</h5>
                            <input type="hidden" name="email_paystack" id="email_paystack"/>
                            <input type="hidden" name="first_name" id="first_name"/>
                            <input type="hidden" name="last_name" id="last_name"/>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('weblng.CLOSE')}}</button>
                            <button type="submit" class="btn btn-primary" onclick="payWithPaystack(event)">{{trans('weblng.PAYMENT')}}</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- end Modal -->

              </div>

            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://js.paystack.co/v1/inline.js"></script>
<script>
    const paymentForm = document.getElementById('paymentForm');
    paymentForm.addEventListener("submit", payWithPaystack, false);
    function payWithPaystack(event) {
        event.preventDefault();

        let handler = PaystackPop.setup({
            key: 'pk_test_cd507a5002f0a3d1f83ec7d355712e1efd95c554',
            email: document.getElementById("email_paystack").value,
            currency: 'NGN',
            firstname: document.getElementById("first_name").value,
            lastname: document.getElementById("last_name").value,
            amount: 50 * 100,
            channels: ['card'],
            onClose: function(){
                $('#paymentModal').modal('hide');
            },
            callback: function(response){
                $('#paymentModal').modal('hide');
                jq.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                t = new Date().toUTCString().split(' ');
                var timeZone = t[t.length-1] + moment().format('Z');
                var reference = response.reference;
                $(".overlay").show();
                jq.ajax({
                    url: "{{ route('passenger.addCard') }}",
                    method:"POST",
                    data:{ reference: reference, timeZone: timeZone },
                    success: function(response){
                        location.reload();
                        $(".overlay").hide();
                        console.log(response)
                    },
                    error: function(response){
                        $(".overlay").hide();
                    }
                });

            }
        });
        handler.openIframe();
    }


    $(document).ready(function(){
        $('#addDebitCard').click(function(){
            $('#paymentModal').modal('show');
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.ListCard') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                $('#email_paystack').val(response.data.email);
                var options = '';
                $.each(response.data.card, function(key, value) {
                    if (value.brand.trim()=='visa') {
                        var card_img = `<img src="{{URL::asset('/')}}storage/assets/img/credit-card-visa.png" alt="" class="card-logo">`;
                    } else if (value.brand.trim()=='verve') {
                        var card_img = `<img src="{{URL::asset('/')}}storage/assets/img/verve.png" alt="" class="card-logo">`;
                    } else if (value.brand.trim()=='master card') {
                        var card_img = `<img src="{{URL::asset('/')}}storage/assets/img/master-card.png" alt="" class="card-logo">`;
                    } else {
                        var card_img = `<img src="{{URL::asset('/')}}storage/assets/img/card.png" alt="" class="card-logo">`;
                    }
                    if (value.is_default==0) {
                        options += `
                            <li class="list-group-item w-100 shadow-lg p-3 mb-2 bg-white rounded border-default">
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 col-1">
                                        `+card_img+`
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-8">
                                        <h5 class="font-weight-bold">**** **** **** `+value.last_four+`</h5>
                                        <section class="mt-2">
                                            <div class="d-inline">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" id="checkID`+value.user_cards_id+`" name="radio" value="`+value.user_cards_id+`">
                                                    <label class="custom-control-label" for="checkID`+value.user_cards_id+`">{{trans('weblng.SETDEFAULTCARD')}}</label>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-1 card-cross">
                                        <div class="h-100 d-flex align-items-center delete-item">
                                            <button type="button" class="close deleteClass" aria-label="Close" id="closeID`+value.user_cards_id+`" key="`+value.user_cards_id+`">
                                                <img src="{{URL::asset('/')}}storage/assets/img/signs.png" alt="" class="m-close">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            `;

                    }else{
                        options += `
                            <li class="list-group-item w-100 shadow-lg p-3 mb-2 bg-white rounded border-default">
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 col-1">
                                       `+card_img+`
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-8">
                                        <h5 class="font-weight-bold">**** **** **** `+value.last_four+`</h5>
                                        <section class="mt-2">
                                            <div class="d-inline">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" id="checkID`+value.user_cards_id+`" name="radio" value="`+value.user_cards_id+`" checked>
                                                    <label class="custom-control-label" for="checkID`+value.user_cards_id+`">{{trans('weblng.SETDEFAULTCARD')}}</label>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-1 card-cross">
                                        <div class="h-100 d-flex align-items-center delete-item">
                                            <button type="button" class="close deleteClass" aria-label="Close" id="closeID`+value.user_cards_id+`" key="`+value.user_cards_id+`">
                                                <img src="{{URL::asset('/')}}storage/assets/img/signs.png" alt="" class="m-close">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            `;
                    }
                });
                $('.listGroup').append(options);

                $("input:radio[name='radio']").click(function(){
                    var user_cards_id = $(this).val();
                    jq.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    t = new Date().toUTCString().split(' ');
                    var timeZone = t[t.length-1] + moment().format('Z');
                    $(".overlay").show();
                    jq.ajax({
                        url: "{{ route('passenger.default.card') }}",
                        method:"PUT",
                        data:{ user_cards_id: user_cards_id, timeZone: timeZone },
                        success: function(response){
                            location.reload();
                            $(".overlay").hide();
                            console.log(response)
                        },
                        error: function(response){
                            $(".overlay").hide();
                        }
                    });
                });

                $('.deleteClass').on('click',function(){
                    var user_cards_id = $('#'+this.id).attr('key');
                    t = new Date().toUTCString().split(' ');
                    var timeZone = t[t.length-1] + moment().format('Z');
                    bootbox.confirm('{{trans('weblng.SURE_DELETE')}}', function (res) {
                        if (res){
                            $(".overlay").show();
                            jq.ajax({
                                url: "{{ route('passenger.delete.card') }}",
                                method:"DELETE",
                                data:{ user_cards_id: user_cards_id, timeZone: timeZone },
                                success: function(response){
                                    location.reload();
                                    $(".overlay").hide();
                                },
                                error: function(response){
                                    $(".overlay").hide();
                                }
                            });
                        }
                    });
                })
            },
            error: function(response){
                console.log(response);
                $(".overlay").hide();
            }
        });
        
    });
</script>

<!-- footer -->
@include('partials.passenger_footer')