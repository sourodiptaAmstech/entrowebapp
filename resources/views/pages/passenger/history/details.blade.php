<!-- header -->
@include('partials.passenger_header')

<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">
                <div class="px-sm-3 pt-5">
                    <div class="alert alert-danger update_alert" style="display: none;">
                        <p class="text-danger" id="update-msg"></p>
                    </div>
                    <h2 class="font-weight-bold mb-0">{{trans('weblng.TRIP_DETAILS')}}</h2>
                    <div class="container my-2" id="tripDetails">
                        <input type="hidden" name="request_id" id="request_id" value="{{$request_id}}">
                        <div class="row rounded-border1 shadow-lg bg-white py-3">
                            <div class="col-lg-12 py-2">
                                <div class="row border-bottom">
                                    <div class="col-sm-2 mx-0 px-0">
                                        <div class="profile-image position-relative my-2 d-flex justify-content-center align-items-center">
                                            <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Profile" id="promap_image">
                                        </div>
                                    </div>
                                    <div class="row col-sm-10 mx-0 px-0">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h5 class="py-1 text-muted" id="m_name"></h5>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h5 class="py-1 text-muted" id="m_date"></h5>
                                        </div>
                                        <div class="col-sm-12 mx-0 px-0">
                                            <h5 class="py-1 text-muted">{{trans('weblng.BOOKINGID')}}: <span id="m_bookingID"></span></h5>
                                        </div>
                                        <div class="col-sm-12 mx-0 px-0">
                                            <div id="rateYo" style="float: left;"></div> 
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-12 pt-2 px-5">
                                <div class="row mb-2" id="CancelD">
                                    
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                            {{trans('weblng.BOOKING.TOTAL_DIST')}}: 
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted"><span id="m_distance"></span> {{trans('weblng.MILES')}}</h6>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                           {{trans('weblng.PROMO_APPLIED')}}:
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_promo"></h6>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                             {{trans('weblng.RPOMO_DISCOUNT')}}:
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_discount"></h6>
                                    </div>
                                </div>
                
                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                            {{trans('weblng.TOTAL_FAIR')}}:
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_fair"></h6>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                           {{trans('weblng.PAYMENTBY')}}:
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_payment"></h6>
                                    </div>
                                </div>
                                <div class="row mb-2 font-weight-bold text-muted" id="CancelFee">
                                    
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                            {{trans('weblng.COMMENT_GIVEN')}}:
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_comment"></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    $(document).ready(function(){
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        var request_id = $('#request_id').val();
        jq.ajax({
            url: "{{ route('passenger.trip.details') }}",
            method:"POST",
            data:{ request_id: request_id, timeZone: timeZone },
            success: function(response){
                console.log(response);
                var picture = response.data.ProfileData.picture;
                if(picture!=''&&picture!=null){
                    $("#promap_image").attr({ "src": picture });
                    $("#promap_image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                }
                var full_name = response.data.ProfileData.first_name+' '+response.data.ProfileData.last_name
                $("#m_name").text(full_name);
                $("#m_date").text(moment(response.data.requestDetails.started_from_source).format('MMM Do YYYY, h:mm A'));
                $("#m_bookingID").text(response.data.requestDetails.request_no);
                $("#m_distance").text(response.data.transaction.distance_miles);
                var promo_code = response.data.transaction.promo_code;
                if (promo_code!=null||promo_code!='') {
                    $("#m_promo").text('--');
                }else{
                    $("#m_promo").text(promo_code);
                }
                if (response.data.transaction.cancelFee!=0) {
                    $("#CancelFee").text('**{{trans('weblng.HOME.PENDING_FARE_P')}} '+response.data.transaction.currency+''+response.data.transaction.cancelFee+' {{trans('weblng.HOME.PENDING_FARE_ADJUSTED_P')}}');
                }
                $("#m_discount").text(response.data.transaction.currency+' '+response.data.transaction.promo_code_value);
                $("#m_fair").text(response.data.transaction.currency+' '+response.data.transaction.cost);
                $("#m_payment").text(response.data.requestDetails.payment_method);
                $("#m_comment").text(response.data.requestDetails.comment_by_passenger);
                var requestStatus = response.data.requestDetails.request_status;
                if (requestStatus=='CANCELBYDRIVER') {
                    $('#CancelD').html(`<div class="col-sm-12 mx-0 px-0">
                                <h6 class="font-weight-bold text-center text-danger">
                                    {{trans('weblng.CANCEL_BYDRIVER')}}
                                </h6>
                            </div>`);
                }
                if (requestStatus=='CANCELBYPASSENGER') {
                    $('#CancelD').html(`<div class="col-sm-12 mx-0 px-0">
                                <h6 class="font-weight-bold text-center text-danger">
                                    {{trans('weblng.CANCEL_BYME')}}
                                </h6>
                            </div>`);
                }
                if (requestStatus=='CANCELBYSYSTEM') {
                    $('#CancelD').html(`<div class="col-sm-12 mx-0 px-0">
                                <h6 class="font-weight-bold text-center text-danger">
                                    {{trans('weblng.CANCEL_BYSYS')}}
                                </h6>
                            </div>`);
                }
                $(function() {
                    $("#rateYo").rateYo({ 
                        rating: response.data.requestDetails.rating_by_passenger, 
                        spacing: "5px", 
                        numStars: 5, 
                        minValue: 0, 
                        maxValue: 5, 
                        normalFill: '#a6a6a6', 
                        ratedFill: 'orange',
                        starWidth: "25px",
                    });
                });

                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.px-sm-3').removeClass('pt-5');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                } 
            }
        });

    });
    
</script>

<!-- footer -->
@include('partials.passenger_footer')