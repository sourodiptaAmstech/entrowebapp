<!-- header -->
@include('partials.driver_header')

<!-- Banner -->
<div class="container my-5">
    <div class="row rounded-border shadow-lg bg-white">

        <div class="px-0 col-lg-12 DriverMap">
            {{-- <div id="map"></div> --}}

            <div id="map-canvas">
                <div class="row justify-content-center my-3 process-icon" style="position: absolute;top: 35%;transform: translate(0, -50%);width: 100%;margin:auto;padding:0">
                    <img src="https://entrong.com/storage/assets/img/loading-map.gif" alt="">
                </div>
            </div>

            <div class="onoffswitch" id="stripe_check">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox stripe_check" id="myonoffswitch" tabindex="0" onchange="cardselect()" >
                <label class="onoffswitch-label" for="myonoffswitch">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>

        </div>
        <div id="FormOnDriverMap" class="edit col-lg-4">
            
        </div>
    </div>
</div>
</section>
<section class="w-100 our-service" style="
    background-color: white;
">
    <div class="container my-5">
        <h2 class="pt-5 text-center"><strong><span class="text-theme">{{trans('weblng.HOME.Easy_book')}}</span></strong></h2>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SelectYourRide')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="selectRide1">
                    {{trans('weblng.HOME.SelectYourRideText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SelectyourDestination')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="home_destination1">
                    {{trans('weblng.HOME.SelectyourDestinationText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.CardPayment')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay1">
                    {{trans('weblng.HOME.CardPaymentText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/carsmiley.jpg" alt="" style="
    width: 45%;
    height: auto;
">
                </div>
                <h4 class="text-center my-4 text-process carsmiley">{{trans('weblng.HOME.EnjoyYourRide')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay1">
                    {{trans('weblng.HOME.EnjoyYourRideText')}}
                </h6>
            </div>
        </div>
    </div>
</section>
<!-- Our service -->
<section class="w-100 our-service" style="
    background-color: white;
">
    <div class="container mb-5">
        <h2 class="pt-0 text-center"><strong><span class="text-theme">{{trans('weblng.HOME.OUR_SERVICE')}} </span></strong></h2>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SELECT_RIDE')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="selectRide">
                
                </h6>
            </div>
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.DESTINATION')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="home_destination">
                
                </h6>
            </div>
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.CARDPAYMENT')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay">
                
                </h6>
            </div>
        </div>
    </div>
</section>
<!-- About -->
<section class="w-100 about" id="about-entro">
    <div class="container">
        <div class="row my-5 py-5">
            <div class="col-lg-6 offset-lg-6 text-white">
                <h2>{{trans('weblng.HOME.ABOUTOUR')}}</h2>
                <div class="my-3">
                    <img src="{{URL::asset('/')}}storage/assets/img/h_underline_white.png" alt="">
                </div>
                <h6 id="rideShare" style="color: powderblue;">
                
                </h6>
            </div>
        </div>
    </div>
</section>
{{-- Extra section --}}

<!-- Ride Sharing -->
{{-- <section class="w-100 rd">
    <div class="container-fluid my-5">
       
        <div class="row ride-sharing py-5">
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="happycustomer"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.HAPPYCUSTOMER')}}</h6>
            </div>
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="ridebooked"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.RIDEBOOK')}}</h6>
            </div>
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="drivertotal"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.DRIVER')}}</h6>
            </div>
            <div class="col-lg-3 py-5">
                <h2 class="text-theme text-center font-weight-bold" id="cartotal"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.CAR')}}</h6>
            </div>
        </div>
    </div>
</section> --}}
<!-- Book a Ride -->
<section class="w-100 book-ride" style="
    background-color: white;
">
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="pt-5"><strong><span class="text-theme">{{trans('weblng.HOME.BOOK_RIDE')}} </span></strong></h2>
                <div class="my-3">
                    <h6 class="text-muted" id="theapp">
                    
                    </h6>
                </div>
                <div class="mb-5 mt-5">
                    <button class="btn btn-lrg grad text-white mr-3 mb-3">
                        <div class="d-flex">
                            <img src="{{URL::asset('/')}}storage/assets/img/apple.png" alt="">
                            <div>
                                <div class="d-block">
                                    <h6 style="font-size: 15px">{{trans('weblng.HOME.DOWNLOAD_ON')}}</h6>
                                </div>
                                <div class="d-block">{{trans('weblng.HOME.APPSTORE')}}</div>
                            </div>
                        </div>
                    </button>
                    <button class="btn btn-lrg grad-black text-white mr-3 mb-3">
                        <div class="d-flex">
                            <img src="{{URL::asset('/')}}storage/assets/img/google-play.png" alt="">
                            <div>
                                <div class="d-block">
                                    <h6 style="font-size: 15px">{{trans('weblng.HOME.ANDROID')}}</h6>
                                </div>
                                <div class="d-block">{{trans('weblng.HOME.Google_Play')}}</div>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row justify-content-center my-3">
                    <img class="img-fluid-home" src="{{URL::asset('/')}}storage/assets/img/book_ride.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer top -->
<section class="w-100" id="footer-top-home">
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.BEST_PRICE')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.HOMEPICK')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.EASYBOOKING')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_4.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.CUSTOMERCARE')}}</h4>
            </div>
        </div>
    </div>

    <!-- reason modal -->
    <div class="modal fade" id="drejectReasonModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header w-100">
                <h5 class="modal-title" id="exampleModalLabel">{{trans('weblng.REJECTREASON')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <form class="w-100" id="drejectFormSubmit">
              <div class="modal-body">
                  <div class="form-group">
                    <label for="dreason" class="col-form-label">{{trans('weblng.REJECTREASON')}}</label>
                    <select class="form-control" name="dreason" id="dreason">
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="dreason_optional" class="col-form-label">{{trans('weblng.REASONOPTIONAL')}}</label>
                    <!-- <input type="text" name="dreason_optional" id="dreason_optional" class="form-control" placeholder="Enter Reason Optional"> -->
                    <textarea id="dreason_optional" name="dreason_optional" class="form-control" placeholder="{{trans('weblng.REASONOPTIONALPL')}}">
                    </textarea>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">{{trans('weblng.BOOKING.SUBMIT')}}</button>
              </div>
            </form>
            </div>
        </div>
    </div>
<!-- reason modal end -->
    
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<style>
@media only screen and (max-width: 499px) {
  .carsmiley {
    padding-top: 18px;
  }
}
@media only screen and (min-width: 500px) and (max-width: 992px) {
  .carsmiley {
    padding-top: 98px;
  }
}
#map-canvas {
    height: 535px;
    width: 100%;
    margin: 0px;
    padding: 0px
}
#stripe_check{
    position: absolute;
    top: 6%;
    left: 50%;
    transform: translate(-6%,-50%);
    z-index: 111;
}

.onoffswitch {
    position: relative; width: 90px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
    position: absolute;
    opacity: 0;
    pointer-events: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
    border: 2px solid #999999; border-radius: 20px;
}
.onoffswitch-inner {
    display: block; width: 200%; margin-left: -100%;
    transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before, .onoffswitch-inner:after {
    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
    box-sizing: border-box;
}
.onoffswitch-inner:before {
    content: "ON";
    padding-left: 10px;
    background-color: #0da612; color: #FFFFFF;
}
.onoffswitch-inner:after {
    content: "OFF";
    padding-right: 10px;
    background-color: dimgrey; color: linen;
    text-align: right;
}
.onoffswitch-switch {
    display: block;
    width: 18px;
    background: #FFFFFF;
    position: absolute;
    top: 0;
    bottom: 0;
    border: 2px solid #999999; border-radius: 20px;
    transition: all 0.3s ease-in 0s;

    margin-bottom: 13px;
    margin-top: 3px;
    margin-right: 6px;
    margin-left: 6px;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
    right: 0px;
}
</style>
<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    jq.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
        }
    });
    var cookieValue = Cookies.get('languageCookie');
    if (cookieValue == '' || cookieValue == 'en') {
        var languageParam = 'en';
    } else {
        var languageParam = 'fr';
    }
    jq(".overlay").show();
    jq.ajax({
        url: "{{ route('home.contents.get') }}",
        method: "POST",
        data: {languageParam: languageParam, timeZone: timeZone},
        success: function(response){
            $.each(response.data, function(key,value){
                if (value.key=='card_payment'||value.key=='card_payment_fr') {
                    $('#cardpay').html(value.value)
                }
                if (value.key=='entro_ride_form_app'||value.key=='entro_ride_form_app_fr') {
                    $('#theapp').html(value.value)
                }
                if (value.key=='entro_ride_sharing'||value.key=='entro_ride_sharing_fr') {
                    $('#rideShare').html(value.value)
                }
                if (value.key=='ride_sharing_analytics'||value.key=='ride_sharing_analytics_fr') {
                    $('#analytics').html(value.value)
                }
                if (value.key=='select_your_destination'||value.key=='select_your_destination_fr') {
                    $('#home_destination').html(value.value)
                }
                if (value.key=='select_your_ride'||value.key=='select_your_ride_fr') {
                    $('#selectRide').html(value.value)
                }
            })
            $(".overlay").hide();
        },
        error: function(response){
            $(".overlay").hide();
        }
    });

    jq.ajax({
        url: "{{ route('home.value.get') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            $('#cartotal').text(response.data.services)
            $('#happycustomer').text(response.data.totalCustomer)
            $('#drivertotal').text(response.data.totalDriver)
            $('#ridebooked').text(response.data.totalRide)
        },
        error: function(response){
        }
    });
    
    function cardselect(){
        if($('.stripe_check').is(":checked")) {
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var status = 'online';
            jq.ajax({
                url: "{{ route('driver.onOff') }}",
                method:"PUT",
                data:{ status: status, timeZone: timeZone },
                success: function(response){
                },
                error: function(response){
                }
            });
        } else {
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var status = 'offline';
            jq.ajax({
                url: "{{ route('driver.onOff') }}",
                method:"PUT",
                data:{ status: status, timeZone: timeZone },
                success: function(response){
                },
                error: function(response){
                }
            });
        }
    }

    $(document).on('click','#Accept', function(event){
        event.preventDefault();
        $(".overlay").show();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'ACCEPTED';
        jq.ajax({
            url: "{{ route('driver.trip.control') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, timeZone: timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").show();
            }
        });
    });

    $(document).on('click','#reached', function(event){
        $(".overlay").show();
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'REACHED';
        $('.fa-map-marker').css({'color':'#093bba'});
        jq.ajax({
            url: "{{ route('driver.trip.control') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, timeZone: timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").show();
            }
        });
    });

    $(document).on('click','#started', function(event){
        $(".overlay").show();
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'STARTED';
        $('.fa-car').css({'color':'#093bba'});
        jq.ajax({
            url: "{{ route('driver.trip.control') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, timeZone: timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").show();
            }
        });
    });

    $(document).on('click','#drop', function(event){
        $(".overlay").show();
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'DROP';
        $('.fa-map-pin').css({'color':'#093bba'});
        jq.ajax({
            url: "{{ route('driver.trip.control') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, timeZone: timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").show();
            }
        });
    });

    $(document).on('click','#payment', function(event){
        $(".overlay").show();
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'PAYMENT';
        var payment_method = 'CASH';
        jq.ajax({
            url: "{{ route('driver.payment') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, payment_method:payment_method, timeZone: timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").show();
            }
        });
    });

    $(document).on('click','#DrivClose', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'RATING';
        var rating = 0;
        var comment = 'N.A';
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('driver.ratingComment') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, rating: rating, comment: comment, timeZone: timeZone },
            success: function(response){
                $('.DriverMap').removeClass('col-lg-8');
                $('.DriverMap').addClass('col-lg-12');
                $('.edit').removeClass('col-lg-4');
                $('#FormOnDriverMap').html('');
               // location.reload();
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").hide();
            }
        });
    });

    $(document).on('submit','#ratingForm', function(event){
        $(".overlay").show();
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'RATING';
        var rating = $('#rating').val();
        var comment = $('#comment').val();
        jq.ajax({
            url: "{{ route('driver.ratingComment') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, rating: rating, comment: comment, timeZone: timeZone },
            success: function(response){
                console.log(response);
                $('.DriverMap').removeClass('col-lg-8');
                $('.DriverMap').addClass('col-lg-12');
                $('.edit').removeClass('col-lg-4');
                $('#FormOnDriverMap').html('');
               // location.reload();
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").show();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('rating')) {
                        $('.drating').show();
                        $('#drating-msg').html(responseMsg.errors.rating);
                    }
                    if (responseMsg.errors.hasOwnProperty('comment')) {
                        $('.drating').show();
                        $('#dcomment-msg').html(responseMsg.errors.comment);
                    }
                }
            }
        });
    });

    // $(document).on('click','#Reject', function(event){
    //     $(".overlay").show();
    //     event.preventDefault();
    //     jq.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
    //     var request_id = $('#request_id').val();
    //     jq.ajax({
    //         url: "{{ route('driver.request.reject') }}",
    //         method:"POST",
    //         data:{ request_id: request_id, timeZone: timeZone },
    //         success: function(response){
    //             console.log(response);
    //             $('.DriverMap').removeClass('col-lg-8');
    //             $('.DriverMap').addClass('col-lg-12');
    //             $('#FormOnDriverMap').html('col-lg-4');
    //             //location.reload();
    //             $(".overlay").show();
    //         },
    //         error: function(response){
    //             $(".overlay").show();
    //         }
    //     });
    // });


    $(document).on('click','#Reject', function(event){
        event.preventDefault();
        $('#drejectReasonModal').modal('show');
        var reason_given_by = 'driver_for_reject';
        jq.ajax({
            url: "{{ route('cancel.reason.get') }}",
            method: "POST",
            data: {reason_given_by: reason_given_by, timeZone: timeZone},
            success: function(response){
                $('#dreason').empty();
                var options = '<option value="">{{trans('weblng.SELECTREASON')}}</option>';
                $.each(response.data, function(key, value) {
                  options += '<option value="'+value.cancel_reason_id+'">'+value.reason+'</option>';
                });

                $('#dreason').append(options);
            },
            error: function(response){
            }
        });
    });

    $('#drejectFormSubmit').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var cancel_reason_id = $('#dreason').val();
        var reason_optional = $('#dreason_optional').val();
        var request_id = $('#request_id').val();
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('driver.request.reject') }}",
            method:"POST",
            data:{ request_id: request_id, cancel_reason_id: cancel_reason_id, reason_optional: reason_optional, timeZone: timeZone },
            success: function(response){
                $('.DriverMap').removeClass('col-lg-8');
                $('.DriverMap').addClass('col-lg-12');
                $('#FormOnDriverMap').html('col-lg-4');
                //location.reload();
                $(".overlay").show();
            },
            error: function(response){
                $(".overlay").show();
            }
        });
    });


</script>
<!-- footer -->
<section class="w-100">
@include('partials.driver_footer')
