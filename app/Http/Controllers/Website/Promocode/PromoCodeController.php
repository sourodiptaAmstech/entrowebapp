<?php

namespace App\Http\Controllers\Website\Promocode;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PromoCodeService;


class PromoCodeController extends Controller
{
	public function promoCodeIndex()
	{
		return view('pages.passenger.promocode.promo');
	}

    public function addPromoCode(Request $request)
	{
		try{
			$this->validate($request, [
                'promo_code' => 'required',
            ]);
			$request->promo_code = $request->promo_code;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$PromoCode = new PromoCodeService;
			$result = $PromoCode->accessAddPromo($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function getPromoCode(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$PromoCode = new PromoCodeService;
			$result = $PromoCode->accessGetPromo($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
