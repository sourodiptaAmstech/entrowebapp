<?php

namespace App\Http\Controllers\Website\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DriverTripService;
use App\Services\DriverWalletService;


class TripController extends Controller
{
  public function declineRequest(Request $request)
	{
		try{
			// $this->validate($request, [
   //              'request_id'=>'required'
   //          ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
      $request->request_id = $request->request_id;
      $request->cancel_reason_id = $request->cancel_reason_id;
      $request->reason_optional = $request->reason_optional;

      $driverTrip = new DriverTripService;
			$result = $driverTrip->accessDeclineRequest($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
    }
	}


	public function tripControl(Request $request)
	{
		try{
			$this->validate($request, [
                'request_id'=>'required',
                'tripStatus'=>'required'
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
  		$request->request_id = $request->request_id;
  		$request->tripStatus = $request->tripStatus;
      //$request->payment_method = $request->payment_method;

      $driverTrip = new DriverTripService;
			$result = $driverTrip->accessTripControl($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
    }
	}

	public function Payment(Request $request)
	{
		try{
			$this->validate($request, [
                'request_id'=>'required',
                'tripStatus'=>'required',
                'payment_method'=>'required'
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
  		$request->request_id = $request->request_id;
  		$request->tripStatus = $request->tripStatus;
  		$request->payment_method = $request->payment_method;

      $request->payment_dispute_reason_id = $request->payment_dispute_reason_id;
      $request->payment_dispute_reason_text = $request->payment_dispute_reason_text;
      $request->payment_report = $request->payment_report;

    	$driverTrip = new DriverTripService;
			$result = $driverTrip->accessTripPayment($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
    }
	}


	public function ratingComment(Request $request)
	{
		try{
			$this->validate($request, [
                'request_id'=>'required',
                'tripStatus'=>'required',
                'rating'=>'required',
                'comment'=>'required'
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
  		$request->request_id = $request->request_id;
  		$request->tripStatus = $request->tripStatus;
  		$request->rating = $request->rating;
  		$request->comment = $request->comment;

    	$driverTrip = new DriverTripService;
			$result = $driverTrip->accessRatingComment($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
    }
	}

  public function tripHistoryIndex(){
    return view('pages.driver.history.history');
  }

  public function driverTripHistory(Request $request)
  {
    try{
          $request['token'] = $request->access_token;
          $request['timeZone'] = $request->timeZone;
          $request->url = $request->url;

          $driverTripService = new DriverTripService;
          $result = $driverTripService->accessTripHistory($request);

          return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
      }
      catch(\Illuminate\Database\QueryException $e){
          return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
      }
  }

  public function tripHistoryDetails(Request $request)
  {
    $request_id = $request->id;
    return view('pages.driver.history.details',compact('request_id'));
  }

  public function driverTripDetails(Request $request)
  {
    try{
          $request['token'] = $request->access_token;
          $request['timeZone'] = $request->timeZone;

          $request->request_id = $request->request_id;

          $driverTripService = new DriverTripService;
          $result = $driverTripService->accessTripDetails($request);

          return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
      }
      catch(\Illuminate\Database\QueryException $e){
          return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
      }
  }

  public function paymentStatistics(Request $request)
  {
    try{
          $request['token'] = $request->access_token;
          $request['timeZone'] = $request->timeZone;

          $DriverWalletService = new DriverWalletService;
          $result = $DriverWalletService->accessPaymentStatistics($request);

          return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
      }
      catch(\Illuminate\Database\QueryException $e){
          return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
      }
  }

    public function wallet(Request $request)
    {
        return view('pages.driver.payment.wallet');
    }

    public function earning(Request $request)
    {
        return view('pages.driver.payment.earning');
    }

	
}
