<?php

namespace App\Services;

use App\Services\CurlService;


class FleetownerService
{

  private function driverRegistrationByFleet($data)
  {
    try{
      $curl_url = env('serverURL').'fleetowner/driver/registration';
      $method = "POST";
      $array = [
                'login_by'=>$data->login_by,
                'device_id'=>$data->device_id,
                'device_token'=>$data->device_token,
                'device_type'=>$data->device_type,
                'first_name'=>$data->first_name,
                'last_name'=>$data->last_name,
                'mobile_no'=>$data->mobile_no,
                'isdCode'=>$data->isdCode,
                'password'=>$data->password,
                'password_confirmation'=>$data->password_confirmation,
                'isActive'=>$data->isActive,
                'model_year'=>$data->model_year,
                'service_type_id'=>$data->service_type_id,
                'registration_no'=>$data->registration_no,
                'make'=>$data->make,
                'model'=>$data->model,
                'gender'=>$data->gender,
                'email_id'=>$data->email_id
              ];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);
      
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }  
  }


  private function GetFleetDrivers($data)
  {
    try{
      $curl_url = env('serverURL').'fleetowner/driver/list';
      $method = "GET";
      $json_encode = "";
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }

  private function GetFleetDriverDetails($data)
  {
    try{
      $curl_url = env('serverURL').'fleetowner/driver/'.$data->user_id;
      $method = "GET";
      $json_encode = "";
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }

  private function updateFleetDriver($data)
  {
    try{
      $curl_url = env('serverURL').'fleetowner/driver/profile/update/'.$data->user_id;
      $method = "POST";
      $array = ['first_name'=>$data->first_name, 'last_name'=>$data->last_name, 'email_id'=>$data->email_id, 'gender'=>$data->gender, 'service_type_id'=>$data->service_type_id, 'make'=>$data->make, 'model'=>$data->model, 'model_year'=>$data->model_year, 'registration_no'=>$data->registration_no];
          $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;
      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);
      
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

  }

  private function GetFleetDriverStatistics($data)
  {
    try{
      $curl_url = env('serverURL').'fleetowner/driver/statistics/'.$data->user_id;
      $method = "GET";
      $json_encode = "";
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }

  private function GetFleetStatistics($data)
  {
    try{
      $curl_url = env('serverURL').'fleetowner/statistics';
      $method = "GET";
      $json_encode = "";
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }

  public function accessGetFleetStatistics($data){
    return $this->GetFleetStatistics($data);
  }

  public function accessGetFleetDriverStatistics($data){
      return $this->GetFleetDriverStatistics($data);
  }
  public function accessUpdateFleetDriver($data){
      return $this->updateFleetDriver($data);
  }
  public function accessGetFleetDriverDetails($data){
      return $this->GetFleetDriverDetails($data);
  }
	public function accessGetFleetDrivers($data){
      return $this->GetFleetDrivers($data);
  }
  public function accessDriverRegistrationByFleet($data){
      return $this->driverRegistrationByFleet($data);
	}

}