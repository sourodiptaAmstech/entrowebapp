<?php

namespace App\Services;

use App\Services\CurlService;


class PassengerRequestService
{
	private function estimatedFare($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/get/estimated/fare';
	      $method = "POST";

	      $array = [
	      	'source'=>$data->source,
	      	'destination'=>$data->destination,
	      	'waypoint'=>$data->waypoint,
	      	'preferences'=>$data->preferences,
	      	'request_type'=>$data->request_type,
	      ];

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);
	    
	      if ($httpcode==401) {
	      	return ['message'=>$response['message'],"data"=>[],"errors"=>[],'statusCode'=>$httpcode];
	      }

	      return ['message'=>$response['message'],"data"=>$response['data'],"last_paid_by"=>$response['last_paid_by'],"isFemaleFriendlyFeatureOn"=>$response['isFemaleFriendlyFeatureOn'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function request($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/request/ride';
	      $method = "POST";

	      $array = [
	      	'request_id'=>$data->request_id,
	      	'service_type_id'=>$data->service_type_id,
	      	'isFemaleFriendly'=>$data->isFemaleFriendly,
	      	'payment_method'=>$data->payment_method,
	      	'card_id'=>$data->card_id
	      ];

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	   
	      curl_close($curl);
	      $response=json_decode($response,true);
	    
	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function ratingComment($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/request/ratingComment';
	      $method = "POST";
	      $array = [
	      	'request_id'=>$data->request_id,
	      	'rating'=>$data->rating,
	      	'comment'=>$data->comment
	      ];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	   
	      curl_close($curl);
	      $response=json_decode($response,true);
	    
	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function scheduleRide($data)
	{
	    try{
	      	$curl_url = env('serverURL').'passenger/request/schedule/ride';
	      	$method = "POST";
	      	$array = [
	      		'request_id'=>$data->request_id,
	      		'service_type_id'=>$data->service_type_id,
	      		'payment_method'=>$data->payment_method,
	      		'date'=>$data->date,
	      		'time'=>$data->time,
	      		'isFemaleFriendly'=>$data->isFemaleFriendly,
	      		'card_id'=>$data->card_id
	      	];
	      	$json_encode = json_encode($array);
	      	
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
	      	curl_close($curl);
	      	$response=json_decode($response,true);
	    
	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function tripHistory($data)
	{
	    try{
	      	// $curl_url = env('serverURL').'passenger/trip/history?page='.$data->page;
	      	$curl_url = $data->url;
	      	$method = "GET";
	      	$json_encode = "";
	      	
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
	      	curl_close($curl);
	      	$response=json_decode($response,true);
	    
	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}

	private function tripDetails($data)
  	{
      	try{
          	$curl_url = env('serverURL').'passenger/trip/details';
          	$method = "POST";
          	$array = ['request_id'=>$data->request_id];
          	$json_encode = json_encode($array);
          
          	$timeZone = $data->timeZone;
          	$token = $data->token;

          	$curlService = new CurlService;
          	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          	$response = curl_exec($curl);
          	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  
          	curl_close($curl);
          	$response=json_decode($response,true);
      
          	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
      	}
      	catch(\Illuminate\Database\QueryException  $e){
          	return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      	}
      	catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          	return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      	}
  	}


	private function upCommingScheduleTrip($data)
	{
	    try{
	      	$curl_url = env('serverURL').'passenger/upcoming/schedule';
	      	$method = "GET";
	      	$json_encode = "";
	      	
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
	      	curl_close($curl);
	      	$response=json_decode($response,true);
	    
	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}

	private function cancelScheduleTrip($data)
	{
	    try{
	      	$curl_url = env('serverURL').'passenger/request/schedule/cancel';
	      	$method = "POST";
          	$array = ['request_id'=>$data->request_id];
          	$json_encode = json_encode($array);
	      	
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
	      	curl_close($curl);
	      	$response=json_decode($response,true);
	    
	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}


	private function cancelRideRequest($data)
	{
	    try{
	      	$curl_url = env('serverURL').'passenger/ride/cancel';
	      	$method = "POST";
          	$array = ['request_id'=>$data->request_id,'cancel_reason_id'=>$data->cancel_reason_id, 'reason_optional'=>$data->reason_optional];
          	$json_encode = json_encode($array);
	      	
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
	      	curl_close($curl);
	      	$response=json_decode($response,true);
	    
	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}


	public function accessCancelRideRequest($data)
	{
    	return $this->cancelRideRequest($data);
  	}

	public function accessEstimatedFare($data)
	{
    	return $this->estimatedFare($data);
  	}

  	public function accessRequest($data)
	{
    	return $this->request($data);
  	}

  	public function accessRatingComment($data)
	{
    	return $this->ratingComment($data);
  	}

  	public function accessScheduleRide($data)
	{
    	return $this->scheduleRide($data);
  	}

  	public function accessTripHistory($data)
	{
    	return $this->tripHistory($data);
  	}

  	public function accessTripDetails($data)
  	{
    	return $this->tripDetails($data);
  	}

  	public function accessUpCommingScheduleTrip($data)
	{
    	return $this->upCommingScheduleTrip($data);
  	}

  	public function accessCancelScheduleTrip($data)
	{
    	return $this->cancelScheduleTrip($data);
  	}

}