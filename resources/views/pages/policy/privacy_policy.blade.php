<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-12 px-2">
                <div class="upper py-5">
                    <div class="alert alert-danger update_alert" style="display: none;">
                        <p class="text-danger" id="update-msg"></p>
                    </div>
                    <h2 class="text-left font-weight-bold">{{trans('weblng.FIELD.PRIVACY_POLICY')}}</h2>
                    <div class="ptnc privacyPolicyText font-theme">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    jq = jQuery.noConflict();
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    jq.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
        }
    });
    var cookieValue = Cookies.get('languageCookie');
    
    if (cookieValue == '' || cookieValue == 'en') {
        var languageParam = 'en';
    } else {
        var languageParam = 'fr';
    }

    jq(".overlay").show();
    jq.ajax({
        url: "{{ route('privacy.policy.get') }}",
        method: "POST",
        data: {languageParam: languageParam, timeZone: timeZone},
        success: function(response){
            console.log(response)
            $('.privacyPolicyText').html(response.data)
            $(".overlay").hide();
        },
        error: function(response){
            $(".overlay").hide();
            var responseMsg = $.parseJSON(response.responseText);
            $('.upper').removeClass('pt-5');
            $('.upper').addClass('pt-4');
            $('.update_alert').show();
            $('#update-msg').html(responseMsg.message)
        }
    });
</script>