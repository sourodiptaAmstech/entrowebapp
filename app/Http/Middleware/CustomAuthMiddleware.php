<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Encryption\DecryptException;
use Closure;

class CustomAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     private function signature(){
      $IP = $_SERVER['REMOTE_ADDR'];
      $mac = shell_exec("ip link | awk '{print $2}'");
      preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
      $output = array_combine($matches[1], $matches[2]);
      $mac_address_values=$IP.":".env('APP_KEY');
      foreach($output as $key=>$val){
        if($val!=""){
          if($mac_address_values!=="")
          {  $mac_address_values=":".$mac_address_values.$val;}
          else
          {  $mac_address_values=$mac_address_values.$val;}
        }
      }
       //$mac_address_values;
      $entroSignature= base64_encode(($mac_address_values));
      
        return base64_encode(($entroSignature));
     }
    
     private function authToketDecode($token){
      return ((decrypt(base64_decode($token))));
     }

    
    public function handle($request, Closure $next)
    {
        if(!isset($_COOKIE["entroSecurity"]) && !isset($_COOKIE["entroSignature"])) {
          if($request->expectsJson()){
            return response()->json(array("msg"=>"Access Denied","reason"=>"Login Failed","code"=>401), 401, [], JSON_PRETTY_PRINT); 
          }
          else{
            return redirect()->route('index');
          }
        } else {
              $entroSecurity=$_COOKIE["entroSecurity"];
              $entroSignature=$_COOKIE["entroSignature"];
              // validate cookies signature
              if($this->signature()===$entroSignature){
                  $request['access_token']=$this->authToketDecode($_COOKIE["entroSecurity"]);
              }
              else{
                           
                if($request->expectsJson()){
                  return response()->json(array("msg"=>"Access Denied","reason"=>"Login Failed","code"=>401), 401, [], JSON_PRETTY_PRINT); 
                }
                else{
                  return redirect()->route('index');
                }
              }
            }
            return $next($request);
    }
}
