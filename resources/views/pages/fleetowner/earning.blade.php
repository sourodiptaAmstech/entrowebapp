<!-- header -->
@include('partials.fleetowner_header')

<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.fleetowner_sidebar')
            </div>
            <div class="col-lg-8">

                    <div class="px-sm-3 pt-5">
                        
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success changsuc_alert" style="display: none;">
                            <p class="text-success" id="changsuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.MYEARNING')}}</h2>

                        <div class="row pt-4">
                            <div class="col-lg-4">
                                <div class="list-group-item1 w-100 shadow-lg mb-2 rounded border-default card-bg-color" style="
    height: 155px;
">
                                    <div class="justify-content-center text-center p-5 text-dark" >
                                        <h5 class="card-font-size">{{trans('weblng.TOTATRIP')}}</h5>
                                        <h3 class="pt-3 font-weight-bold" id="trip"></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="list-group-item1 w-100 shadow-lg p-5 mb-2 rounded border-default card-bg-color" style="
    height: 155px;
">
                                    <div class="justify-content-center text-center text-dark" >
                                        <h5 class="card-font-size">{{trans('weblng.TOTALEARN')}}</h5>
                                        <h3 class="pt-3 font-weight-bold" id="totalbalan"></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="list-group-item1 w-100 shadow-lg p-5 mb-2 rounded border-default card-bg-color" style="
    height: 155px;
">
                                    <div class="justify-content-center text-center text-dark" >
                                        <h5 class="card-font-size">{{trans('weblng.THISTODAY')}}</h5>
                                        <h3 class="pt-3 font-weight-bold" id="daybalan"></h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-4">
                            <div class="col-lg-4">
                                <div class="list-group-item1 w-100 shadow-lg mb-2 rounded border-default card-bg-color" style="
    height: 155px;
">
                                    <div class="justify-content-center text-center p-5 text-dark" >
                                        <h5 class="card-font-size">{{trans('weblng.THISWEEK')}}</h5>
                                        <h3 class="pt-3 font-weight-bold" id="weekbalan"></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="list-group-item1 w-100 shadow-lg mb-2 rounded border-default card-bg-color" style="
    height: 155px;
">
                                    <div class="justify-content-center text-center p-5 text-dark" >
                                        <h5 class="card-font-size">{{trans('weblng.THISMONTH')}}</h5>
                                        <h3 class="pt-3 font-weight-bold" id="monthbalan"></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="list-group-item1 w-100 shadow-lg p-5 mb-2 rounded border-default card-bg-color" style="
    height: 155px;
">
                                    <div class="justify-content-center text-center text-dark" >
                                        <h5 class="card-font-size">{{trans('weblng.THISYEAR')}}</h5>
                                        <h3 class="pt-3 font-weight-bold" id="yearbalan"></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<style>
    .card-bg-color{
    background-color: #D6FAFA;
}
.card-font-size{
    font-size: initial;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    jq = jQuery.noConflict();
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    jq.ajax({
        url: "{{ route('fleetowner.statistics') }}",
        method:"GET",
        data:{ timeZone: timeZone },
        success: function(response){
            $('#yearbalan').text(response.data.currency+``+parseFloat(response.data.currentYear).toFixed(2));
            $('#monthbalan').text(response.data.currency+``+parseFloat(response.data.currentMonth).toFixed(2));
            $('#daybalan').text(response.data.currency+``+parseFloat(response.data.currentToday).toFixed(2));
            $('#weekbalan').text(response.data.currency+``+parseFloat(response.data.currentWeek).toFixed(2));
            $('#totalbalan').text(response.data.currency+``+parseFloat(response.data.totalBalance).toFixed(2));
            $('#trip').text(response.data.totalTripecount);
        },
        error: function(response){
        }
    });

</script>
<!-- footer -->
@include('partials.fleetowner_footer')