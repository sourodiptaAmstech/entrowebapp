<!-- header -->
@include('partials.driver_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.driver_sidebar')
            </div>
            <div class="col-lg-8">
                <form class="" id="fromtodate">
                    <div class="px-sm-3 pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success changsuc_alert" style="display: none;">
                            <p class="text-success" id="changsuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.SIDEBAR.SUMMARY')}}</h2>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="card rounded-border mt-1">
                                    <div class="card-body">
                                        <h5 class="card-title">{{trans('weblng.FIELD.RIDE_THIS_WEEK')}}:</h5>
                                        <h2 class="card-text text-info" id="this_week"></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card rounded-border mt-1">
                                    <div class="card-body">
                                        <h5 class="card-title">{{trans('weblng.FIELD.RIDE_MONTHLY')}}:</h5>
                                        <h2 class="card-text text-info" id="monthly"></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card rounded-border mt-1">
                                    <div class="card-body">
                                        <h5 class="card-title">{{trans('weblng.FIELD.RIDE_YEARLY')}}:</h5>
                                        <h2 class="card-text text-info" id="yearly"></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <select id="year" class="pull-right">
                                    
                                </select>
                            </div>
                        </div>

                        <canvas id="chartContainer" class="mt-3"></canvas>

                        <div class="alert alert-danger date_alert mt-5" style="display: none;">
                            <p class="text-danger" id="date-msg"></p>
                        </div>

                        <div class="row datemt mt-5">
                           
                            <div class="col-sm-4">
                                <label for="fromDate" class="font-weight-bold1">{{trans('weblng.FIELD.EARNING_FROM')}}</label>
                                <input type="text" name="fromDate" id="fromDate" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FROMDATE')}}" onblur="return validate()">
                            </div>
                            <div class="col-sm-4">
                                <label for="toDate" class="font-weight-bold1">{{trans('weblng.FIELD.EARNING_TO')}}</label>
                                <input type="text" name="toDate" id="toDate" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.TO_DATE')}}" onblur="return validate()">
                            </div>
                            <div class="col-sm-4">
                                <button type="sumbit" class="btn btn-info btn-sm" style="margin-top: 42px;">{{trans('weblng.FIELD.SEARCH')}}</button>
                                {{-- <div id="total" class="form-control inset-input text-info" style="margin-top: 33px;background-color: white;line-height: 39px;"></div> --}}
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-sm-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">{{trans('weblng.DATE')}}</th>
                                            <th scope="col">{{trans('weblng.FIELD.RIDE_ID')}}</th>
                                            <th scope="col">{{trans('weblng.FIELD.TRAN_ID')}}</th>
                                            <th scope="col">{{trans('weblng.FIELD.AMOUNT')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody class="datewise">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

<script>
jq = jQuery.noConflict();

function validate(){
    var status=null;

    var fromDate = document.getElementById('fromDate').value;
    if (fromDate == '') {
        jq('#fromDate').addClass('has-error');
        status = false
    } else {
        jq('.px-sm-3').addClass('pt-5');
        jq('.dupdate_alert').hide();
        jq('#dupdate-msg').html('');
        jq('.datemt').addClass('mt-5');
        jq('.date_alert').hide();
        jq('#date-msg').html('');
        jq('#fromDate').removeClass('has-error');
        status=true;
    }

    var toDate = document.getElementById('toDate').value
    if (toDate == '') {
        jq('#toDate').addClass('has-error');
        status=false
    } else {
        jq('#toDate').removeClass('has-error');
    }
    return status
}

jq(document).ready(function(){
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    //year dropdowm
    var i, currentYear, startYear, endYear, newOption, dropdownYear;
    dropdownYear = document.getElementById("year");
    currentYear = (new Date()).getFullYear();
    startYear = currentYear - 5;
    endYear = currentYear;
    for (i=endYear;i>=startYear;i--) {
        newOption = document.createElement("option");
        newOption.value = i;
        newOption.label = i;
            if (i == currentYear) {
                newOption.selected = true;
            }
        dropdownYear.appendChild(newOption);
    }

    jq.ajax({
        url: "{{ route('driver.get.summary.status') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            console.log(response)
            $('#this_week').text(response.data.currentWeeklyTripCount)
            $('#monthly').text(response.data.currentMonthTripCount)
            $('#yearly').text(response.data.currentYearTripCount)
        },
        error: function(response){
            if (response.status == 400){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message);
            }
            if (response.status == 500){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message);
            }
        }
    });

    var graphdata =''
    getYearly()

    $('#year').on('change',function(event){
        event.preventDefault();
        getYearly()
    })

    function getYearly(){
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var year = $('#year').val()
        jq.ajax({
            url: "{{ route('driver.summary.yearly') }}",
            method: "POST",
            data: {year: year, timeZone: timeZone},
            success: function(response){
                graphdata =[
                    response.data.graphData.January.tripecount, 
                    response.data.graphData.February.tripecount,
                    response.data.graphData.March.tripecount,
                    response.data.graphData.April.tripecount,
                    response.data.graphData.May.tripecount,
                    response.data.graphData.June.tripecount, 
                    response.data.graphData.July.tripecount,
                    response.data.graphData.August.tripecount,
                    response.data.graphData.September.tripecount,
                    response.data.graphData.October.tripecount,
                    response.data.graphData.November.tripecount,
                    response.data.graphData.December.tripecount
                ];
            },
            error: function(response){
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
            }
        });
    }

    $('#fromtodate').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var fromDate = $('#fromDate').val()
        var toDate = $('#toDate').val()
        jq.ajax({
            url: "{{ route('driver.summary.datewise') }}",
            method: "POST",
            data: {fromDate: fromDate, toDate: toDate, timeZone: timeZone},
            success: function(response){
                console.log(response.data.currency)
                var currency = response.data.currency
                jq('.datewise').empty();
                var optiond = '';
                var total = 0
                jq.each(response.data.transaction, function(key, value) {
                    total +=value.total
                    optiond += `
                            <tr>
                                <td>`+value.date+`</td>
                                <td>`+value.request_no+`</td>
                                <td>`+value.transaction_log_id+`</td>
                                <td class="text-info">`+currency+value.total.toFixed(2)+`</td>
                            </tr>`;
                });
                $('#total').text(currency+total.toFixed(2))
                jq('.datewise').append(optiond);
            },
            error: function(response){
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('fromDate')) {
                        jq('#fromDate').addClass('has-error');
                    }
                    if (responseMsg.errors.hasOwnProperty('toDate')) {
                         jq('#toDate').addClass('has-error');
                    }
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.datemt').removeClass('mt-5');
                    jq('.date_alert').show();
                    jq('#date-msg').html(responseMsg.message);
                }
                if (response.status == 401){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.datemt').removeClass('mt-5');
                    jq('.date_alert').show();
                    jq('#date-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.datemt').removeClass('mt-5');
                    jq('.date_alert').show();
                    jq('#date-msg').html(responseMsg.message);
                }
            }
        });
    });

    jq(document).ajaxComplete(function(event,xhr,settings) {
        console.log(settings.url)
        if(settings.url == "{{ route('driver.summary.yearly') }}")
        {
            var ctx = document.getElementById('chartContainer');
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                    datasets: [{
                        label: '{{trans('weblng.FIELD.TRIP_COUNT')}}',
                        backgroundColor: 'rgb(101, 192, 252)',
                        borderColor: 'rgb(101, 192, 252)',
                        data: graphdata
                    }]
                },
                options: {}
            });
        }
    });

});
</script>

<!-- footer -->
@include('partials.driver_footer')