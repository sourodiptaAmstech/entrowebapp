<!-- header -->
@include('partials.fleetowner_header')
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.fleetowner_sidebar')
            </div>
            <div class="col-lg-8">
              <div class="px-sm-3 pt-5">
                <h2 class="font-weight-bold mb-4">{{trans('weblng.DRIVER_LI')}}</h2>

                <ul class="list-group listDriver">
                </ul>

              </div>
            </div>
        </div>
    </div>
</div>
{{-- Model --}}
<div class="modal fade" id="driverDetailsModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="p-2 w-100">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="{{URL::asset('/')}}storage/assets/img/signs.png" alt="" class="m-close">
            </button>
        </div>
            <div class="modal-body w-100">
                <div class="col-sm-12 mx-0 px-0">
                    <div class="profile-image position-relative my-2 d-flex justify-content-center align-items-center">
                        <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Profile" id="driver_image">
                    </div>
                </div>
                <div class="container my-2" id="driverDetails">  
                </div>
            </div>
        </div>
    </div>
</div>
{{-- model earning --}}
<div class="modal fade" id="driverEarningModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="p-2 w-100">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="{{URL::asset('/')}}storage/assets/img/signs.png" alt="" class="m-close">
                </button>
            </div>
            <div class="modal-body w-100">
                <div class="container my-2" id="driverEarning">  
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('get.fleetowner.driver') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                console.log(response);
                var options = '';
                if(response.data.user_profile.length==0){
                    options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">{{trans('weblng.DRIVER_BLANK')}}</h6>`;
                }
                $.each(response.data.user_profile, function(key, value) {
                    console.log(value);
                    var edit_button = `<a href="{{ route('fleetowner.driver.update') }}?driver=`+value.user_id+`" class="btn btn-info btn-xs mt-1"><i class="fa fa-pencil"></i> {{trans('weblng.EDIT')}}</a>`;
                    var detail_button = `<a href="javascript:void(0)" class="btn btn-info btn-xs detail-button mt-1" key="`+value.user_id+`"><i class="fa fa-eye"></i> {{trans('weblng.DETAILS')}}</a>`;
                    var earning_button = `<a href="javascript:void(0)" class="btn btn-info btn-xs earning-button mt-1" key="`+value.user_id+`"><i class="fa fa-eye"></i> {{trans('weblng.EARNINGS')}}</a>`;
                    options += `
                        <li class="list-group-item w-100 shadow-lg p-3 mb-2 bg-white rounded border-default">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-6">
                                    <h5 class="font-weight-bold1">{{trans('weblng.DRIVERNAME')}}: <span class="text-muted">`+value.first_name+` `+value.last_name+`</span></h5>
                                    <h6 class="pt-1">{{trans('weblng.MOBILEno')}}: <span class="text-muted">`+value.isd_code+`-`+value.mobile_no+`</span></h6>
                                    <h6 class="pt-1">{{trans('weblng.FIELD.SERVICE_TYPE')}}: <span class="text-muted">`+value.service_name+`</span></h6>
                                </div>
                                <div class="col-md-6 col-sm-6 col-6">
                                    <div class="text-right tran-img1">
                                        `+edit_button+` `+detail_button+` `+earning_button+`
                                    </div>
                                </div> 
                            </div>
                        </li>
                        `;
                });
                $('.listDriver').append(options);
                $(".overlay").hide();

                $('.detail-button').click(function(){
                    var user_id = $(this).attr("key");
                    $(".overlay").show();
                    jq.ajax({
                        url: "{{ route('get.fleetowner.driver.details') }}?driver_id="+user_id,
                        method:"GET",
                        data:{ timeZone: timeZone },
                        success: function(response){
                            $(".overlay").hide();
                            console.log(response);
                            $('#driverDetails').html('');
                            var picture = response.data.user_profile[0].picture;
                            if(picture!=''&&picture!=null){
                                $("#driver_image").attr({ "src": picture });
                                $("#driver_image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                            }

                            var option = `<div class="row shadow-lg1 bg-white">
                                <div class="col-lg-12 px-5 py-3">
                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h5 class="font-weight-bold text-muted">
                                                   {{trans('weblng.DRIVERNAME')}}:
                                                </h5>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h5 class="text-muted">`+response.data.user_profile[0].first_name+` `+response.data.user_profile[0].last_name+`</h5>
                                            </div>
                                        </div>
                                       
                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.MOBILEno')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted">`+response.data.user_profile[0].isd_code+`-`+response.data.user_profile[0].mobile_no+`</h6>
                                            </div>
                                        </div>
                                        
                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.FIELD.EMAIL')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted">`+response.data.user_profile[0].email_id+`</h6>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.FIELD.CAR_MAKE')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted">`+response.data.user_profile[0].make+`</h6>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.FIELD.CAR_MODEL')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted">`+response.data.user_profile[0].model+`</h6>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.FIELD.MODEL_YEAR')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted">`+response.data.user_profile[0].model_year+`</h6>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.FIELD.CAR_NO')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted">`+response.data.user_profile[0].registration_no+`</h6>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-12 mx-0 px-0 text-center">
                                                <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center img-fluid">
                                                    <img src="`+response.data.user_profile[0].service_image+`">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-12 mx-0 px-0 text-center">
                                                <h6 class="text-muted">
                                                    `+response.data.user_profile[0].service_name+`
                                                </h6>
                                            </div>
                                        </div>

                                </div>
                            </div>`;                
                            $('#driverDetails').append(option);
                            $("#driverDetailsModal").modal('show');
                        },
                        error: function(response){
                            console.log(response);
                            $(".overlay").hide();
                        }
                    });

                })
                
                $('.earning-button').click(function(){
                    var user_id = $(this).attr("key");
                    $(".overlay").show();
                    jq.ajax({
                        url: "{{ route('fleetowner.get.driver.statistics') }}?driver_id="+user_id,
                        method:"GET",
                        data:{ timeZone: timeZone },
                        success: function(response){
                            $(".overlay").hide();
                            console.log(response);
                            $('#driverEarning').html('');
                            var option = `<div class="row shadow-lg1 bg-white">
                                <div class="col-lg-12 px-5 py-3">
                                    <div class="row mb-2">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h5 class="font-weight-bold text-muted">
                                               {{trans('weblng.TOTALEARN')}}:
                                            </h5>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h5 class="text-muted">`+response.data.currency+``+response.data.totalBalance+`</h5>
                                        </div>
                                    </div>

                                    <div class="row mb-2">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="font-weight-bold text-muted">
                                                {{trans('weblng.THISTODAY')}}: 
                                            </h6>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="text-muted">`+response.data.currency+``+response.data.currentToday+`</h6>
                                        </div>
                                    </div>

                                    <div class="row mb-2">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="font-weight-bold text-muted">
                                                {{trans('weblng.THISWEEK')}}: 
                                            </h6>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="text-muted">`+response.data.currency+``+response.data.currentWeek+`</h6>
                                        </div>
                                    </div>

                                    <div class="row mb-2">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="font-weight-bold text-muted">
                                                {{trans('weblng.THISMONTH')}}: 
                                            </h6>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="text-muted">`+response.data.currency+``+response.data.currentMonth+`</h6>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="font-weight-bold text-muted">
                                                {{trans('weblng.THISYEAR')}}: 
                                            </h6>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="text-muted">`+response.data.currency+``+response.data.currentYear+`</h6>
                                        </div>
                                    </div>
                                   
                                    <div class="row mb-2">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="font-weight-bold text-muted">
                                                {{trans('weblng.TOTATRIP')}}: 
                                            </h6>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h6 class="text-muted">`+response.data.totalTripecount+`</h6>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>`;                
                            $('#driverEarning').append(option);
                            $("#driverEarningModal").modal('show');
                        },
                        error: function(response){
                            console.log(response);
                            $(".overlay").hide();
                        }
                    });

                })
                //end earning call

            },
            error: function(response){
                $(".overlay").hide();
            }
        });
        
    });
</script>

<!-- footer -->
@include('partials.fleetowner_footer')