<?php

namespace App\Services;

use Curl;
use App\Services\CurlService;


class PassengerRegistrationService
{
	private function sendOTP($data)
	{
    try{
      $curl_url = env('serverURL').'passenger/signup/otp';
      $method = "POST";
      $array = ['mobile_no'=>$data->mobile_no,'isdCode'=>$data->isdCode ];
     
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
	
	}

  private function register($data)
  {
    try{
      $curl_url = env('serverURL').'passenger/signup';
      $method = "POST";
      $array = [
                'login_by'=>$data->login_by,
                'device_id'=>$data->device_id,
                'device_token'=>$data->device_token,
                'device_type'=>$data->device_type,
                'first_name'=>$data->first_name,
                'last_name'=>$data->last_name,
                'mobile_no'=>$data->mobile_no,
                'isdCode'=>$data->isdCode,
                'password'=>$data->password,
                'password_confirmation'=>$data->password_confirmation,
                'isActive'=>$data->isActive,
              ];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);
      
      if($httpcode==201){
        $IP = $_SERVER['REMOTE_ADDR'];
        $mac = shell_exec("ip link | awk '{print $2}'");
        preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
        $output = array_combine($matches[1], $matches[2]);
        $mac_address_values=$IP.":".env('APP_KEY');
        foreach($output as $key=>$val){
          if($val!=""){
            if($mac_address_values!=="")
            {  $mac_address_values=":".$mac_address_values.$val;}
            else
            {  $mac_address_values=$mac_address_values.$val;}
          }
        }
        $entroSignature= base64_encode(($mac_address_values));
         
        $cookie_name1 = "entroSignature";
        $cookie_value1 = base64_encode(($entroSignature));
        setcookie($cookie_name1, $cookie_value1, 0, "/");

        $cookie_name = "entroSecurity";
        $cookie_value = base64_encode(encrypt($response["data"]["token_type"]." ".$response["data"]["access_token"]));
        setcookie($cookie_name, $cookie_value, 0, "/");
      }

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    } 
  }


  private function socialRegistration($data)
  {
    try{
      $curl_url = env('serverURL').'passenger/signup';
      $method = "POST";
      $array = [
                'social_unique_id'=>$data->social_unique_id,
                'login_by'=>$data->login_by,
                'device_id'=>$data->device_id,
                'device_token'=>$data->device_token,
                'device_type'=>$data->device_type,
                'first_name'=>$data->first_name,
                'last_name'=>$data->last_name,
                'mobile_no'=>$data->mobile_no,
                'isdCode'=>$data->isdCode,
                'email_id'=>$data->email_id,
                'picture'=>$data->picture,
                'isActive'=>$data->isActive,
              ];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);
      
      if($httpcode==201){
        $IP = $_SERVER['REMOTE_ADDR'];
        $mac = shell_exec("ip link | awk '{print $2}'");
        preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
        $output = array_combine($matches[1], $matches[2]);
        $mac_address_values=$IP.":".env('APP_KEY');
        foreach($output as $key=>$val){
          if($val!=""){
            if($mac_address_values!=="")
            {  $mac_address_values=":".$mac_address_values.$val;}
            else
            {  $mac_address_values=$mac_address_values.$val;}
          }
        }
        $entroSignature= base64_encode(($mac_address_values));
         
        $cookie_name1 = "entroSignature";
        $cookie_value1 = base64_encode(($entroSignature));
        setcookie($cookie_name1, $cookie_value1, 0, "/");

        $cookie_name = "entroSecurity";
        $cookie_value = base64_encode(encrypt($response["data"]["token_type"]." ".$response["data"]["access_token"]));
        setcookie($cookie_name, $cookie_value, 0, "/");
      }

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    } 
  }


  private function emergencyContact($data)
  {
    try{
      $curl_url = env('serverURL').'passenger/emergencyContact';
      $method = "POST";

      $array = [
                'email_id'=>$data->email_id,
                'emergOne_id'=>$data->emergOne_id,
                'emergOne_name'=>$data->emergOne_name,
                'emergOne_isdCode'=>$data->emergOne_isdCode,
                'emergOne_contact_no'=>$data->emergOne_contact_no,
                'emergTwo_id'=>$data->emergTwo_id,
                'emergTwo_name'=>$data->emergTwo_name,
                'emergTwo_isdCode'=>$data->emergTwo_isdCode,
                'emergTwo_contact_no'=>$data->emergTwo_contact_no,
              ];
      //dd($array);
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;
      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      
      curl_close($curl);
      $response=json_decode($response,true);

      if($httpcode==201){
        $IP = $_SERVER['REMOTE_ADDR'];
        $mac = shell_exec("ip link | awk '{print $2}'");
        preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
        $output = array_combine($matches[1], $matches[2]);
        $mac_address_values=$IP.":".env('APP_KEY');
        foreach($output as $key=>$val){
          if($val!=""){
            if($mac_address_values!=="")
            {  $mac_address_values=":".$mac_address_values.$val;}
            else
            {  $mac_address_values=$mac_address_values.$val;}
          }
        }
        $entroSignature= base64_encode(($mac_address_values));
         
        $cookie_name1 = "entroSignature";
        $cookie_value1 = base64_encode(($entroSignature));
        setcookie($cookie_name1, $cookie_value1, 0, "/");

        $cookie_name = "entroSecurity";
        $cookie_value = base64_encode(encrypt($response["data"]["token_type"]." ".$response["data"]["access_token"]));
        setcookie($cookie_name, $cookie_value, 0, "/");
      }
     
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

  }

	public function accessSendOTP($data){
      return $this->sendOTP($data);
  }

  public function accessRegister($data){
      return $this->register($data);
  }

  public function accessEmergencyContact($data){
      return $this->emergencyContact($data);
  }

  public function accessSocialRegistration($data){
      return $this->socialRegistration($data);
  }

}