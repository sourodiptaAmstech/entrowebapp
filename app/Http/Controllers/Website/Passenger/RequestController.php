<?php

namespace App\Http\Controllers\Website\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerRequestService;


class RequestController extends Controller
{
	public function estimated(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'source' =>'required',
                'destination' => 'required',
                'sourceLat' => 'required',
                'destinationLat'=>'required',
	        ]);
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
	        $source = ["longitude"=>$request->sourceLng,"latitude"=>$request->sourceLat,"address"=>$request->source,"order"=>0];
	        $destination = ["longitude"=>$request->destinationLng,"latitude"=>$request->destinationLat,"address"=>$request->destination,"order"=>0];
	        $request->source = $source;
	        $request->destination = $destination;
	        $request->waypoint = [];
	        $request->preferences = "";
	        $request->request_type = 'RIDENOW';

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessEstimatedFare($request);

			if ($result['statusCode']==401) {
				return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
			}

	        return response(['message'=>$result['message'],"data"=>$result['data'],"last_paid_by"=>$result['last_paid_by'],"isFemaleFriendlyFeatureOn"=>$result['isFemaleFriendlyFeatureOn'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function confirmRequest(Request $request)
	{
		try{
	        $this->validate($request, [
                'service_type' => 'required',
	        ]);
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->request_id = $request->request_id;
	        $request->service_type_id = $request->service_type;
	        $request->isFemaleFriendly = $request->female_friendly;
	        $request->payment_method = $request->payment_method;
	        if ($request->payment_method=='CARD') {
	        	$request->card_id = $request->card_id;
	        }
        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessRequest($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function ratingComment(Request $request)
	{
		try{
	        $this->validate($request, [
                'request_id' =>'required',
                'rating'=>'required',
                'comment'=>'required'
	        ]);
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->request_id = $request->request_id;
	        $request->rating = $request->rating;
	        $request->comment = $request->comment;
	       	
        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessRatingComment($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function scheduleRide(Request $request)
	{
		try{
	        $this->validate($request, [
                'service_type' => 'required',
                'date'=>'required',
                'time'=>'required',
	        ]);
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->request_id = $request->request_id;
	        $request->service_type_id = $request->service_type;
	        $request->payment_method = $request->payment_method;
	        if ($request->payment_method=='CARD') {
	        	$request->card_id = $request->card_id;
	        }
	        $request->date = $request->date;
	        $request->time = $request->time;
	        $request->isFemaleFriendly = $request->female_friendly;
        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessScheduleRide($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function tripHistoryIndex()
	{
		
		return view('pages.passenger.history.history');
	}


	public function passengerTripHistory(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
	        $request->url = $request->url;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessTripHistory($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function tripHistoryDetails(Request $request)
	{
		$request_id = $request->id;
		return view('pages.passenger.history.details',compact('request_id'));
	}

	public function passengerTripDetails(Request $request)
	{
	    try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->request_id = $request->request_id;

	        $passengerRequest = new PassengerRequestService;
	        $result = $passengerRequest->accessTripDetails($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
	    }
	    catch(\Illuminate\Database\QueryException $e){
	        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
	    }
	}

	public function scheduleTripIndex()
	{
		return view('pages.passenger.history.schedule');
	}


	public function upcommingScheduleTrip(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessUpCommingScheduleTrip($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function cancelScheduleTrip(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
	        $request->request_id = $request->request_id;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessCancelScheduleTrip($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function cancelRide(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
			$request->request_id = $request->request_id;
			$request->cancel_reason_id = $request->cancel_reason_id;
			$request->reason_optional = $request->reason_optional;
        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessCancelRideRequest($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
