<?php

namespace App\Services;

use App\Services\CurlService;


class DriverTripService
{
  private function declineRequest($data)
  {
      try{
        $curl_url = env('serverURL').'driver/request/reject';
        $method = "POST";
        $array = ['request_id'=>$data->request_id, 'cancel_reason_id'=>$data->cancel_reason_id,'reason_optional'=>$data->reason_optional];
        $json_encode = json_encode($array);
        $timeZone = $data->timeZone;
        $token = $data->token;

        $curlService = new CurlService;
        $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);

        return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  
  }


  private function tripControl($data)
  {
      try{
        $curl_url = env('serverURL').'driver/trip/control';
        $method = "POST";
        $array = ['request_id'=>$data->request_id,'tripStatus'=>$data->tripStatus];
        
        $json_encode = json_encode($array);
        $timeZone = $data->timeZone;
        $token = $data->token;

        $curlService = new CurlService;
        $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);

        return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  
  }


  private function tripPayment($data)
  {
    try{
      $curl_url = env('serverURL').'driver/trip/control';
      $method = "POST";
      $array = ['request_id'=>$data->request_id,'tripStatus'=>$data->tripStatus,'payment_method'=>$data->payment_method,'payment_dispute_reason_id'=>$data->payment_dispute_reason_id,'payment_dispute_reason_text'=>$data->payment_dispute_reason_text,'payment_report'=>$data->payment_report];
      
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      print_r($httpcode);
      curl_close($curl);
      $response=json_decode($response,true);
      dd($response);
      
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  
  }


  private function ratingComment($data)
  {
    try{
      $curl_url = env('serverURL').'driver/trip/control';
      $method = "POST";
      $array = ['request_id'=>$data->request_id,'tripStatus'=>$data->tripStatus,'rating'=>$data->rating,'comment'=>$data->comment];
      
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      print_r($httpcode);
      curl_close($curl);
      $response=json_decode($response,true);
      dd($response);
      
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  
  }


  private function tripHistory($data)
  {
      try{
          // $curl_url = env('serverURL').'driver/trip/history';
          $curl_url = $data->url;
          $method = "GET";
          $json_encode = "";
          
          $timeZone = $data->timeZone;
          $token = $data->token;

          $curlService = new CurlService;
          $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          $response = curl_exec($curl);
          $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  
          curl_close($curl);
          $response=json_decode($response,true);
      
          return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  }


  private function tripDetails($data)
  {
      try{
          $curl_url = env('serverURL').'driver/ride/details';
          $method = "POST";
          $array = ['request_id'=>$data->request_id];
          $json_encode = json_encode($array);
          
          $timeZone = $data->timeZone;
          $token = $data->token;

          $curlService = new CurlService;
          $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          $response = curl_exec($curl);
          $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          curl_close($curl);
          $response=json_decode($response,true);
      
          return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  }


  public function accessTripHistory($data)
  {
    return $this->tripHistory($data);
  }

  public function accessTripDetails($data)
  {
    return $this->tripDetails($data);
  }

  public function accessDeclineRequest($data)
  {
      return $this->declineRequest($data);
  }

  public function accessTripControl($data)
  {
      return $this->tripControl($data);
  }

  public function accessTripPayment($data)
  {
      return $this->tripPayment($data);
  }

  public function accessRatingComment($data)
  {
      return $this->ratingComment($data);
  }

}