<?php

namespace App\Services;

use App\Services\CurlService;


class SummeryService
{
	
	private function getSummeryStatus($data)
	{
		try{
	      $curl_url = env('serverURL').'driver/get/summery/statistics';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}

	private function getSummaryYearly($data)
	{
		try{
	      $curl_url = env('serverURL').'driver/get/summery/yearly';
	      $method = "POST";
	      $array = ['year'=>$data->year];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}

	private function getSummaryDateWise($data)
	{
		try{
	      $curl_url = env('serverURL').'driver/get/date/waise/summary';
	      $method = "GET";
	      $method = "POST";
	      $array = ['toDate'=>$data->toDate, 'fromDate'=>$data->fromDate];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}

	public function accessGetSummeryStatus($data)
	{
    	return $this->getSummeryStatus($data);
  	}

  	public function accessGetSummaryYearly($data)
	{
    	return $this->getSummaryYearly($data);
  	}

  	public function accessGetSummaryDateWise($data)
	{
    	return $this->getSummaryDateWise($data);
  	}

}