<?php

namespace App\Services;

use App\Services\CurlService;


class LanguageService
{
	private function passengerUpdateLang($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/update/lang';
	      $method = "PUT";
	      $array = ['lang'=>$data->lang];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function driverUpdateLang($data)
	{
	    try{
	      $curl_url = env('serverURL').'driver/update/lang';
	      $method = "PUT";
	      $array = ['lang'=>$data->lang];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
		  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
		  $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function fleetownerUpdateLang($data)
	{
	    try{
	      $curl_url = env('serverURL').'fleetowner/update/lang';
	      $method = "PUT";
	      $array = ['lang'=>$data->lang];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
		  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
		  $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getCancelReason($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/cancel/reason';
	      $method = "POST";
	      $array = ['reason_given_by'=>$data->reason_given_by];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
		  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
		  $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	public function accessGetCancelReason($data)
	{
    	return $this->getCancelReason($data);
  	}

	public function accessPassengerUpdateLang($data)
	{
    	return $this->passengerUpdateLang($data);
  	}

  	public function accessDriverUpdateLang($data)
	{
    	return $this->driverUpdateLang($data);
  	}

  	public function accessFleetownerUpdateLang($data)
	{
    	return $this->fleetownerUpdateLang($data);
  	}
  	
}