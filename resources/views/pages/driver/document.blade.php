<!-- header -->
@include('partials.driver_header')

<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.driver_sidebar')
            </div>
            <div class="col-lg-8">
                    <div class="px-sm-3 pt-5">
                        
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success changsuc_alert" style="display: none;">
                            <p class="text-success" id="changsuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.DOC_UPLOAD')}}</h2>
                        <div class="alert alert-warning document_alert" style="display: none;">
                            <p style="color: red;font-size: 20px;">{{trans('weblng.DOC_UPLOAD_MSG_REQ')}}</p>
                        </div>
                        <div class="alert alert-success document_success" style="display: none;">
                            <p style="color:#3a8418;font-size: 20px;">{{trans('weblng.DOC_UPLOAD_MSG_SUCCESS')}}</p>
                        </div>
                        <h4 class="font-weight-bold1 text-info mb-1">{{trans('weblng.DRIVER_DOC')}}</h4> 
                        <table class="table table-bordered">
                            <tbody class="driver_doc">
                                
                            </tbody>
                        </table>

                        <h4 class="font-weight-bold1 text-info mb-1">{{trans('weblng.VEHICLE_DOC')}}</h4> 
                        <table class="table table-bordered">
                            <tbody class="vehicle_doc">
                                
                            </tbody>
                        </table>

                        {{-- <h4 class="font-weight-bold1 text-info mb-1">Vehicle Image</h4> 
                        <table class="table table-bordered">
                            <tbody class="vehicle_img">
                                
                            </tbody>
                        </table> --}}

                    </div>
        
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    jq = jQuery.noConflict();
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
        jq.ajax({
            url: "{{ route('driver.document.list') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                console.log(response);

                jq('.driver_doc').empty();
                var optiond = '';
                jq.each(response.data.DriverDocuments, function(key, value) {
                    if (value.driver_document_status!='') {
                        var status = value.driver_document_status;
                    } else {
                        var status = '{{trans('weblng.MISSING')}}';
                    }
                  optiond += '<tr><td>'+value.name+'</td><td>'+status+'</td><td><div style="display: none;"><form id="'+value.id+'formdata_f" method="POST" enctype="multipart/form-data"><input type="hidden" name="document_id" id="document_id" value="'+value.id+'"><input type="file" name="file" id="'+value.id+'file"><input type="hidden" name="timeZone_f" class="timeZone_f"><button type="submit" id="'+value.id+'submit_f"></button></form></div><button class="btn btn-info" id="'+value.id+'doc_button" onclick="uploadDocument('+value.id+')">{{trans('weblng.UPLOAD')}}</button></td></tr>';
                });
                jq('.driver_doc').append(optiond);


                jq('.vehicle_doc').empty();
                var optionv = '';
                jq.each(response.data.VehicleDocuments, function(key, value) {
                    if (value.driver_document_status!='') {
                        var status = value.driver_document_status;
                    } else {
                        var status = '{{trans('weblng.MISSING')}}';
                    }
                  optionv += '<tr><td>'+value.name+'</td><td>'+status+'</td><td><div style="display: none;"><form id="'+value.id+'formdata_f" method="POST" enctype="multipart/form-data"><input type="hidden" name="document_id" id="document_id" value="'+value.id+'"><input type="file" name="file" id="'+value.id+'file"><input type="hidden" name="timeZone_f" class="timeZone_f"><button type="submit" id="'+value.id+'submit_f"></button></form></div><button class="btn btn-info" id="'+value.id+'doc_button" onclick="uploadDocument('+value.id+')">{{trans('weblng.UPLOAD')}}</button></td></tr>';
                });
                jq('.vehicle_doc').append(optionv);


                // jq('.vehicle_img').empty();
                // var optioni = '';
                // jq.each(response.data.VehicleImage, function(key, value) {
                //     if (value.driver_document_status!='') {
                //         var status = value.driver_document_status;
                //     } else {
                //         var status = '{{trans('weblng.MISSING')}}';
                //     }
                //   optioni += '<tr><td>'+value.name+'</td><td>'+status+'</td><td><div style="display: none;"><form id="'+value.id+'formdata_f" method="POST" enctype="multipart/form-data"><input type="hidden" name="document_id" id="document_id" value="'+value.id+'"><input type="file" name="file" id="'+value.id+'file"><input type="hidden" name="timeZone_f" class="timeZone_f"><button type="submit" id="'+value.id+'submit_f"></button></form></div><button class="btn btn-info" id="'+value.id+'doc_button" onclick="uploadDocument('+value.id+')">{{trans('weblng.UPLOAD')}}</button></td></tr>';
                // });
                // jq('.vehicle_img').append(optioni);

            },
            error: function(response){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message);
            }
        });

    jq(document).ajaxComplete(function(event,xhr,settings) {
        if(settings.url == "{{ route('driver.document.list') }}?timeZone=GMT%2B05%3A30")
        {
            jq('.timeZone_f').val(timeZone);  
        }
    });

    function uploadDocument(id){
        jq(document).on('click','#'+id+'doc_button',function(e) {
        // jq('#'+id+'doc_button').click(function(e) {
            jq("#"+id+"file").click();
        });

        jq(document).on('change','#'+id+'file',function() {
        // jq('#'+id+'file').change(function() {
            jq("#"+id+"submit_f").click();
        });

        jq(document).on('submit','#'+id+'formdata_f', function(event){
        // jq('#'+id+'formdata_f').on('submit', function(event){ 
            event.preventDefault();
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
                }
            });
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('driver.document.upload') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response){
                    jq(".overlay").hide();
                    $('#bootboxModal').modal('show');
                    jq('.bootboxBody').text('{{trans('weblng.DOC_UPLOAD_MESSAGE')}}');
                    jq('button.bootboxBtn').on('click', function(){
                        $("#bootboxModal").modal('hide');
                        location.reload();
                    });
                },
                error: function(response){
                    jq(".overlay").hide();
                    if (response.status == 422) {
                        var responseMsg = jq.parseJSON(response.responseText);
                        jq('.px-sm-3').removeClass('pt-5');
                        jq('.dupdate_alert').show();
                        jq('#dupdate-msg').html(responseMsg.errors.file);
                    }
                }
            });
        });
    }

</script>
<!-- footer -->
@include('partials.driver_footer')