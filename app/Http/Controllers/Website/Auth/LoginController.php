<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerAuthService;
use App\Services\DriverAuthService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Services\FleetownerAuthService;


class LoginController extends Controller
{
	public function homeIndex(Request $request)
    {
    	return view('pages.home.home');
    }

	public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

  
    public function handleProviderCallback(Request $request)
    {
        $user = Socialite::driver('google')->user();
        $request['social_unique_id'] = $user->user['id'];
        $request['login_by'] = 'google';
        $request['timeZone'] = $_COOKIE['timezoneCookie'];
        $arrayName = array('social_unique_id'=>$user->user['id'],'given_name'=>$user->user['given_name'],'family_name'=>$user->user['family_name'],'email'=>$user->user['email'],'picture'=>$user->user['picture'],'login_by'=>'google');
        $encodeData = json_encode($arrayName);
        $loginInfo = $this->socialLogin($request);

        if($loginInfo['statusCode']==401){
        	return redirect('social-signup?param='.encrypt($encodeData));
        }
       	return redirect()->route('passenger.account');
    }


    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback(Request $request)
    {
    
        $user = Socialite::driver('facebook')->user();
        // $create['name'] = $user->getName();
        // $create['email'] = $user->getEmail();
        // $create['facebook_id'] = $user->getId();
        $request['social_unique_id'] = $user->user['id'];
        $request['login_by'] = 'facebook';
        $request['timeZone'] = $_COOKIE['timezoneCookie'];

        $arrayName = array('social_unique_id'=>$user->user['id'],'given_name'=>$user->user['name'],'family_name'=>$user->nickname,'email'=>$user->user['email'],'picture'=>$user->avatar,'login_by'=>'facebook');
        $encodeData = json_encode($arrayName);
        $loginInfo = $this->socialLogin($request);

        if($loginInfo['statusCode']==401){
            return redirect('social-signup?param='.encrypt($encodeData));
        }
        return redirect()->route('passenger.account');
    }


    public function socialLogin(Request $request)
	{
		try{
	        $request['social_unique_id'] = $request->social_unique_id;
	        $request['timeZone'] = $request->timeZone;
	        $request->device_id = 'website';
	        $request->device_token = 'device_token';
	        $request->device_type = 'web';
	        $request->login_by = $request->login_by;
			
			$passengerAuth = new PassengerAuthService;
			$result = $passengerAuth->accessSocialLogin($request);

	        return ['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors'],'statusCode'=>$result['statusCode']];
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
	}


	public function index()
	{
		return view('pages.auth.login');
	}


	public function login(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'user' => 'required',
	            'mobile_no' => 'required|digits_between:5,10',
	            'password' => 'required|between:6,255',
	        ]);
	        // dd($request->device_token);
	        $request['timeZone'] = $request->timeZone;
	        $request->device_id = 'website';
	        $request->device_token = $request->device_token;
	        $request->device_type = 'web';
	        $request->login_by = 'manual';

	        $request->username = $request->mobile_no;
	        $request->isdCode = '+'.$request->isdCode;
	        $request->password = $request->password;

			if($request->user=='passenger'){
				$passengerAuth = new PassengerAuthService;
				$result = $passengerAuth->accessLogin($request);
	        }

	        if($request->user=='driver'){
	        	$driverAuth = new DriverAuthService;
				$result = $driverAuth->accessLogin($request);
	        }

            if($request->user=='fleetowner'){
                $fleetownerAuth = new FleetownerAuthService;
                $result = $fleetownerAuth->accessLogin($request);
            }

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function passengerLogout(Request $request)
	{
		try{
	        $request['timeZone'] = $request->timeZone;
	        $request['token'] = $request->access_token;
	       
			$passengerAuth = new PassengerAuthService;
			$result = $passengerAuth->accessLogout($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function driverLogout(Request $request)
	{
		try{
	        $request['timeZone'] = $request->timeZone;
	        $request['token'] = $request->access_token;
	       
   			$driverAuth = new DriverAuthService;
			$result = $driverAuth->accessLogout($request);
	    
	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

    public function fleetownerLogout(Request $request)
    {
        try{
            $request['timeZone'] = $request->timeZone;
            $request['token'] = $request->access_token;
           
            $driverAuth = new FleetownerAuthService;
            $result = $driverAuth->accessLogout($request);
        
            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }


}
