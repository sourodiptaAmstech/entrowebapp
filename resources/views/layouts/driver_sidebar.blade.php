<style>
    #image{
        cursor: pointer;
    }
</style>
<div class="shadow-lg p-3 my-5 bg-white rounded">
    <div class="d-block">
        <div class="profile-image position-relative my-3 d-flex justify-content-center align-items-center" data-toggle="tooltip" title="{{trans('weblng.SIDEBAR.EDIT_IMAGE')}}">

            <img src="{{URL::asset('/')}}storage/assets/img/download1.png" id="image" alt="Profile">

            <span class="edit-profile bg-white rounded-circle d-inline-flex justify-content-center align-items-center position-absolute" data-placement="bottom">
            <i class="fas fa-edit"></i>
            </span>

            <div style="display: none;">
            <form id="formdata" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" name="picture" id="picture">
                <input type="hidden" name="timeZone" id="timeZone">
                <button type="submit" id="submit"></button>
            </form>
            </div>

        </div>
        <a href="{{ url('driver/account') }}" data-toggle="tooltip" title="My Account">
        <h4 class="text-center font-weight-bold profile_name" style="color: grey;"></h4></a>
    </div>
    <hr class="my-4" style="width: 60%" />
    <ul class="list-group sidebar mb-4">
        {{-- <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/web.png" alt="" class="mr-3 li-icon">
            Inbox
        </li> --}}
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/business-and-finance.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('driver/wallet') ? 'active text-dark' : '' }}" href="{{ route('driver.wallet') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.WALLET')}}</a>
        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/money.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('driver/earning') ? 'active text-dark' : '' }}" href="{{ route('driver.earning') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.MYEARN')}}</a>
        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/ransaction.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('driver/transaction') ? 'active text-dark' : '' }}" href="{{ route('driver.transaction') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.TRAN')}}</a>
        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/tools-and-utensils.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('driver/trip-history') ? 'active text-dark' : '' }}{{ Request::is('driver/trip-details') ? 'active text-dark' : '' }}" href="{{ route('driver.trip-history') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.HISTORY')}}</a>
        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/driver_dashboard_transactions.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('driver/summary') ? 'active text-dark' : '' }}" href="{{ route('driver.summary') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.SUMMARY')}}</a>
        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/chat.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('driver/chat-support') ? 'active text-dark' : '' }}" href="{{ route('driver.chat-support') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.CHATSUPPORT')}}</a>
        </li>
    </ul>
</div>
<script>
jq = jQuery.noConflict();
jq(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    
    jq.ajax({
        url: "{{ route('service.list') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            jq('#service_type').empty();
            var options = '<option value="">{{trans('weblng.CHOOSE_SERVICE')}}</option>';
            jq.each(response.data, function(key, value) {
              options += '<option value="'+value.id+'">'+value.name+'</option>';
            });

            jq('#service_type').append(options);
        },
        error: function(response){
            if (response.status == 400){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#service_type').addClass('has-error');
                    });;
            }
            if (response.status == 500){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#service_type').addClass('has-error');
                    });;
            }
        }
    });

    jq(".overlay").show();
    jq.ajax({
        url: "{{ route('service.make') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            $(".overlay").hide();
            $('#car_make').empty();
            console.log(response)
            var options = '<option value="">{{trans('weblng.CHOOSE_MAKE')}}</option>';
            $.each(response.data, function(key, value) {
                console.log(value)
                options += '<option style="color:#080600;" value="'+value.make+'">'+value.make+'</option>';
            });
            $('#car_make').append(options);
        },
        error: function(response){
            $(".overlay").hide();
            if (response.status == 404){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#car_make').addClass('has-error');
                    });
            }
            if (response.status == 400){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#car_make').addClass('has-error');
                    });
            }
            if (response.status == 500){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#car_make').addClass('has-error');
                    });
            }
        }
    });

    jq('#car_make').change(function(){
        var make = jq(this).val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.model') }}",
            method: "POST",
            data: {make: make, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#car_model').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_MODEL')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.model+'">'+value.model+'</option>';
                });
                $('#car_model').append(options);
                jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                    jq('#car_model').addClass('has-error');
                });
                $('#model_year').html('');
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#car_model').html('');
                    jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                        jq('#car_model').addClass('has-error');
                    });

                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
            }
        });
    });

    jq('#car_model').change(function(){
        var model = jq(this).val();
        var make = jq('#car_make').val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.year') }}",
            method: "POST",
            data: {make: make, model: model, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#model_year').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_YEAR')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.year+'">'+value.year+'</option>';
                });
                $('#model_year').append(options);
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
            }
        });

    });


    jq(document).ajaxComplete(function(event,xhr,settings) {
        if(settings.url == "{{ route('service.make') }}?timeZone=GMT%2B05%3A30")
        {   
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('driver.profile.list') }}",
                method:"GET",
                data:{ timeZone: timeZone },
                success: function(response){
                    jq(".overlay").hide();
                    var first_name = response.data.user_profile.first_name;
                    var last_name = response.data.user_profile.last_name;
                    var profile_name = first_name.charAt(0).toUpperCase()+ first_name.slice(1)+' '+last_name.charAt(0).toUpperCase()+ last_name.slice(1);
                    jq('.profile_name').text(profile_name);
                    jq('#first_name').val(response.data.user_profile.first_name);
                    jq('#last_name').val(response.data.user_profile.last_name);
                    jq('#gender').val(response.data.user_profile.gender);
                    jq('#email_id').val(response.data.user_profile.email_id);
                    var picture = response.data.user_profile.picture;
                    if(picture!=''&&picture!=null){
                        jq("#image").attr({ "src": picture });
                        jq("#image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });

                    }
                    jq('#service_type').val(response.data.driver_services.service_type_id);

                    var make = response.data.driver_services.make;
                    var model = response.data.driver_services.model;

                    jq('#car_make').val(make);

                    if (make) {

                        jq.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $(".overlay").show();
                        jq.ajax({
                            url: "{{ route('service.model') }}",
                            method: "POST",
                            data: {make: make, timeZone: timeZone},
                            success: function(response){
                                $(".overlay").hide();
                                $('#car_model').empty();
                                console.log(response)
                                var options = '<option value="">{{trans('weblng.CHOOSE_MODEL')}}</option>';
                                $.each(response.data, function(key, value) {
                                    console.log(value)
                                    options += '<option style="color:#080600;" value="'+value.model+'">'+value.model+'</option>';
                                });
                                $('#car_model').append(options);

                                jq('#car_model').val(model);
                               
                            },
                            error: function(response){
                                $(".overlay").hide();
                                if (response.status == 404){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#car_model').addClass('has-error');
                                        });
                                }
                                if (response.status == 400){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#car_model').addClass('has-error');
                                        });
                                }
                                if (response.status == 500){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#car_model').addClass('has-error');
                                        });
                                }
                            }
                        });
                        
                    }

                    var modelY = response.data.driver_services.model_year;
                    if (model) {
                        jq.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $(".overlay").show();
                        jq.ajax({
                            url: "{{ route('service.year') }}",
                            method: "POST",
                            data: {make: make, model: model, timeZone: timeZone},
                            success: function(response){
                                $(".overlay").hide();
                                $('#model_year').empty();
                                console.log(response)
                                var options = '<option value="">{{trans('weblng.CHOOSE_YEAR')}}</option>';
                                $.each(response.data, function(key, value) {
                                    console.log(value)
                                    options += '<option style="color:#080600;" value="'+value.year+'">'+value.year+'</option>';
                                });
                                $('#model_year').append(options);
                                $('#model_year').val(modelY);
                            },
                            error: function(response){
                                $(".overlay").hide();
                                if (response.status == 404){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#model_year').addClass('has-error');
                                        });
                                }
                                if (response.status == 400){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#model_year').addClass('has-error');
                                        });
                                }
                                if (response.status == 500){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#model_year').addClass('has-error');
                                        });
                                }
                            }
                        });
                    }

                    jq('#car_number').val(response.data.driver_services.registration_no);
                },
                error: function(response){
                    jq(".overlay").hide();
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
            });
        }
    });
});

    jq('#timeZone').val(timeZone);
    jq(".profile-image").click(function(e) {
        jq("#picture").click();
    });

    function fasterPreview(uploader) {
        if ( uploader.files && uploader.files[0] ){
        jq('#image').attr('src',
        window.URL.createObjectURL(uploader.files[0]) );
        }
        jq("#image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
    }
    jq('input:file').change(function() {
        fasterPreview(this);
        jq("#submit").click();
    });
    jq('#formdata').on('submit', function(event){
        event.preventDefault();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('driver.profile.image') }}",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                jq(".overlay").hide();
                $('#bootboxModal').modal('show');
                jq('.bootboxBody').text('{{trans('weblng.PICTURE_UPDATE')}}');
                jq('button.bootboxBtn').on('click', function(){
                    $("#bootboxModal").modal('hide');
                    location.reload();
                });
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422) {
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.errors.picture);
                }
            }
        });
    });
</script>
