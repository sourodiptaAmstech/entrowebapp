@include('partials.header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="container mb-5">
    <form class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="submitContactForm">
        <h1 class="text-center"><strong>{{trans('weblng.RESET.EMERGENCY_CONTACT')}}</strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <div class="alert alert-danger emerg_alert" style="display: none;">
                <p class="text-danger" id="emerg-msg"></p>
            </div>

        	<div class="form-group">
                <label for="email_id" class="font-weight-bold">{{trans('weblng.FIELD.EMAIL')}}</label>
                @if(isset($email_id))
                    <input type="email" class="form-control inset-input" name="email_id" id="email_id" value="{{ $email_id }}" placeholder="{{trans('weblng.FIELD.EMAIL_PLACE')}}">
                @else
                    <input type="email" class="form-control inset-input" name="email_id" id="email_id" placeholder="{{trans('weblng.FIELD.EMAIL_PLACE')}}">
                @endif
                <span class="text-danger" id="email-err"></span>
            </div>
      		<h4 class="pb-2"><strong class="text-theme">{{trans('weblng.RESET.EMERGENCY_CONTACT_DETAILS')}}:</strong></h4>
            <div class="form-group">
                <label for="first_contact_name" class="font-weight-bold">{{trans('weblng.FIELD.FIRST_CONTACT_NAME')}}</label>
                <input type="text" name="first_contact_name" id="first_contact_name" value="{{ old('first_contact_name') }}" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_CONTACT_NAME_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="fname"></span>
            </div> 
            <div class="form-group">
                <label for="first_contact_mobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}</label>
                <input type="hidden" name="first_code" id="first_code">
                <input type="text" class="form-control inset-input" name="first_contact_mobile_no" id="first_contact_mobile_no" value="{{ old('first_contact_mobile_no') }}" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="fmobile"></span>
            </div>
            <div class="form-group">
                <label for="second_contact_name" class="font-weight-bold">{{trans('weblng.FIELD.SECOND_CONTACT_NAME')}}</label>
                <input type="text" class="form-control inset-input" name="second_contact_name" id="second_contact_name" value="{{ old('second_contact_name') }}" placeholder="{{trans('weblng.FIELD.SECOND_CONTACT_NAME_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="sname"></span>
            </div>
            <div class="form-group">
                <label for="second_contact_mobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}</label>
                <input type="hidden" name="second_code" id="second_code">
                <input type="text" class="form-control inset-input" name="second_contact_mobile_no" id="second_contact_mobile_no" value="{{ old('second_contact_mobile_no') }}" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="smobile"></span>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.BOOKING.SUBMIT')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="reload" style="display: none;"></i></div></button>
            </div>
        </div>
    </form>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    
    jq = jQuery.noConflict();
    use :
        var s = jq("#first_contact_mobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });
        var s = jq("#second_contact_mobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

    insteadof :
        var d = jq("#first_contact_mobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#first_code").val(d);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#first_code").val((jq("#first_contact_mobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });

        var d = jq("#second_contact_mobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#second_code").val(d);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#second_code").val((jq("#second_contact_mobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });

function validate(){
        var status=null;

        var fname = document.getElementById('first_contact_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_NAME')}}'
            document.getElementById('first_contact_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_NAME_LEAST')}}'
            document.getElementById('first_contact_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_NAME_GREATER')}}'
            document.getElementById('first_contact_name').classList.add('has-error')
            status=false
        } else {
            jq('.offset-lg-2').addClass('pt-5');
            jq('.emerg_alert').hide();
            jq('#emerg-msg').html('');
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_contact_name').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('second_contact_name').value
        if (lname == '') {
            document.getElementById("sname").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_NAME')}}'
            document.getElementById('second_contact_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("sname").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_NAME_LEAST')}}'
            document.getElementById('second_contact_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("sname").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_NAME_GREATER')}}'
            document.getElementById('second_contact_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("sname").innerHTML=''
            document.getElementById('second_contact_name').classList.remove('has-error')
        }

        var fmobileno = document.getElementById('first_contact_mobile_no').value
        if (fmobileno == '') {
            document.getElementById("fmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_MOBILE')}}'
            document.getElementById('first_contact_mobile_no').classList.add('has-error')
            status=false

        } else if (!fmobileno.match(/^([0-9\s\-\+\(\)]*)$/) || fmobileno.length<5 || fmobileno.length>10) {
            document.getElementById("fmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_MOBILE_REQ')}}'
            document.getElementById('first_contact_mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("fmobile").innerHTML=''
            document.getElementById('first_contact_mobile_no').classList.remove('has-error')
        }

     	var smobileno = document.getElementById('second_contact_mobile_no').value
        if (smobileno == '') {
            document.getElementById("smobile").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_MOBILE')}}'
            document.getElementById('second_contact_mobile_no').classList.add('has-error')
            status=false

        } else if (!smobileno.match(/^([0-9\s\-\+\(\)]*)$/) || smobileno.length<5 || smobileno.length>10) {
            document.getElementById("smobile").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_MOBILE_MSG')}}'
            document.getElementById('second_contact_mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("smobile").innerHTML=''
            document.getElementById('second_contact_mobile_no').classList.remove('has-error')
        }
        
        return status
    }

jq(document).ready(function () {
	jq('#submitContactForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var email_id = jq('#email_id').val();
        var first_contact_name = jq('#first_contact_name').val();
        var first_code = jq('#first_code').val();
        var first_contact_mobile_no = jq('#first_contact_mobile_no').val();
        var second_contact_name = jq('#second_contact_name').val();
        var second_code = jq("#second_code").val();
        var second_contact_mobile_no = $('#second_contact_mobile_no').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.passEmergencyContact') }}",
            method:"POST",
            data:{ email_id: email_id, first_contact_name: first_contact_name, first_code: first_code, first_contact_mobile_no: first_contact_mobile_no, second_contact_name: second_contact_name, second_code: second_code, second_contact_mobile_no: second_contact_mobile_no, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                location.href="{{ route('passenger.account') }}";
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_contact_name')) {
                        jq('#fname').html(responseMsg.errors.first_contact_name).promise().done(function(){
                            jq('#first_contact_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('first_contact_mobile_no')) {
                        jq('#fmobile').html(responseMsg.errors.first_contact_mobile_no).promise().done(function(){
                            jq('#first_contact_mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('second_contact_name')) {
                        jq('#sname').html(responseMsg.errors.second_contact_name).promise().done(function(){
                            jq('#second_contact_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('second_contact_mobile_no')) {
                        jq('#smobile').html(responseMsg.errors.second_contact_mobile_no).promise().done(function(){
                            jq('#second_contact_mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#email-err').html(responseMsg.message);
                    }
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.emerg_alert').show();
                    jq('#emerg-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.emerg_alert').show();
                    jq('#emerg-msg').html(responseMsg.message);
                }
            }
        });
    });
});
</script>
@include('partials.footer')