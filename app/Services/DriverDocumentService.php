<?php

namespace App\Services;

use App\Services\CurlService;


class DriverDocumentService
{
	private function documentList($data)
	{
		try{
			$curl_url = env('serverURL').'driver/document/list';
			$method = "GET";
			$json_encode = "";
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			
			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}


	private function uploadDocument($data)
	{
		try{
			$curl_url = env('serverURL').'driver/document/upload/'.$data->document_id;
			$postfields = ['file'=> new \CURLFILE($data->tmp_name,$data->type,$data->name)];

			$curlService = new CurlService;
			$curl = $curlService->accessAuthFileCurl($curl_url,$postfields,$data);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);

			if ($httpcode == 422) {
				return ['message'=>$response['message'],"field"=>$response['field'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
			}

			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}


	public function accessDocumentList($data)
	{
    	return $this->documentList($data);
  	}

  	public function accessUploadDocument($data)
	{
    	return $this->uploadDocument($data);
  	}

}