$(document).ready(function() {
    $('#datepicker1').datetimepicker({
        format: 'DD-MM-YYYY',
    });
    $('#timepicker1, #timepicker2').datetimepicker({
        format: 'hh:mm A',
    });

    $('#dateOfJourny').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: new Date()
    });
    $('#dob').datetimepicker({
        format: 'YYYY-MM-DD',
        maxDate: new Date()
    });
    $('#dateOfJournyd').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: new Date()
    });
    $('#fromTime, #toTime').datetimepicker({
        format: 'hh:mm A',
    });
    $('#fromdate, #todate').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: new Date()
    });

    $('#fromDate').datetimepicker({
        format: 'YYYY-MM-DD',
        maxDate: new Date()
    });
    $('#toDate').datetimepicker({
        format: 'YYYY-MM-DD',
        maxDate: new Date()
    });
    
    $('#selectpicker1, #selectpicker2, #languagePicker').selectpicker();
});