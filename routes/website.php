<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['ChangeLanguage']], function () {
    
    Route::get('/', function () {
        return view('index');
    })->name('index');

    //GOOGLE LOGIN
    Route::get('login/google', 'Website\Auth\LoginController@redirectToProvider')->name('login.google');
    Route::get('login/google/callback', 'Website\Auth\LoginController@handleProviderCallback')->name('login.google.callback');
    //FACEBOOK LOGIN
    Route::get('login/facebook', 'Website\Auth\LoginController@redirectToFacebook')->name('login.facebook');
    Route::get('login/facebook/callback', 'Website\Auth\LoginController@handleFacebookCallback')->name('login.facebook.callback');

    Route::get('login', 'Website\Auth\LoginController@index')->name('login');
    Route::post('loggedin', 'Website\Auth\LoginController@login')->name('loggedin');

    Route::get('social-signup', 'Website\Auth\SignupController@passengerSocialSignup')->name("social-signup");

    Route::post('passenger/social/signup/otp', 'Website\Auth\SignupController@sendOTPFirstSocialPassenger')->name("passenger.social.signup.otp");

    Route::post('passenger/social/signup', 'Website\Auth\SignupController@passengerSocialRegister')->name("passenger.social.signup");

    Route::get('forgotPassword', 'Website\Auth\ForgetPasswordController@forgetPasswordIndex')->name('forgotPassword');
    Route::post('forget/password', 'Website\Auth\ForgetPasswordController@forgetPassword')->name('forget.password');

    Route::get('resetPassword', 'Website\Auth\ForgetPasswordController@resetPasswordIndex')->name('resetPassword');
    Route::post('reset/password', 'Website\Auth\ForgetPasswordController@resetPassword')->name('reset.password');

    Route::get('signup','Website\Auth\SignupController@signup')->name('signup');

    Route::post('passenger/signup/otp','Website\Auth\SignupController@sendOTPFirstPassenger')->name('passenger.signup.otp');

    Route::post('driver/signup/otp','Website\Auth\SignupController@sendOTPFirstDriver')->name('driver.signup.otp');

    Route::post('verifyotp','Website\Auth\SignupController@verifyOTP')->name('verifyotp');

    Route::post('passenger/signup', 'Website\Auth\SignupController@passengerRegister')->name("passenger.signup");

    Route::post('driver/signup', 'Website\Auth\SignupController@driverRegister')->name("driver.signup");

    Route::post('fleetowner/signup/otp','Website\Auth\SignupController@sendOTPFirstFleetowner')->name('fleetowner.signup.otp');
    Route::post('fleetowner/signup', 'Website\Auth\SignupController@fleetownerRegister')->name("fleetowner.signup");

    Route::get('service/list', 'Website\Auth\SignupController@masterServiceList')->name("service.list");

    Route::get('model/list', 'Website\Auth\SignupController@modelList')->name("model.list");

    Route::get('service/leasing/list', 'Website\Passenger\LeasingController@serviceLeasingList')->name("service.leasing.list");

    Route::get('home', 'Website\Auth\LoginController@homeIndex')->name('home');

    Route::post('payment/verify/transaction', 'Website\Payment\PayStackController@verifyTransaction')->name("payment.verify.transaction");

    Route::get('email/verification/{param}', 'Website\Payment\PaymentController@verifyEmail')->name("email.verification");

    //vehicle service
    Route::get('service/make', 'Website\Vehicle\VehicleController@getMake')->name("service.make");
    Route::post('service/model', 'Website\Vehicle\VehicleController@getModel')->name("service.model");
    Route::post('service/year', 'Website\Vehicle\VehicleController@getYear')->name("service.year");

    Route::get('privacy/policy', 'Website\TermCondition\TermConditionController@privacyPolicy')->name('privacy.policy');
    Route::get('terms/conditions', 'Website\TermCondition\TermConditionController@termsConditions')->name('terms.conditions');

    //passenger's privacy policy for api
    Route::post('privacy/policy/get', 'Website\TermCondition\TermConditionController@GetPrivacyPolicy')->name('privacy.policy.get');
    //driver's privacy policy for api
    Route::post('privacy/policy/get/driver', 'Website\TermCondition\TermConditionController@GetPrivacyPolicyDriver')->name('privacy.policy.get.driver');

    Route::post('terms/conditions/get', 'Website\TermCondition\TermConditionController@GetTermsConditions')->name('terms.conditions.get');
    Route::post('terms/conditions/get/driver', 'Website\TermCondition\TermConditionController@GetTermsConditionsDriver')->name('terms.conditions.get.driver');

    Route::post('home/contents/get', 'Website\TermCondition\TermConditionController@GetHomeContents')->name('home.contents.get');

    Route::get('home/value/get', 'Website\TermCondition\TermConditionController@GetHomeValue')->name('home.value.get');

    Route::group(['middleware' => ['CustomAuthMiddleware']], function () {

        //=========================PASSENGER ROUTE==========================//
        Route::get('passenger/home','Website\Passenger\ProfileController@homeIndex')->name('passenger.home');

        Route::get('passenger/account', 'Website\Passenger\ProfileController@accountIndex')->name('passenger.account');

        //message
        Route::post('passenger/ride/message', 'Website\Message\MessageController@sendMessage')->name("passenger.ride.message");
        Route::post('passenger/get/message', 'Website\Message\MessageController@getMessage')->name("passenger.get.message");

        //payment
        Route::get('passenger/payment', 'Website\Payment\PaymentController@paymentIndex')->name('passenger.payment');

        Route::post('passenger/transaction/email','Website\Payment\PaymentController@UpdateTranscationEmail')->name("passenger.updateTranscationEmail");

        Route::get('passenger/card-list', 'Website\Payment\PaymentController@CheckVerifyEmail')->name("passenger.ListCard");

        Route::post('passenger/card', 'Website\Payment\PaymentController@AddTransactionCard')->name("passenger.addCard");
        Route::put('passenger/default/card', 'Website\Payment\PaymentController@setDefaultTransactionCard')->name("passenger.default.card");
        Route::delete('passenger/delete/card', 'Website\Payment\PaymentController@deleteTransactionCard')->name("passenger.delete.card");
        //..............
        Route::get('passenger/addpayment', 'Website\Payment\PaymentController@addPayment')->name("passenger.addpayment");
        Route::get('passenger/transaction', 'Website\Payment\PaymentController@transactionIndex')->name("passenger.transaction");
        Route::get('passenger/transaction/list', 'Website\Payment\PaymentController@getTransaction')->name("passenger.transaction.list");

        //leasing 
        Route::get('passenger/lease','Website\Passenger\LeasingController@passengerLeasing')->name('passenger.lease');

        Route::post('passenger/leasing/hourly', 'Website\Passenger\LeasingController@hourlyLeaseRequest')->name("passenger.leasing.hourly");

        Route::post('passenger/leasing/daily', 'Website\Passenger\LeasingController@dailyLeaseRequest')->name("passenger.leasing.daily");

        Route::post('passenger/leasing/longtime', 'Website\Passenger\LeasingController@longLeaseRequest')->name("passenger.leasing.longtime");

        Route::get('passenger/logout', 'Website\Auth\LoginController@passengerLogout')->name("passenger.logout");

        Route::get('passenger/emergencyContact', 'Website\Auth\SignupController@emergencyContactIndex')->name("passenger.emergencyContact");

        Route::post('passenger/passEmergencyContact', 'Website\Auth\SignupController@passengerEmergencyContact')->name("passenger.passEmergencyContact");

        Route::get('passenger/change/password/index', 'Website\Passenger\ChangePasswordController@changePasswordIndex')->name("passenger.change.password.index");

        Route::post('passenger/change/password', 'Website\Passenger\ChangePasswordController@changePassword')->name("passenger.change.password");

        Route::post('passenger/profile/image', 'Website\Passenger\ProfileController@profileImageUpdate')->name("passenger.profile.image");

        Route::get('passenger/profile/list', 'Website\Passenger\ProfileController@getProfile')->name("passenger.profile.list");

        Route::post('passenger/profile/update', 'Website\Passenger\ProfileController@updatePassengerProfile')->name("passenger.profile.update");

        Route::post('passenger/background/data', 'Website\Passenger\BackgroundController@passengerBackgroundData')->name("passenger.background.data");

        Route::post('passenger/get/estimated/fare','Website\Passenger\RequestController@estimated')->name("passenger.estimatedFare");

        Route::post('passenger/request/ride','Website\Passenger\RequestController@confirmRequest')->name("passenger.request");

        Route::post('passenger/request/ratingComment','Website\Passenger\RequestController@ratingComment')->name("passenger.ratingComment");

        Route::post('passenger/request/schedule/ride','Website\Passenger\RequestController@scheduleRide')->name("passenger.scheduleRide");

          // trip history
        Route::get('passenger/trip-history', 'Website\Passenger\RequestController@tripHistoryIndex')->name("passenger.trip-history");

        Route::get('passenger/trip/history', 'Website\Passenger\RequestController@passengerTripHistory')->name("passenger.trip.history");

        Route::get('passenger/trip-details', 'Website\Passenger\RequestController@tripHistoryDetails')->name("passenger.trip-details");

        Route::post('passenger/trip/details', 'Website\Passenger\RequestController@passengerTripDetails')->name("passenger.trip.details");

        Route::get('passenger/upcoming-trip', 'Website\Passenger\RequestController@scheduleTripIndex')->name("passenger.upcoming-trip");

        Route::get('passenger/upcoming/schedule', 'Website\Passenger\RequestController@upcommingScheduleTrip')->name("passenger.upcoming.scheduleTrip");

        Route::post('passenger/cancel/schedule', 'Website\Passenger\RequestController@cancelScheduleTrip')->name("passenger.cancel.schedule");

        Route::post('passenger/cancel/ride', 'Website\Passenger\RequestController@cancelRide')->name("passenger.cancel.ride");

          //admin chat support
        Route::get('passenger/chat-support', 'Website\Message\MessageController@sendPassengerMessageAdminIndex')->name("passenger.chat-support");
        Route::post('passenger/chat/support', 'Website\Message\MessageController@sendPassengerMessageAdmin')->name("passenger.chat.support");
        Route::post('passenger/chat/support/get', 'Website\Message\MessageController@getPassengerMessageAdmin')->name("passenger.chat.support.get");

        Route::post('passenger/add/promocode', 'Website\Promocode\PromoCodeController@addPromoCode')->name("passenger.add.promocode");
        Route::get('passenger/get/promocode', 'Website\Promocode\PromoCodeController@getPromoCode')->name("passenger.get.promocode");

        Route::get('passenger/promocode', 'Website\Promocode\PromoCodeController@promoCodeIndex')->name("passenger.promocode");

        Route::put('passenger/lang', 'Website\Language\LanguageController@UpdatePassengerLanguage')->name("passenger.lang");

        //get passenger's privacy policy
        Route::get('passenger/privacy/policy', 'Website\TermCondition\TermConditionController@passengerPrivacyPolicy')->name('passenger.privacy.policy');
        //get passenger's terms & conditions
        Route::get('passenger/terms/conditions', 'Website\TermCondition\TermConditionController@passengerTermsConditions')->name('passenger.terms.conditions');
        //==========================END PASSENGER ROUTE===========================//


        //==========================DRIVER ROUTE==================================//
        //get driver's privacy policy
        Route::get('driver/privacy/policy', 'Website\TermCondition\TermConditionController@driverPrivacyPolicy')->name('driver.privacy.policy');
        //get driver's terms & conditions
        Route::get('driver/terms/conditions', 'Website\TermCondition\TermConditionController@driverTermsConditions')->name('driver.terms.conditions');

        Route::get('driver/summary', 'Website\Summery\SummeryController@driverSummery')->name("driver.summary");
        Route::get('driver/get/summary/status', 'Website\Summery\SummeryController@getDriverSummeryStatus')->name("driver.get.summary.status");
        Route::post('driver/summary/yearly', 'Website\Summery\SummeryController@driverSummeryYearly')->name("driver.summary.yearly");
        Route::post('driver/summary/datewise', 'Website\Summery\SummeryController@driverSummeryDateWise')->name("driver.summary.datewise");

        Route::put('driver/lang', 'Website\Language\LanguageController@UpdateDriverLanguage')->name("driver.lang");

        Route::post('driver/cancel/ride', 'Website\Driver\ProfileController@cancelRide')->name("driver.cancel.ride");

          //admin chat support
        Route::get('driver/chat-support', 'Website\Message\MessageController@sendDriverMessageAdminIndex')->name("driver.chat-support");
        Route::post('driver/chat/support', 'Website\Message\MessageController@sendDriverMessageAdmin')->name("driver.chat.support");
        Route::post('driver/chat/support/get', 'Website\Message\MessageController@getDriverMessageAdmin')->name("driver.chat.support.get");

          // trip history
        Route::get('driver/trip-history', 'Website\Driver\TripController@tripHistoryIndex')->name("driver.trip-history");
        Route::get('driver/trip/history', 'Website\Driver\TripController@driverTripHistory')->name("driver.trip.history");
        Route::get('driver/trip-details', 'Website\Driver\TripController@tripHistoryDetails')->name("driver.trip-details");
        Route::post('driver/trip/details', 'Website\Driver\TripController@driverTripDetails')->name("driver.trip.details");

         //message
        Route::post('driver/ride/message', 'Website\Message\MessageController@sendMessageDriver')->name("driver.ride.message");
        Route::post('driver/get/message', 'Website\Message\MessageController@getMessageDriver')->name("driver.get.message");


        Route::get('driver/home','Website\Driver\ProfileController@homeIndex')->name('driver.home');

        Route::get('driver/email', 'Website\Auth\SignupController@driverEmailIndex')->name("driver.email");
        
        Route::post('driver/email/update', 'Website\Auth\SignupController@updateEmail')->name("driver.email.update");

        Route::get('driver/account', 'Website\Driver\ProfileController@accountIndex')->name('driver.account');

        Route::get('driver/map', 'Website\Driver\ProfileController@mapLocation')->name('driver.map');

        Route::get('driver/logout', 'Website\Auth\LoginController@driverLogout')->name("driver.logout");

        Route::get('driver/change/password/index', 'Website\Driver\ChangePasswordController@changePasswordIndex')->name("driver.change.password.index");

        Route::post('driver/change/password', 'Website\Driver\ChangePasswordController@changePassword')->name("driver.change.password");

        Route::post('driver/profile/image', 'Website\Driver\ProfileController@profileImageUpdate')->name("driver.profile.image");

        Route::get('driver/profile/list', 'Website\Driver\ProfileController@getProfile')->name("driver.profile.list");

        Route::post('driver/profile/update', 'Website\Driver\ProfileController@updateDriverProfile')->name("driver.profile.update");

        Route::get('driver/document', 'Website\Driver\DocumentController@document')->name("driver.document");

        Route::get('driver/document/list', 'Website\Driver\DocumentController@documentList')->name("driver.document.list");

        Route::post('driver/document/upload', 'Website\Driver\DocumentController@uploadDocument')->name("driver.document.upload");

        Route::post('driver/background/data', 'Website\Driver\BackgroundController@driversBackgroundData')->name("driver.background.data");

        Route::post('driver/request/reject', 'Website\Driver\TripController@declineRequest')->name("driver.request.reject");

        Route::post('driver/trip/control', 'Website\Driver\TripController@tripControl')->name("driver.trip.control");

        Route::post('driver/payment', 'Website\Driver\TripController@Payment')->name("driver.payment");

        Route::post('driver/request/ratingComment', 'Website\Driver\TripController@ratingComment')->name("driver.ratingComment");

        Route::put('driver/on/off/update', 'Website\Driver\ProfileController@driverOnOffStatus')->name("driver.onOff");

        Route::get('driver/wallet', 'Website\Driver\TripController@wallet')->name("driver.wallet");
        Route::get('driver/payment/statistics', 'Website\Driver\TripController@paymentStatistics')->name("driver.payment.statistics");
        Route::get('driver/earning', 'Website\Driver\TripController@earning')->name("driver.earning");

        Route::get('driver/transaction', 'Website\Payment\PaymentController@transactionDriverIndex')->name("driver.transaction");
        Route::get('driver/transaction/list', 'Website\Payment\PaymentController@getDriverTransaction')->name("driver.transaction.list");

        //get payment dispute reason
        Route::get('driver/payment/dispute/reason/get', 'Website\Payment\PaymentController@getReasonPaymentDispute')->name("driver.payment.dispute.reason.get");
        //======================END DRIVER ROUTE===============================//

        //===========================FLEET OWNER ROUTE========================//
        //get fleet's privacy policy
        Route::get('fleetowner/privacy/policy', 'Website\TermCondition\TermConditionController@fleetownerPrivacyPolicy')->name('fleetowner.privacy.policy');
        //get fleet's terms & conditions
        Route::get('fleetowner/terms/conditions', 'Website\TermCondition\TermConditionController@fleetownerTermsConditions')->name('fleetowner.terms.conditions');

        Route::get('fleetowner/home','Website\FleetOwner\ProfileController@homeIndex')->name('fleetowner.home');
        Route::get('fleetowner/logout', 'Website\Auth\LoginController@fleetownerLogout')->name("fleetowner.logout");
        Route::put('fleetowner/lang', 'Website\Language\LanguageController@UpdateFleetownerLanguage')->name("fleetowner.lang");
        Route::get('fleetowner/account', 'Website\FleetOwner\ProfileController@accountIndex')->name('fleetowner.account');
        Route::get('fleetowner/driver/account', 'Website\FleetOwner\ProfileController@driverAccountIndex')->name('fleetowner.driver.account');

        Route::post('fleetowner/driver/account/create', 'Website\FleetOwner\ProfileController@driverRegisterByFleet')->name('fleetowner.driver.account.create');

        Route::post('fleetowner/profile/image', 'Website\FleetOwner\ProfileController@profileImageUpdate')->name("fleetowner.profile.image");

        Route::get('fleetowner/profile/list', 'Website\FleetOwner\ProfileController@getProfile')->name("fleetowner.profile.list");

        Route::post('fleetowner/profile/update', 'Website\FleetOwner\ProfileController@updateFleetownerProfile')->name("fleetowner.profile.update");

        Route::get('fleetowner/change/password/index', 'Website\FleetOwner\ChangePasswordController@changePasswordIndex')->name("fleetowner.change.password.index");

        Route::post('fleetowner/change/password', 'Website\FleetOwner\ChangePasswordController@changePassword')->name("fleetowner.change.password");

        Route::get('fleetowner/driver/list', 'Website\FleetOwner\DriverController@driverIndex')->name("fleetowner.driver.list");
        Route::get('get/fleetowner/driver', 'Website\FleetOwner\DriverController@getFleetDrivers')->name("get.fleetowner.driver");

        Route::get('get/fleetowner/driver/details', 'Website\FleetOwner\DriverController@getFleetDriverDetails')->name("get.fleetowner.driver.details");

        Route::get('fleetowner/driver/update', 'Website\FleetOwner\DriverController@driverUpdateIndex')->name("fleetowner.driver.update");

        Route::post('fleetowner/driver/edit', 'Website\FleetOwner\DriverController@updateFleetDriverProfile')->name("fleetowner.driver.edit");

        Route::get('fleetowner/get/driver/statistics', 'Website\FleetOwner\DriverController@getFleetDriverStatistics')->name("fleetowner.get.driver.statistics");

        Route::get('fleetowner/earning', 'Website\FleetOwner\DriverController@earning')->name("fleetowner.earning");

        Route::get('fleetowner/statistics', 'Website\FleetOwner\DriverController@getFleetStatistics')->name("fleetowner.statistics");

        Route::post('cancel/reason/get', 'Website\Language\LanguageController@GetCancelReason')->name("cancel.reason.get");

        //==========================END FLEET OWNER ROUTE==========================//

    });
});