<!-- header -->
@include('partials.passenger_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">
                <form class="" id="profileUpdateFrom">
                    <div class="px-sm-3 pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <div class="alert alert-success updatesuc_alert" style="display: none;">
                            <p class="text-success" id="updatesuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.PERSONAL_INFO')}}</h2>
                        <div class="form-group">
                            <label for="first_name" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_FIRST_NAME')}}</label>
                            <input type="text" name="first_name" id="first_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="fname"></span>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_LAST_NAME')}}</label>
                            <input type="text" name="last_name" id="last_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.LAST_NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="lname"></span>
                        </div>
                        <div class="form-group">
                            <label for="gender" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_GENDER')}}</label>
                            <select class="form-control inset-input" name="gender" id="gender">
                                <option value="">{{trans('weblng.FIELD.Select')}}</option>
                                <option value="Male">{{trans('weblng.FIELD.MALE')}}</option>
                                <option value="Female">{{trans('weblng.FIELD.FEMALE')}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="dob" class="font-weight-bold">{{trans('weblng.FIELD.DATE_OF_BIRTH')}}</label>
                            <input type="text" class="form-control inset-input" name="dob" id="dob" placeholder="{{trans('weblng.FIELD.DATE_OF_BIRTH')}}">
                        </div>
                        <div class="form-group">
                            <label for="email_id" class="font-weight-bold">{{trans('weblng.FIELD.EMAIL_ADDRESS')}}</label>
                            <input type="email" name="email_id" id="email_id" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.EMAIL')}}">
                            <span class="text-danger" id="emailadd"></span>
                        </div>
                        <div class="form-group mb-5">
                            <label for="mobile" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}</label>
                            <input type="hidden" name="isdCode" id="isdCode">
                            <input type="text" name="mobile" id="mobile" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.MOBILE_NO')}}" readonly style="background: white;">
                        </div>


                        <h2 class="font-weight-bold mb-4">{{trans('weblng.MY_EMERGENCY_CONTACT')}}</h2>
                        <input type="hidden" name="emergOne_id" id="emergOne_id">
                        <div class="form-group">
                            <label for="first_contact_name" class="font-weight-bold">{{trans('weblng.FIELD.1ST_CONTACT_NAME')}}</label>
                            <input type="text" name="first_contact_name" id="first_contact_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="fcname"></span>
                        </div>

                        <div class="form-group">
                            <label for="first_contact_mobile_no" class="font-weight-bold">{{trans('weblng.FIELD.FIRST_CONTACT_PHONE')}}<br><span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span></label>
                            <input type="hidden" name="emergOne_isdCode" id="emergOne_isdCode">
                            <input type="text" name="first_contact_mobile_no" id="first_contact_mobile_no" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.NUMBER')}}" onkeyup="return validate()">
                            <span class="text-danger" id="fcmobile"></span>
                        </div>
                        <input type="hidden" name="emergTwo_id" id="emergTwo_id">
                        <div class="form-group">
                            <label for="second_contact_name" class="font-weight-bold">{{trans('weblng.FIELD.2ND_CONTACT_NAME')}}</label>
                            <input type="text" name="second_contact_name" id="second_contact_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="scname"></span>
                        </div>
                        <div class="form-group">
                            <label for="second_contact_mobile_no" class="font-weight-bold">{{trans('weblng.FIELD.SECOND_CONTACT_PHONE')}}<br><span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span></label>
                            <input type="hidden" name="emergTwo_isdCode" id="emergTwo_isdCode">
                            <input type="text" name="second_contact_mobile_no" id="second_contact_mobile_no" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.NUMBER')}}" onkeyup="return validate()">
                            <span class="text-danger" id="scmobile"></span>
                        </div>
                        <div id="changePassword">
                           <a href="{{ route('passenger.change.password.index') }}" class="btn btn-md grad text-white">{{trans('weblng.CHANGE_PASSWORD')}}</a> 
                        </div>

                        <div class="d-flex justify-content-start mb-3 mt-5">
                            <button type="submit" class="btn btn-lrg grad text-white">{{trans('weblng.FIELD.SAVE')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>

<script>
jq(document).ajaxComplete(function(event,xhr,settings) {
    if(settings.url == "{{ route('passenger.profile.list') }}?timeZone=GMT%2B05%3A30")
        {
    use :
        var s = jq("#first_contact_mobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

        var s = jq("#second_contact_mobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

        var s = jq("#mobile").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

    insteadof :
   
        var countryData1 = window.intlTelInputGlobals.getCountryData();
        var iso21;
        var isdcode1 = jq("#emergOne_isdCode").val();
        for (var i = 0; i < countryData1.length; i++) {
            if (countryData1[i].dialCode == parseInt(isdcode1)){
                iso21 = countryData1[i].iso2;
                break;
            }
        }
        if(iso21){
            jq("#first_contact_mobile_no").intlTelInput("setCountry", iso21);
        }
        jq(document).on('countrychange', function (e, countryData) {
            jq("#emergOne_isdCode").val((jq("#first_contact_mobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });

        var countryData2 = window.intlTelInputGlobals.getCountryData();
        var iso22;
        var isdcode2 = jq("#emergTwo_isdCode").val();
        for (var i = 0; i < countryData2.length; i++) {
            if (countryData2[i].dialCode == parseInt(isdcode2)){
                iso22 = countryData2[i].iso2;
                break;
            }
        }
        if(iso22){
            jq("#second_contact_mobile_no").intlTelInput("setCountry", iso22);
        }
        jq(document).on('countrychange', function (e, countryData) {
            jq("#emergTwo_isdCode").val((jq("#second_contact_mobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });

        var countryData3 = window.intlTelInputGlobals.getCountryData();
        var iso23;
        var isdcode3 = jq("#isdCode").val();
        for (var i = 0; i < countryData3.length; i++) {
            if (countryData3[i].dialCode == parseInt(isdcode3)){
                iso23 = countryData3[i].iso2;
                break;
            }
        }
        if(iso23){
            jq("#mobile").intlTelInput("setCountry", iso23);
        }
        jq(document).on('countrychange', function (e, countryData) {
            jq("#isdCode").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
        });
    }
});

    function validate(){
        var status=null;

        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            jq('.px-sm-3').addClass('pt-5');
            jq('.update_alert').hide();
            jq('#update-msg').html('');
            jq('.updatesuc_alert').hide();
            jq('#updatesuc-msg').html('');
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var fcname = document.getElementById('first_contact_name').value
        if (fcname == '') {
            document.getElementById("fcname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_NAME')}}'
            document.getElementById('first_contact_name').classList.add('has-error')
            status=false
        } else if (fcname.length<3) {
            document.getElementById("fcname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_NAME_LEAST')}}'
            document.getElementById('first_contact_name').classList.add('has-error')
            status=false
        } else if (fcname.length>255) {
            document.getElementById("fcname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_NAME_GREATER')}}'
            document.getElementById('first_contact_name').classList.add('has-error')
            status=false
        } else {
            document.getElementById("fcname").innerHTML=''
            document.getElementById('first_contact_name').classList.remove('has-error')
        }

        var scname = document.getElementById('second_contact_name').value
        if (scname == '') {
            document.getElementById("scname").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_NAME')}}'
            document.getElementById('second_contact_name').classList.add('has-error')
            status=false
        } else if (scname.length<3) {
            document.getElementById("scname").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_NAME_LEAST')}}'
            document.getElementById('second_contact_name').classList.add('has-error')
            status=false

        } else if (scname.length>255) {
            document.getElementById("scname").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_NAME_GREATER')}}'
            document.getElementById('second_contact_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("scname").innerHTML=''
            document.getElementById('second_contact_name').classList.remove('has-error')
        }

        var fmobileno = document.getElementById('first_contact_mobile_no').value
        if (fmobileno == '') {
            document.getElementById("fcmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_MOBILE')}}'
            document.getElementById('first_contact_mobile_no').classList.add('has-error')
            status=false

        } else if (!fmobileno.match(/^([0-9\s\-\+\(\)]*)$/) || fmobileno.length<5 || fmobileno.length>10) {
            document.getElementById("fcmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_CONTACT_MOBILE_REQ')}}'
            document.getElementById('first_contact_mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("fcmobile").innerHTML=''
            document.getElementById('first_contact_mobile_no').classList.remove('has-error')
        }

        var smobileno = document.getElementById('second_contact_mobile_no').value
        if (smobileno == '') {
            document.getElementById("scmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_MOBILE')}}'
            document.getElementById('second_contact_mobile_no').classList.add('has-error')
            status=false

        } else if (!smobileno.match(/^([0-9\s\-\+\(\)]*)$/) || smobileno.length<5 || smobileno.length>10) {
            document.getElementById("scmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.SECOND_CONTACT_MOBILE_MSG')}}'
            document.getElementById('second_contact_mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("scmobile").innerHTML=''
            document.getElementById('second_contact_mobile_no').classList.remove('has-error')
        }
        
        return status
    }

jq = jQuery.noConflict();

jq(document).ready(function () {

    //SUBMIT PASSENGER FORM.................................
    jq('#profileUpdateFrom').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var first_name = jq('#first_name').val();
        var last_name = jq('#last_name').val();
        var gender = jq('#gender').val();
        var dob = jq('#dob').val();
        var email_id = jq('#email_id').val();
        var first_contact_name = jq('#first_contact_name').val();
        var first_contact_mobile_no = jq('#first_contact_mobile_no').val();
        var emergOne_isdCode = jq('#emergOne_isdCode').val();
        var emergOne_id = jq('#emergOne_id').val();
        var second_contact_name = jq('#second_contact_name').val();
        var second_contact_mobile_no = $('#second_contact_mobile_no').val();
        var emergTwo_isdCode = jq('#emergTwo_isdCode').val();
        var emergTwo_id = jq('#emergTwo_id').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.profile.update') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, gender: gender, dob: dob, email_id: email_id, first_contact_name: first_contact_name, first_contact_mobile_no: first_contact_mobile_no,emergOne_isdCode: emergOne_isdCode, emergOne_id: emergOne_id, second_contact_name: second_contact_name, second_contact_mobile_no: second_contact_mobile_no, emergTwo_isdCode: emergTwo_isdCode, emergTwo_id: emergTwo_id,
                timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                var first_name = response.data.user_profile.first_name;
                var last_name = response.data.user_profile.last_name;
                jq('.profile_name').text(first_name.charAt(0).toUpperCase()+ first_name.slice(1)+' '+last_name.charAt(0).toUpperCase()+ last_name.slice(1));
                jq('.px-sm-3').removeClass('pt-5');
                jq('.updatesuc_alert').show();
                jq('#updatesuc-msg').html(response.message);
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('first_contact_name')) {
                        jq('#fcname').html(responseMsg.errors.first_contact_name).promise().done(function(){
                            jq('#first_contact_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('first_contact_mobile_no')) {
                        jq('#fcmobile').html(responseMsg.errors.first_contact_mobile_no).promise().done(function(){
                            jq('#first_contact_mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('second_contact_name')) {
                        jq('#scname').html(responseMsg.errors.second_contact_name).promise().done(function(){
                            jq('#second_contact_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('second_contact_mobile_no')) {
                        jq('#scmobile').html(responseMsg.errors.second_contact_mobile_no).promise().done(function(){
                            jq('#second_contact_mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#emailadd').html(responseMsg.message).promise().done(function(){
                            jq('#email_id').addClass('has-error');
                        });
                    }
                }
                if (response.status == 403){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.update_alert').show();
                    jq('#update-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.update_alert').show();
                    jq('#update-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT PASSENGER FORM.................................
});

</script>

<!-- footer -->
@include('partials.passenger_footer')