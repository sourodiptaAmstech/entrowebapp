<?php

namespace App\Services;

use App\Services\CurlService;


class PromoCodeService
{
	private function addPromo($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/add/promo';
	      $method = "POST";
	      $array = ['promo_code'=>$data->promo_code];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getPromo($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/get/promo';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	public function accessAddPromo($data)
	{
    	return $this->addPromo($data);
  	}

  	public function accessGetPromo($data)
	{
    	return $this->getPromo($data);
  	}
}