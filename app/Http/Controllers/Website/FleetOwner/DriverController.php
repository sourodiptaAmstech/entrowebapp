<?php

namespace App\Http\Controllers\Website\FleetOwner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\FleetownerService;


class DriverController extends Controller
{
    public function driverIndex(Request $request)
	{
		return view('pages.fleetowner.driverlist');
	}
	

	public function getFleetDrivers(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
			$passengerProfile = new FleetownerService;
			$result = $passengerProfile->accessGetFleetDrivers($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function getFleetDriverDetails(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$request->user_id = $request->driver_id;
			
			$passengerProfile = new FleetownerService;
			$result = $passengerProfile->accessGetFleetDriverDetails($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function driverUpdateIndex(Request $request)
	{
		//dd($request->driver);
		$driver_id = $request->driver;

		return view('pages.fleetowner.driver-update',compact('driver_id'));
	}

	public function updateFleetDriverProfile(Request $request)
	{
		try{
			$this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'gender'=>'sometimes|nullable|in:Male,Female',
                'service_type'=>'required',
                'car_make'=>'required',
                'car_model'=>'required',
                'model_year'=>'required',
                'car_number'=>'required'
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->user_id = $request->driver_id;
      		$request->first_name = $request->first_name;
      		$request->last_name = $request->last_name;
      		$request->gender = $request->gender;
      		$request->email_id = $request->email_id;
      		$request->service_type_id = $request->service_type;
			$request->make = $request->car_make;
			$request->model = $request->car_model;
      		$request->model_year = $request->model_year;
      		$request->registration_no = $request->car_number;

        	$driverProfile = new FleetownerService;
			$result = $driverProfile->accessUpdateFleetDriver($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function getFleetDriverStatistics(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$request->user_id = $request->driver_id;
			
			$passengerProfile = new FleetownerService;
			$result = $passengerProfile->accessGetFleetDriverStatistics($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function earning()
	{
		return view('pages.fleetowner.earning');
	}

	public function getFleetStatistics(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
			$FleetownerService = new FleetownerService;
			$result = $FleetownerService->accessGetFleetStatistics($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
