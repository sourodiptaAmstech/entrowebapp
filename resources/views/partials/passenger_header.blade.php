<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{trans('weblng.TITLE')}}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Font Lato -->
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/bootstrap-datetimepicker.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/bootstrap-select.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/css/main.css" crossorigin="anonymous">
        
        <link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/js/rateyo-js/jquery.rateyo.min.css"/>

    </head>
    <body>

        <div class="container-fluid">
            <div class="row">

                <!-- section main -->
                <section id="main">

                    <div class="container">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="navbar-brand ml-0" href="{{ route('passenger.home') }}">
                                <img class="logo" src="{{URL::asset('/')}}storage/assets/img/logo.png" alt="">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-5">
                                    <li class="nav-item {{ Request::is('passenger/home') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ route('passenger.home') }}">
                                            <strong>{{trans('weblng.HEADER.HOME')}}</strong>
                                        </a>
                                    </li>
                                   {{--  <li class="nav-item">
                                        <a class="nav-link px-4" href="#">
                                            <strong>Book a Ride</strong>
                                        </a>
                                    </li> --}}
                                    <li class="nav-item {{ Request::is('passenger/lease') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ route('passenger.lease') }}">
                                            <strong>{{trans('weblng.HEADER.LEASE')}}</strong>
                                        </a>
                                    </li>

                                    <li class="nav-item {{ Request::is('passenger/account') ? 'active' : '' }}">
                                        <a class="nav-link px-4" href="{{ route('passenger.account') }}">
                                            <strong>{{trans('weblng.HEADER.ACCOUNT')}}</strong>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link px-4 py-0" href="#">
                                            <strong>
                                                <select name="languagePicker" id="languagePicker">
                                                    {{-- <option value="">{{trans('weblng.HOME.CHANGE_LANG')}}</option> --}}
                                                    <option value="en">{{trans('weblng.HEADER.ENGLISH')}}</option>
                                                    <option value="fr">{{trans('weblng.HEADER.FRENCH')}}</option>
                                                </select>
                                            </strong>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link px-4" href="javascript:void(0)" id="logout"><strong>{{trans('weblng.HEADER.LOGOUT')}}</strong> <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>

                    <div id="bootboxModal" class="modal fade" data-backdrop="static">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body bootboxBody">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary bootboxBtn">{{trans('weblng.HEADER.OK')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>

    {{-- here model for map --}}
    <div class="modal fade bd-example-modal-xl" id="bookingMap" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
  aria-hidden="true">
      <div class="modal-dialog modal-xl" style="max-width: 1140px;">
        <div class="modal-content">

            <div class="row rounded-border shadow-lg bg-white">
                <div class="col-lg-8 PassengerMap">
                    <div id="map-canvas"></div>

                    <div class="inline" style="display: none;">
                        <img src="{{URL::asset('/')}}storage/assets/img/search_waves.gif" alt="" class="img-fluid rounded-border">
                    </div>

                    <div class="inline_field">
                        <div class="input-group source_map">
                            <div style="background-color: #48b741;width: 12px;height: 12px;margin-top: 13px;"></div>
                            <input type="text" name="source_map" id="source_map" class="form-control source_map" style="margin-left: 10px;" placeholder="{{trans('weblng.FIELD.SOURCE')}}">
                        </div>
                        <div class="input-group destination_map">
                            <div style="background-color:#f30a20;width: 12px;height: 12px;margin-top: 17px;border-radius: 50%;"></div>
                            <input type="text" name="destination_map" id="destination_map" class="form-control destination_map" style="margin-left: 10px;" placeholder="{{trans('weblng.FIELD.DESTINATION')}}">
                        </div>
                    </div>
                </div>
                <div id="FormOnPassengerMap" class="edit col-lg-4">

                </div>
            </div>

        </div>
      </div>
    </div>

    <!-- reason modal -->
    <div class="modal fade" id="cancelReasonModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header w-100">
                <h5 class="modal-title" id="exampleModalLabel">{{trans('weblng.CANCELREASON')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <form class="w-100" id="reasonFormSubmit">
              <div class="modal-body">
                    <div class="form-group">
                        <label for="reason" class="col-form-label">{{trans('weblng.CANCELREASON')}}</label>
                        <select class="form-control" name="reason" id="reason">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="reason_optional" class="col-form-label">{{trans('weblng.CANSONOPTIONAL')}}</label>
                        <textarea id="reason_optional" name="reason_optional" class="form-control" placeholder="{{trans('weblng.REASONOPTIONALPL')}}">
                        </textarea>
                    </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">{{trans('weblng.BOOKING.SUBMIT')}}</button>
              </div>
            </form>
            </div>
        </div>
    </div>
<!-- reason modal end -->

<div class="overlay">
    <div class="overlay__inner">
        <div class="overlay__content"><span class="spinner"></span></div>
    </div>
</div>

<link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/chatbox/css/chat.css">
<link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/chatbox/css/style.css">
<link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/chatbox/css/typing.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<style>
    .overlay {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        position: fixed;
        background: #222222c4;
        z-index:999999;
        display: none;
    }

    .overlay__inner {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        position: absolute;
    }

    .overlay__content {
        left: 50%;
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%);
    }

    .spinner {
        width: 75px;
        height: 75px;
        display: inline-block;
        border-width: 2px;
        border-color: rgba(255, 255, 255, 0.05);
        border-top-color: #fff;
        animation: spin 1s infinite linear;
        border-radius: 100%;
        border-style: solid;
    }

    @keyframes spin {
      100% {
        transform: rotate(360deg);
      }
    }
    #map-canvas {
        height: 610px;
        width: 100%;
        margin: 0px;
        padding: 0px
    }
</style>
<script>
t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');

function validate(){
    var option=document.getElementsByName('service_type');
    if (option[0].checked || option[1].checked || option[2].checked) {
        jq('.request_alert').hide();
        jq('#request-msg').html('');
        $('.home-header').addClass('py-5');
    }
}

function validateSh(){
    var option=document.getElementsByName('service_type_sh');
    if (option[0].checked || option[1].checked || option[2].checked) {
        jq('.request_alert').hide();
        jq('#request-msg').html('');
        $('.home-header').addClass('py-0');
    }
}

//map
const map = document.querySelector("#map-canvas");

function initMap() {
    const googleMap = new google.maps.Map(map, {
        center: { lat: 0, lng: 0 },
        zoom: 14
    });
    function onError() {
        map.textContent = "Unable to locate you";
    }
    function onSuccess(geo) {
        const position = {
        lat: geo.coords.latitude,
        lng: geo.coords.longitude
        };
        // Reposition the map to the detected location
        googleMap.setCenter(position);
        googleMap.setZoom(14);
        // Add a marker
        var icon = {
            url: "https://entrong.com/storage/assets/img/location@3x.png",
            scaledSize: new google.maps.Size(50, 50),
        };
        new google.maps.Marker({ 
            position,
            map: googleMap,
            icon: icon
        });
    }

    if (!navigator.geolocation) {
        onError();
    } else {
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }
}
//End Map

jq = jQuery.noConflict();

jq(document).ready(function(){
    var clearMap=0;
    function acceptForm(request_id,name,picture,overall_rating,image,model,registration_no,estimatedTimeToReached,destinationAdd,sourceAdd){
        $('.inline').hide();
        $('#source_map').val(sourceAdd);
        $('#destination_map').val(destinationAdd);
        //$('.inline_field').html('');
        $('#FormOnPassengerMap').html(`
            <style>
            
            </style>
            <div class="col-lg-12 px-0 d-flex align-items-center" id="acceptForm">
            <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                <div class="py-5 px-md-4 home-header">
                    <div class="row mb-3 mt-3 border-bottom">
                        <h6 class="font-weight-bold mb-2">
                            <span class="bg-primary rounded-circle li-icon-sm d-inline-block mr-1"></span>`+name+`
                            <span class="text-muted">{{trans('weblng.BOOKING.ACCEPTED_REQ')}}</span>
                        </h6>
                    </div>

                    <div class="row border-bottom my-2">
                        <div class="col-sm-6">

                            <div class="profile-image position-relative my-3 d-flex justify-content-center align-items-center">
                                <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Profile" id="profile_image">
                            </div>

                            <h4 class="text-center font-weight-bold">`+name+`</h4>
                            <div id="rateYo"></div> 
                        </div>
                        <div class="col-sm-6">
                            <div class="service-image position-relative my-3 d-flex justify-content-center align-items-center">
                                <img src="`+image+`" alt="Profile">
                            </div>
                            <h5 class="text-center mb-2">`+model+`</h5>
                            <h6 class="text-center text-muted">`+registration_no+`</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h6 class="text-center text-muted mb-2">{{trans('weblng.BOOKING.ARRIVING_IN')}} `+estimatedTimeToReached+` mins.</h6>
                        </div>
                        <div class="col-sm-3 px-2 py-1">
                            <div class="h-100 w-100 grad rounded-circle map-link" style="width: 55px !important;height: 55px !important;object-fit: cover;border-radius: 50% !important;padding:12px;">
                                <img src="{{URL::asset('/')}}storage/assets/img/phone.png" alt="call">
                            </div>
                        </div>

                        <div class="col-sm-3 px-2 py-1">

        <div class="chatbox">
            <div class="chatbox__support">
                <div class="chatbox__header" style="width:100%">
                    <div class="chatbox__image--header" style="margin-left:0">

                        <div class="profile-image position-relative d-flex justify-content-center align-items-center">
                                <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Chat" id="chat_image">
                        </div>

                    </div>
                    <div class="chatbox__content--header" style="margin-left:0; margin-right:50%">
                        <h4 class="chatbox__heading--header">`+name+`</h4>
                        <p class="chatbox__description--header"></p>
                    </div>
                    
                </div>
                <div class="chatbox__messages" style="width:100%">
                    <div id="text_message" style="width:100%">
                        
                        <div class="messages__item messages__item--typing">
                            <span class="messages__dot"></span>
                            <span class="messages__dot"></span>
                            <span class="messages__dot"></span>
                        </div>
                    </div>
                </div>
                <div class="chatbox__footer" style="width:100%">
                    <form id="messageForm" style="width:100%">
                    <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                    <input type="text" name="message" id="message" placeholder="{{trans('weblng.BOOKING.WRITE_MSG')}}..." autocomplete="off" required style="width:85%">
                    <button type="submit" class="chatbox__send--footer float-right"><i class="fas fa-paper-plane"></i></button>
                    </form>
                </div>
            </div>
            

            <div class="h-100 w-100 grad rounded-circle map-link chatbox__button">
                <img src="{{URL::asset('/')}}storage/assets/img/chat-btn.png" alt="chat">
            </div>

        </div>
   
                        </div>

                        <div class="col-sm-6 px-2 py-2">
                            <button type="button" class="btn btn-lg grad text-white w-100 my-3" id="cancel" style="font-size: 16px;">{{trans('weblng.BOOKING.CANCEL_RIDE')}}</button>
                        </div>

                    </div>
                </div>
            </div>



            `);
    }

    function reachForm(request_id,name,picture,overall_rating,image,model,registration_no,destinationAdd,sourceAdd){
        // $('.inline').hide();
        // $('.inline_field').html('');
        $('#source_map').val(sourceAdd);
        $('#destination_map').val(destinationAdd);
        $('#FormOnPassengerMap').html(`
            <div class="col-lg-12 px-0 d-flex align-items-center" id="reachForm">
             <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                <div class="py-5 px-md-4 home-header">
                    <div class="row mb-3 mt-3 border-bottom">
                        <h6 class="font-weight-bold mb-2">
                            <span class="bg-primary rounded-circle li-icon-sm d-inline-block mr-1"></span>`+name+`
                            <span class="text-muted">{{trans('weblng.BOOKING.REACHED_LOCATION')}}</span>
                        </h6>
                    </div>

                    <div class="row border-bottom my-2">
                        <div class="col-sm-6">
                            <div class="profile-image position-relative my-3 d-flex justify-content-center align-items-center">
                                <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Profile" id="profile_image">
                            </div>
                            <h4 class="text-center font-weight-bold">`+name+`</h4>
                            <div id="rateYo"></div> 
                        </div>
                        <div class="col-sm-6">
                            <div class="service-image position-relative my-3 d-flex justify-content-center align-items-center">
                                <img src="`+image+`" alt="Profile">
                            </div>
                            <h5 class="text-center mb-2">`+model+`</h5>
                            <h6 class="text-center text-muted">`+registration_no+`</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h6 class="text-center text-muted mb-2"></h6>
                        </div>
                        <div class="col-sm-3 px-2 py-1">
                            <div class="h-100 w-100 grad rounded-circle map-link" style="width: 55px !important;height: 55px !important;object-fit: cover;border-radius: 50% !important;padding:12px;">
                                <img src="{{URL::asset('/')}}storage/assets/img/phone.png" alt="call">
                            </div>
                        </div>
                        <div class="col-sm-3 px-2 py-1">


        <div class="chatbox">
            <div class="chatbox__support">
                <div class="chatbox__header" style="width:100%;">
                    <div class="chatbox__image--header" style="margin-left:0">
                        <div class="profile-image position-relative d-flex justify-content-center align-items-center">
                                <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Chat" id="chat_image">
                        </div>
                    </div>
                    <div class="chatbox__content--header" style="margin-left:0; margin-right:50%">
                        <h4 class="chatbox__heading--header">`+name+`</h4>
                        <p class="chatbox__description--header"></p>
                    </div>

                </div>
                <div class="chatbox__messages" style="width:100%">
                    <div id="text_message" style="width:100%">
                        
                        <div class="messages__item messages__item--typing">
                            <span class="messages__dot"></span>
                            <span class="messages__dot"></span>
                            <span class="messages__dot"></span>
                        </div>
                    </div>
                </div>
                <div class="chatbox__footer" style="width:100%;">
                    
                    <form id="messageForm" style="width:100%">
                    <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                    <input type="text" name="message" id="message" placeholder="{{trans('weblng.BOOKING.WRITE_MSG')}}..." autocomplete="off" required style="width:85%">
                    <button type="submit" class="chatbox__send--footer float-right"><i class="fas fa-paper-plane"></i></button>
                    </form>
                   
                </div>
            </div>
            
            <div class="h-100 w-100 grad rounded-circle map-link chatbox__button">
                <img src="{{URL::asset('/')}}storage/assets/img/chat-btn.png" alt="chat">
            </div>

        </div>


                        </div>

                        <div class="col-sm-6 px-2 py-2">
                            <button type="button" class="btn btn-lg grad text-white w-100 my-3" id="cancel" style="font-size: 16px;">{{trans('weblng.BOOKING.CANCEL_RIDE')}}</button>
                        </div>

                    </div>
                </div>
            </div>
            `);
    }

    function startedForm(name,picture,overall_rating,image,model,registration_no,destinationAdd,sourceAdd){
        // $('.inline').hide();
        // $('.inline_field').html('');
        $('#source_map').val(sourceAdd);
        $('#destination_map').val(destinationAdd);
        $('#FormOnPassengerMap').html(`
            <div class="col-lg-12 px-0 d-flex align-items-center" id="startedForm">
                <div class="py-5 px-md-4 home-header">
                    <div class="row mb-3 mt-3 border-bottom">
                        <h6 class="font-weight-bold mb-2">
                            <span class="bg-primary rounded-circle li-icon-sm d-inline-block mr-1"></span>
                            <span class="text-muted">{{trans('weblng.BOOKING.ON_RIDE')}}</span>
                        </h6>
                    </div>

                    <div class="row border-bottom my-2">
                        <div class="col-sm-6">
                            <div class="profile-image position-relative my-3 d-flex justify-content-center align-items-center">
                                <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Profile" id="profile_image">
                            </div>
                            <h4 class="text-center font-weight-bold">`+name+`</h4>
                            <div id="rateYo"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="service-image position-relative my-3 d-flex justify-content-center align-items-center">
                                <img src="`+image+`" alt="Profile">
                            </div>
                            <h5 class="text-center mb-2">`+model+`</h5>
                            <h6 class="text-center text-muted">`+registration_no+`</h6>
                        </div>
                    </div>

                </div>
            </div>`);
    }


    function tripInvoiceForm(basefare,distanceFare,waitTimeCost,tax,total,promo_value,cost,sourceAdd,destinationAdd){
        // $('.inline').hide();
        // $('.inline_field').html('');
        $('#source_map').val(sourceAdd);
        $('#destination_map').val(destinationAdd);
        $('#FormOnPassengerMap').html(`<div class="col-lg-12 px-0 d-flex align-items-center" id="tripInvoiceForm">
            <div class="py-5 px-md-4 home-header" style="margin: 0 !important;width: 100%;display: block;">

                <div class="row mb-3">
                    <div class="col-sm-12 mx-0 px-0">
                       <p class="text-center font-weight-bold">{{trans('weblng.INVOICE.TRIP_INVOICE')}}</p>
                    </div>
                </div>

                <div class="row mb-0">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">
                            Base fare:
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-right">`+basefare+`</h6>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">
                            {{trans('weblng.INVOICE.DIST_FARE')}}:
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-right">`+distanceFare+`</h6>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">
                            {{trans('weblng.INVOICE.WAIT_FEE')}}:
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-right">`+waitTimeCost+`</h6>
                    </div>
                </div>
                <div class="row mb-0 border-bottom">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">
                            {{trans('weblng.INVOICE.TAX')}}:
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-right">`+tax+`</h6>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">
                            {{trans('weblng.INVOICE.SUB_TOTAL')}}:
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-right">`+total+`</h6>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">
                             {{trans('weblng.INVOICE.PROMO_DISCOUNT')}}:
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-right">- `+promo_value+`</h6>
                    </div>
                </div>

                <p class="text-center mb-1"><strong>{{trans('weblng.INVOICE.TOTAL')}}</strong></p>
                <div class="row border-bottom">
                    <p class="text-center mb-1"><strong>`+cost+`</strong></p>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-6 mx-0 px-0">
                        <img src="{{URL::asset('/')}}storage/assets/img/cash.png" alt="call" style="width: 22px;">
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted text-right">{{trans('weblng.INVOICE.CASH')}}</h6>
                    </div>
                </div>

            </div>
        </div>`);
    }

    function ratingRequestForm(name,picture,request_id){
        $('#FormOnPassengerMap').html(
            `<div class="col-lg-12 px-0 d-flex align-items-center" id="ratingRequestForm">

                <div class="py-4 px-md-4 home-header w-100">
                    <div class="col-lg-12 w-100">
                        <button type="button" class="close" id="PassClose" title="cancel" aria-label="Close">
                            <img src="{{URL::asset('/')}}storage/assets/img/signs.png" alt="" class="m-close">
                        </button>
                    </div>
                    <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                    <div class="alert alert-danger prating w-100 mt-5" style="display: none;">
                        <p class="text-danger" id="prating-msg"></p>
                        <p class="text-danger" id="pcomment-msg"></p>
                    </div>
                    <h4 class="text-center">{{trans('weblng.BOOKING.RATE_TRIP')}}</h4>

                    <div class="profile-image position-relative my-3 d-flex justify-content-center align-items-center">
                        <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Profile" id="profile_image">
                    </div>
                    <p class="text-center font-weight-bold">`+name+`</p>
                    <form id="ratingForm">
                        <div id="rateYo"></div>
                        <input type="hidden" name="rating" id="rating">
                        <div class="border-bottom pt-5">
                            <input type="text" name="comment" id="comment" placeholder="{{trans('weblng.BOOKING.GIVE_FEEDBACK')}}" class="input_field" style="padding-top:15px;">
                        </div>

                        <div class="col-sm-10 px-3 py-1" style="width:180px;">
                            <button type="submit" class="btn btn-lg grad text-white w-100 h-100 my-4">{{trans('weblng.BOOKING.SUBMIT')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>`);
    }


    function BackgroundRecursive(){
        
        window.navigator.geolocation.getCurrentPosition((position) => {
            lat = position.coords.latitude;
            lng = position.coords.longitude;

            jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var device_latitude = lat;
            console.log('lat='+device_latitude);
            var device_longitude = lng;
            console.log('lng='+device_longitude);
            jq.ajax({
                url: "{{ route('passenger.background.data') }}",
                method:"POST",
                data:{ device_latitude: device_latitude, device_longitude: device_longitude, timeZone: timeZone },
                success: function(response){
                    console.log(response.data.status);

                    switch (response.data.status) {

                        case "NOSERVICEFOUND":
                            $("#bookingMap").modal('hide');
                            $('#source_map').val('');
                            $('#destination_map').val('');
                            $('#content_form').html('');
                            $('.inline').hide();

                            $('.home-header').removeClass('py-5');
                            $('.home-header').addClass('py-4');
                            $('.home_alert').show();
                            $('#home-msg').html(response.data.messsage);
                            break;

                        case "ACCEPTED":
                            clearMap=1
                            var request_id = response.data.rideDetails.requestDetials.request_id;
                            var name = response.data.rideDetails.driverProfile.first_name+' '+response.data.rideDetails.driverProfile.last_name;
                            var picture = response.data.rideDetails.driverProfile.picture;
                            var registration_no = response.data.rideDetails.driverService.registration_no;
                            var model = response.data.rideDetails.driverService.model;
                            var image = response.data.rideDetails.ServiceDetails.image;
                            var overall_rating = response.data.rideDetails.driverProfile.overall_rating;
                            var estimatedTimeToReached = response.data.rideDetails.estimatedTimeToReached;
                            var source_latitude = response.data.rideDetails.location_details.source.latitude;
                            var source_longitude = response.data.rideDetails.location_details.source.longitude;
                            var destination_latitude = response.data.rideDetails.location_details.destination.latitude;
                            var destination_longitude = response.data.rideDetails.location_details.destination.longitude;

                            var destinationAdd = response.data.rideDetails.location_details.destination.address;
                            var sourceAdd = response.data.rideDetails.location_details.source.address;
                            
                            if($("#acceptForm").length==0){
                                $("#bookingMap").modal('show');
                                acceptForm(request_id,name,picture,overall_rating,image,model,registration_no,estimatedTimeToReached,destinationAdd,sourceAdd);
                                var iconS = {
                                    url: "https://entrong.com/storage/assets/img/location-2@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                var iconD = {
                                    url: "https://entrong.com/storage/assets/img/location-1@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                function mapLocation() {
                                    var directionsDisplay;
                                    var directionsService = new google.maps.DirectionsService();
                                    var map;
                                    function initialize() {
                                        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                                        var source = new google.maps.LatLng(source_latitude,source_longitude);
                                        var mapOptions = {
                                            zoom: 14,
                                            center: source
                                        };
                                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                                        directionsDisplay.setMap(map);
                                        calcRoute();
                                    }
                                    function calcRoute() {
                                        var start = new google.maps.LatLng(source_latitude,source_longitude);
                                        var end = new google.maps.LatLng(destination_latitude,destination_longitude);
                                        var bounds = new google.maps.LatLngBounds();
                                        bounds.extend(start);
                                        bounds.extend(end);
                                        map.fitBounds(bounds);

                                        var markerS = new google.maps.Marker({
                                            position: start,
                                            map: map,
                                            icon: iconS
                                        });
                                        var infowindowS = new google.maps.InfoWindow({
                                            content: sourceAdd
                                            });
                                        infowindowS.open(map,markerS);

                                        var markerD = new google.maps.Marker({
                                            position: end,
                                            map: map,
                                            icon: iconD
                                        });
                                        var infowindowD = new google.maps.InfoWindow({
                                            content: destinationAdd
                                            });
                                        infowindowD.open(map,markerD);
                                        
                                        var request = {
                                            origin: start,
                                            destination: end,
                                            travelMode: google.maps.TravelMode.DRIVING
                                        };
                                        directionsService.route(request, function (response, status) {
                                            if (status == google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setDirections(response);
                                                directionsDisplay.setMap(map);
                                            } else {
                                                alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                                            }
                                        });
                                    }
                                    initialize();
                                }
                                mapLocation();
                                if(picture!=''){
                                    jq("#profile_image").attr({ "src": picture });
                                    jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                                    jq("#chat_image").attr({ "src": picture });
                                }
                                $("[style*='custom-label']").css({'text-shadow': '2px 2px #000'})
                                $(function() {
                                    $("#rateYo").rateYo({ 
                                        rating: overall_rating, 
                                        spacing: "5px", 
                                        numStars: 5, 
                                        minValue: 0, 
                                        maxValue: 5, 
                                        normalFill: '#a6a6a6', 
                                        ratedFill: 'orange',
                                        starWidth: "23px",
                                    });
                                });

                                chatBoxJS(request_id);
                            }
                            getMessage(request_id);
                            break;

                        case "REACHED":
                            clearMap=1
                            var request_id = response.data.rideDetails.requestDetials.request_id;
                            var name = response.data.rideDetails.driverProfile.first_name+' '+response.data.rideDetails.driverProfile.last_name;
                            var picture = response.data.rideDetails.driverProfile.picture;
                            var registration_no = response.data.rideDetails.driverService.registration_no;
                            var model = response.data.rideDetails.driverService.model;
                            var image = response.data.rideDetails.ServiceDetails.image;
                            var overall_rating = response.data.rideDetails.driverProfile.overall_rating;
                            var source_latitude = response.data.rideDetails.location_details.source.latitude;
                            var source_longitude = response.data.rideDetails.location_details.source.longitude;
                            var destination_latitude = response.data.rideDetails.location_details.destination.latitude;
                            var destination_longitude = response.data.rideDetails.location_details.destination.longitude;
                            var destinationAdd = response.data.rideDetails.location_details.destination.address;
                            var sourceAdd = response.data.rideDetails.location_details.source.address;
                            
                            if($("#reachForm").length==0){
                                $("#bookingMap").modal('show');
                                reachForm(request_id,name,picture,overall_rating,image,model,registration_no,destinationAdd,sourceAdd);
                                var iconS = {
                                    url: "https://entrong.com/storage/assets/img/location-2@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                var iconD = {
                                    url: "https://entrong.com/storage/assets/img/location-1@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                function mapLocation() {
                                    var directionsDisplay;
                                    var directionsService = new google.maps.DirectionsService();
                                    var map;
                                    function initialize() {
                                        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                                        var source = new google.maps.LatLng(source_latitude,source_longitude);
                                        var mapOptions = {
                                            zoom: 14,
                                            center: source
                                        };
                                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                                        directionsDisplay.setMap(map);
                                        calcRoute();
                                    }
                                    function calcRoute() {
                                        var start = new google.maps.LatLng(source_latitude,source_longitude);
                                        var end = new google.maps.LatLng(destination_latitude,destination_longitude);
                                        var bounds = new google.maps.LatLngBounds();
                                        bounds.extend(start);
                                        bounds.extend(end);
                                        map.fitBounds(bounds);

                                        var markerS = new google.maps.Marker({
                                            position: start,
                                            map: map,
                                            icon: iconS
                                        });
                                        var infowindowS = new google.maps.InfoWindow({
                                            content: sourceAdd
                                            });
                                        infowindowS.open(map,markerS);

                                        var markerD = new google.maps.Marker({
                                            position: end,
                                            map: map,
                                            icon: iconD
                                        });
                                        var infowindowD = new google.maps.InfoWindow({
                                            content: destinationAdd
                                            });
                                        infowindowD.open(map,markerD);

                                        var request = {
                                            origin: start,
                                            destination: end,
                                            travelMode: google.maps.TravelMode.DRIVING
                                        };
                                        directionsService.route(request, function (response, status) {
                                            if (status == google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setDirections(response);
                                                directionsDisplay.setMap(map);
                                            } else {
                                                alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                                            }
                                        });
                                    }
                                    initialize();
                                }
                                mapLocation();
                                if(picture!=''){
                                    jq("#profile_image").attr({ "src": picture });
                                    jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                                    jq("#chat_image").attr({ "src": picture });
                                }
                                $(function() {
                                    $("#rateYo").rateYo({ 
                                        rating: overall_rating, 
                                        spacing: "5px", 
                                        numStars: 5, 
                                        minValue: 0, 
                                        maxValue: 5, 
                                        normalFill: '#a6a6a6', 
                                        ratedFill: 'orange',
                                        starWidth: "23px",
                                    });
                                });
                                chatBoxJS(request_id);
                            }
                            getMessage(request_id);
                            break;

                        case "STARTED":
                            clearMap=1
                            var name = response.data.rideDetails.driverProfile.first_name+' '+response.data.rideDetails.driverProfile.last_name;
                            var picture = response.data.rideDetails.driverProfile.picture;
                            var registration_no = response.data.rideDetails.driverService.registration_no;
                            var model = response.data.rideDetails.driverService.model;
                            var image = response.data.rideDetails.ServiceDetails.image;
                            var overall_rating = response.data.rideDetails.driverProfile.overall_rating;
                            var source_latitude = response.data.rideDetails.location_details.source.latitude;
                            var source_longitude = response.data.rideDetails.location_details.source.longitude;
                            var destination_latitude = response.data.rideDetails.location_details.destination.latitude;
                            var destination_longitude = response.data.rideDetails.location_details.destination.longitude;
                            var destinationAdd = response.data.rideDetails.location_details.destination.address;
                            var sourceAdd = response.data.rideDetails.location_details.source.address;

                            if($("#startedForm").length==0){
                                $("#bookingMap").modal('show');
                                startedForm(name,picture,overall_rating,image,model,registration_no,destinationAdd,sourceAdd);
                                var iconS = {
                                    url: "https://entrong.com/storage/assets/img/location-2@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                var iconD = {
                                    url: "https://entrong.com/storage/assets/img/location-1@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                function mapLocation() {
                                    var directionsDisplay;
                                    var directionsService = new google.maps.DirectionsService();
                                    var map;
                                    function initialize() {
                                        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                                        var source = new google.maps.LatLng(source_latitude,source_longitude);
                                        var mapOptions = {
                                            zoom: 14,
                                            center: source
                                        };
                                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                                        directionsDisplay.setMap(map);
                                        calcRoute();
                                    }
                                    function calcRoute() {
                                        var start = new google.maps.LatLng(source_latitude,source_longitude);
                                        var end = new google.maps.LatLng(destination_latitude,destination_longitude);
                                        var bounds = new google.maps.LatLngBounds();
                                        bounds.extend(start);
                                        bounds.extend(end);
                                        map.fitBounds(bounds);

                                        var markerS = new google.maps.Marker({
                                            position: start,
                                            map: map,
                                            icon: iconS
                                        });
                                        var infowindowS = new google.maps.InfoWindow({
                                            content: sourceAdd
                                            });
                                        infowindowS.open(map,markerS);

                                        var markerD = new google.maps.Marker({
                                            position: end,
                                            map: map,
                                            icon: iconD
                                        });
                                        var infowindowD = new google.maps.InfoWindow({
                                            content: destinationAdd
                                            });
                                        infowindowD.open(map,markerD);

                                        var request = {
                                            origin: start,
                                            destination: end,
                                            travelMode: google.maps.TravelMode.DRIVING
                                        };
                                        directionsService.route(request, function (response, status) {
                                            if (status == google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setDirections(response);
                                                directionsDisplay.setMap(map);
                                            } else {
                                                alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                                            }
                                        });
                                    }
                                    initialize();
                                }
                                mapLocation();
                                if(picture!=''){
                                    jq("#profile_image").attr({ "src": picture });
                                    jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                                }
                                $(function() {
                                    $("#rateYo").rateYo({ 
                                        rating: overall_rating, 
                                        spacing: "5px", 
                                        numStars: 5, 
                                        minValue: 0, 
                                        maxValue: 5, 
                                        normalFill: '#a6a6a6', 
                                        ratedFill: 'orange',
                                        starWidth: "23px",
                                    });
                                });
                            }
                            break;

                        case "DROP":
                            clearMap=1
                            var currency = response.data.rideDetails.payable.currency;
                            var basefare = currency+' '+response.data.rideDetails.payable.break_up.basefare;
                            var distanceFare = currency+' '+response.data.rideDetails.payable.break_up.distanceFare;
                            var waitTimeCost = currency+' '+response.data.rideDetails.payable.break_up.waitTimeCost;
                            var tax = currency+' '+response.data.rideDetails.payable.break_up.tax;
                            var total = currency+' '+response.data.rideDetails.payable.break_up.total;
                            var promo_value = currency+' '+response.data.rideDetails.payable.break_up.promo_code_value;
                            var cost = currency+' '+response.data.rideDetails.payable.cost;

                            var source_latitude = response.data.rideDetails.location_details.source.latitude;
                            var source_longitude = response.data.rideDetails.location_details.source.longitude;
                            var destination_latitude = response.data.rideDetails.location_details.destination.latitude;
                            var destination_longitude = response.data.rideDetails.location_details.destination.longitude;
                            var destinationAdd = response.data.rideDetails.location_details.destination.address;
                            var sourceAdd = response.data.rideDetails.location_details.source.address;
                            if($("#tripInvoiceForm").length==0){
                                $("#bookingMap").modal('show');   
                                tripInvoiceForm(basefare,distanceFare,waitTimeCost,tax,total,promo_value,cost,sourceAdd,destinationAdd);
                                var iconS = {
                                    url: "https://entrong.com/storage/assets/img/location-2@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                var iconD = {
                                    url: "https://entrong.com/storage/assets/img/location-1@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                function mapLocation() {
                                    var directionsDisplay;
                                    var directionsService = new google.maps.DirectionsService();
                                    var map;
                                    function initialize() {
                                        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                                        var source = new google.maps.LatLng(source_latitude,source_longitude);
                                        var mapOptions = {
                                            zoom: 14,
                                            center: source
                                        };
                                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                                        directionsDisplay.setMap(map);
                                        calcRoute();
                                    }
                                    function calcRoute() {
                                        var start = new google.maps.LatLng(source_latitude,source_longitude);
                                        var end = new google.maps.LatLng(destination_latitude,destination_longitude);
                                        var bounds = new google.maps.LatLngBounds();
                                        bounds.extend(start);
                                        bounds.extend(end);
                                        map.fitBounds(bounds);

                                        var markerS = new google.maps.Marker({
                                            position: start,
                                            map: map,
                                            icon: iconS
                                        });
                                        var infowindowS = new google.maps.InfoWindow({
                                            content: sourceAdd
                                            });
                                        infowindowS.open(map,markerS);

                                        var markerD = new google.maps.Marker({
                                            position: end,
                                            map: map,
                                            icon: iconD
                                        });
                                        var infowindowD = new google.maps.InfoWindow({
                                            content: destinationAdd
                                            });
                                        infowindowD.open(map,markerD);

                                        var request = {
                                            origin: start,
                                            destination: end,
                                            travelMode: google.maps.TravelMode.DRIVING
                                        };
                                        directionsService.route(request, function (response, status) {
                                            if (status == google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setDirections(response);
                                                directionsDisplay.setMap(map);
                                            } else {
                                                alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                                            }
                                        });
                                    }
                                    initialize();
                                }
                                mapLocation();
                            }

                            break;

                        case "PAYMENT":
                            clearMap=1
                            var name = response.data.rideDetails.driverProfile.first_name+' '+response.data.rideDetails.driverProfile.last_name;
                            var picture = response.data.rideDetails.driverProfile.picture;
                            var request_id = response.data.rideDetails.requestDetials.request_id;
                            var passenger_rating_status = response.data.rideDetails.requestDetials.passenger_rating_status;
                            var destination_latitude = response.data.rideDetails.location_details.destination.latitude;
                            var destination_longitude = response.data.rideDetails.location_details.destination.longitude;
                            if($("#ratingRequestForm").length==0){
                                $("#bookingMap").modal('show');
                                ratingRequestForm(name,picture,request_id);
                                var icon = {
                                    url: "https://entrong.com/storage/assets/img/location@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                function mapLocation() {
                                    var directionsDisplay;
                                    var directionsService = new google.maps.DirectionsService();
                                    var map;
                                    function initialize() {
                                        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                                        var source = new google.maps.LatLng(destination_latitude,destination_longitude);
                                        var mapOptions = {
                                            zoom: 14,
                                            center: source
                                        };
                                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                                        directionsDisplay.setMap(map);
                                        const position = {
                                            lat: destination_latitude,
                                            lng: destination_longitude
                                            };
                                        new google.maps.Marker({ 
                                            position,
                                            map: map,
                                            icon: icon
                                        });
                                    }
                                    initialize();
                                }
                                mapLocation();
                                if(picture!=''){
                                    jq("#profile_image").attr({ "src": picture });
                                    jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                                }
                                $(function () {
                                    $("#rateYo").rateYo().on("rateyo.set", function (e, data) {
                                        $('#rating').val(data.rating);
                                    });
                                });
                            }
                            break;

                        case "COMPLETED":
                            clearMap=1
                            var name = response.data.rideDetails.driverProfile.first_name+' '+response.data.rideDetails.driverProfile.last_name;
                            var picture = response.data.rideDetails.driverProfile.picture;
                            var request_id = response.data.rideDetails.requestDetials.request_id;
                            var passenger_rating_status = response.data.rideDetails.requestDetials.passenger_rating_status;
                            if($("#ratingRequestForm").length==0){
                                $("#bookingMap").modal('show');
                                ratingRequestForm(name,picture,request_id);
                                if(picture!=''){
                                    jq("#profile_image").attr({ "src": picture });
                                    jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                                }
                                $(function () {
                                    $("#rateYo").rateYo().on("rateyo.set", function (e, data) {
                                        $('#rating').val(data.rating);
                                    });
                                });
                            }
                            break;

                        case "NORMAL":
                            console.log(response.data.nearByDriver.length)
                            if(response.data.nearByDriver.length!=0){
                                var icon = {
                                    url: "https://entrong.com/storage/assets/img/location@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                var iconDL = {
                                    url: "https://entrong.com/storage/assets/img/Car@3x.png",
                                    scaledSize: new google.maps.Size(50, 50),
                                };
                                function mapLocation() {
                                    var directionsDisplay;
                                    var directionsService = new google.maps.DirectionsService();
                                    var map;
                                    function initialize() {
                                        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                                        var source = new google.maps.LatLng(device_latitude,device_longitude);
                                        var mapOptions = {
                                            zoom: 14,
                                            center: source
                                        };
                                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                                        directionsDisplay.setMap(map);
                                        const position = {
                                            lat: device_latitude,
                                            lng: device_longitude
                                            };
                                        new google.maps.Marker({ 
                                            position,
                                            map: map,
                                            icon: icon
                                        });
                                        $.each(response.data.nearByDriver, function(key, value) {
                                            var driverLoc = new google.maps.LatLng(value.latitude,value.longitude);
                                            new google.maps.Marker({
                                                position: driverLoc,
                                                map: map,
                                                icon: iconDL
                                            });
                                        });
                                    }
                                    initialize();
                                }
                                mapLocation();

                            }
                            if(clearMap==1)
                            {
                                var nearByDriver = response.data.nearByDriver;
                               
                                clearMap=0;
                                location.reload();
                            }
                            break;
                    }
                    setTimeout(function(){
                        BackgroundRecursive();

                    }, 5000);
                },
                error: function(response){
                    console.log(response);
                    setTimeout(function(){
                        BackgroundRecursive();
                    }, 5000);
                }
            });
        });
    }


    jq(document).on('submit','#requestConfirmForm',function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var service_type = jq("input[type=radio][name=service_type]:checked").val();
        var request_id = jq('#request_id').val();
        if ($('#female_friendly').is(":checked"))
        {
            var female_friendly = 'Y';
        } else {
            var female_friendly = 'N';
        }
        var payment_method = jq('#payment_method').val();
        var card_id = jq('#card_id').val();
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.request') }}",
            method: "POST",
            data: { service_type:service_type, request_id:request_id,female_friendly:female_friendly, payment_method:payment_method,card_id:card_id, timeZone:timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").hide();
                $('.inline').show();
                //show cancel button
                $('.Confirm').hide();
                $('.Cancel').show()
                $(document).on('click','#cancel1', function(event){
                    event.preventDefault();
                    
                    CancelRide(request_id,cancel_reason_id=null,reason_optional=null);
                });
            },
            error: function(response){
                console.log(response);
                $(".overlay").hide();
                var responseMsg = jq.parseJSON(response.responseText);
                $('.home-header').removeClass('py-5');
                $('.home-header').addClass('py-3');
                jq('.request_alert').show();
                jq('#request-msg').html(responseMsg.errors.service_type);
            }
        });
    });


    jq(document).on('submit','#requestConfirmFormSchedule',function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var service_type = jq("input[type=radio][name=service_type_sh]:checked").val();
        var request_id = jq('#request_id').val();
        var date = jq('#date').val();
        var time = jq('#time').val();
        
        if ($('#female_friendly').is(":checked"))
        {
            var female_friendly = 'Y';
        } else {
            var female_friendly = 'N';
        }
        var payment_method = jq('#payment_method').val();
        var card_id = jq('#card_id').val();
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.scheduleRide') }}",
            method: "POST",
            data: { service_type:service_type, request_id:request_id, payment_method:payment_method, card_id:card_id, date:date, time:time, female_friendly:female_friendly, timeZone:timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").hide();
                $("#bookingMap").modal('hide');
                $('#source_map').val('');
                $('#destination_map').val('');
                $('#content_form').html('');

                $('.home-header').removeClass('py-5');
                $('.home-header').addClass('py-4');
                $('.home_alert_suc').show();
                $('#home-msg-suc').html(response.message);
            },
            error: function(response){
                console.log(response);
                $(".overlay").hide();
                var responseMsg = jq.parseJSON(response.responseText);
                if (responseMsg.errors.hasOwnProperty('service_type')) {
                    $('.home-header').removeClass('py-5');
                    $('.home-header').addClass('py-3');
                    jq('.request_alert').show();
                    jq('#request-msg').html(responseMsg.errors.service_type);
                }
                if (responseMsg.errors.hasOwnProperty('date')) {
                    jq('#sdate').html(responseMsg.errors.date).promise().done(function(){
                        jq('#date').addClass('has-error');
                    });
                }
                if (responseMsg.errors.hasOwnProperty('time')) {
                    jq('#stime').html(responseMsg.errors.time).promise().done(function(){
                        jq('#time').addClass('has-error');
                    });
                }
            }
        });
    });
    
    $(document).on('click','#PassClose', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'RATING';
        var rating = 0;
        var comment = 'N.A';
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.ratingComment') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, rating: rating, comment: comment, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                // $('.PassengerMap').removeClass('col-lg-8');
                // $('.PassengerMap').addClass('col-lg-12');
                // $('#FormOnPassengerMap').html('');
                $("#bookingMap").modal('hide');
                location.reload();
            },
            error: function(response){
                $(".overlay").hide();
            }
        });
    });

    $(document).on('submit','#ratingForm', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var tripStatus = 'RATING';
        var rating = $('#rating').val();
        var comment = $('#comment').val();
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.ratingComment') }}",
            method:"POST",
            data:{ request_id: request_id, tripStatus: tripStatus, rating: rating, comment: comment, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                // $('.PassengerMap').removeClass('col-lg-8');
                // $('.PassengerMap').addClass('col-lg-12');
                // $('#FormOnPassengerMap').html('');
                $("#bookingMap").modal('hide');
                location.reload();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('rating')) {
                        $('.prating').show();
                        $('#prating-msg').html(responseMsg.errors.rating);
                    }
                    if (responseMsg.errors.hasOwnProperty('comment')) {
                        $('.prating').show();
                        $('#pcomment-msg').html(responseMsg.errors.comment);
                    }
                }
            }
        });
    });

    $(document).on('submit','#messageForm', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var message = $('#message').val();
        var user_scope = 'passenger-service';
        jq.ajax({
            url: "{{ route('passenger.ride.message') }}",
            method:"POST",
            data:{ request_id: request_id, user_scope: user_scope, message: message, timeZone: timeZone },
            success: function(response){
                console.log(response);

                getMessage(request_id);
                $('#message').val('');
            },
            error: function(response){
            }
        });
    });


    function CancelRide(request_id,cancel_reason_id,reason_optional){
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.cancel.ride') }}",
            method:"POST",
            data:{ request_id: request_id, cancel_reason_id: cancel_reason_id, reason_optional:reason_optional, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                $("#bookingMap").modal('hide');
                location.reload();
            },
            error: function(response){
                $(".overlay").hide();
            }
        });
    }

    $(document).on('click','#cancel', function(event){
        event.preventDefault();
        $('#cancelReasonModal').modal('show');
        var reason_given_by = 'passenger';
        jq.ajax({
            url: "{{ route('cancel.reason.get') }}",
            method: "POST",
            data: {reason_given_by: reason_given_by, timeZone: timeZone},
            success: function(response){
                $('#reason').empty();
                var options = '<option value="">{{trans('weblng.SELECTREASON')}}</option>';
                $.each(response.data, function(key, value) {
                  options += '<option value="'+value.cancel_reason_id+'">'+value.reason+'</option>';
                });

                $('#reason').append(options);
            },
            error: function(response){
                // if (response.status == 400){
                //     var responseMsg = jq.parseJSON(response.responseText);
                //     jq('.offset-lg-2').removeClass('pt-5');
                //     jq('.signup_alert').show();
                //     jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                //             jq('#service_type').addClass('has-error');
                //         });
                // }
                // if (response.status == 500){
                //     var responseMsg = jq.parseJSON(response.responseText);
                //     jq('.offset-lg-2').removeClass('pt-5');
                //     jq('.signup_alert').show();
                //     jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                //             jq('#service_type').addClass('has-error');
                //         });
                // }
            }
        });

        //CancelRide(request_id)

    });

    jq('#reasonFormSubmit').on('submit', function(event){
        event.preventDefault();
        var cancel_reason_id = $('#reason').val();
        var reason_optional = $('#reason_optional').val();
        var request_id = $('#request_id').val();
        CancelRide(request_id,cancel_reason_id,reason_optional);
    });




    jq('#logout').on('click', function(event){
        event.preventDefault();
        bootbox.confirm('{{trans('weblng.HEADER.ARE_YOU_SURE')}}', function (res) {
            if (res){
                $(".overlay").show();
                jq.ajax({
                    url: "{{ route('passenger.logout') }}",
                    method:"GET",
                    data:{ timeZone: timeZone },
                    success: function(response){
                        $(".overlay").hide();
                        document.cookie = "entroSecurity= ; expires=0; path=/;";
                        document.cookie = "entroSignature= ; expires=0; path=/;";
                        location.href="{{route('login')}}";
                    },
                    error: function(response){
                        $(".overlay").hide();
                    }
                });
            }
        });
    });

    var cookieValue = Cookies.get('languageCookie');
    if (cookieValue=='') {
        $('#languagePicker').val('en');
    } else {
        $('#languagePicker').val(cookieValue);
    }
    $('#languagePicker').change(function(){
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var language = $(this).val()
        document.cookie = "languageCookie="+language+"; expires=0; path=/;";
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.lang') }}",
            method:"PUT",
            data:{ language: language, timeZone: timeZone },
            success: function(response){
                location.reload()
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
            }
        });
    })

    BackgroundRecursive();
});

function chatBoxJS(request_id){
    class InteractiveChatbox {
        constructor(a, b, c) {
            this.args = {
                button: a,
                chatbox: b
            }
            this.icons = c;
            this.state = false; 
        }

        display() {
            const {button, chatbox} = this.args;
            
            button.addEventListener('click', () => this.toggleState(chatbox))
        }

        toggleState(chatbox) {
            this.state = !this.state;
            this.showOrHideChatBox(chatbox, this.args.button);
        }

        showOrHideChatBox(chatbox, button) {
            if(this.state) {
                chatbox.classList.add('chatbox--active')
                this.toggleIcon(true, button);
            } else if (!this.state) {
                chatbox.classList.remove('chatbox--active')
                this.toggleIcon(false, button);
            }
        }

        toggleIcon(state, button) {
            const { isClicked, isNotClicked } = this.icons;
            let b = button.children[0].innerHTML;

            if(state) {
                button.children[0].innerHTML = isClicked; 
            } else if(!state) {
                button.children[0].innerHTML = isNotClicked;
            }
        }
    }

    const chatButton = document.querySelector('.chatbox__button');
    const chatContent = document.querySelector('.chatbox__support');
    const icons = {
        isClicked: '<img src="./images/icons/chatbox-icon.svg" />',
        isNotClicked: '<img src="./images/icons/chatbox-icon.svg" />'
    }
    const chatbox = new InteractiveChatbox(chatButton, chatContent, icons);
    chatbox.display();
    chatbox.toggleIcon(false, chatButton);

    getMessage(request_id);
}

timeAgo = (date) => {
    var ms = (new Date()).getTime() - date.getTime();
    var seconds = Math.floor(ms / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);
    var months = Math.floor(days / 30);
    var years = Math.floor(months / 12);
    if (ms === 0) {
        return '{{trans('weblng.CHAT_MESSAGE.JUST_NOW')}}';
    } if (seconds < 0) {
        return '0 {{trans('weblng.CHAT_MESSAGE.SECOUND_AGO')}}';
    } if (seconds === 0 || seconds === 1) {
        return seconds + ' {{trans('weblng.CHAT_MESSAGE.SECOUND_AGO')}}';
    } if (seconds < 60 && seconds > 1) {
        return seconds + ' {{trans('weblng.CHAT_MESSAGE.SECOUNDS_AGO')}}';
    } if (minutes === 1) {
        return minutes + ' {{trans('weblng.CHAT_MESSAGE.MIN_AGO')}}';
    } if (minutes < 60 && minutes > 1) {
        return minutes + ' {{trans('weblng.CHAT_MESSAGE.MINS_AGO')}}';
    } if (hours === 1) {
        return hours + ' {{trans('weblng.CHAT_MESSAGE.HR_AGO')}}';
    } if (hours < 24 && hours > 1) {
        return hours + ' {{trans('weblng.CHAT_MESSAGE.HRS_AGO')}}';
    } if (days === 1) {
        return '{{trans('weblng.CHAT_MESSAGE.YESTERDAY')}}';
    } else {
        return moment(date).format('YYYY-MMM-DD');
    }
}

function getMessage(request_id){
     jq.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var request_id=request_id;
    jq.ajax({
            url: "{{ route('passenger.get.message') }}",
            method:"POST",
            data:{ request_id: request_id, timeZone: timeZone },
            success: function(response){
                console.log(response.data);
                $('#text_message').html('');
                var options='';
                $.each(response.data, function(key, value) {
                    console.log(value.user_scope);
                    if (value.user_scope=='passenger-service') {
                        options+=`<div class="messages__item messages__item--operator" style="margin-right:0">
                            <p class="text-muted pt-0 text-right">`+value.fname+` `+value.lname+`</p>
                            <p class="py-1" style="color: black;">`+value.message+`</p>
                            <p class="text-left text-muted pb-0" style="font-size: 11px;">`+timeAgo(new Date(value.updated_at))+`</p>
                        </div>`;
                    }
                    if (value.user_scope=='driver-service') {
                        options+=`<div class="messages__item messages__item--visitor" style="margin-left:0">
                            <p class="text-muted pt-0">`+value.fname+` `+value.lname+`</p>
                            <p class="py-1" style="color: black;">`+value.message+`</p>
                            <p class="text-right text-muted pb-0" style="font-size: 11px;">`+timeAgo(new Date(value.updated_at))+`</p>
                        </div>`;
                    }
                });
                $('#text_message').append(options);
                
            },
            error: function(response){
            }
        });
}
</script>
