<?php

namespace App\Http\Controllers\Website\Summery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\SummeryService;


class SummeryController extends Controller
{
	public function driverSummery()
	{
		return view('pages.driver.summery');
	}

	public function getDriverSummeryStatus(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$summeryService = new SummeryService;
			$result = $summeryService->accessGetSummeryStatus($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function driverSummeryYearly(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->year = $request->year;

			$summeryService = new SummeryService;
			$result = $summeryService->accessGetSummaryYearly($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function driverSummeryDateWise(Request $request)
	{
		try{
			$this->validate($request, [
                'fromDate' => 'required',
                'toDate' => 'required',
            ]);
            if($request->fromDate>$request->toDate){
	       		return response(['message'=>'To date cannot be before the From date',"data"=>(object)[],"errors"=>(object)[]],401);
	       	}
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->fromDate = $request->fromDate;
			$request->toDate = $request->toDate;

			$summeryService = new SummeryService;
			$result = $summeryService->accessGetSummaryDateWise($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
