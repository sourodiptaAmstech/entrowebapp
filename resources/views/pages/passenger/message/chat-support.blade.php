
<!-- header -->
@include('partials.passenger_header')
<link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/chatbox/css/chat.css">
<link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/chatbox/css/style.css">
<link rel="stylesheet" href="{{URL::asset('/')}}storage/assets/chatbox/css/typing.css">
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">
                <div class="px-sm-3 pt-5">
                    <h2 class="font-weight-bold mb-0">
                       {{trans('weblng.CHATADMIN')}}
                    </h2>
                    <div class="d-block mt-3 border rounded p-4">
                        {{-- <div id="chatPassengerMsg" style="overflow-y: auto;background-size: cover; height: 450px; overflow-x: hidden;">
                            
                        </div> --}}
                        
                        <div class="chatbox__messages" style="width:100%;height: 390px;">
                            <div id="text_message" style="width:100%;margin-bottom: 4px;">
                                
                                <div class="messages__item messages__item--typing">
                                    <span class="messages__dot"></span>
                                    <span class="messages__dot"></span>
                                    <span class="messages__dot"></span>
                                </div>
                            </div>
                        </div>
                        
                    
                        <form class="input-group" id="messageFormPassenger">
                            {{-- <form id="messageForm"> --}}
                                {{-- <div class="input-group-prepend">
                                    <span class="input-group-text border-0">
                                    <img src="{{URL::asset('/')}}storage/assets/img/emoji_emotions-24px.svg" alt="">
                                    </span>
                                </div> --}}
                                <input type="text" name="message" id="message" class="form-control border-0 input-group-text p-3" required placeholder="{{trans('weblng.BOOKING.WRITE_MSG')}}..." autocomplete="off">
                                <div class="input-group-append">
                                    <span class="input-group-text border-0">
                                    <button type="submit">
                                        <img src="{{URL::asset('/')}}storage/assets/img/send-24px.svg" alt="">
                                    </button>
                                    </span>
                                </div>
                            {{-- </form> --}}

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    timeAgo = (date) => {
        var ms = (new Date()).getTime() - date.getTime();
        var seconds = Math.floor(ms / 1000);
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        var days = Math.floor(hours / 24);
        var months = Math.floor(days / 30);
        var years = Math.floor(months / 12);
        if (ms === 0) {
            return '{{trans('weblng.CHAT_MESSAGE.JUST_NOW')}}';
        } if (seconds < 0) {
            return '0 {{trans('weblng.CHAT_MESSAGE.SECOUND_AGO')}}';
        } if (seconds === 0 || seconds === 1) {
            return seconds + ' {{trans('weblng.CHAT_MESSAGE.SECOUND_AGO')}}';
        } if (seconds < 60 && seconds > 1) {
            return seconds + ' {{trans('weblng.CHAT_MESSAGE.SECOUNDS_AGO')}}';
        } if (minutes === 1) {
            return minutes + ' {{trans('weblng.CHAT_MESSAGE.MIN_AGO')}}';
        } if (minutes < 60 && minutes > 1) {
            return minutes + ' {{trans('weblng.CHAT_MESSAGE.MINS_AGO')}}';
        } if (hours === 1) {
            return hours + ' {{trans('weblng.CHAT_MESSAGE.HR_AGO')}}';
        } if (hours < 24 && hours > 1) {
            return hours + ' {{trans('weblng.CHAT_MESSAGE.HRS_AGO')}}';
        } if (days === 1) {
            return '{{trans('weblng.CHAT_MESSAGE.YESTERDAY')}}';
        } else {
            return moment(date).format('YYYY-MMM-DD');
        }
    }

    $(document).on('submit','#messageFormPassenger', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var message = $('#message').val();
        var user_scope = 'passenger-service';
        jq.ajax({
            url: "{{ route('passenger.chat.support') }}",
            method:"POST",
            data:{ user_scope: user_scope, message: message, timeZone: timeZone },
            success: function(response){
                console.log(response);
                getMessageP()
                $('#message').val('');
            },
            error: function(response){
            }
        });
    });

    getMessageP()

    function getMessageP(){
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var user_scope='passenger-service';
        jq.ajax({
            url: "{{ route('passenger.chat.support.get') }}",
            method:"POST",
            data:{ user_scope: user_scope, timeZone: timeZone },
            success: function(response){
                console.log(response.data);
                // $('#chatPassengerMsg').html('');
                $('#text_message').html('');
                var options='';
                $.each(response.data, function(key, value) {

                    if (value.user_scope=='passenger-service') {
                        options+=`
                        <div class="messages__item messages__item--operator" style="margin-right:0">
                            <p class="text-muted pt-0 text-right">`+value.fname+` `+value.lname+`</p>
                            <p class="py-1" style="color: black;">`+value.message+`</p>
                            <p class="text-left text-muted pb-0" style="font-size: 11px;">`+timeAgo(new Date(value.updated_at))+`</p>
                        </div>`;
                    }
                    if (value.user_scope=='admin-service') {
                        options+=`
                        <div class="messages__item messages__item--visitor" style="margin-left:0">
                            <p class="text-muted pt-0">`+value.fname+` `+value.lname+`</p>
                            <p class="py-1" style="color: black;">`+value.message+`</p>
                            <p class="text-right text-muted pb-0" style="font-size: 11px;">`+timeAgo(new Date(value.updated_at))+`</p>
                        </div>`;
                    }

                    // if (value.user_scope=='admin-service') {
                    //     if (value.picture != null) {
                    //         var adminPic = `<div class="rounded-circle bg-light" style="height: 50px; width: 50px">
                    //                         <img  class="h-100 w-100 rounded-circle" style="object-fit: cover"
                    //                         src="{{URL::asset('/')}}`+value.picture+`" alt="">
                    //                     </div>`;

                    //     }else{
                    //         var adminPic = `<div class="rounded-circle bg-light" style="height: 50px; width: 50px">
                    //                         <img  class="h-100 w-100 rounded-circle" style="object-fit: cover"
                    //                         src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="">
                    //                     </div>`;
                    //     }
                    //     options+=`
                    //         <div class="row justify-content-start">
                    //             <div class="col-md-10 mx-0 mb-3">
                    //                 <div class="row">
                    //                     <div class="col-2 d-flex align-items-end">
                    //                         <div class="rounded-circle bg-light" style="height: 50px; width: 50px">
                    //                             <img  class="h-100 w-100 rounded-circle" style="object-fit: cover"
                    //                         src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="">
                    //                         </div>
                    //                     </div>
                    //                     <div class="col-10 rounded-theme-left shadow-lg bg-light">
                                            // <p class="text-muted pt-1">`+value.fname+` `+value.lname+`</p>
                                            // <p class="py-2">`+value.message+`</p>
                    //                         <p class="text-right text-muted pb-3"></p>
                    //                     </div>
                    //                 </div>
                    //             </div>
                    //         </div>`;

                    // }
                    // if (value.user_scope=='passenger-service') {
                    //     options+=`
                    //         <div class="row justify-content-end">
                    //             <div class="col-md-10 mx-0 mb-3">
                    //                 <div class="row">
                    //                     <div class="col-10 rounded-theme-right shadow-lg bg-info-theme mr-3">
                    //                         <p class="py-3">`+value.message+`</p>
                    //                         <p class="text-right text-muted pb-3"></p>
                    //                     </div>
                    //                 </div>
                    //             </div>
                    //         </div>`;
                    // }

                });
                // $('#chatPassengerMsg').append(options);
                $('#text_message').append(options);
                
                setTimeout(function(){
                  getMessageP()
                }, 5000);
                
            },
            error: function(response){
            }
        });
    }
</script>

<!-- footer -->
@include('partials.passenger_footer')