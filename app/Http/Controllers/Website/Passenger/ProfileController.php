<?php

namespace App\Http\Controllers\Website\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerProfileService;


class ProfileController extends Controller
{
	public function homeIndex()
	{
		return view('pages.home.passenger_home');
	}

	public function accountIndex(Request $request)
	{
		return view('pages.passenger.myaccount');
	}

	public function profileImageUpdate(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'picture' => 'required|mimes:jpeg,bmp,png',
	        ]);
	        
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $tmp_name = $_FILES['picture']['tmp_name'];
	        $type = $_FILES['picture']['type'];
	        $name = basename($_FILES['picture']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
			$request['name'] = $name;
			$request['size']=$_FILES['picture']['size'];
			
			$passengerProfile = new PassengerProfileService;
			$result = $passengerProfile->accessProfileImageUpdate($request);
		
			if ($result['statusCode']==422) {
				return response(['message'=>$result['message'],"field"=>$result['field'],"errors"=>$result['errors']],$result['statusCode']);
			}
	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function getProfile(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$passengerProfile = new PassengerProfileService;
			$result = $passengerProfile->accessGetProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function updatePassengerProfile(Request $request)
	{
		try{
			$this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'gender'=>'sometimes|nullable|in:Male,Female',
                'first_contact_name' => 'required|min:3|max:255',
	            'first_contact_mobile_no' => 'required|digits_between:5,10',
	            'second_contact_name' => 'required|min:3|max:255',
	            'second_contact_mobile_no' => 'required|digits_between:5,10',
            ]);

			$request->first_name = $request->first_name;
			$request->last_name = $request->last_name;
			$request->email_id = $request->email_id;
			$request->gender = $request->gender;
			$request->dob = $request->dob;
			$request->emergOne_id = $request->emergOne_id;
	        $request->emergOne_isdCode = '+'.(integer)$request->emergOne_isdCode;
			$request->emergOne_contact_no = $request->first_contact_mobile_no; 
			$request->emergOne_name = $request->first_contact_name;
			$request->emergTwo_id = $request->emergTwo_id;
			$request->emergTwo_isdCode = '+'.(integer)$request->emergTwo_isdCode;
			$request->emergTwo_contact_no = $request->second_contact_mobile_no; 
			$request->emergTwo_name = $request->second_contact_name;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			

        	$driverProfile = new PassengerProfileService;
			$result = $driverProfile->accessUpdateProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


}
