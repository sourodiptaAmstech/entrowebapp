<!-- header -->
@include('partials.header')

<div class="container mb-5">
    <div class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg" style="height: 393px;">
        <h1 class="text-center"><strong style="color: dodgerblue;line-height: revert;">Thank you for verifying your email!</strong></h1>
        <h6 class="text-center" style="line-height: revert;color: black;">You have successfully verified your email and completed your account set-up.</h6>

        {{-- <div class="w-100 d-flex justify-content-center mt-5">
            <a href="javascript:void(0)" class="btn btn-info">Back to Dashboard</a>
        </div> --}}
    </div>
</div>
<!-- footer -->
@include('partials.footer')