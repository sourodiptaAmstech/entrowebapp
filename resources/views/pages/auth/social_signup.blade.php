@include('partials.header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="container mb-5">
    <div class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg">
        <h1 class="text-center"><strong>{{trans('weblng.SIGN_UP.SOCIAL_SIGNUP_TO')}}<span class="text-theme">{{trans('weblng.LOGIN.RIDE')}}</span></strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <div class="alert alert-danger signup_alert" style="display: none;">
                <p class="text-danger" id="signup-msg"></p>
            </div>

        <form id="submitPassengerForm">
            <input type="hidden" name="social_unique_id" id="social_unique_id" value="{{ $arrayName->social_unique_id }}">
            <input type="hidden" name="picture" id="picture" value="{{ $arrayName->picture }}">
            <input type="hidden" name="login_by" id="login_by" value="{{ $arrayName->login_by }}">
            <input type="hidden" name="email_id" id="email_id" value="{{ $arrayName->email }}">
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="fname" class="font-weight-bold">{{trans('weblng.FIELD.FIRST_NAME')}}</label>
                    <input type="text" name="first_name" id="first_name" value="{{ $arrayName->given_name }}" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME_PLACE')}}" onkeyup="return validate()" >
                    <span class="text-danger" id="fname"></span>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label for="last_name" class="font-weight-bold">{{trans('weblng.FIELD.LAST_NAME')}}</label>
                    <input type="text" class="form-control inset-input" name="last_name" id="last_name" value="{{ $arrayName->family_name }}" placeholder="{{trans('weblng.FIELD.LAST_NAME_PLACE')}}" onkeyup="return validate()" >
                    <span class="text-danger" id="lname"></span>
                </div>
                </div>
            </div>
          
            <div class="form-group">
                <label for="mobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}</label>
                <input type="hidden" name="code" id="code">
                <input type="text" class="form-control inset-input" name="mobile_no" id="mobile_no" value="{{ old('mobile_no') }}" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validate()" >
                <span class="text-danger" id="mobile"></span>
            </div>
           
            {{-- <div class="form-group">
                <label for="email_id" class="font-weight-bold">Email Address</label>
                <input type="email" class="form-control inset-input" name="email_id" id="email_id" value="{{ $arrayName->email }}" placeholder="Enter Email Address" onkeyup="return validate()" >
                <span class="text-danger" id="email"></span>
            </div> --}}
          
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.HEADER.SIGNUP')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="reload" style="display: none;"></i></div></button>
            </div>
        </form>

        </div>
    </div>
    <!--Hidden-->
    <input type="hidden" name="hidden_otp" id="hidden_otp">
    {{-- <input type="hidden" name="hidden_user" id="hidden_user"> --}}
    <!-- Modal -->
    <div class="modal fade" id="verificationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="p-2 w-100">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="{{URL::asset('/')}}storage/assets/img/signs.png" alt="" class="m-close">
            </button>
        </div>
        <form id="modalOTP">
            <div class="modal-body w-100">
                <h1 class="text-center"><strong>{{trans('weblng.SIGN_UP.VERIFY_ACCOUNT')}} <span class="text-theme">{{trans('weblng.SIGN_UP.ACCOUNT')}}</span></strong></h1>
                <p class="text-center text-muted my-2">{{trans('weblng.SIGN_UP.OTP_MESSAGES')}}</p>
                <div class="row justify-content-center my-3">
                    <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
                </div>
                <p class="text-center my-2">{{trans('weblng.FIELD.OTP')}}</p>
                <p class="text-center text-danger my-1" id="otperror"></p>
                <div class="d-flex">
                    <div class="col-md-8 offset-md-2">
                    <input type="text" class="form-control inset-input" name="otp" id="otp" placeholder="{{trans('weblng.FIELD.OTP_PLACE')}}" onkeyup="return validateOTP()">
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-5">
                <button type="submit" class="btn btn-lrg grad text-white mx-auto">{{trans('weblng.BOOKING.SUBMIT')}}</button>
            </div>
        </form>
        </div>
    </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
<script>
    jq = jQuery.noConflict();
    use :
        var s = jq("#mobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

    insteadof :
        var d = jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#code").val(d);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#code").val((jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });


    function validate(){
        var status=null;

        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var mobileno = document.getElementById('mobile_no').value
        if (mobileno == '') {
            document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE')}}'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else if (!mobileno.match(/^([0-9\s\-\+\(\)]*)$/) || mobileno.length<5 || mobileno.length>10) {
            document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE_REQ')}}'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("mobile").innerHTML=''
            document.getElementById('mobile_no').classList.remove('has-error')
        }

        return status
    }

    function validateOTP(){
        var otp = document.getElementById('otp').value;
        if (!otp.match(/^([0-9\s\-\+\(\)]*)$/) || otp.length>4) {
            document.getElementById("otperror").innerHTML='{{trans('weblng.VALIDATION_MSG.OTP_VALID')}}';
            return false;
        } else {
            document.getElementById("otperror").innerHTML='';
            return true;
        }
    }

jq(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

//MODEL OTP FOR PASSENGER.......................
    jq('#modalOTP').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var otp = jq('#otp').val();
        var hidden_otp = jq('#hidden_otp').val();
        //var hidden_user = jq('#hidden_user').val();
        jq.ajax({
            url: "{{ route('verifyotp') }}",
            method: "POST",
            data: { otp: otp, hidden_otp: hidden_otp},
            success: function(response){
                $("#verificationModal").modal('hide');
                $('#bootboxModal').modal('show');
                $('.bootboxBody').text('{{trans('weblng.VALIDATION_MSG.OTP_SUCCESS')}}');

                var first_name = jq('#first_name').val();
                var last_name = jq('#last_name').val();
                var mobile_no = jq('#mobile_no').val();
                var isdCode = jq('#code').val();
                //var email_id = jq('#email_id').val();
                var social_unique_id = jq('#social_unique_id').val();
                var picture = jq('#picture').val();
                var login_by = jq('#login_by').val();
                var isActive = response.isActive;
                jq.ajax({
                    url: "{{ route('passenger.social.signup') }}",
                    method: "POST",
                    data: { first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, social_unique_id: social_unique_id, picture:picture, login_by: login_by, isActive: isActive, timeZone: timeZone },
                    success: function(response){
                        console.log(response)
                        $('button.bootboxBtn').on('click', function(){
                            $("#bootboxModal").modal('hide');
                            var email_id = jq('#email_id').val();
                            var value = { email_id: email_id, };
                            var JSONvalue = JSON.stringify(value);
                            location.href = 'passenger/emergencyContact?'+'data='+btoa(JSONvalue);
                        });
                    },
                    error: function(response){
                        $('button.bootboxBtn').on('click', function(){
                            $("#bootboxModal").modal('hide');
                            if (response.status == 404){
                                var responseMsg = jq.parseJSON(response.responseText);
                                jq('.offset-lg-2').removeClass('pt-5');
                                jq('.signup_alert').show();
                                jq('#signup-msg').html(responseMsg.message)
                            }
                            if (response.status == 403){
                                var responseMsg = jq.parseJSON(response.responseText);
                                jq('.offset-lg-2').removeClass('pt-5');
                                jq('.signup_alert').show();
                                jq('#signup-msg').html(responseMsg.message)
                            }
                            if (response.status == 500){
                                var responseMsg = jq.parseJSON(response.responseText);
                                jq('.offset-lg-2').removeClass('pt-5');
                                jq('.signup_alert').show();
                                jq('#signup-msg').html(responseMsg.message)
                            }
                        });
                    }
                });
            },
            error: function(response){
                jq('#otperror').html(response.responseJSON.message);
            }

        });
    });
//END MODEL OTP FOR PASSENGER.......................
                
//SUBMIT PASSENGER FORM.................................
    jq('#submitPassengerForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var first_name = jq('#first_name').val();
        var last_name = jq('#last_name').val();
        var mobile_no = jq('#mobile_no').val();
        var isdCode = jq('#code').val();
        //var email_id = jq('#email_id').val();
        jq('#reload').css({'display':'block'});
        jq.ajax({
            url: "{{ route('passenger.social.signup.otp') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, timeZone: timeZone },
            success: function(response){
                jq('#reload').css({'display':'none'});
                var otp = response.data.OTP;
                jq('#hidden_otp').val(otp);
                //jq('#hidden_user').val('passenger');
                $("#verificationModal").modal('show');
            },
            error: function(response){
                console.log(response.status)
                jq('#reload').css({'display':'none'});
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        jq('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            jq('#mobile_no').addClass('has-error');
                        });
                    }
                    // if (responseMsg.errors.hasOwnProperty('email_id')) {
                    //     jq('#email').html(responseMsg.errors.email_id).promise().done(function(){
                    //         jq('#email_id').addClass('has-error');
                    //     });
                    // }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#mobile').html(responseMsg.message).promise().done(function(){
                            jq('#mobile_no').addClass('has-error');
                        });
                    }
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('#mobile').html(responseMsg.message).promise().done(function(){
                        jq('#mobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT PASSENGER FORM.................................
});  
</script>
<!-- footer -->
@include('partials.footer')