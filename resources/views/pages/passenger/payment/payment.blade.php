<!-- header -->
@include('partials.passenger_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">

              <div class="px-sm-3 pt-5">
               
                <div class="form-group debit_card" style="display: block;width: fit-content;">
                    <button class="form-control inset-input1 btn-info" id="DebitCard" style="cursor: pointer;">{{trans('weblng.DEBIT_CARD')}}</button>
                </div>

                {{-- <div class="form-group addCard" style="display: none;">
                    <button class="form-control btn-info" id="addDebitCard" style="cursor: pointer;">Add Your Card</button>
                </div> --}}

                <!-- Modal -->
                <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      {{-- <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div> --}}
                      <form id="paymentForm">
                          <div class="modal-body">
                            <h5>{{trans('weblng.CHARGE')}} <span>&#8358;</span>{{trans('weblng.CHARGE_NEXT')}}</h5>
                            <input type="hidden" name="email_paystack" id="email_paystack"/>
                            <input type="hidden" name="first_name" id="first_name"/>
                            <input type="hidden" name="last_name" id="last_name"/>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('weblng.CLOSE')}}</button>
                            <button type="submit" class="btn btn-primary" onclick="payWithPaystack(event)">{{trans('weblng.PAYMENT')}}</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- end Modal -->
                <div id="card-field" class="pt-2 pb-5" style="display: none;">
                    <div class="alert alert-success emailsuc_alert" style="display: none;">
                            <p class="text-success" id="emailsuc-msg"></p>
                        </div>
                    <form id="submitEmail">
                        <div class="form-group mb-4">
                            <label for="email" class="font-weight-bold" style="font-size: 22px;">{{trans('weblng.ENTER_EMAIL')}}</label>
                            <input type="email" name="email" id="email" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.EMAIL_PLACE')}}" onkeyup="return validate()" autocomplete="off">
                            <span class="text-danger" id="emailadd"></span>
                        </div>
                        <div class="d-flex justify-content-start mb-3 mt-0">
                            <button type="submit" class="btn btn-lrg grad text-white">{{trans('weblng.FIELD.SAVE')}}</button>
                        </div>
                    </form>
                </div>

                <div class="form-group" style="display: block;width: fit-content;">
                    <a href="{{ route('passenger.promocode') }}" class="form-control inset-input1 btn-info" style="cursor: pointer;">{{trans('weblng.GIFTCODE')}}</a>
                </div>

                {{-- <div class="form-group" style="display: block;width: fit-content;">
                    <a href="{{ route('passenger.transaction') }}" class="form-control inset-input1 btn-info" style="cursor: pointer;">Transaction</a>
                </div> --}}

                <div class="text-center mt-5">
                    {{-- <h3 class="font-weight-bold ">View Transaction</h3> --}}
                    <a href="{{ route('passenger.transaction') }}" class="btn btn-lrg text-dark" style="width: auto">{{trans('weblng.VIEW_TRANSACTION')}}</a>
                </div>
               
              </div>

            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://js.paystack.co/v1/inline.js"></script>
<script>
    const paymentForm = document.getElementById('paymentForm');
    paymentForm.addEventListener("submit", payWithPaystack, false);
    function payWithPaystack(event) {
        event.preventDefault();

        let handler = PaystackPop.setup({
            key: 'pk_test_cd507a5002f0a3d1f83ec7d355712e1efd95c554', // Replace with your public key
            email: document.getElementById("email_paystack").value,
            // amount: document.getElementById("amount").value * 100,
            currency: 'NGN',
            firstname: document.getElementById("first_name").value,
            lastname: document.getElementById("last_name").value,
            amount: 50 * 100,
            channels: ['card'],
            //ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
            // label: "Optional string that replaces customer email"
            onClose: function(){
                //alert('Window closed.');
                $('#paymentModal').modal('hide');
            },
            callback: function(response){
                $('#paymentModal').modal('hide');
                //let message = 'Payment complete! Reference: ' + response.reference;
                jq.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                t = new Date().toUTCString().split(' ');
                var timeZone = t[t.length-1] + moment().format('Z');
                var reference = response.reference;
                $(".overlay").show();
                jq.ajax({
                    url: "{{ route('passenger.addCard') }}",
                    method:"POST",
                    data:{ reference: reference, timeZone: timeZone },
                    success: function(response){
                        $(".overlay").hide();
                        console.log(response)
                    },
                    error: function(response){
                        $(".overlay").hide();
                    }
                });

            }
        });
        handler.openIframe();
    }


    function validate(){
        var status=null;

        var email_id = document.getElementById('email').value;
        if (email_id == '') {
            document.getElementById("emailadd").innerHTML='{{trans('weblng.VALIDATION_MSG.EMAIL_MSG')}}'
            document.getElementById('email').classList.add('has-error')
            status = false
        } else {
            document.getElementById("emailadd").innerHTML='';
            document.getElementById('email').classList.remove('has-error')
            status=true;
        }
        return status
    }

    $(document).ready(function(){

        $('#DebitCard').on('click', function(event){
            event.preventDefault();
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            $(".overlay").show();
            jq.ajax({
                url: "{{ route('passenger.ListCard') }}",
                method:"GET",
                data:{ timeZone: timeZone },
                success: function(response){
                    console.log(response.data.emailStatus);
                    if (response.data.emailStatus=='EMAILNOTVERIFIED'||response.data.emailStatus=='EMAILNOTADDED') {
                        $(".overlay").hide();
                        $('.debit_card').css({'display':'none'});
                        $('#card-field').css({'display':'block'});
                    }
                    if (response.data.emailStatus=='EMAILVERIFIED') {
                        // $('.debit_card').css({'display':'none'});
                        // $('.addCard').css({'display':'block'});
                        //$('#email_paystack').val(response.data.email);
                        //$('#paymentModal').modal('show');
                        location.href='{{route('passenger.addpayment')}}';
                    }
                },
                error: function(response){
                    $(".overlay").hide();
                }
            });
        });

        $('#submitEmail').on('submit', function(event){
            event.preventDefault();
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            var email_id = $('#email').val();
            $(".overlay").show();
            jq.ajax({
                url: "{{ route('passenger.updateTranscationEmail') }}",
                method:"POST",
                data:{ email_id: email_id, timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    $('#email').val('');
                    $('.emailsuc_alert').show();
                    $('#emailsuc-msg').html(response.message);
                },
                error: function(response){
                    $(".overlay").hide();
                    var responseMsg = $.parseJSON(response.responseText);
                    if (response.status == 422){
                        if (responseMsg.errors.hasOwnProperty('email_id')) {
                            jq('#emailadd').html(responseMsg.errors.email_id).promise().done(function(){
                                jq('#email').addClass('has-error');
                            });
                        }else{
                            jq('#emailadd').html(responseMsg.message).promise().done(function(){
                                    jq('#email').addClass('has-error');
                                });
                        }
                    }
                }
            });
        });

    });
</script>

<!-- footer -->
@include('partials.passenger_footer')