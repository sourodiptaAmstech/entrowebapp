
            </section>

            <div id="footer" class="d-flex align-items-center" class="mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="row justify-content-md-center align-items-center w-100 h-100">
                                <img class="logo" src="{{URL::asset('/')}}storage/assets/img/logo.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <nav class="navbar navbar-expand-lg navbar-light">
                            <div id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item">
                                        <a class="nav-link text-footer px-3" href="{{ route('driver.home') }}"><strong>{{trans('weblng.HEADER.HOME')}}</strong></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-footer px-3" href="{{route('driver.terms.conditions')}}"><strong>{{trans('weblng.FIELD.TERM_CONDITION')}}</strong></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-footer px-3" href="{{route('driver.privacy.policy')}}"><strong>{{trans('weblng.FIELD.PRIVACY_POLICY')}}</strong></a>
                                    </li>
                                   {{--  <div class="passb_footer">
                                    <li class="nav-item">
                                        <a class="nav-link text-footer px-3" href="#"><strong>Book a Ride</strong></a>
                                    </li>
                                    </div> --}}
                                    {{-- <div class="passl_footer">
                                    <li class="nav-item">
                                        <a class="nav-link text-footer px-3" href="{{ url('lease') }}"><strong>Leasing</strong></a>
                                    </li>
                                    </div> --}}
                                   {{--  <div class="driv_footer">
                                    <li class="nav-item">
                                        <a class="nav-link text-footer px-3" href="#"><strong>Find a Ride</strong></a>
                                    </li>
                                    </div> --}}
                                </ul>
                            </div>
                            </nav>
                            <div class="row justify-content-center text-muted">
                                &copy; <script>document.write(new Date().getFullYear())</script> {{trans('weblng.FOOTER.MESSAGE')}}
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="row w-100 h-100 align-items-center">
                                <ul class="list-group list-group-horizontal footer">
                                    <li class="list-group-item text-white rounded-circle bg-dark m-2">
                                        <i class="fab fa-facebook-f"></i>
                                    </li>
                                    <li class="list-group-item text-white rounded-circle bg-dark m-2">
                                        <i class="fab fa-twitter"></i>
                                    </li>
                                    <li class="list-group-item text-white rounded-circle bg-dark m-2">
                                        <i class="fab fa-linkedin-in"></i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <script src="{{URL::asset('/')}}storage/assets/js/jquery-slim.min.js" crossorigin="anonymous"></script>
        <script src="{{URL::asset('/')}}storage/assets/js/popper.min.js" crossorigin="anonymous"></script>
        <script src="{{URL::asset('/')}}storage/assets/js/bootstrap.min.js" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
        <script src="{{URL::asset('/')}}storage/assets/js/bootstrap-datetimepicker.min.js" crossorigin="anonymous"></script>
        <script src="{{URL::asset('/')}}storage/assets/js/bootstrap-select.min.js" crossorigin="anonymous"></script>
        <!-- Custom JS -->
        <script src="{{URL::asset('/')}}storage/assets/js/main.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>

        <script type="text/javascript" src="{{URL::asset('/')}}storage/assets/js/rateyo-js/jquery.rateyo.js"></script>

    <!-- push notification   -->
    <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js"></script>
    <script src="{{URL::asset('/')}}storage/assets/js/push.notification.sg.js" crossorigin="anonymous"></script>
    <!-- end push notification -->
    </body>
</html>
