<!-- header -->
@include('partials.header')
<style>
    .has-error{
        border:1px solid red;
    }
</style>
<div class="container mb-5">
    <form class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="resetPasswordForm">
        <h1 class="text-center"><strong>{{trans('weblng.RESET.RESET_YOUR')}} <span class="text-theme">{{trans('weblng.FIELD.PASSWORD')}}</span></strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <input type="hidden" name="otpH" id="otpH" value="{{$otp}}">
            <input type="hidden" name="userH" id="userH" value="{{$user}}">
            <input type="hidden" name="isdCodeH" id="isdCodeH" value="{{$isd}}">
            <input type="hidden" name="mobilenoH" id="mobilenoH" value="{{$mobile}}">

            <div class="alert alert-danger reset_alert" style="display: none;">
                <p class="text-danger" id="reset-msg"></p>
            </div>

            <div class="form-group">
                <label for="otp" class="font-weight-bold">{{trans('weblng.FIELD.OTP_PLACE')}}</label>
                <input type="text" name="otp" id="otp" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.OTP')}}" onkeyup="return validate()">
                <span class="text-danger" id="otp-err"></span>
            </div>
            <div class="form-group">
                <label for="password" class="font-weight-bold">{{trans('weblng.FIELD.NEW_PASSWORD')}}</label>
                <input type="password" name="password" id="password" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.NEW_PASSWORD_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="passw"></span>
            </div>
            <div class="form-group">
                <label for="password_confirmation" class="font-weight-bold">{{trans('weblng.FIELD.NEW_CONFIRM_PASSWORD')}}</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.CONFIRM_PASSWORD_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="cpassw"></span>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white">{{trans('weblng.BOOKING.SUBMIT')}}</button>
            </div>
        </div>
    </form>

</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    jq = jQuery.noConflict();

      function validate(){
        var status=null;

        var otp = document.getElementById('otp').value;
        if (otp == '') {
            document.getElementById("otp-err").innerHTML='{{trans('weblng.VALIDATION_MSG.OTP_FIELD')}}';
            document.getElementById('otp').classList.add('has-error');
            status = false
        } else if (!otp.match(/^([0-9\s\-\+\(\)]*)$/) || otp.length>4) {
            document.getElementById("otp-err").innerHTML='{{trans('weblng.VALIDATION_MSG.OTP_VALID')}}';
            document.getElementById('otp').classList.add('has-error')
            status = false;
        } else {
            jq('.offset-lg-2').addClass('pt-5');
            jq('.reset_alert').hide();
            jq('#reset-msg').html('');
            document.getElementById("otp-err").innerHTML='';
            document.getElementById('otp').classList.remove('has-error');
            status=true;
        }

        var password = document.getElementById('password').value
        if (password == '') {
            document.getElementById("passw").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD')}}'
            document.getElementById('password').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("passw").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_REQ')}}'
            document.getElementById('password').classList.add('has-error')
            status=false

        } else {
            document.getElementById("passw").innerHTML=''
            document.getElementById('password').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('password_confirmation').value
        if (password_confirm == '') {
            document.getElementById("cpassw").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM')}}'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("cpassw").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM_REQ')}}'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("cpassw").innerHTML=''
            document.getElementById('password_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    jq(document).ready(function(){
        $('#resetPasswordForm').on('submit', function(event){
            event.preventDefault();
            
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            var user = $("#userH").val();
            var mobile_no = $('#mobilenoH').val();
            var isdCode = $('#isdCodeH').val();
            var otpH = $('#otpH').val();
            var otp = $('#otp').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('reset.password') }}",
                method:"POST",
                data:{ mobile_no: mobile_no, isdCode: isdCode, user: user, otpH: otpH, otp: otp, password: password, password_confirmation: password_confirmation, timeZone: timeZone },
                success: function(response){
                    jq(".overlay").hide();
                    $('#bootboxModal').modal('show');
                    $('.bootboxBody').text(response.message);
                    $('button.bootboxBtn').on('click', function(){
                        $("#bootboxModal").modal('hide');
                        window.location.href = '{{ route('login') }}'; 
                    });
                },
                error: function(response){
                    jq(".overlay").hide();
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('otp')) {
                            $('#otp-err').html(responseMsg.errors.otp).promise().done(function(){
                                $('#otp').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password')) {
                            $('#passw').html(responseMsg.errors.password).promise().done(function(){
                                $('#password').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                            $('#cpassw').html(responseMsg.errors.password_confirmation).promise().done(function(){
                                $('#password_confirmation').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('exception')) {
                            $('.offset-lg-2').removeClass('pt-5');
                            $('.reset_alert').show();
                            $('#reset-msg').html(responseMsg.message);
                        } 
                    }
                    if (response.status == 404){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('#otp-err').html(responseMsg.message).promise().done(function(){
                            $('#otp').addClass('has-error');
                        });
                    }
                    if (response.status == 403){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.offset-lg-2').removeClass('pt-5');
                        $('.reset_alert').show();
                        $('#reset-msg').html(responseMsg.message);
                    }
                    if (response.status == 400){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.offset-lg-2').removeClass('pt-5');
                        $('.reset_alert').show();
                        $('#reset-msg').html(responseMsg.message);
                    }
                    if (response.status == 500){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.offset-lg-2').removeClass('pt-5');
                        $('.reset_alert').show();
                        $('#reset-msg').html(responseMsg.message);
                    }
                }
            });
        });
    });
</script>
<!-- footer -->
@include('partials.footer')
