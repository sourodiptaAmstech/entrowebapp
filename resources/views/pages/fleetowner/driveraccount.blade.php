<!-- header -->
@include('partials.fleetowner_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.fleetowner_sidebar')
            </div>
            <div class="col-lg-8">

                {{--DRIVER SIGNUP  --}}
                    {{-- <div class="driver_fields" style="display: block"> --}}
                    <form id="DriverFormSubmit">
                    <div class="px-sm-3 pt-5">
                         <h2 class="font-weight-bold mb-4">{{trans('weblng.SIDEBAR.DRIVER_ACCOUNT')}}</h2>
                        <div class="alert alert-danger signup_alert" style="display: none;">
                            <p class="text-danger" id="signup-msg"></p>
                        </div>
                        <div class="alert alert-success updatesuc_alert" style="display: none;">
                            <p class="text-success" id="updatesuc-msg"></p>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="dfirst_name" class="font-weight-bold">{{trans('weblng.FIELD.FIRST_NAME')}}</label>
                                <input type="text" name="first_name" id="dfirst_name" value="{{ old('first_name') }}" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME_PLACE')}}" onkeyup="return validateDriver()">
                                <span class="text-danger" id="fdname"></span>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="dlast_name" class="font-weight-bold">{{trans('weblng.FIELD.LAST_NAME')}}</label>
                                <input type="text" class="form-control inset-input" name="last_name" id="dlast_name" value="{{ old('last_name') }}" placeholder="{{trans('weblng.FIELD.LAST_NAME_PLACE')}}" onkeyup="return validateDriver()">
                                <span class="text-danger" id="ldname"></span>
                            </div>
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="dgender" class="font-weight-bold">{{trans('weblng.FIELD.GENDER')}}</label>
                            <select name="gender" id="dgender" class="form-control inset-input" onchange="return validateDriver()">
                                <option value="">{{trans('weblng.FIELD.Select')}}</option>
                                <option value="Male">{{trans('weblng.FIELD.MALE')}}</option>
                                <option value="Female">{{trans('weblng.FIELD.FEMALE')}}</option>
                            </select>
                            <span class="text-danger" id="gend"></span>
                        </div>
                       
                        <div class="form-group">
                        <label for="dmobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}&nbsp;<span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span></label>
                            <input type="hidden" name="dcode" id="dcode">
                            <input type="text" class="form-control inset-input" name="mobile_no" id="dmobile_no" value="{{ old('mobile_no') }}" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validateDriver()">
                            <span class="text-danger" id="dmobile"></span>
                        </div>
                        <div class="form-group">
                            <label for="demail_id" class="font-weight-bold">{{trans('weblng.FIELD.EMAIL')}}</label>
                            <input type="email" class="form-control inset-input" name="email_id" id="demail_id" value="{{ old('email_id') }}" placeholder="{{trans('weblng.FIELD.EMAIL_ADDRESS')}}" onkeyup="return validateDriver()">
                            <span class="text-danger" id="email"></span>
                        </div>
                        <div class="form-group">
                            <label for="service_type" class="font-weight-bold">{{trans('weblng.FIELD.SERVICE_TYPE')}}</label>
                            <select name="service_type" id="service_type" class="form-control inset-input" onchange="return validateDriver()">
                                
                            </select>
                            <span class="text-danger" id="service"></span>
                        </div>
                        <div class="form-group">
                            <label for="car_make" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MAKE')}}</label>
                            <select name="car_make" id="car_make" class="form-control inset-input" onchange="return validateDriver()">
                                
                            </select>
                            <span class="text-danger" id="make"></span>
                        </div>
                        <div class="form-group">
                            <label for="car_model" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MODEL')}}</label>
                            <select name="car_model" id="car_model" class="form-control inset-input" onchange="return validateDriver()">
                                
                            </select>
                            <span class="text-danger" id="model"></span>
                        </div>
                        <div class="form-group">
                            <label for="model_year" class="font-weight-bold">{{trans('weblng.FIELD.MODEL_YEAR')}}</label>
                            <select name="model_year" id="model_year" class="form-control inset-input" onchange="return validateDriver()">
                                
                            </select>
                            <span class="text-danger" id="year"></span>
                        </div>
                        <div class="form-group">
                            <label for="car_number" class="font-weight-bold">{{trans('weblng.FIELD.CAR_NO')}}</label>
                            <input type="text" class="form-control inset-input" name="car_number" id="car_number" value="{{ old('car_number') }}" placeholder="{{trans('weblng.FIELD.CAR_NO_PLACE')}}" onkeyup="return validateDriver()">
                            <span class="text-danger" id="carno"></span>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="dpassword" class="font-weight-bold">{{trans('weblng.FIELD.CREATE_PASS')}}</label>
                                <input type="password" class="form-control inset-input" name="password" id="dpassword" placeholder="{{trans('weblng.FIELD.PASSWORD')}}" onkeyup="return validateDriver()">
                                <span class="text-danger" id="dpass"></span>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="dpassword_confirmation" class="font-weight-bold">{{trans('weblng.FIELD.CONFIRM_PASSWORD')}}</label>
                                <input type="password" class="form-control inset-input" name="password_confirmation" id="dpassword_confirmation" placeholder="{{trans('weblng.FIELD.CONFIRM_PASSWORD_PLACE')}}" onkeyup="return validateDriver()">
                                <span class="text-danger" id="dcpass"></span>
                            </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mb-3 mt-5">
                            <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.BOOKING.SUBMIT')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="dreload" style="display: none;"></i></div></button>
                        </div>
                    </div>
                    </form>
                    {{-- </div> --}}
                {{--END DRIVER SIGNUP  --}}
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>

<script>
    jq = jQuery.noConflict();
    use :
        var s = jq("#dmobile_no").intlTelInput({
            autoPlaceholder: 'polite',
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: 'ng',
            preferredCountries:["ng"]
        });

    insteadof :
        var d = jq("#dmobile_no").intlTelInput("getSelectedCountryData").dialCode;
        jq("#dcode").val(d);
        jq(document).on('countrychange', function (e, countryData) {
            jq("#dcode").val((jq("#dmobile_no").intlTelInput("getSelectedCountryData").dialCode));
        });


    
    function validateDriver(){

        var status=null;

        var dfname = document.getElementById('dfirst_name').value
        if (dfname == '') {
            document.getElementById("fdname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else if (dfname.length<3) {
            document.getElementById("fdname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else if (dfname.length>255) {
            document.getElementById("fdname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else {
            // jq('.offset-lg-2').addClass('pt-5');
            jq('.signup_alert').hide();
            jq('#signup-msg').html('');
            jq('.updatesuc_alert').hide();
            jq('#updatesuc-msg').html('');
            document.getElementById("fdname").innerHTML=''
            document.getElementById('dfirst_name').classList.remove('has-error')
            status=true
        }

        var dlname = document.getElementById('dlast_name').value
        if (dlname == '') {
            document.getElementById("ldname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false
        } else if (dlname.length<3) {
            document.getElementById("ldname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false

        } else if (dlname.length>255) {
            document.getElementById("ldname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("ldname").innerHTML=''
            document.getElementById('dlast_name').classList.remove('has-error')
        }

        var Gender = document.getElementById('dgender').value
        if (Gender == '') {
            document.getElementById("gend").innerHTML='{{trans('weblng.VALIDATION_MSG.GENDER')}}'
            document.getElementById('dgender').classList.add('has-error')
            status=false
        } else {
            document.getElementById("gend").innerHTML=''
            document.getElementById('dgender').classList.remove('has-error')
        }

        var mobileno = document.getElementById('dmobile_no').value
        if (mobileno == '') {
            document.getElementById("dmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE')}}'
            document.getElementById('dmobile_no').classList.add('has-error')
            status=false

        } else if (!mobileno.match(/^([0-9\s\-\+\(\)]*)$/) || mobileno.length<5 || mobileno.length>10) {
            document.getElementById("dmobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE_REQ')}}'
            document.getElementById('dmobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dmobile").innerHTML=''
            document.getElementById('dmobile_no').classList.remove('has-error')
        }

        var emailid = document.getElementById('demail_id').value
        if (emailid == '') {
            document.getElementById("email").innerHTML='{{trans('weblng.VALIDATION_MSG.EMAIL_MSG')}}'
            document.getElementById('demail_id').classList.add('has-error')
            status=false
        } else {
            document.getElementById("email").innerHTML=''
            document.getElementById('demail_id').classList.remove('has-error')
        }

        var serviceType = document.getElementById('service_type').value
        if (serviceType == '') {
            document.getElementById("service").innerHTML='{{trans('weblng.VALIDATION_MSG.SERVICE_TYPE')}}'
            document.getElementById('service_type').classList.add('has-error')
            status=false
        } else {
            document.getElementById("service").innerHTML=''
            document.getElementById('service_type').classList.remove('has-error')
        }

        var carMake = document.getElementById('car_make').value
        if (carMake == '') {
            document.getElementById("make").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_MAKE')}}'
            document.getElementById('car_make').classList.add('has-error')
            status=false
        } else {
            document.getElementById("make").innerHTML=''
            document.getElementById('car_make').classList.remove('has-error')
        }

        var carModel = document.getElementById('car_model').value
        if (carModel == '') {
            document.getElementById("model").innerHTML='{{trans('weblng.VALIDATION_MSG.MODEL')}}'
            document.getElementById('car_model').classList.add('has-error')
            status=false
        } else {
            document.getElementById("model").innerHTML=''
            document.getElementById('car_model').classList.remove('has-error')
        }

        var modelYear = document.getElementById('model_year').value
        if (modelYear == '') {
            document.getElementById("year").innerHTML='{{trans('weblng.VALIDATION_MSG.YEAR')}}'
            document.getElementById('model_year').classList.add('has-error')
            status=false
        } else {
            document.getElementById("year").innerHTML=''
            document.getElementById('model_year').classList.remove('has-error')
        }

        var carNumber = document.getElementById('car_number').value
        if (carNumber == '') {
            document.getElementById("carno").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_NUMBER')}}'
            document.getElementById('car_number').classList.add('has-error')
            status=false
        } else {
            document.getElementById("carno").innerHTML=''
            document.getElementById('car_number').classList.remove('has-error')
        }

        var password = document.getElementById('dpassword').value
        if (password == '') {
            document.getElementById("dpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD')}}'
            document.getElementById('dpassword').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("dpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_REQ')}}'
            document.getElementById('dpassword').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dpass").innerHTML=''
            document.getElementById('dpassword').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('dpassword_confirmation').value
        if (password_confirm == '') {
            document.getElementById("dcpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM')}}'
            document.getElementById('dpassword_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("dcpass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_CONFIRM_REQ')}}'
            document.getElementById('dpassword_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dcpass").innerHTML=''
            document.getElementById('dpassword_confirmation').classList.remove('has-error')
        }
        
        return status
    }


jq(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    jq('#car_make').change(function(){
        var make = jq(this).val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.model') }}",
            method: "POST",
            data: {make: make, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#car_model').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_MODEL')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.model+'">'+value.model+'</option>';
                });
                $('#car_model').append(options);
                jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                    jq('#car_model').addClass('has-error');
                });
                $('#model_year').html('');
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#car_model').html('');
                    jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                        jq('#car_model').addClass('has-error');
                    });

                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
            }
        });
    });

    jq('#car_model').change(function(){
        var model = jq(this).val();
        var make = jq('#car_make').val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.year') }}",
            method: "POST",
            data: {make: make, model: model, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#model_year').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_YEAR')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.year+'">'+value.year+'</option>';
                });
                $('#model_year').append(options);
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
            }
        });

    });

    //DRIVER SERVICE MAKE..........
        jq("input[name='mobile_no']").css('padding-left','95px');

        jq.ajax({
            url: "{{ route('service.list') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('#service_type').empty();
                var options = '<option value="">{{trans('weblng.CHOOSE_SERVICE')}}</option>';
                $.each(response.data, function(key, value) {
                  options += '<option value="'+value.id+'">'+value.name+'</option>';
                });

                $('#service_type').append(options);
            },
            error: function(response){
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                }
            }
        });

        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.make') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#car_make').empty();
                var options = '<option value="">{{trans('weblng.CHOOSE_MAKE')}}</option>';
                $.each(response.data, function(key, value) {
                    options += '<option style="color:#080600;" value="'+value.make+'">'+value.make+'</option>';
                });
                $('#car_make').append(options);
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                }
            }
        });
//END DRIVER SERVICE MAKE..........
          
//SUBMIT DRIVER FORM.................................
    jq('#DriverFormSubmit').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var first_name = jq('#dfirst_name').val();
        var last_name = jq('#dlast_name').val();
        var mobile_no = jq('#dmobile_no').val();
        var isdCode = jq('#dcode').val();
        var service_type = jq('#service_type').val();
        var car_make = jq('#car_make').val();
        var car_model = jq('#car_model').val();
        var model_year = jq('#model_year').val();
        var car_number = jq('#car_number').val();
        var gender = jq('#dgender').val();
        var email_id = jq('#demail_id').val();
        var password = jq('#dpassword').val();
        var password_confirmation = $('#dpassword_confirmation').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('fleetowner.driver.account.create') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, service_type: service_type, car_make: car_make, car_model: car_model, model_year: model_year, car_number: car_number, password: password, password_confirmation: password_confirmation, gender: gender, email_id: email_id, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                // jq('.updatesuc_alert').show();
                // jq('#updatesuc-msg').html(response.message);
                location.href ="{{route('fleetowner.driver.list')}}";
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#fdname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#dfirst_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#ldname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#dlast_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('gender')) {
                        jq('#gend').html(responseMsg.errors.gender).promise().done(function(){
                            jq('#dgender').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        jq('#dmobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            jq('#dmobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('email_id')) {
                        jq('#email').html(responseMsg.errors.email_id).promise().done(function(){
                            jq('#demail_id').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('service_type')) {
                        jq('#service').html(responseMsg.errors.service_type).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_make')) {
                        jq('#make').html(responseMsg.errors.car_make).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_model')) {
                        jq('#model').html(responseMsg.errors.car_model).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('model_year')) {
                        jq('#year').html(responseMsg.errors.model_year).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_number')) {
                        jq('#carno').html(responseMsg.errors.car_number).promise().done(function(){
                            jq('#car_number').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        jq('#dpass').html(responseMsg.errors.password).promise().done(function(){
                            jq('#dpassword').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                        jq('#dcpass').html(responseMsg.errors.password_confirmation).promise().done(function(){
                            jq('#dpassword_confirmation').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        // jq('#dmobile').html(responseMsg.message).promise().done(function(){
                        //     jq('#dmobile_no').addClass('has-error');
                        // });
                        jq('.signup_alert').show();
                        jq('#signup-msg').html(responseMsg.message);
                    }
                   
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('#dmobile').html(responseMsg.message).promise().done(function(){
                        jq('#dmobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    // jq('.offset-lg-2').removeClass('pt-5');
                    jq('.signup_alert').show();
                    jq('#signup-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT DRIVER FORM.................................

});
</script>
<!-- footer -->
@include('partials.fleetowner_footer')