<!-- header -->
@include('partials.fleetowner_header')
<!-- Banner -->
<div class="container mb-5">
    <div class="row mt-5">
        <div class="col-lg-7 mt-4">
            <div class="py-3 px-4 text-white header-text">
                <h2>{{trans('weblng.HOME.RIDETOWN')}}</h2>
                <h1 class="font-weight-bold1" style="
    font-size: x-large;
">{{trans('weblng.HOME.ENTRO_OUTSTATION')}}</h1>
            </div>
        </div>
        <div class="col-lg-5">
        
        </div>
    </div>
</div>

</section>
<section class="w-100 our-service" style="
    background-color: white;
">
    <div class="container my-5">
        <h2 class="pt-5 text-center"><strong><span class="text-theme">{{trans('weblng.HOME.Easy_book')}}</span></strong></h2>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SelectYourRide')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="selectRide1">
                    {{trans('weblng.HOME.SelectYourRideText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SelectyourDestination')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="home_destination1">
                    {{trans('weblng.HOME.SelectyourDestinationText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.CardPayment')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay1">
                    {{trans('weblng.HOME.CardPaymentText')}}
                </h6>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/carsmiley.jpg" alt="" style="
    width: 45%;
    height: auto;
">
                </div>
                <h4 class="text-center my-4 text-process carsmiley">{{trans('weblng.HOME.EnjoyYourRide')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay1">
                    {{trans('weblng.HOME.EnjoyYourRideText')}}
                </h6>
            </div>
        </div>
    </div>
</section>
<!-- Our service -->
<section class="w-100 our-service" style="
    background-color: white;
">
    <div class="container mb-5">
        <h2 class="pt-0 text-center"><strong><span class="text-theme">{{trans('weblng.HOME.OUR_SERVICE')}}</span> </strong></h2>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.SELECT_RIDE')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="selectRide">
               
                </h6>
            </div>
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.DESTINATION')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="home_destination">
               
                </h6>
            </div>
            <div class="col-lg-4">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/process_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process">{{trans('weblng.HOME.CARDPAYMENT')}}</h4>
                <h6 class="text-center mb-4 px-4 text-muted" id="cardpay">
                
                </h6>
            </div>
        </div>
    </div>
</section>
<!-- About -->
<section class="w-100 about" id="about-entro">
    <div class="container">
        <div class="row my-5 py-5">
            <div class="col-lg-6 offset-lg-6 text-white">
                <h2>{{trans('weblng.HOME.ABOUTOUR')}}</h2>
                <div class="my-3">
                    <img src="{{URL::asset('/')}}storage/assets/img/h_underline_white.png" alt="">
                </div>
                <h6 id="rideShare" style="color: powderblue;">
                
                </h6>
            </div>
        </div>
    </div>
</section>
{{-- Extra section --}}

<!-- Ride Sharing -->
{{-- <section class="w-100 rd">
    <div class="container-fluid my-5">
       
        <div class="row ride-sharing py-5">
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="happycustomer"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.HAPPYCUSTOMER')}}</h6>
            </div>
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="ridebooked"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.RIDEBOOK')}}</h6>
            </div>
            <div class="col-lg-3 py-5 ride-border">
                <h2 class="text-theme text-center font-weight-bold" id="drivertotal"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.DRIVER')}}</h6>
            </div>
            <div class="col-lg-3 py-5">
                <h2 class="text-theme text-center font-weight-bold" id="cartotal"></h2>
                <h6 class="text-secondary text-center pt-2">{{trans('weblng.HOME.CAR')}}</h6>
            </div>
        </div>
    </div>
</section> --}}
<!-- Book a Ride -->
<section class="w-100 book-ride" style="
    background-color: white;
">
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="pt-5"><strong><span class="text-theme">{{trans('weblng.HOME.BOOK_RIDE')}} </span></strong></h2>
                <div class="my-3">
                    <h6 class="text-muted" id="theapp">
                    
                    </h6>
                </div>
                <div class="mb-5 mt-5">
                    <button class="btn btn-lrg grad text-white mr-3 mb-3">
                        <div class="d-flex">
                            <img src="{{URL::asset('/')}}storage/assets/img/apple.png" alt="">
                            <div>
                                <div class="d-block">
                                    <h6 style="font-size: 15px">{{trans('weblng.HOME.DOWNLOAD_ON')}}</h6>
                                </div>
                                <div class="d-block">{{trans('weblng.HOME.APPSTORE')}}</div>
                            </div>
                        </div>
                    </button>
                    <button class="btn btn-lrg grad-black text-white mr-3 mb-3">
                        <div class="d-flex">
                            <img src="{{URL::asset('/')}}storage/assets/img/google-play.png" alt="">
                            <div>
                                <div class="d-block">
                                    <h6 style="font-size: 15px">{{trans('weblng.HOME.ANDROID')}}</h6>
                                </div>
                                <div class="d-block">{{trans('weblng.HOME.Google_Play')}}</div>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row justify-content-center my-3">
                    <img class="img-fluid-home" src="{{URL::asset('/')}}storage/assets/img/book_ride.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer top -->
<section class="w-100" id="footer-top-home">
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_1.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.BEST_PRICE')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_2.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.HOMEPICK')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_3.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.EASYBOOKING')}}</h4>
            </div>
            <div class="col-lg-3">
                <div class="row justify-content-center my-3 process-icon">
                    <img src="{{URL::asset('/')}}storage/assets/img/bottom_icon_4.png" alt="">
                </div>
                <h4 class="text-center my-4 text-process pt-2">{{trans('weblng.HOME.CUSTOMERCARE')}}</h4>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
@media only screen and (max-width: 499px) {
  .carsmiley {
    padding-top: 18px;
  }
}
@media only screen and (min-width: 500px) and (max-width: 992px) {
  .carsmiley {
    padding-top: 98px;
  }
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
{{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCigjyVjdY8tB90olMzBp7bp5bW8izPYdE&libraries=places"></script> --}}
<script>

    

    jq = jQuery.noConflict();
    jq(document).ready(function(){

        


        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var cookieValue = Cookies.get('languageCookie');
        if (cookieValue == '' || cookieValue == 'en') {
            var languageParam = 'en';
        } else {
            var languageParam = 'fr';
        }
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('home.contents.get') }}",
            method: "POST",
            data: {languageParam: languageParam, timeZone: timeZone},
            success: function(response){
                $.each(response.data, function(key,value){
                    if (value.key=='card_payment'||value.key=='card_payment_fr') {
                        $('#cardpay').html(value.value)
                    }
                    if (value.key=='entro_ride_form_app'||value.key=='entro_ride_form_app_fr') {
                        $('#theapp').html(value.value)
                    }
                    if (value.key=='entro_ride_sharing'||value.key=='entro_ride_sharing_fr') {
                        $('#rideShare').html(value.value)
                    }
                    if (value.key=='ride_sharing_analytics'||value.key=='ride_sharing_analytics_fr') {
                        $('#analytics').html(value.value)
                    }
                    if (value.key=='select_your_destination'||value.key=='select_your_destination_fr') {
                        $('#home_destination').html(value.value)
                    }
                    if (value.key=='select_your_ride'||value.key=='select_your_ride_fr') {
                        $('#selectRide').html(value.value)
                    }
                })
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
            }
        });

        jq.ajax({
            url: "{{ route('home.value.get') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('#cartotal').text(response.data.services)
                $('#happycustomer').text(response.data.totalCustomer)
                $('#drivertotal').text(response.data.totalDriver)
                $('#ridebooked').text(response.data.totalRide)
            },
            error: function(response){
            }
        });

    });

</script>
<!-- footer -->
<section class="w-100">
@include('partials.fleetowner_footer')
