<!-- header -->
@include('partials.passenger_header')
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">
                <div class="px-sm-3 pt-5">
                    <div class="alert alert-danger update_alert" style="display: none;">
                        <p class="text-danger" id="update-msg"></p>
                    </div>
                    <h2 class="font-weight-bold mb-0">{{trans('weblng.SIDEBAR.SCHETRIP')}}</h2>
                    <div class="container my-2" id="tripHistory">  
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    $(document).ready(function(){
        $('.booking').addClass('btn-primary');
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.upcoming.scheduleTrip') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                var options = '';
                console.log(response.data.data.length)
                if(response.data.data.length==0){
                    options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">{{trans('weblng.SCHEDULE_NOT_APPLIED')}}</h6>`;
                    $('#tripHistory').addClass('px-0');
                }
                $.each(response.data.data, function(key, value) {
                    
                    options += `
    <div class="row shadow-lg bg-white">

        <div class="col-lg-12 px-0 d-flex">
            <img src="`+value.requestDetails.static_map+`" alt="" style="width: 100%;height: auto;">
        </div>

        <div class="col-lg-12 px-5 py-2">

                <div class="row mb-2">
                    <div class="col-sm-6 mx-0 px-0">
                        <h5 class="font-weight-bold text-muted">
                           {{trans('weblng.BOOKINGID')}}: 
                        </h5>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h5 class="text-muted">`+value.requestDetails.request_no+`</h5>
                    </div>
                </div>
               
                <div class="row mb-2">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-muted">
                            {{trans('weblng.DATE')}}: 
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">`+moment(value.requestDetails.schedule_datetime).format('MMM Do YYYY, h:mm A')+`</h6>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-sm-12 mx-0 px-0 text-center">
                        <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center img-fluid">
                            <img src="`+value.driverSer.image+`">
                        </div>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-sm-12 mx-0 px-0 text-center">
                        <h6 class="text-muted">
                            `+value.driverSer.name+`
                        </h6>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-sm-12 mx-0 px-0 text-center">
                        <h5 class="font-weight-bold text-muted">
                            <a style="width:16%;cursor: pointer;" href="javascript:void(0)" class="btn btn-sm btn-danger cancelSchedule" key="`+value.requestDetails.request_id+`" id="cancelSchedule">{{trans('weblng.CANCELTRIP')}}</a>
                        </h5>
                    </div>
                </div>
        </div>
    </div><br>
                    `;
                });
                $('#tripHistory').append(options);

                $('.cancelSchedule').on('click',function(){
                    var request_id = $(this).attr('key');
                    t = new Date().toUTCString().split(' ');
                    var timeZone = t[t.length-1] + moment().format('Z');
                    bootbox.confirm('{{trans('weblng.SCHEDULE_CONFIRM')}}', function (res) {
                        if (res){
                            $(".overlay").show();
                            jq.ajax({
                                url: "{{ route('passenger.cancel.schedule') }}",
                                method:"POST",
                                data:{ request_id: request_id, timeZone: timeZone },
                                success: function(response){
                                    location.reload();
                                    $(".overlay").hide();
                                },
                                error: function(response){
                                    $(".overlay").hide();
                                }
                            });
                        }
                    });
                });

                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.col-lg-8').removeClass('pt-5');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                } 
            }
        });
    });
    
</script>

<!-- footer -->
@include('partials.passenger_footer')