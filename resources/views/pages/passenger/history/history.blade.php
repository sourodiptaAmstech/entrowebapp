<!-- header -->
@include('partials.passenger_header')
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.passenger_sidebar')
            </div>
            <div class="col-lg-8">
                <div class="px-sm-3 pt-5">
                    <div class="alert alert-danger update_alert" style="display: none;">
                        <p class="text-danger" id="update-msg"></p>
                    </div>
                    <h2 class="font-weight-bold mb-0">{{trans('weblng.TRIP_HISTORY')}}</h2>
                    <div class="container my-2" id="tripHistory">  
                    </div>
                    <div>
                        <button class="btn btn-sm  prev_page prevpage" >&laquo; prev</button>
                        <p class="btn btn-sm btn-default current_page" disabled></p>
                        <button class="btn btn-sm  next_page nextpage" >next &raquo;</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    $(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var url = "https://app.entrong.com/api/passenger/trip/history?page=1";
        AjaxCall(timeZone,url);

        function AjaxCall(timeZone,url){

            $(".overlay").show();
            jq.ajax({
                url: "{{ route('passenger.trip.history') }}?url="+url,
                method:"GET",
                data:{ timeZone: timeZone },
                success: function(response){
                    $('.current_page').text(response.data.current_page);
                    if (response.data.prev_page_url==null) {
                        $('.prev_page').addClass('disabled');
                        $('.prevpage').removeClass('prev_page');
                        $('.prevpage').removeClass('btn-info');
                        $('.prevpage').addClass('btn-default');
                        $('.prevpage').removeAttr("key");
                    } else {
                        $('.prevpage').removeClass('disabled');
                        $('.prevpage').addClass('prev_page');
                        $('.prevpage').removeClass('btn-default');
                        $('.prevpage').addClass('btn-info');
                        $(".prevpage").attr("key",response.data.prev_page_url);
                    }
                    if (response.data.next_page_url==null) {
                        $('.next_page').addClass('disabled');
                        $('.nextpage').removeClass('next_page');
                        $('.nextpage').removeClass('btn-info');
                        $('.nextpage').addClass('btn-default');
                        $('.nextpage').removeAttr("key");
                    } else {
                        $('.nextpage').removeClass('disabled');
                        $('.nextpage').addClass('next_page');
                        $('.nextpage').removeClass('btn-default');
                        $('.nextpage').addClass('btn-info');
                        $(".nextpage").attr("key",response.data.next_page_url);
                    }
                    var options = '';
                    $('#tripHistory').text('');
                    if(response.data.data.length==0){
                        options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">{{trans('weblng.TRIP_NOT_APPLY')}}</h6>`;
                        $('#tripHistory').addClass('px-0');
                    }
                    $.each(response.data.data, function(key, value) {
                        console.log(value);
                        var requestStatus=value.requestDetails.request_status;
                        if (requestStatus=='CANCELBYDRIVER'||requestStatus=='CANCELBYPASSENGER'||requestStatus=='CANCELBYSYSTEM') {
                            var cancel='<p class="text-danger">{{trans('weblng.CANCEL')}}</p>';
                        }else{
                            var cancel='';
                        }
                        options += `
                            <div class="row shadow-lg bg-white">
                                <div class="col-lg-12 px-5 py-3">
                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h5 class="font-weight-bold text-muted">
                                                   {{trans('weblng.BOOKINGID')}}:
                                                </h5>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h5 class="text-muted">`+value.requestDetails.request_no+`</h5>
                                            </div>
                                        </div>
                                       
                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.DATE')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted">`+moment(value.requestDetails.started_from_source).format('MMM Do YYYY, h:mm A')+`</h6>
                                            </div>
                                        </div>
                                        
                                        <div class="row mb-2">
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="font-weight-bold text-muted">
                                                    {{trans('weblng.FAIR')}}: 
                                                </h6>
                                            </div>
                                            <div class="col-sm-6 mx-0 px-0">
                                                <h6 class="text-muted1">`+value.transaction.currency+``+value.transaction.cost+`</h6>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-12 mx-0 px-0 text-center">
                                                <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center img-fluid">
                                                    <img src="`+value.driverSer.image+`">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-12 mx-0 px-0 text-center">
                                                <h6 class="text-muted">
                                                    `+value.driverSer.serviceName+`
                                                </h6>
                                            </div>
                                        </div>

                                        <div class="row mb-2">
                                            <div class="col-sm-10 mx-0 px-0 text-center">
                                                <h5 class="font-weight-bold text-muted">
                                                    <a href="{{ route('passenger.trip-details') }}?id=`+value.requestDetails.request_id+`" class="btn btn-sm btn-info trip-detail">{{trans('weblng.DETAILS')}}</a>
                                                </h5>
                                            </div>
                                            <div class="col-sm-2 mx-0 px-0 text-center">
                                                <h5 class="font-weight-bold text-muted">
                                                    `+cancel+`
                                                </h5>
                                            </div>
                                        </div>

                                </div>
                            </div><br>
                        `;
                    });
                    $('#tripHistory').append(options);
                    $(".overlay").hide();
                },
                error: function(response){
                    $(".overlay").hide();
                    if (response.status == 500){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.px-sm-3').removeClass('pt-5');
                        $('.update_alert').show();
                        $('#update-msg').html(responseMsg.message);
                    } 
                }
            });

        }

        $(document).on('click','.prev_page',function(){
            var url = $(this).attr('key');
            AjaxCall(timeZone,url);
        })

        $(document).on('click','.next_page',function(){
            var url = $(this).attr('key');
            AjaxCall(timeZone,url);
        })
        
    });
    
</script>

<!-- footer -->
@include('partials.passenger_footer')