<?php

namespace App\Http\Controllers\Website\FleetOwner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\FleetownerService;
use App\Services\FleetownerProfileService;


class ProfileController extends Controller
{
    public function homeIndex()
	{
		return view('pages.home.fleetowner_home');
	}

	public function accountIndex(Request $request)
	{
		return view('pages.fleetowner.myaccount');
	}

	public function driverAccountIndex(Request $request)
	{
		return view('pages.fleetowner.driveraccount');
	}
	

	public function profileImageUpdate(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'picture' => 'required|mimes:jpeg,bmp,png',
	        ]);
	        
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $tmp_name = $_FILES['picture']['tmp_name'];
	        $type = $_FILES['picture']['type'];
	        $name = basename($_FILES['picture']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
			$request['name'] = $name;
			$request['size']=$_FILES['picture']['size'];
			
			$passengerProfile = new FleetownerProfileService;
			$result = $passengerProfile->accessProfileImageUpdate($request);
		
			if ($result['statusCode']==422) {
				return response(['message'=>$result['message'],"field"=>$result['field'],"errors"=>$result['errors']],$result['statusCode']);
			}
	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function getProfile(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
			$passengerProfile = new FleetownerProfileService;
			$result = $passengerProfile->accessGetProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function updateFleetownerProfile(Request $request)
	{
		try{
			$this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'gender'=>'sometimes|nullable|in:Male,Female'
            ]);

            $request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
			$request->first_name = $request->first_name;
			$request->last_name = $request->last_name;
			$request->email_id = $request->email_id;
			$request->gender = $request->gender;
			
        	$driverProfile = new FleetownerProfileService;
			$result = $driverProfile->accessUpdateProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function driverRegisterByFleet(Request $request)
	{
		try{
			$this->validate($request, [
	            'first_name' => 'required|min:3|max:255',
	            'last_name' => 'required|min:3|max:255',
	            'mobile_no' => 'required|digits_between:5,10',
	            'service_type' => 'required',
	            'car_make' => 'required',
	            'car_model' => 'required',
	            'model_year' => 'required',
	            'car_number' => 'required',
	            'gender' => 'required',
	            'password' => 'required|between:6,255|confirmed',
	            'password_confirmation'=>'required',
	            'email_id'=>'required'
	        ]);
	        $request['token'] = $request->access_token;
			$request['timeZone'] = $request->timeZone;
			$request->login_by = 'manual';
	        $request->device_id = 'device_id';
	        $request->device_token = 'device_token';
	        $request->device_type = 'web';
	        $request->first_name = $request->first_name;
	        $request->last_name = $request->last_name;
	        $request->mobile_no = $request->mobile_no;
	        $request->isdCode = '+'.$request->isdCode;
	        $request->service_type_id = $request->service_type;
			$request->make = $request->car_make;
			$request->model = $request->car_model;
			$request->model_year = $request->model_year;

			$request->registration_no = $request->car_number;
			$request->gender = $request->gender;
	        $request->password = $request->password;
	        $request->password_confirmation = $request->password_confirmation;
	        $request->isActive = 1;//(integer)$request->isActive;

	        $obj = new FleetownerService;
			$result = $obj->accessDriverRegistrationByFleet($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
