<?php

namespace App\Services;

use App\Services\CurlService;


class PassengerBackgroundService
{
	private function backgroundData($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/background/data';
	      $method = "POST";
	      $array = ['device_latitude'=>$data->device_latitude,'device_longitude'=>$data->device_longitude ];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	public function accessBackgroundData($data)
	{
    	return $this->backgroundData($data);
  	}

}