<?php

namespace App\Http\Controllers\Website\FleetOwner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\FleetownerProfileService;


class ChangePasswordController extends Controller
{
	public function changePasswordIndex()
	{
		return view('pages.fleetowner.password');
	}
    
    public function changePassword(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'old_password' => 'required|between:6,255',
                'password' => 'required|between:6,255|confirmed',
                'password_confirmation'=>'required'
	        ]);

	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->old_password = $request->old_password;
	        $request->password = $request->password;
	        $request->password_confirmation = $request->password_confirmation;

			$passengerProfile = new FleetownerProfileService;
			$result = $passengerProfile->accessChangePassword($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
