jqMap = jQuery.noConflict();
jqMap(document).ready(function(){
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    var mapElement=document.querySelector("#map-canvas");
    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true});
    var googleMap="";
    var clearMap=0;
    var oldLocation={lat:"",log:""};
    var options = {
        enableHighAccuracy: true,
        timeout: 4000,
        maximumAge: 0
    };

    var dates=new Date();
    var getCurrentLocation=(isFirst)=>{
        console.log(isFirst);
        console.log((new Date())-dates);
        if(isFirst!="yes"){
            if((new Date())-dates<5000){
                setTimeout(function(){  getCurrentLocation("no"); }, (new Date())-dates);
                return false;
            }
        }
        dates=new Date();
        navigator.geolocation.getCurrentPosition(success, error);
    }
    function success(pos) {

        var location = pos.coords;

        if(googleMap=="" ){
            if($("#map-canvas").length>0){
                getCurrentLocation("no");
                googleMap=new google.maps.Map(mapElement);
                drawMap(new google.maps.LatLng(location.latitude,location.longitude));
            }
            else{
                console.log('Your current position is:');
                console.log(`Latitude : ${location.latitude}`);
                console.log(`Longitude: ${location.longitude}`);
                console.log(`More or less ${location.accuracy} meters.`);
                callBackgroundAPI(location);
            }
        }
        else{
            // call background loop function
            callBackgroundAPI(location);
        }

    }
    function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    var googleDrawRoute=(locationData)=>{
        var request = {
            origin: locationData.origin,
            destination: locationData.destination,
            travelMode: google.maps.TravelMode.DRIVING
        };

        var icon = {
            url: "https://entrong.com/storage/assets/img/location-1@3x.png",
            scaledSize: new google.maps.Size(50, 50),
        };

        new google.maps.Marker({
            position: request.destination,
            map: googleMap,
            icon: icon
        });

        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsRenderer.setDirections(response);
                directionsRenderer.setMap(googleMap);
            } else {
                alert(status+"Off here");
            }
        });
    }

    var iconCar = {
        url: "https://entrong.com/storage/assets/img/Car@3x.png",
        scaledSize: new google.maps.Size(50, 50),
    };

    var drawMap=(position)=>{
        googleMap.setOptions({
            center: position,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        new google.maps.Marker({ position, map: googleMap, icon: iconCar });
        directionsRenderer.setMap(googleMap);
    }

    var callBackgroundAPI=(currentLocation)=>{
        var urlPath = jQuery(location).attr('pathname');
        jqMap.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jqMap.ajax({
            // url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/driver/background/data",
            url: "https://entrong.com/driver/background/data",
            method:"POST",
            data:{ device_latitude: currentLocation.latitude, device_longitude: currentLocation.longitude, timeZone: timeZone },
            success: function(response){
                if (response.data.document_completion<100) {
                    $('.document_alert').show();
                    // /debarati/entrowebapp/public/driver/document
                    if (urlPath!='/debarati/entrowebapp/public/driver/document') {
                        // location.href = 'https://demos.mydevfactory.com/debarati/entrowebapp/public/driver/document';
                        location.href = 'https://entrong.com/driver/document';
                    }
                } else {
                    $('.document_success').show();
                }
                switch (response.data.status){
                    case "REQUESTED":
                        clearMap=1;
                        var onoff = response.data.onOffStatus;
                        onOffStatus(onoff);
                        var image = response.data.requestData.passenger_img;
                        var name = response.data.requestData.passenger_firstName+' '+response.data.requestData.passenger_lastName;
                        var pick_point = response.data.requestData.location_details.source.address;
                        var drop_point = response.data.requestData.location_details.destination.address;
                        var source_latitude = response.data.requestData.location_details.source.latitude;
                        var source_longitude = response.data.requestData.location_details.source.longitude;
                        var destination_latitude = response.data.requestData.location_details.destination.latitude;
                        var destination_longitude = response.data.requestData.location_details.destination.longitude;
                        var total_distance = response.data.requestData.estimatedFare.estimated_distance_miles;
                        var estimated_earning = response.data.requestData.estimatedFare.currency+''+response.data.requestData.estimatedFare.estimated_cost;
                        var request_id = response.data.requestData.request_id;
                        if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=source_latitude || oldLocation.Dlog!=source_longitude){
                            googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(source_latitude,source_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=source_latitude; oldLocation.Dlog=source_longitude;
                        }
                        if($("#acceptRequestForm").length==0){
                            acceptRequestForm(image,name,pick_point,drop_point,total_distance,estimated_earning,request_id);
                        }
                        if(image!=''){
                            jq("#profile_image").attr({ "src": image });
                            jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                        }
                        break;

                    case "ACCEPTED":
                        clearMap=1;
                        var onoff = response.data.onOffStatus;
                        onOffStatus(onoff);
                        var image = response.data.requestData.passenger_img;
                        var name = response.data.requestData.passenger_firstName+' '+response.data.requestData.passenger_lastName;
                        var phone_no = response.data.requestData.passenger_isdcode+' '+response.data.requestData.passenger_mobile;
                        var source_latitude = response.data.requestData.location_details.source.latitude;
                        var source_longitude = response.data.requestData.location_details.source.longitude;
                        var destination_latitude = response.data.requestData.location_details.destination.latitude;
                        var destination_longitude = response.data.requestData.location_details.destination.longitude;
                        var request_id = response.data.requestData.request_id;
                        var status = response.data.status;
                        if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=source_latitude || oldLocation.Dlog!=source_longitude){
                            googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(source_latitude,source_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=source_latitude; oldLocation.Dlog=source_longitude;
                        }
                        if($("#arriveRequestForm").length==0){
                            arriveRequestForm(name,phone_no,request_id);
                            chatBoxJS(request_id);
                            if(image!=''){
                                jq("#chat_image").attr({ "src": image });
                                jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                            }
                        }
                        getMessage(request_id);
                        break;

                    case "REACHED":
                        clearMap=1;
                        var onoff = response.data.onOffStatus;
                        onOffStatus(onoff);
                        var image = response.data.requestData.passenger_img;
                        var name = response.data.requestData.passenger_firstName+' '+response.data.requestData.passenger_lastName;
                        var phone_no = response.data.requestData.passenger_isdcode+' '+response.data.requestData.passenger_mobile;
                        var source_latitude = response.data.requestData.location_details.source.latitude;
                        var source_longitude = response.data.requestData.location_details.source.longitude;
                        var destination_latitude = response.data.requestData.location_details.destination.latitude;
                        var destination_longitude = response.data.requestData.location_details.destination.longitude;
                        var request_id = response.data.requestData.request_id;
                        var status = response.data.status;

                        if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=source_latitude || oldLocation.Dlog!=source_longitude){
                            googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(source_latitude,source_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=source_latitude; oldLocation.Dlog=source_longitude;
                        }
                        if($("#reachRequestForm").length==0){
                            reachRequestForm(name,phone_no,request_id);
                            chatBoxJS(request_id);
                            if(image!=''){
                                jq("#chat_image").attr({ "src": image });
                                jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                            }
                        }
                        getMessage(request_id);
                        break;

                    case "STARTED":
                        clearMap=1;
                        var onoff = response.data.onOffStatus;
                        onOffStatus(onoff);
                        var name = response.data.requestData.passenger_firstName+' '+response.data.requestData.passenger_lastName;
                        var phone_no = response.data.requestData.passenger_isdcode+' '+response.data.requestData.passenger_mobile;
                        var source_latitude = response.data.requestData.location_details.source.latitude;
                        var source_longitude = response.data.requestData.location_details.source.longitude;
                        var destination_latitude = response.data.requestData.location_details.destination.latitude;
                        var destination_longitude = response.data.requestData.location_details.destination.longitude;
                        var request_id = response.data.requestData.request_id;
                        var status = response.data.status;
                        if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude ||
                            oldLocation.Dlat!=destination_latitude || oldLocation.Dlog!=destination_longitude){
                            googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(destination_latitude,destination_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=destination_latitude; oldLocation.Dlog=destination_longitude;
                        }
                        if($("#startedRequestForm").length==0){
                            startedRequestForm(name,phone_no,request_id);
                        }
                        break;

                    case "DROP":
                        clearMap=1;
                        var onoff = response.data.onOffStatus;
                        onOffStatus(onoff);
                        var name = response.data.requestData.passenger_firstName+' '+response.data.requestData.passenger_lastName;
                        var request_id = response.data.requestData.request_id;
                        var estimated_earning = response.data.requestData.payable.currency+''+response.data.requestData.payable.cost;
                        var destination_latitude = response.data.requestData.location_details.destination.latitude;
                        var destination_longitude = response.data.requestData.location_details.destination.longitude;
                        if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude ||
                            oldLocation.Dlat!=destination_latitude || oldLocation.Dlog!=destination_longitude){
                            googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(destination_latitude,destination_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=destination_latitude; oldLocation.Dlog=destination_longitude;
                        }
                        if($("#paymentRequestForm").length==0){
                            paymentRequestForm(name,estimated_earning,request_id);
                        }
                        break;

                    case "PAYMENT":
                        clearMap=1;
                        var onoff = response.data.onOffStatus;
                        onOffStatus(onoff);
                        var name = response.data.requestData.passenger_firstName+' '+response.data.requestData.passenger_lastName;
                        var request_id = response.data.requestData.request_id;
                        var picture = response.data.requestData.passenger_img;
                        var driver_rating_status = response.data.requestData.driver_rating_status;
                        var destination_latitude = response.data.requestData.location_details.destination.latitude;
                        var destination_longitude = response.data.requestData.location_details.destination.longitude;
                        if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude ||
                            oldLocation.Dlat!=destination_latitude || oldLocation.Dlog!=destination_longitude){
                            googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(destination_latitude,destination_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=destination_latitude; oldLocation.Dlog=destination_longitude;
                        }
                        if($("#ratingRequestForm").length==0){
                            ratingRequestForm(name,picture,request_id);
                            $(function () {
                                $("#rateYo").rateYo().on("rateyo.set", function (e, data) {
                                    $('#rating').val(data.rating);
                                });
                            });
                        }
                        if(picture!=''){
                           jq("#profile_image").attr({ "src": picture });
                           jq("#profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                        }
                        break;

                        case "NORMAL":
                            var onoff = response.data.onOffStatus;
                            onOffStatus(onoff);
                           // googleMap="";
                           if(clearMap==1)
                           {
                               googleMap="";
                               clearMap=0;
                               location.reload();
                            }
                            //drawMap(mapObject,new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude));
                        break;
                    }
                    $(".overlay").hide();
                    getCurrentLocation("no");
            },
            error: function(response){
                console.log(response);
                $(".overlay").hide();
                getCurrentLocation("no");
            }
        });
    }


    function onOffStatus(onoff){
        if (onoff=='online') {
            $('.stripe_check').attr('checked','checked');
        }
        if (onoff=='offline') {
            $('.stripe_check').removeAttr('checked');
        }
    }

    function acceptRequestForm(image,name,pick_point,drop_point,total_distance,estimated_earning,request_id){
        $('.DriverMap').removeClass('col-lg-12');
        $('.DriverMap').addClass('col-lg-8');
        $('#FormOnDriverMap').html(`
            <div class="col-lg-12 px-0 d-flex align-items-center" id="acceptRequestForm">
                <div class="py-5 px-md-4 home-header">

                    <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                    <div class="d-block my-2">
                        <div class="profile-image position-relative my-0 d-flex justify-content-center align-items-center">
                            <img src="https://entrong.com/storage/assets/img/download1.png" alt="Profile" id="profile_image">
                        </div>
                    </div>

                    <div class="row border-bottom">
                        <h2 class="text-center mb-3"><strong>`+name+`</strong></h2>
                    </div>
                    <div class="pt-4 px-2">
                        <div class="row mb-3">
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="font-weight-bold">
                                    <span class="bg-dark li-icon-sm d-inline-block mr-1"></span>Pick up point:
                                </h6>
                            </div>
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="text-muted">`+pick_point+`</h6>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="font-weight-bold">
                                    <span class="bg-warning rounded-circle li-icon-sm d-inline-block mr-1"></span>Drop off point:
                                </h6>
                            </div>
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="text-muted">`+drop_point+`</h6>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="font-weight-bold">
                                    Total distance:
                                </h6>
                            </div>
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="text-muted">`+total_distance+` miles</h6>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="font-weight-bold">
                                    Estimated earnings:
                                </h6>
                            </div>
                            <div class="col-sm-6 mx-0 px-0">
                                <h6 class="text-muted">`+estimated_earning+`</h6>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-lg grad text-white w-100 my-3" id="Accept">Accept</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-lg btn-outline-dark w-100 my-3" id="Reject">Reject</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        `);
    }

    function arriveRequestForm(name,phone_no,request_id){
        $('.DriverMap').removeClass('col-lg-12');
        $('.DriverMap').addClass('col-lg-8');
        $('#FormOnDriverMap').html(`
            <style>

            </style>
            <div class="p-5 row justify-content-center" id="arriveRequestForm">
            <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 8px 14px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
                <div class="bg-dark px-1 my-auto mx-1"
                    style="
                        height: 4px;
                        width: 20px;
                    "></div>
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 7px 12px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-car" aria-hidden="true"></i>
                </div>
                <div class="bg-dark px-1 my-auto mx-1"
                    style="
                        height: 4px;
                        width: 20px;
                    "></div>
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 9px 16px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-map-pin" aria-hidden="true"></i>
                </div>
            </div>
            <div class="row px-5">
               <div class="col-sm-8">
                    <h4 class="text-left text-muted mb-2">`+name+`</h4>
                            <h5>`+phone_no+`</h5>
                </div>
                <div class="col-sm-4 px-2 py-1">
                    <div class="rounded-circle grad" style="
                            height: 55px;
                            width: 55px;
                            padding: 14px;
                            background-color: blue;
                            float: right;
                            padding: 12px;
                        ">
                        <img src="https://entrong.com/storage/assets/img/phone.png" alt="call">
                    </div>
                </div>
            </div>
            <div class="row px-5">
                <div class="col-sm-12 px-2 py-1">
                    <div class="chatbox" style="bottom: -58px;right:0px">
                        <div class="chatbox__support" style="height: 477px;">
                            <div class="chatbox__header" style="width:100%">
                                <div class="chatbox__image--header" style="margin-left:0">

                                    <div class="profile-image position-relative d-flex justify-content-center align-items-center">
                                            <img src="https://entrong.com/storage/assets/img/download1.png" alt="Chat" id="chat_image">
                                    </div>

                                </div>
                                <div class="chatbox__content--header" style="margin-left:0; margin-right:50%">
                                    <h4 class="chatbox__heading--header">`+name+`</h4>
                                    <p class="chatbox__description--header"></p>
                                </div>
                            </div>
                            <div class="chatbox__messages" style="width:100%">
                                <div id="text_message" style="width:100%">

                                    <div class="messages__item messages__item--typing">
                                        <span class="messages__dot"></span>
                                        <span class="messages__dot"></span>
                                        <span class="messages__dot"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="chatbox__footer" style="width:100%">
                                <form id="messageForm" style="width:100%">
                                <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                                <input type="text" name="message" id="message" placeholder="Write a message..." autocomplete="off" required style="width:85%">
                                <button type="submit" class="chatbox__send--footer float-right"><i class="fas fa-paper-plane"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="rounded-circle grad chatbox__button" style="
                                        height: 55px;
                                        width: 55px;
                                        padding: 14px;
                                        background-color: blue;
                                        float: right;
                                        margin-right: 7px;
                                        padding: 12px;
                                    ">
                            <img src="https://entrong.com/storage/assets/img/chat-btn.png" alt="chat">
                        </div>

                    </div>

                </div>
            </div>
            <div class="row px-5 py-5">
                <div class="col-sm-12 px-2 py-1">
                <button type="submit" class="btn btn-lg grad text-white w-100 my-3" id="reached">{{trans('weblng.BOOKING.TAP_ARIVE')}}</button>
                </div>
                <div class="col-sm-12 px-2 py-0">
                <button type="button" class="btn btn-lg grad text-white w-100 my-3" id="cancel">Cancel Ride</button>
                </div>
            </div>

            `);
    }

    function reachRequestForm(name,phone_no,request_id){
        $('.DriverMap').removeClass('col-lg-12');
        $('.DriverMap').addClass('col-lg-8');
        $('#FormOnDriverMap').html(
            `
            <div class="p-5 row justify-content-center" id="reachRequestForm">
            <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 8px 14px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-map-marker" aria-hidden="true" style="color:#093bba;"></i>
                </div>
                <div class="bg-dark px-1 my-auto mx-1"
                    style="
                        height: 4px;
                        width: 20px;
                    "></div>
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 7px 12px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-car" aria-hidden="true"></i>
                </div>
                <div class="bg-dark px-1 my-auto mx-1"
                    style="
                        height: 4px;
                        width: 20px;
                    "></div>
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 9px 16px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-map-pin" aria-hidden="true"></i>
                </div>
            </div>
            <div class="row px-5">
               <div class="col-sm-8">
                    <h4 class="text-left text-muted mb-2">`+name+`</h4>
                            <h5>`+phone_no+`</h5>
                </div>
                <div class="col-sm-4 px-2 py-1">
                    <div class="rounded-circle grad" style="
                            height: 55px;
                            width: 55px;
                            padding: 14px;
                            background-color: blue;
                            float: right;
                            padding: 12px;
                        ">
                        <img src="https://entrong.com/storage/assets/img/phone.png" alt="call">
                    </div>
                </div>
            </div>
            <div class="row px-5">
                <div class="col-sm-12 px-2 py-1">
                    <div class="chatbox" style="bottom: -58px;right:0px">
                        <div class="chatbox__support" style="height: 477px;">
                            <div class="chatbox__header" style="width:100%">
                                <div class="chatbox__image--header" style="margin-left:0">
                                    <div class="profile-image position-relative d-flex justify-content-center align-items-center">
                                            <img src="https://entrong.com/storage/assets/img/download1.png" alt="Chat" id="chat_image">
                                    </div>
                                </div>
                                <div class="chatbox__content--header" style="margin-left:0; margin-right:50%">
                                    <h4 class="chatbox__heading--header">`+name+`</h4>
                                    <p class="chatbox__description--header"></p>
                                </div>
                            </div>
                            <div class="chatbox__messages" style="width:100%">
                                <div id="text_message" style="width:100%">

                                    <div class="messages__item messages__item--typing">
                                        <span class="messages__dot"></span>
                                        <span class="messages__dot"></span>
                                        <span class="messages__dot"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="chatbox__footer" style="width:100%">
                                <form id="messageForm" style="width:100%">
                                <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                                <input type="text" name="message" id="message" placeholder="Write a message..." autocomplete="off" required style="width:85%">
                                <button type="submit" class="chatbox__send--footer float-right"><i class="fas fa-paper-plane"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="rounded-circle grad chatbox__button" style="
                                        height: 55px;
                                        width: 55px;
                                        padding: 14px;
                                        background-color: blue;
                                        float:right;
                                        margin-right: 7px;
                                        padding: 12px;
                                    ">
                            <img src="https://entrong.com/storage/assets/img/chat-btn.png" alt="chat">
                        </div>
                    </div>
                </div>

            </div>

            <div class="row px-5 py-5">
                <div class="col-sm-12 px-2 py-1">
                <button type="submit" class="btn btn-lg grad text-white w-100 my-3" id="started">Tap When Start</button>
                </div>
            </div>
            `);
    }

    function startedRequestForm(name,phone_no,request_id){
        $('.DriverMap').removeClass('col-lg-12');
        $('.DriverMap').addClass('col-lg-8');
        $('#FormOnDriverMap').html(
            `<div class="p-5 row justify-content-center" id="startedRequestForm">
            <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 8px 14px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-map-marker" aria-hidden="true" style="color:#093bba;"></i>
                </div>
                <div class="bg-dark px-1 my-auto mx-1"
                    style="
                        height: 4px;
                        width: 20px;
                    "></div>
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 7px 12px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-car" aria-hidden="true" style="color:#093bba;"></i>
                </div>
                <div class="bg-dark px-1 my-auto mx-1"
                    style="
                        height: 4px;
                        width: 20px;
                    "></div>
                <div class="rounded-circle mx-2"
                    style="
                        height: 40px;
                        width: 40px;
                        padding: 9px 16px;
                        background-color: #bdbdbd;
                    ">
                    <i class="fa fa-map-pin" aria-hidden="true"></i>
                </div>
            </div>
            <div class="row px-5">
               <div class="col-sm-9">
                    <h4 class="text-left text-muted mb-2">`+name+`</h4>
                            <h5>`+phone_no+`</h5>
                </div>
                <div class="col-sm-3 px-2 py-1">
                    <div class="rounded-circle grad" style="
                            height: 55px;
                            width: 55px;
                            padding: 12px;
                            background-color: blue;
                        ">
                        <img src="https://entrong.com/storage/assets/img/phone.png" alt="call">
                    </div>
                </div>
            </div>
            <div class="row px-5">
                <button type="submit" class="btn btn-lg grad text-white w-100 my-3" id="drop">Tap When Dropped</button>
            </div>`);
    }

    function paymentRequestForm(name,estimated_earning,request_id){
        $('.DriverMap').removeClass('col-lg-12');
        $('.DriverMap').addClass('col-lg-8');
        $('#FormOnDriverMap').html(
            `<div class="col-lg-12 px-0 d-flex align-items-center" id="paymentRequestForm">
                <div class="py-5 px-md-4 home-header">
                    <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                    <div class="row mb-3">
                        <img src="https://entrong.com/storage/assets/img/cash.png" alt="call">
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-12 mx-0 px-0">
                            <h2 class="font-weight-bold text-primary text-center" style="font-size: 45px;">`+estimated_earning+`</h2>
                        </div>
                    </div>
                    <div class="col-sm-12 mx-0 px-0">
                        <h5 class="text-muted text-center">Cash to be collected from</h5>
                    </div>
                    <div class="col-sm-12 mx-0 px-0">
                        <h5 class="text-muted text-center font-weight-bold">`+name+`</h5>
                    </div>
                    <div class="col-sm-10 px-3 py-1">
                        <button type="button" class="btn btn-lg grad text-white w-100 h-100 my-3" id="payment">Collect</button>
                    </div>
                </div>
            </div>`);
    }

    function ratingRequestForm(name,picture,request_id){
        $('.DriverMap').removeClass('col-lg-12');
        $('.DriverMap').addClass('col-lg-8');

        $('#FormOnDriverMap').html(
        `<div class="col-lg-12 px-0 d-flex align-items-center" id="ratingRequestForm">
            <div class="py-4 px-md-4 home-header w-100">
                <div class="col-lg-12 w-100">
                    <button type="button" class="close" id="DrivClose" title="cancel" aria-label="Close">
                        <img src="https://entrong.com/storage/assets/img/signs.png" alt="" class="m-close">
                    </button>
                </div>
                <input type="hidden" name="request_id" id="request_id" value="`+request_id+`">
                <div class="alert alert-danger drating w-100 mt-5" style="display:none;">
                    <p class="text-danger" id="drating-msg"></p>
                    <p class="text-danger" id="dcomment-msg"></p>
                </div>
                <h4 class="text-center">Rate your trip</h4>

                <div class="profile-image position-relative my-3 d-flex justify-content-center align-items-center">
                    <img src="https://entrong.com/storage/assets/img/download1.png" alt="Profile" id="profile_image">
                </div>
                <p class="text-center font-weight-bold">`+name+`</p>

                <form id="ratingForm">
                    <div id="rateYo"></div>
                    <input type="hidden" name="rating" id="rating">

                    <div class="border-bottom pt-5" style="padding-top:1rem !important">
                        <input type="text" name="comment" id="comment" placeholder="Write your comments" class="input_field" style="padding-top: 15px;">
                    </div>

                    <div class="col-sm-10 px-3 py-1" style="width:180px;">
                        <button type="submit" style="margin-bottom:0 !important" class="btn btn-lg grad text-white w-100 h-100 my-4">
                        Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>`);
    }
    getCurrentLocation("yes");

    jqMap(document).on('submit','#messageForm', function(event){
        event.preventDefault();
        jqMap.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        var message = $('#message').val();
        var user_scope = 'driver-service';
        jqMap.ajax({
            // url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/driver/ride/message",
            url: "https://entrong.com/driver/ride/message",
            method:"POST",
            data:{ request_id: request_id, user_scope: user_scope, message: message, timeZone: timeZone },
            success: function(response){
                console.log(response);

                getMessage(request_id);
                $('#message').val('');
            },
            error: function(response){
            }
        });
    });

    jqMap(document).on('click','#cancel', function(event){
        event.preventDefault();
        jqMap.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var request_id = $('#request_id').val();
        $(".overlay").show();
        jqMap.ajax({
            // url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/driver/cancel/ride",
            url: "https://entrong.com/driver/cancel/ride",
            method:"POST",
            data:{ request_id: request_id, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                location.reload();
            },
            error: function(response){
                $(".overlay").hide();
            }
        });
    });


});


function chatBoxJS(request_id){
    class InteractiveChatbox {
        constructor(a, b, c) {
            this.args = {
                button: a,
                chatbox: b
            }
            this.icons = c;
            this.state = false;
        }

        display() {
            const {button, chatbox} = this.args;

            button.addEventListener('click', () => this.toggleState(chatbox))
        }

        toggleState(chatbox) {
            this.state = !this.state;
            this.showOrHideChatBox(chatbox, this.args.button);
        }

        showOrHideChatBox(chatbox, button) {
            if(this.state) {
                chatbox.classList.add('chatbox--actived')
                this.toggleIcon(true, button);
            } else if (!this.state) {
                chatbox.classList.remove('chatbox--actived')
                this.toggleIcon(false, button);
            }
        }

        toggleIcon(state, button) {
            const { isClicked, isNotClicked } = this.icons;
            let b = button.children[0].innerHTML;

            if(state) {
                button.children[0].innerHTML = isClicked;
            } else if(!state) {
                button.children[0].innerHTML = isNotClicked;
            }
        }
    }

    const chatButton = document.querySelector('.chatbox__button');
    const chatContent = document.querySelector('.chatbox__support');
    const icons = {
        isClicked: '<img src="./images/icons/chatbox-icon.svg" />',
        isNotClicked: '<img src="./images/icons/chatbox-icon.svg" />'
    }
    const chatbox = new InteractiveChatbox(chatButton, chatContent, icons);
    chatbox.display();
    chatbox.toggleIcon(false, chatButton);

    getMessage(request_id);
}

timeAgo = (date) => {
    var ms = (new Date()).getTime() - date.getTime();
    var seconds = Math.floor(ms / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);
    var months = Math.floor(days / 30);
    var years = Math.floor(months / 12);
    if (ms === 0) {
        return 'just now';
    } if (seconds < 0) {
        return '0 second ago';
    } if (seconds === 0 || seconds === 1) {
        return seconds + ' second ago';
    } if (seconds < 60 && seconds > 1) {
        return seconds + ' seconds ago';
    } if (minutes === 1) {
        return minutes + ' minute ago';
    } if (minutes < 60 && minutes > 1) {
        return minutes + ' minutes ago';
    } if (hours === 1) {
        return hours + ' hour ago';
    } if (hours < 24 && hours > 1) {
        return hours + ' hours ago';
    } if (days === 1) {
        return 'yesterday';
    } else {
        return moment(date).format('YYYY-MMM-DD');
    }
}

function getMessage(request_id){
    jqMap.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var request_id=request_id;
    jqMap.ajax({
            // url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/driver/get/message",
            url: "https://entrong.com/driver/get/message",
            method:"POST",
            data:{ request_id: request_id, timeZone: timeZone },
            success: function(response){
                console.log(response.data);
                $('#text_message').html('');
                var options='';
                $.each(response.data, function(key, value) {
                    console.log(value.user_scope);
                    if (value.user_scope=='passenger-service') {
                        options+=`<div class="messages__item messages__item--visitor" style="margin-left:0">
                            <p class="text-muted pt-0">`+value.fname+` `+value.lname+`</p>
                            <p class="py-1" style="color: black;">`+value.message+`</p>
                            <p class="text-right text-muted pb-0" style="font-size: 11px;">`+timeAgo(new Date(value.updated_at))+`</p>
                        </div>`;
                    }
                    if (value.user_scope=='driver-service') {
                        options+=`<div class="messages__item messages__item--operator" style="margin-right:0">
                            <p class="text-muted pt-0 text-right">`+value.fname+` `+value.lname+`</p>
                            <p class="py-1" style="color: black;">`+value.message+`</p>
                            <p class="text-left text-muted pb-0" style="font-size: 11px;">`+timeAgo(new Date(value.updated_at))+`</p>
                        </div>`;
                    }
                });
                $('#text_message').append(options);

            },
            error: function(response){
            }
        });
}
