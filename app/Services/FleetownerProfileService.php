<?php

namespace App\Services;

use App\Services\CurlService;


class FleetownerProfileService
{

	private function changePassword($data)
	{
		try{
			$curl_url = env('serverURL').'fleetowner/change/password';
			$method = "POST";
			$array = ['old_password'=>$data->old_password, 'password'=>$data->password, 'password_confirmation'=>$data->password_confirmation];
      		$json_encode = json_encode($array);
			$timeZone = $data->timeZone;
			$token = $data->token;
			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);
			
			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}

	private function profileImageUpdate($data)
	{
		try{
			$curl_url = env('serverURL').'fleetowner/profile/image';
			$postfields = ['picture'=> new \CURLFILE($data->tmp_name,$data->type,$data->name)];

			$curlService = new CurlService;
			$curl = $curlService->accessAuthFileCurl($curl_url,$postfields,$data);
			
			$response = curl_exec($curl); 
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);

			if ($httpcode == 422) {
				return ['message'=>$response['message'],"field"=>$response['field'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
			}
			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}
	

	private function getProfile($data)
	{
		try{
			$curl_url = env('serverURL').'fleetowner/profile';
			$method = "GET";
			$json_encode = "";
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);

			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}


	private function updateProfile($data)
	{
		try{
			$curl_url = env('serverURL').'fleetowner/profile/update';
			$method = "POST";
			$array = [
				'first_name'=>$data->first_name,
				'last_name'=>$data->last_name,
				'email_id'=>$data->email_id,
				'gender'=>$data->gender
			];

      		$json_encode = json_encode($array);
			$timeZone = $data->timeZone;
			$token = $data->token;
			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			
			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}


	public function accessChangePassword($data)
	{
    	return $this->changePassword($data);
  	}

  	public function accessProfileImageUpdate($data)
	{
    	return $this->profileImageUpdate($data);
  	}

  	public function accessGetProfile($data)
	{
    	return $this->getProfile($data);
  	}

  	public function accessUpdateProfile($data)
	{
    	return $this->updateProfile($data);
  	}

}
