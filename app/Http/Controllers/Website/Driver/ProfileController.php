<?php

namespace App\Http\Controllers\Website\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DriverProfileService;


class ProfileController extends Controller
{
	public function homeIndex()
	{
		return view('pages.home.driver_home');
	}

	public function accountIndex()
	{
		return view('pages.driver.myaccount');
	}

	public function mapLocation()
	{
		return view('pages.driver.map');
	}

	public function profileImageUpdate(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'picture' => 'required|mimes:jpeg,bmp,png',
	        ]);
	
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $tmp_name = $_FILES['picture']['tmp_name'];
	        $type = $_FILES['picture']['type'];
	        $name = basename($_FILES['picture']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
	        $request['name'] = $name;
	        $request['size']=$_FILES['picture']['size'];

        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessProfileImageUpdate($request);
	    
	      	if ($result['statusCode']==422) {
				return response(['message'=>$result['message'],"field"=>$result['field'],"errors"=>$result['errors']],$result['statusCode']);
			}
	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function getProfile(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessGetProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function updateDriverProfile(Request $request)
	{
		try{
			$this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'gender'=>'sometimes|nullable|in:Male,Female',
                'service_type'=>'required',
                'car_make'=>'required',
                'car_model'=>'required',
                'model_year'=>'required',
                'car_number'=>'required'
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
      		$request->first_name = $request->first_name;
      		$request->last_name = $request->last_name;
      		$request->gender = $request->gender;
      		$request->email_id = $request->email_id;
      		$request->service_type_id = $request->service_type;
   			// $array = explode(',', $request->car_model);
			// $request->model = $array[0].' '.$array[1];
			$request->make = $request->car_make;
			$request->model = $request->car_model;
      		$request->model_year = $request->model_year;
      		$request->registration_no = $request->car_number;

        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessUpdateProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function driverOnOffStatus(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->status = $request->status;

        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessDriverOnOff($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function cancelRide(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
			$request->request_id = $request->request_id;
			$request->cancel_reason_id = $request->cancel_reason_id;
			$request->reason_optional = $request->reason_optional;
        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessCancelRideRequest($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}
	
}
