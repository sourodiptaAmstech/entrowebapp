<style>
    #image{
        cursor: pointer;
    }
</style>
<div class="shadow-lg p-3 my-5 bg-white rounded">
    <div class="d-block">
        <div class="profile-image position-relative my-3 d-flex justify-content-center align-items-center" data-toggle="tooltip" title="{{trans('weblng.SIDEBAR.EDIT_IMAGE')}}">
            <img src="{{URL::asset('/')}}storage/assets/img/download1.png" alt="Profile" id="image">

            <span class="edit-profile bg-white rounded-circle d-inline-flex justify-content-center align-items-center position-absolute" data-placement="bottom">
                <i class="fas fa-edit"></i>
            </span>

            <div style="display: none;">
            <form id="formdata" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" name="picture" id="picture">
                <input type="hidden" name="timeZone" id="timeZone">
            <button type="submit" id="submit"></button>
            </form>
            </div>

        </div>

        <a href="{{ url('passenger/account') }}" class="text-default" data-toggle="tooltip" title="My Account">
        <h4 class="text-center font-weight-bold profile_name" style="color: grey;"></h4></a>

    </div>
    <hr class="my-4" style="width: 60%" />
    <ul class="list-group sidebar mb-4">
        {{-- <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/web.png" alt="" class="mr-3 li-icon">
            Inbox
        </li> --}}

        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/business-and-finance.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('passenger/payment') ? 'active text-dark' : '' }}{{ Request::is('passenger/addpayment') ? 'active text-dark' : '' }}{{ Request::is('passenger/transaction') ? 'active text-dark' : '' }}" href="{{ route('passenger.payment') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.PAYMENTS')}}</a>
        </li>

        {{-- <li class="list-group-item font-weight-bold">
            <a href="#">
                <span class="s-text">Vehicle</span>
                <span class="s-caret"><i class="fa fa-angle-down"></i></span>
            </a>
            <ul>
                <li><a href="#">List Vehicles</a></li>
                <li><a href="#">Add New Vehicle</a></li>
            </ul>
        </li> --}}

        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/tools-and-utensils.png" alt="" class="mr-3 li-icon">
           
            <a class="{{ Request::is('passenger/trip-history') ? 'active text-dark' : '' }}{{ Request::is('passenger/trip-details') ? 'active text-dark' : '' }}" href="{{ route('passenger.trip-history') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.HISTORY')}}</a>

        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/resume.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('passenger/upcoming-trip') ? 'active text-dark' : '' }}" href="{{ route('passenger.upcoming-trip') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.SCHETRIP')}}</a>
        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/chat.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('passenger/chat-support') ? 'active text-dark' : '' }}" href="{{ route('passenger.chat-support') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.CHATSUPPORT')}}</a>
        </li>
        <li class="list-group-item font-weight-bold">
            <img src="{{URL::asset('/')}}storage/assets/img/social-media.png" alt="" class="mr-3 li-icon">
            <a class="{{ Request::is('passenger/promocode') ? 'active text-dark' : '' }}" href="{{ route('passenger.promocode') }}" style="color: currentcolor;">{{trans('weblng.SIDEBAR.PROMO')}}</a>
        </li>
    </ul>
</div>

<script>
    jq = jQuery.noConflict();
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    jq(".overlay").show();
    jq.ajax({
        url: "{{ route('passenger.profile.list') }}",
        method:"GET",
        data:{ timeZone: timeZone },
        success: function(response){
            console.log(response);
            jq(".overlay").hide();
            var first_name = response.data.user_profile.first_name;
            var last_name = response.data.user_profile.last_name;
            jq('.profile_name').text(first_name.charAt(0).toUpperCase()+ first_name.slice(1)+' '+last_name.charAt(0).toUpperCase()+ last_name.slice(1));

            jq('#first_name').val(response.data.user_profile.first_name);
            jq('#last_name').val(response.data.user_profile.last_name);
            jq('#gender').val(response.data.user_profile.gender);
            jq('#dob').val(response.data.user_profile.dob);
            jq('#email_id').val(response.data.user_profile.email_id);
            jq('#isdCode').val(response.data.user_profile.isdCode);
            jq('#mobile').val(response.data.user_profile.mobile);

            var picture = response.data.user_profile.picture;
            if(picture!=''&&picture!=null){
                jq("#image").attr({ "src": picture });
                jq("#image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
            }
            var login_by = response.data.user_profile.login_by;
            if(login_by == 'manual'){
                jq('#changePassword').show();
            } else {
                jq('#changePassword').hide();
            }
            jq('#emergOne_id').val(response.data.emergency_contacts[0].emergency_id);
            jq('#first_contact_name').val(response.data.emergency_contacts[0].name);
            jq('#emergOne_isdCode').val(response.data.emergency_contacts[0].isd_code);
            jq('#first_contact_mobile_no').val(response.data.emergency_contacts[0].contact_no);

            jq('#emergTwo_id').val(response.data.emergency_contacts[1].emergency_id);
            jq('#second_contact_name').val(response.data.emergency_contacts[1].name);
            jq('#emergTwo_isdCode').val(response.data.emergency_contacts[1].isd_code);
            jq('#second_contact_mobile_no').val(response.data.emergency_contacts[1].contact_no);
        },
        error: function(response){
            console.log(response);
            jq(".overlay").hide();
            var responseMsg = jq.parseJSON(response.responseText);
            jq('.px-sm-3').removeClass('pt-5');
            jq('.update_alert').show();
            jq('#update-msg').html(responseMsg.message);
        }
    });


    jq('#timeZone').val(timeZone);
    jq(".profile-image").click(function(e) {
        jq("#picture").click();
     //   jq(".profile-image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
    });
    function fasterPreview(uploader) {
        if ( uploader.files && uploader.files[0] ){
        jq('#image').attr('src',
        window.URL.createObjectURL(uploader.files[0]) );
        jq("#image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
        }
    }
    jq('input:file').change(function() {
        fasterPreview(this);
        jq("#submit").click();
    });
    jq('#formdata').on('submit', function(event){
        event.preventDefault();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('passenger.profile.image') }}",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                jq(".overlay").hide();
                $('#bootboxModal').modal('show');
                $('.bootboxBody').text('{{trans('weblng.PICTURE_UPDATE')}}');
                $('button.bootboxBtn').on('click', function(){
                    $("#bootboxModal").modal('hide');
                    location.reload();
                });
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422) {
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.update_alert').show();
                    jq('#update-msg').html(responseMsg.errors.picture);
                }
            }
        });
    });

</script>
