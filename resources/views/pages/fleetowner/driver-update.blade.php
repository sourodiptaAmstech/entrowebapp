<!-- header -->
@include('partials.fleetowner_header')
<style type="text/css">
    .has-error{
        border:1px solid red;
    }
</style>
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.fleetowner_sidebar')
            </div>
            <div class="col-lg-8">
                <form class="" id="fleetdriverProfileUpdateFrom">
                    <div class="px-sm-3 pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success dupdatesuc_alert" style="display: none;">
                            <p class="text-success" id="dupdatesuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">Driver Info{{-- {{trans('weblng.PERSONAL_INFO')}} --}}</h2>
                        <input type="hidden" name="driver_id" value="{{$driver_id}}" id="driver_id">
                        <div class="form-group">
                            <label for="first_name" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_FIRST_NAME')}}</label>
                            <input type="text" name="first_name" id="first_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.FIRST_NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="fname"></span>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_LAST_NAME')}}</label>
                            <input type="text" name="last_name" id="last_name" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.LAST_NAME')}}" onkeyup="return validate()" style="text-transform: capitalize;">
                            <span class="text-danger" id="lname"></span>
                        </div>
                        <div class="form-group">
                            <label for="gender" class="font-weight-bold">{{trans('weblng.FIELD.YOUR_GENDER')}}</label>
                            <select class="form-control inset-input" name="gender" id="gender">
                                <option value="">{{trans('weblng.FIELD.Select')}}</option>
                                <option value="Male">{{trans('weblng.FIELD.MALE')}}</option>
                                <option value="Female">{{trans('weblng.FIELD.FEMALE')}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email_id" class="font-weight-bold">{{trans('weblng.FIELD.EMAIL_ADDRESS')}}</label>
                            <input type="email" name="email_id" id="email_id" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.EMAIL')}}">
                            <span class="text-danger" id="emailadd"></span>
                        </div>
                        <div class="form-group mb-5">
                            <label for="mobile" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}}</label>
                            <input type="hidden" name="isdCode" id="isdCode">
                            <input type="text" name="mobile" id="mobile" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.MOBILE_NO')}}" readonly style="background: white;">
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.SERVICE_DETAILS')}}</h2>
                        <div class="form-group">
                            <label for="service_type" class="font-weight-bold">{{trans('weblng.FIELD.SERVICE_TYPE')}}</label>
                            <select name="service_type" id="service_type" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="service"></span>
                        </div>

                        <div class="form-group">
                            <label for="car_make" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MAKE')}}</label>
                            <select name="car_make" id="car_make" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="make"></span>
                        </div>
                        <div class="form-group">
                            <label for="car_model" class="font-weight-bold">{{trans('weblng.FIELD.CAR_MODEL')}}</label>
                            <select name="car_model" id="car_model" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="model"></span>
                        </div>
                        <div class="form-group">
                            <label for="model_year" class="font-weight-bold">{{trans('weblng.FIELD.MODEL_YEAR')}}</label>
                            <select name="model_year" id="model_year" class="form-control inset-input" onchange="return validate()">
                                
                            </select>
                            <span class="text-danger" id="year"></span>
                        </div>

                        <div class="form-group">
                            <label for="car_number" class="font-weight-bold">{{trans('weblng.FIELD.CAR_NO')}}</label>
                            <input type="text" class="form-control inset-input" name="car_number" id="car_number" placeholder="{{trans('weblng.FIELD.CAR_NO_PLACE')}}" onkeyup="return validate()">
                            <span class="text-danger" id="carno"></span>
                        </div>

                        <div class="d-flex justify-content-start mb-3 mt-5">
                            <button type="submit" class="btn btn-lrg grad text-white">{{trans('weblng.FIELD.SAVE')}}</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>

<script>
    jq = jQuery.noConflict();
   
    
    function validate(){
        var status=null;

        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_LEAST')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='{{trans('weblng.VALIDATION_MSG.FIRST_NAME_GREATER')}}'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            jq('.px-sm-3').addClass('pt-5');
            jq('.dupdate_alert').hide();
            jq('#dupdate-msg').html('');
            jq('.dupdatesuc_alert').hide();
            jq('#dupdatesuc-msg').html('');
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_LEAST')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='{{trans('weblng.VALIDATION_MSG.LAST_NAME_GREATER')}}'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var serviceType = document.getElementById('service_type').value
        if (serviceType == '') {
            document.getElementById("service").innerHTML='{{trans('weblng.VALIDATION_MSG.SERVICE_TYPE')}}'
            document.getElementById('service_type').classList.add('has-error')
            status=false
        } else {
            document.getElementById("service").innerHTML=''
            document.getElementById('service_type').classList.remove('has-error')
        }

        var carMake = document.getElementById('car_make').value
        if (carMake == '') {
            document.getElementById("make").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_MAKE')}}'
            document.getElementById('car_make').classList.add('has-error')
            status=false
        } else {
            document.getElementById("make").innerHTML=''
            document.getElementById('car_make').classList.remove('has-error')
        }

        var carModel = document.getElementById('car_model').value
        if (carModel == '') {
            document.getElementById("model").innerHTML='{{trans('weblng.VALIDATION_MSG.MODEL')}}'
            document.getElementById('car_model').classList.add('has-error')
            status=false
        } else {
            document.getElementById("model").innerHTML=''
            document.getElementById('car_model').classList.remove('has-error')
        }

        var modelYear = document.getElementById('model_year').value
        if (modelYear == '') {
            document.getElementById("year").innerHTML='{{trans('weblng.VALIDATION_MSG.YEAR')}}'
            document.getElementById('model_year').classList.add('has-error')
            status=false
        } else {
            document.getElementById("year").innerHTML=''
            document.getElementById('model_year').classList.remove('has-error')
        }

        var carNumber = document.getElementById('car_number').value
        if (carNumber == '') {
            document.getElementById("carno").innerHTML='{{trans('weblng.VALIDATION_MSG.CAR_NUMBER')}}'
            document.getElementById('car_number').classList.add('has-error')
            status=false
        } else {
            document.getElementById("carno").innerHTML=''
            document.getElementById('car_number').classList.remove('has-error')
        }
        
        return status
    }



jq(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    //update fleet driver
    jq.ajax({
        url: "{{ route('service.list') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            jq('#service_type').empty();
            var options = '<option value="">{{trans('weblng.CHOOSE_SERVICE')}}</option>';
            jq.each(response.data, function(key, value) {
              options += '<option value="'+value.id+'">'+value.name+'</option>';
            });

            jq('#service_type').append(options);
        },
        error: function(response){
            if (response.status == 400){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#service_type').addClass('has-error');
                    });;
            }
            if (response.status == 500){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#service_type').addClass('has-error');
                    });;
            }
        }
    });

    jq(".overlay").show();
    jq.ajax({
        url: "{{ route('service.make') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            $(".overlay").hide();
            $('#car_make').empty();
            console.log(response)
            var options = '<option value="">{{trans('weblng.CHOOSE_MAKE')}}</option>';
            $.each(response.data, function(key, value) {
                console.log(value)
                options += '<option style="color:#080600;" value="'+value.make+'">'+value.make+'</option>';
            });
            $('#car_make').append(options);
        },
        error: function(response){
            $(".overlay").hide();
            if (response.status == 404){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#car_make').addClass('has-error');
                    });
            }
            if (response.status == 400){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#car_make').addClass('has-error');
                    });
            }
            if (response.status == 500){
                var responseMsg = jq.parseJSON(response.responseText);
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdate_alert').show();
                jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        jq('#car_make').addClass('has-error');
                    });
            }
        }
    });

    jq('#car_make').change(function(){
        var make = jq(this).val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.model') }}",
            method: "POST",
            data: {make: make, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#car_model').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_MODEL')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.model+'">'+value.model+'</option>';
                });
                $('#car_model').append(options);
                jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                    jq('#car_model').addClass('has-error');
                });
                $('#model_year').html('');
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#car_model').html('');
                    jq('#model').html('{{trans('weblng.VALIDATION_MSG.MODEL')}}').promise().done(function(){
                        jq('#car_model').addClass('has-error');
                    });

                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                }
            }
        });
    });

    jq('#car_model').change(function(){
        var model = jq(this).val();
        var make = jq('#car_make').val();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('service.year') }}",
            method: "POST",
            data: {make: make, model: model, timeZone: timeZone},
            success: function(response){
                $(".overlay").hide();
                $('#model_year').empty();
                console.log(response)
                var options = '<option value="">{{trans('weblng.CHOOSE_YEAR')}}</option>';
                $.each(response.data, function(key, value) {
                    console.log(value)
                    options += '<option style="color:#080600;" value="'+value.year+'">'+value.year+'</option>';
                });
                $('#model_year').append(options);
                jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                    jq('#model_year').addClass('has-error');
                });
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    console.log(response.status)
                    $('#model_year').html('');
                    jq('#year').html('{{trans('weblng.VALIDATION_MSG.YEAR')}}').promise().done(function(){
                        jq('#model_year').addClass('has-error');
                    });
                }
                if (response.status == 404){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                }
            }
        });

    });


    jq(document).ajaxComplete(function(event,xhr,settings) {
        if(settings.url == "{{ route('service.make') }}?timeZone=GMT%2B05%3A30")
        {   
            var driver_id = jq('#driver_id').val();
            jq(".overlay").show();
            jq.ajax({
                url: "{{ route('get.fleetowner.driver.details') }}?driver_id="+driver_id,
                method:"GET",
                data:{ timeZone: timeZone },
                success: function(response){
                    jq(".overlay").hide();
                    jq('#first_name').val(response.data.user_profile[0].first_name);
                    jq('#last_name').val(response.data.user_profile[0].last_name);
                    jq('#gender').val(response.data.user_profile[0].gender);
                    jq('#email_id').val(response.data.user_profile[0].email_id);
                    jq('#isdCode').val(response.data.user_profile[0].isd_code);
                    jq('#mobile').val(response.data.user_profile[0].mobile_no);
                    // var picture = response.data.user_profile[0].picture;
                    // if(picture!=''&&picture!=null){
                    //     jq("#image").attr({ "src": picture });
                    //     jq("#image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });

                    // }
                    jq('#service_type').val(response.data.user_profile[0].service_type_id);

                    var make = response.data.user_profile[0].make;
                    var model = response.data.user_profile[0].model;

                    jq('#car_make').val(make);

                    if (make) {

                        jq.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $(".overlay").show();
                        jq.ajax({
                            url: "{{ route('service.model') }}",
                            method: "POST",
                            data: {make: make, timeZone: timeZone},
                            success: function(response){
                                $(".overlay").hide();
                                $('#car_model').empty();
                                console.log(response)
                                var options = '<option value="">{{trans('weblng.CHOOSE_MODEL')}}</option>';
                                $.each(response.data, function(key, value) {
                                    console.log(value)
                                    options += '<option style="color:#080600;" value="'+value.model+'">'+value.model+'</option>';
                                });
                                $('#car_model').append(options);

                                jq('#car_model').val(model);
                               
                            },
                            error: function(response){
                                $(".overlay").hide();
                                if (response.status == 404){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#car_model').addClass('has-error');
                                        });
                                }
                                if (response.status == 400){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#car_model').addClass('has-error');
                                        });
                                }
                                if (response.status == 500){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#car_model').addClass('has-error');
                                        });
                                }
                            }
                        });
                        
                    }

                    var modelY = response.data.user_profile[0].model_year;
                    if (model) {
                        jq.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $(".overlay").show();
                        jq.ajax({
                            url: "{{ route('service.year') }}",
                            method: "POST",
                            data: {make: make, model: model, timeZone: timeZone},
                            success: function(response){
                                $(".overlay").hide();
                                $('#model_year').empty();
                                console.log(response)
                                var options = '<option value="">{{trans('weblng.CHOOSE_YEAR')}}</option>';
                                $.each(response.data, function(key, value) {
                                    console.log(value)
                                    options += '<option style="color:#080600;" value="'+value.year+'">'+value.year+'</option>';
                                });
                                $('#model_year').append(options);
                                $('#model_year').val(modelY);
                            },
                            error: function(response){
                                $(".overlay").hide();
                                if (response.status == 404){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#model_year').addClass('has-error');
                                        });
                                }
                                if (response.status == 400){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#model_year').addClass('has-error');
                                        });
                                }
                                if (response.status == 500){
                                    var responseMsg = jq.parseJSON(response.responseText);
                                    jq('.px-sm-3').removeClass('pt-5');
                                    jq('.dupdate_alert').show();
                                    jq('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                                            jq('#model_year').addClass('has-error');
                                        });
                                }
                            }
                        });
                    }

                    jq('#car_number').val(response.data.user_profile[0].registration_no);
                },
                error: function(response){
                    jq(".overlay").hide();
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
            });
        }
    });


 jq(document).ajaxComplete(function(event,xhr,settings) {
        var driver_id = jq('#driver_id').val();
        if(settings.url == "{{ route('get.fleetowner.driver.details') }}?driver_id="+driver_id+"&timeZone=GMT%2B05%3A30")
            {
        use :
            var s = jq("#mobile").intlTelInput({
                autoPlaceholder: 'polite',
                separateDialCode: true,
                formatOnDisplay: true,
                initialCountry: 'ng',
                preferredCountries:["ng"]
            });

        insteadof :
            var countryData3 = window.intlTelInputGlobals.getCountryData();
            var iso23;
            var isdcode3 = jq("#isdCode").val();
            for (var i = 0; i < countryData3.length; i++) {
                if (countryData3[i].dialCode == parseInt(isdcode3)){
                    iso23 = countryData3[i].iso2;
                    break;
                }
            }
            if(iso23){
                jq("#mobile").intlTelInput("setCountry", iso23);
            }
            jq(document).on('countrychange', function (e, countryData) {
                jq("#isdCode").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
            });
        }
    });


 
    //SUBMIT DRIVER FORM.................................
    jq('#fleetdriverProfileUpdateFrom').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var driver_id = jq('#driver_id').val();
        var first_name = jq('#first_name').val();
        var last_name = jq('#last_name').val();
        var gender = jq('#gender').val();
        var email_id = jq('#email_id').val();
        var service_type = jq('#service_type').val();
        var car_make = jq('#car_make').val();
        var car_model = jq('#car_model').val();
        var model_year = jq('#model_year').val();
        var car_number = jq('#car_number').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('fleetowner.driver.edit') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, gender: gender, email_id: email_id, service_type: service_type, car_make: car_make, car_model: car_model,model_year: model_year, car_number: car_number, driver_id: driver_id, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                var msgs = "{{trans('weblng.DRIVER_PRO_EDIT')}}";
                jq('.px-sm-3').removeClass('pt-5');
                jq('.dupdatesuc_alert').show();
                jq('#dupdatesuc-msg').html(msgs);
            },

            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        jq('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            jq('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        jq('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            jq('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('service_type')) {
                        jq('#service').html(responseMsg.errors.service_type).promise().done(function(){
                            jq('#service_type').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_make')) {
                        jq('#make').html(responseMsg.errors.car_make).promise().done(function(){
                            jq('#car_make').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_model')) {
                        jq('#model').html(responseMsg.errors.car_model).promise().done(function(){
                            jq('#car_model').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('model_year')) {
                        jq('#year').html(responseMsg.errors.model_year).promise().done(function(){
                            jq('#model_year').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('car_number')) {
                        jq('#carno').html(responseMsg.errors.car_number).promise().done(function(){
                            jq('#car_number').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#emailadd').html(responseMsg.message).promise().done(function(){
                            jq('#email_id').addClass('has-error');
                        });
                    }
                }
                if (response.status == 403){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.px-sm-3').removeClass('pt-5');
                    jq('.dupdate_alert').show();
                    jq('#dupdate-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT DRIVER FORM.................................
});
</script>
<!-- footer -->
@include('partials.fleetowner_footer')