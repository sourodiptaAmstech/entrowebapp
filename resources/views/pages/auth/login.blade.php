<!-- header -->
@include('partials.header')

<div class="container mb-5">
    <form class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="loginForm">
        <h1 class="text-center"><strong>{{trans('weblng.LOGIN.SIGN_IN_TO')}} <span class="text-theme">{{trans('weblng.LOGIN.RIDE')}}</span></strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>

        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <div class="alert alert-danger danger_alert" style="display: none;">
                <p class="text-danger" id="error-msg"></p>
            </div>

            <div class="mb-4">
            <div class="row justify-content-center">
                <div class="d-inline">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio1" name="user" value="passenger" onchange="return validate()" checked>
                        <label class="custom-control-label font-weight-bold" for="radio1">{{trans('weblng.LOGIN.CUSTOMER')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline fleet-margin">
                        <input type="radio" class="custom-control-input" id="radio2" name="user" value="driver" onchange="return validate()">
                        <label class="custom-control-label font-weight-bold" for="radio2">{{trans('weblng.LOGIN.DRIVER')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline fleet-margin">
                        <input type="radio" class="custom-control-input" id="radio3" name="user" value="fleetowner" onchange="return validate()">
                        <label class="custom-control-label font-weight-bold" for="radio3">{{trans('weblng.LOGIN.FLEETOWNER')}}</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <span id="usertype" class="text-danger" style="margin-right: 250px;"></span>
            </div>
            </div>

            <div class="form-group">
                <label for="mobile_no" class="font-weight-bold">{{trans('weblng.FIELD.MOBILE_NO')}} <br><span class="text-muted number-alert"><i class="fa fa-info-circle"></i>{{trans('weblng.MOBILE_REQ_MSG')}}</span></label>
                <input type="hidden" name="code" id="code">
                <input type="text" class="form-control inset-input" name="mobile_no" id="mobile_no" placeholder="{{trans('weblng.FIELD.MOBILE_NO_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="mobile"></span>
            </div>
            <div class="form-group">
                <label for="password" class="font-weight-bold">{{trans('weblng.FIELD.PASSWORD')}}</label>
                <input type="password" name="password" class="form-control inset-input" id="password" placeholder="{{trans('weblng.FIELD.PASSWORD_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="pass"></span>
            </div>
            <div>
                <h6 class="text-right text-muted"><a href="{{ url('forgotPassword') }}" class="text-dark">{{trans('weblng.LOGIN.FORGOT')}}?</a></h6>
            </div>
            <div class="d-flex justify-content-center my-3">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.LOGIN.SIGN_IN')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="dreload" style="display: none;"></i></div></button>
            </div>
            <div class="d-flex justify-content-center mb-5">
                <h5 class="text-muted">{{trans('weblng.LOGIN.DONT_HAVE_ACC')}}? <strong>
                    <a href="{{ url('signup') }}" class="text-dark">{{trans('weblng.LOGIN.SIGN_UP')}}</a>
                </strong></h5>
            </div>

            <div class="google_facebook">
                <div class="w-100 d-flex justify-content-center mt-5">
                    <a href="{{ route('login.facebook') }}" class="btn btn-facebook" style="font-size: 15px;">{{trans('weblng.LOGIN.LOGIN_FB')}}</a>
                </div>
                <div class="w-100 d-flex justify-content-center mt-3 mb-5">
                    <a href="{{ route('login.google') }}" class="btn btn-google" style="font-size: 15px;">{{trans('weblng.LOGIN.LOGIN_GOOGLE')}}</a>
                </div>
            </div>

        </div>
    </form>
</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<style type="text/css">.iti { width: 100%; }</style>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>

<script>
jq = jQuery.noConflict();
use :
    var s = jq("#mobile_no").intlTelInput({
        autoPlaceholder: 'polite',
        separateDialCode: true,
        formatOnDisplay: true,
        initialCountry: 'ng',
        preferredCountries:["ng"]
    });

insteadof :
    var d = jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode;
    jq("#code").val(d);
    jq(document).on('countrychange', function (e, countryData) {
        jq("#code").val((jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode));
    });


t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');
document.cookie = "timezoneCookie="+timeZone+"; expires=0; path=/;";

function validate(){
    var status=null;

    var option=document.getElementsByName('user');
    if (!(option[0].checked || option[1].checked || option[2].checked)) {
        document.getElementById("usertype").innerHTML='{{trans('weblng.VALIDATION_MSG.USER')}}'
        status = false;
    } else {
        jq('.offset-lg-2').addClass('pt-5');
        jq('.danger_alert').hide();
        jq('#error-msg').html('');
        document.getElementById("usertype").innerHTML=''
        status=true
    }

    var mobileno = document.getElementById('mobile_no').value
    if (mobileno == '') {
        document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE')}}'
        document.getElementById('mobile_no').classList.add('has-error')
        status=false
    } else if (!mobileno.match(/^([0-9\s\-\+\(\)]*)$/) || mobileno.length<5 || mobileno.length>10) {
        document.getElementById("mobile").innerHTML='{{trans('weblng.VALIDATION_MSG.MOBILE_REQ')}}'
        document.getElementById('mobile_no').classList.add('has-error')
        status=false
    } else {
        document.getElementById("mobile").innerHTML=''
        document.getElementById('mobile_no').classList.remove('has-error')
    }

    var password = document.getElementById('password').value
    if (password == '') {
        document.getElementById("pass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD')}}'
        document.getElementById('password').classList.add('has-error')
        return false
    } else if (password.length<6||password.length>255) {
        document.getElementById("pass").innerHTML='{{trans('weblng.VALIDATION_MSG.PASSWORD_REQ')}}'
        document.getElementById('password').classList.add('has-error')
        status=false
    } else {
        document.getElementById("pass").innerHTML=''
        document.getElementById('password').classList.remove('has-error')
    }

    return status
}

jq(document).ready(function(){
    var user = $('input[name="user"]:checked').val()
    if (user=='passenger') {
        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
    }
    $('#radio1').on('click',function(){
       if(this.checked){
        $('.google_facebook').show();

        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
       }
    });
    $('#radio2').on('click',function(){
       if(this.checked){
        $('.google_facebook').hide();

        $('.passb_header').hide();
        $('.passl_header').hide();
        $('.driv_header').show();
        $('.passb_footer').hide();
        $('.passl_footer').hide();
        $('.driv_footer').show();
       }
    });

     $('#radio3').on('click',function(){
       if(this.checked){
        $('.google_facebook').hide();

        // $('.passb_header').hide();
        // $('.passl_header').hide();
        // $('.driv_header').show();
        // $('.passb_footer').hide();
        // $('.passl_footer').hide();
        // $('.driv_footer').show();
       }
    });

    $('#loginForm').on('submit', function(event){
        event.preventDefault();
        $(".overlay").show();
        var device_token = initializeFireBaseMsg(function(device_token){
            jq.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var user = $("input[type=radio][name=user]:checked").val();
            var mobile_no = $('#mobile_no').val();
            var isdCode = jq("#code").val();
            var password = $('#password').val();
            jq.ajax({
                url: "{{ route('loggedin') }}",
                method:"POST",
                data:{ mobile_no: mobile_no, isdCode: isdCode, password: password, user: user, device_token: device_token, timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    if(response.data.hasOwnProperty('emergency_contacts')){
                        if(response.data.emergency_contacts.length==0){
                            window.location.href = '{{ route('passenger.emergencyContact') }}';
                        } else {
                            location.href="{{ route('passenger.home') }}";
                        }
                    } else if (response.data.hasOwnProperty('fleetowner')) {
                        location.href="{{ route('fleetowner.home') }}";
                    } else {
                        location.href="{{ route('driver.home') }}";
                    }
                },
                error: function(response){
                    $(".overlay").hide();
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                            $('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                                $('#mobile_no').addClass('has-error');
                            });
                        }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        $('#pass').html(responseMsg.errors.password).promise().done(function(){
                            $('#password').addClass('has-error');
                        });
                    }
                    $('#usertype').html(responseMsg.errors.user);
                }
                if (response.status == 401){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-2').removeClass('pt-5');
                    $('.danger_alert').show();
                    $('#error-msg').html(responseMsg.message);
                    $('#error-msg').html(responseMsg.msg);
                }

                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-2').removeClass('pt-5');
                    $('.danger_alert').show();
                    $('#error-msg').html(responseMsg.message);
                }

                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-2').removeClass('pt-5');
                    $('.danger_alert').show();
                    $('#error-msg').html(responseMsg.message);
                }
            }
        });
    });
});
});
</script>

<!-- footer -->
@include('partials.footer')
