@include('partials.header')
<style>
    .has-error{
        border:1px solid red;
    }
</style>
<div class="container mb-5">
    <form class="bg-white my-5 mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="submitEmailForm">
        <h1 class="text-center"><strong>{{trans('weblng.RESET.EMAIL_FILL_UP')}}</strong></h1>
        <div class="row justify-content-center my-3">
            <img src="{{URL::asset('/')}}storage/assets/img/h_underline.png" alt="">
        </div>
        <div class="col-lg-8 offset-lg-2 px-sm-3 pt-5">
            <div class="alert alert-danger email_alert" style="display: none;">
                <p class="text-danger" id="email-msg"></p>
            </div>

        	<div class="form-group">
                <label for="email_id" class="font-weight-bold">{{trans('weblng.FIELD.EMAIL')}}</label>
                <input type="email" class="form-control inset-input" name="email_id" id="email_id" placeholder="{{trans('weblng.FIELD.EMAIL_PLACE')}}" onkeyup="return validate()">
                <span class="text-danger" id="email-err"></span>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-lrg grad text-white"><div style="display: -webkit-inline-box;line-height: 18px;">{{trans('weblng.BOOKING.SUBMIT')}} &nbsp;<i class="fa fa-circle-o-notch fa-spin" id="ereload" style="display: none;"></i></div></button>
            </div>

        </div>
    </form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');

jq = jQuery.noConflict();

function validate(){
    var email_id = document.getElementById('email_id').value;
    if (email_id == '') {
        document.getElementById("email-err").innerHTML='{{trans('weblng.VALIDATION_MSG.EMAIL_MSG')}}';
        document.getElementById('email_id').classList.add('has-error');
        return false;
    } else {
        jq('.offset-lg-2').addClass('pt-5');
        jq('.email_alert').hide();
        jq('#email-msg').html('');
        document.getElementById("email-err").innerHTML='';
        document.getElementById('email_id').classList.remove('has-error');
        return true;
    }
}

jq(document).ready(function () {

    jq('#submitEmailForm').on('submit', function(event){
        event.preventDefault();
        jq.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jq('meta[name="csrf-token"]').attr('content')
            }
        });
        var email_id = jq('#email_id').val();
        jq(".overlay").show();
        jq.ajax({
            url: "{{ route('driver.email.update') }}",
            method:"POST",
            data:{ email_id: email_id, timeZone: timeZone },
            success: function(response){
                jq(".overlay").hide();
                location.href="{{ route('driver.account') }}";
            },
            error: function(response){
                jq(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = jq.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('email_id')) {
                        jq('#email-err').html(responseMsg.errors.email_id).promise().done(function(){
                            jq('#email_id').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        jq('#email-err').html(responseMsg.message).promise().done(function(){
                            jq('#email_id').addClass('has-error');
                        });
                    }
                }
                if (response.status == 400){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.email_alert').show();
                    jq('#email-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = jq.parseJSON(response.responseText);
                    jq('.offset-lg-2').removeClass('pt-5');
                    jq('.email_alert').show();
                    jq('#email-msg').html(responseMsg.message);
                }
            }
        });
    });
});
</script>
@include('partials.footer')